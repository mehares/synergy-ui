import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/*** AUth Guard **/
import { AuthGuard } from './guards/auth-guard.service';

/** Imports components **/
import { LayoutComponent } from './dashboard/layout/layout.component';

import { LoginComponent } from './auth/login/login.component';
import { SupplierEnquiryComponent } from './dashboard/supplierEnquiry/supplier-enquiry.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'supplier-enquiry',
    loadChildren: () => import('./dashboard/supplierEnquiry/supplier-enquiry.module').then(m => m.SupplierEnquiryModule)
  },
     {    
       path: 'forgot-password',
       loadChildren: () => import('./dashboard/forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule)
     },
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Dashboard'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'purchase-order',
        loadChildren: () => import('./dashboard/purchase-order/purchase-order.module').then(m => m.PurchaseOrderModule)
      },
      {
        path: 'vessel',
        loadChildren: () => import('./dashboard/vessel/vessel.module').then(m => m.VesselModule),
      },
      {
        path: 'port',
        loadChildren: () => import('./dashboard/port/port.module').then(m => m.PortModule)
      },
      {
        path: 'currency',
        loadChildren: () => import('./dashboard/currency/currency.module').then(m => m.CurrencyModule)
      },

       {
        path: 'item-group',
        loadChildren: () => import('./dashboard/item-group/item-group.module').then(m => m.ItemGroupModule)
      },   
       {
        path: 'invoice',
        loadChildren: () => import('./dashboard/invoice/invoice.module').then(m => m.InvoiceModule)
      },
        {
        path: 'payment',
        loadChildren: () => import('./dashboard/payment/payment.module').then(m => m.PaymentModule)
      },
      {
        path: 'vendor',
        loadChildren: () => import('./dashboard/vendor/vendor.module').then(m => m.VendorModule)
      },
      {
        path: 'company',
        loadChildren: () => import('./dashboard/company/company.module').then(m => m.CompanyModule)
      },
      {
        path: 'uom',
        loadChildren: () => import('./dashboard/uom/uom.module').then(m => m.UomModule)
      },
      {
        path: 'change-password',
        loadChildren: () => import('./dashboard/change-password/change-password.module').then(m => m.ChangePasswordModule)
      },
      {
        path: 'invoiceInstruction',
        loadChildren: () => import('./dashboard/invoice-instruction/invoice-instruction.module').then(m => m.InvoiceInstructionModule)
      }, {
        path: 'customer',
        loadChildren: () => import('./dashboard/customer/customer.module').then(m => m.CustomerModule)
      },
      {
        path: 'user',
        loadChildren: () => import('./dashboard/user/user.module').then(m => m.UserModule)
      },
      {    
        path: 'user-rights',
        loadChildren: () => import('./dashboard/user-rights/user-rights.module').then(m => m.UserRightsModule)
      },
       {    
        path: 'enquiry',
        loadChildren: () => import('./dashboard/enquiry/enquiry.module').then(m => m.EnquiryModule)
      },
      {    
       path: 'quotation',
       loadChildren: () => import('./dashboard/quotation/quotation.module').then(m => m.QuotationModule)
     },
      {    
       path: 'report',
       loadChildren: () => import('./dashboard/report/report.module').then(m => m.ReportModule)
     },
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
