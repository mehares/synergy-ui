import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {globalConstants} from '../../constants/global-constants';
import * as _ from 'lodash';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';

import * as CryptoJS from 'crypto-js';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class CommonFunctionsService {
  mlist = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

  constructor() {
  }

  findPermissions() {
    /** Permission Decryption **/
    var decryptedData = CryptoJS.AES.decrypt(localStorage.getItem('permissions'), globalConstants.ENC_KEY);
    var permissions = JSON.parse(decryptedData.toString(CryptoJS.enc.Utf8));
    permissions = permissions ? JSON.parse(permissions) : {};
    console.log("permissions",permissions);
    return permissions;
    /** End   **/
  }

  convertDateObjectToYYMMDD = (objectDate )=>{
    var yymmdd='';//2020-03-12 06:20:50.194 +00:00
    var month=parseInt (objectDate.getMonth()) ;
    month++ ;
     yymmdd = objectDate.getFullYear() +'-'+ month + '-'+objectDate.getDate();
    // ddmmyy=new Date(objectDate).toISOString();
    return yymmdd;
    
  }

  convertDateObjectToDDMMYY = (objectDate )=>{
    if(objectDate == null){
      return null;
    }else{
      var ddmmyy='';//2020-03-12 06:20:50.194 +00:00
      var month=parseInt (objectDate.getMonth()) ;
      month++ ;
      ddmmyy = objectDate.getDate()+'-'+ month + '-'+objectDate.getFullYear();
      // ddmmyy=new Date(objectDate).toISOString();
      return (ddmmyy == '1-1-1970') ? null : ddmmyy;
    }
    
  }

  // return true if valid from to date
  checkToDateIsGreaterThanFrom=(from=null,to=null) => {
    if(from == null || to ==null){
      return false;
    }
  }

  // accept YYYY-mm-dd and return object
  jsonDatetoDateObject=(date=null) => {
    if(date == null){
      return null;
    }else{
      console.log("date",date.year)
      if(date.year && date.month && date.day){
        let dt = date.year +"-"+date.month+"-"+date.day;
        return new Date(dt).toISOString()
      }else{
        return null;
      }
      
    }
  }
  //function for string display
  convertDateObjectToDDMONTHYY = (objectDate )=>{
    if(objectDate=="" || objectDate == null){
      return null;
    }else{
      var ddmmyy="";//2020-03-12 06:20:50.194 +00:00
      var month=parseInt (objectDate.getMonth()) ;
      var year = objectDate.getFullYear();
      // month++ ;
      ddmmyy =("0" +  objectDate.getDate() ).slice(-2)+'-'+ this.mlist[month] + '-'+year;
      // ddmmyy=new Date(objectDate).toISOString();
      return (year == '1970') ? null : ddmmyy;
    }
    
  }


  // accept date and return object
  dateObjectToJsonDate=(date=null) => {
    var month:number;
    if(date == null){
      return null;
    }else{
      let dateObj = new Date(date);
      if(dateObj && (dateObj.getFullYear() && dateObj.getMonth() && dateObj.getDate()) ){        
        month= dateObj.getMonth();
        month++
        let rDate = {
          "year": dateObj.getFullYear(),
          "month":month,
          "day":dateObj.getDate()
        }
        console.log("date json",rDate)
        return rDate
      }else{
        return null;
      }
      
    }
  }

  

}
