import { Injectable } from '@angular/core';
import { HttpService } from '../../@core/http/http.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { globalConstants } from '../../constants/global-constants';
import * as axios from 'axios';


@Injectable({
  providedIn: 'root',
})
export class CommonservicesService {

  innerHeight = 600;
  innerWidth = 1500;
  token = localStorage.getItem('token')



  constructor(private httpService: HttpService
  ) { }

  getData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }  
  getDataUnsafe(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_DATA_UNSAFE,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  } 

  setData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SET_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  setUserData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SET_USER_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  setUserStatus(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SET_USER_STATUS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  removeData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.REMOVE_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  getUserData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_USER_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

   getLoginData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.LOGIN_USER_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  countUserData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.COUNT_USER_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  sendMail(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SEND_MAIL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  poMailer(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.PO_MAILER,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  getStateList(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_STATE_LIST,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  createVendors(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.CREATE_VENDORS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  getVendorList(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_VENDOR_LIST,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  getVendorCount(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_VENDOR_COUNT,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  getVendorDetails(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_VENDOR_DETAILS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  updateVendors(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_UPDATE_VENDOR_DETAILS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  /* common function to delete vendor address and save return vendor address */
  deleteVendorAddress(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_DELETE_ADDRESS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  getCustomerList(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_CUSTOMER_LIST,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  createCustomers(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_CREATE_CUSTOMERS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  getCustomerCount(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_CUSTOMER_COUNT,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  changeVendorStatus(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_CHANGE_VENDOR_STATUS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  changePortStatus(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_CHANGE_PORT_STATUS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  changeVesselStatus(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_CHANGE_VESSEL_STATUS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }



  savePoOrder(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SAVE_PO_ORDER,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  getPurchaseOrderList(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_PURCHASE_ORDER_LIST,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  getOrderDetails(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_ORDER_DETAILS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  searchPurchaseOrders(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SEARCH_PURCHASE_ORDERS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  updatePoOrder(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.UPDATE_PO_ORDER,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  saveDocuments(params) {
    let _this = this
    return new Observable<any>(observer => {
      /* this.httpService
         .postFiles(
           environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SAVE_DOCUMENTS,
           params
         )
         .subscribe(res => {
           observer.next(res);
           observer.complete();
         });*/

        if(_this.token ==null){
        console.log("token empty ")
        _this.token = localStorage.getItem('token')
      }

      axios.default.post(environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SAVE_DOCUMENTS,params,{headers:{'x-access-token':_this.token}}).then(res => {
        console.log('res', res)
        observer.next(res);
        observer.complete();
      });


    });
  }

  saveUserProfile(params) {
    let _this = this
    return new Observable<any>(observer => {
       if(_this.token ==null){
        console.log("token empty ")
        _this.token = localStorage.getItem('token')
      }
      axios.default.post(environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SAVE_USER_PROFILE, params,{headers:{'x-access-token':_this.token}}).then(res => {
        console.log('res', res)
        observer.next(res);
        observer.complete();
      });


    });
  }

  getProfileImage(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_USER_IMAGE,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  gePortData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_PORT_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  createCompanyData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_COMPANY_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  savePortData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_PORT_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  updatePortData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.UPDATE_PORT_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  countPortData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.COUNT_PORT_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  generateInvoice(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_GENERATE_INVOICE,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  getCompanyData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_COMPANY_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  getUomData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_UOM_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  getInvoiceDetails(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_INVOICE_DETAILS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  setUomData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SET_UOM_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  getInvoiceList(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_INVOICE_LIST,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  setGroupsData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_GROUPS_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  searchInvoice(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_SEARCH_INVOICE,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  getGroupsData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_GROUPS_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  updateGroupsData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.UPDATE_GROUPS_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  updateInvoiceDetails(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_UPDATE_INVOICE_DETAILS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  countGroupsData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.COUNT_GROUPS_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  setCurrencyData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_CURRENCY_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  getCurrencyData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_CURRENCY_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  updateCurrencyData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.UPDATE_CURRENCY_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  countCurrencyData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.COUNT_CURRENCY_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  generatePDFInvoice(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_GENERATE_PDF_INVOICE,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  generatePurchase(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GENERATE_PURCHASE,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
   updatePoStatus(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.PO_STATUS_CHANGE,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  


  getItemPerPage() {
    this.innerHeight = window.innerHeight;
    this.innerWidth = window.innerWidth;
    console.log('innerHeight', innerHeight);
    console.log('innerWidth', innerWidth);
    if ((this.innerHeight <= 750 && this.innerHeight > 400) && (this.innerWidth <= 1020)) {
      return 5;
    }
    if (this.innerWidth > 750 && innerHeight < 500) {
      return 10;
    }
    if (this.innerHeight > 500 && this.innerHeight < 900 && this.innerWidth > 300 && this.innerWidth < 500) {
      return 8;
    }
    if (this.innerWidth == 768) {
      return 12;
    }
    if (this.innerHeight >= 1024) {
      return 20;
    }
    if (this.innerHeight >= 900) {
      return 15;
    }
    if (this.innerHeight >= 750 && (this.innerWidth > 1020)) {
      return 15;
    }
    if (this.innerHeight >= 600 && (this.innerWidth > 1020)) {
      return 10;
    }

  }

  searchCustomerPO(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_SEARCH_CUSTOMER_PO,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

generateNewInvoice(params) {
  return new Observable<any>(observer => {
    this.httpService
      .post(
        environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GENERATE_COMBINE_INVOICE,
        params
      )
      .subscribe(res => {
        observer.next(res);
        observer.complete();
      });
  });
}

saveInvoiceDocuments(params) {
  let _this = this
  return new Observable<any>(observer => {
          if(_this.token ==null){
        console.log("token empty ")
        _this.token = localStorage.getItem('token')
      }
    axios.default.post(environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SAVE_INVOICE_DOCUMENTS, params,{headers:{'x-access-token':_this.token}}).then(res => {
      console.log('res', res)
      observer.next(res);
      observer.complete();
    });
  });
}

 getPaymentData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_PAYMENT_DATA,
       params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }   
setPaymentData(params) {
  return new Observable<any>(observer => {
    this.httpService
      .post(
        environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SET_PAYMENT_DATA,
   params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }   
 setInstructionData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_INSTRUCTION_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
 getInstructionData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_INSTRUCTION_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  updateInstructionData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.UPDATE_INSTRUCTION_DATA,
            params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  countInstructionData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.COUNT_INSTRUCTION_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  setInstructionStatus(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_INSTRUCTION_STATUS,
            params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  getLatestCurrencyExchangeRates(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_LATEST_CURRENCY_EXCHANGE_RATES,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  getLatestCurrencyExchangeRatesUnsafe(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_LATEST_CURRENCY_EXCHANGE_RATES_UNSAFE,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }




//GET_VESSSELSUBTYPES

getVesselSubTypes(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_VESSSELSUBTYPES,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  countVesselsData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_COUNT_VESSELS_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


//search vendors
  searchVendors(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SEARCH_VENDORS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  //search users
  searchUsers(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SEARCH_USERS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  saveVessel(params) {
    return new Observable<any>(observer => {
        this.httpService
          .post(
            environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_SAVE_VESSEL,
            params
          )
          .subscribe(res => {
            observer.next(res);
            observer.complete();
          });
      });
  }

  editVessel(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_EDIT_VESSEL,
                params
        )
          .subscribe(res => {
            observer.next(res);
            observer.complete();
          });
      });
  }

  updateVessel(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_UPDATE_VESSEL,
           params
        )
          .subscribe(res => {
            observer.next(res);
            observer.complete();
          });
      });
  }

    getVesselData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_EDIT_VESSEL,
          params
        )
          .subscribe(res => {
            observer.next(res);
            observer.complete();
          });
      });
  }


 //excel export starts
    uomSetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.UOM_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

    vendorSetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.VENDOR_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  customerSetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.CUSTOMER_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

      vesselSetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.VESSEL_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

      companySetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.COMPANY_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

      userSetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.USER_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

      portsSetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.PORTS_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
    itemGroupsSetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.ITEMGROUPS_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

      currencySetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.CURRENCY_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

      invoiceInstructionSetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.INVOICEINSTRUCTION_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  invoiceSetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.INVOICE_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  purchaseSetExcel(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.PURCHASE_SET_EXCEL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  //search vessels
  searchVessels(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_SEARCH_VENDORS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
          
  // search customer
  searchCustomer(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SEARCH_CUSTOMERS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  logout(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.LOGOUT_USER,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
//search ports
  searchPort(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SEARCH_PORTS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  searchItemGroup(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SEARCH_ITEMGROUPS,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  searchUom(params) {
        return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SEARCH_UOM,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  countCompanyData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.COUNT_COMPANY_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  companyUpdate(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.UPDATE_COMPANY_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  countUomData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.COUNT_UOM_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  uomUpdate(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.UPDATE_UOM_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  changePassword(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.CHANGE_PASSWORD,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  saveEnquiry(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SAVE_ENQUIRY,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

saveEnquiryDocuments(params) {
  let _this = this
    return new Observable<any>(observer => {
      console.log("token",localStorage.getItem('token'))
      if(_this.token ==null){
        console.log("token empty ")
        _this.token = localStorage.getItem('token')
      }
        axios.default.post(environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.ENQUIRY_DOCUMENTS, params,{headers:{'x-access-token':_this.token}}).then(res => {
            console.log('res', res)
            observer.next(res);
            observer.complete();
        });
    });
}

getEnquiryDetails(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_ENQUIRY_DETAILS,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}
updateEnquiiry(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.UPDATE_ENQUIRY,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}


enqMailer(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.ENQ_MAILER,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

updateEnqStatus(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.ENQ_STATUS_CHANGE,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

cancelEnqMailer(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.CANCEL_ENQ_MAILER,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

getEnquiryList(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_ENQUIRY_LIST,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

enquirySetExcel(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.ENQUIRY_SET_EXCEL,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

createQuotation(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_SAVE_QUOTATION,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

updateQuotation(params) {
  return new Observable<any>(observer => {
      this.httpService
          .post(
              environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_UPDATE_QUOTATION,
              params
          )
          .subscribe(res => {
              observer.next(res);
              observer.complete();
          });
  });
}


getEnquiryDetailsById(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_ENQUIRY_DETAILS_BYID_FOR_QUOTATION,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}
saveQuotationDocuments(params) {
  let _this = this
    return new Observable<any>(observer => {
            if(_this.token ==null){
        console.log("token empty ")
        _this.token = localStorage.getItem('token')
      }
        axios.default.post(environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_SAVE_QUOTATION_DOCUMENTS, params,{headers:{'x-access-token':_this.token}}).then(res => {
            console.log('res', res)
            observer.next(res);
            observer.complete();
        });
    });
}

getQuotationList(params) {
    return new Observable<any>(observer => {
       this.httpService.post(environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_SEARCH_QUOTATION, params)
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

quotationSetExcel(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.QUOTATION_SET_EXCEL,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}


getUserModulesData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_USERMODULES_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }  

  getSupplierEnquiryDetails(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_SUPPLIER_ENQUIRY_DETAILS,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}
declineEnqQuote(params) {
  return new Observable<any>(observer => {
      this.httpService
          .post(
              environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_DECLINE_ENQ_QUOTE,
              params
          )
          .subscribe(res => {
              observer.next(res);
              observer.complete();
          });
  });
}

updateSupplierEnquiiry(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.UPDATE_SUPPLIER_ENQUIRY,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

saveSupplierDocuments(params) {
    return new Observable<any>(observer => {
        axios.default.post(environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.SUPPLIER_DOCUMENTS, params).then(res => {
            console.log('res', res)
            observer.next(res);
            observer.complete();
        });
    });
}

getQuotationDetailsById(params) {
  return new Observable<any>(observer => {
      this.httpService
          .post(
              environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_QUOTATION_DETAILS_BYID_FOR_QUOTATION,
              params
          )
          .subscribe(res => {
              observer.next(res);
              observer.complete();
          });
  });
}


  getEnquiryVendorsData(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.ENQUIRY_VENDORS_GET_DATA,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }  

  generateQuotationPdfOrMail(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_GENERATE_QUOTATION_PDF_OR_MAIL,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }



  cancelEnquiry(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.CANCEL_ENQUIRY,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

  generateCustomerQuotation(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_GENERATE_CUSTOMER_QUOTATION,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
// select pending enquiry list for po generation 
  searchEnquiryForGenQuotation(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_SEARCH_ENQ_FOR_GEN_QUOTATION,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
  }

    searchQuotationForGenPO(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_SEARCH_QT_FOR_GEN_PO,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
  }


  getEnquiryDetailsByIdPO(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_ENQUIRY_DETAILS_BYID_FOR_PO,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
}

    searchEnquiryForGenPO(params) {
    return new Observable<any>(observer => {
        this.httpService
            .post(
                environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.POST_SEARCH_ENQ_FOR_GEN_PO,
                params
            )
            .subscribe(res => {
                observer.next(res);
                observer.complete();
            });
    });
  }


  getDashboard(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GET_DASHBOARD,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }


  forgotPassword(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.FORGOT_PASSWORD,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }  

  newPassword(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.NEW_PASSWORD,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  generateReport(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(
          environment.SYNERGY_BACKEND + globalConstants.ENDPOINTS.GENERATE_REPORT,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }

}