import {AuthService} from './auth.service';
import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import { CommonFunctionsService } from './../helper/commonFunctions/common-functions.service';

@Injectable()
export class PageGuard implements CanActivate {
  constructor(private _authService: AuthService,
              private _router: Router, private CF: CommonFunctionsService) {
  }
permissionsT = this.CF.findPermissions()

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log('AlwaysAuthGuard');
   let roles = route.data["permissions"]
    console.log("****roles", roles);
    console.log("this.permissions", this.permissionsT[roles])
    if (this.permissionsT[roles] || this.permissionsT['']) {
      return true
    } 
    else {
      this._router.navigate(['/dashboard']);
    }
  }
}
