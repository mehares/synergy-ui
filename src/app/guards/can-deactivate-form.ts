import { NgForm } from '@angular/forms';
import { CanDeactivateComponent } from './can-deactivate';

export abstract class CanDeactivateForm extends CanDeactivateComponent {
  abstract get dataForm(): NgForm;

  canDeactivate(): boolean {
    if (this.dataForm) {
      return this.dataForm.submitted || !this.dataForm.dirty;
    }
    return true;
  }
}
