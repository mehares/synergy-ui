import {Injectable} from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';

const TOKEN_HEADER_KEY = 'x-access-token';

@Injectable({
    providedIn: 'root'
})
export class IntercepterService implements HttpInterceptor {

    constructor() {
    }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let authReq = request;
    const token = localStorage.getItem('token')
    if (token != null) {

       authReq = request.clone({ headers: request.headers.set(TOKEN_HEADER_KEY, token) });
    }
    return next.handle(authReq);

        var httpsReq ;
        if(request && request.headers && request.headers['lazyUpdate'] && request.headers['lazyUpdate'].length){
             httpsReq = request.clone({
                setHeaders: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            });
        }else{
             httpsReq = request.clone({
                setHeaders: {
                    'Content-Type': 'application/json'
                }
            });
        }
        return next.handle(httpsReq);


    }
}
