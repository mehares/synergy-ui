import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommonservicesService} from '../../helper/commonservices/commonservices.service';
import * as CryptoJS from 'crypto-js';
import {globalConstants} from '../../constants/global-constants';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private CS: CommonservicesService) {
  }

  submitted = false;
  registerForm: FormGroup;
  error = false;
  success = false;
  loading = false;

  ngOnInit() {
    this.createForm();
  }


  // convenience getter for easy access to form fields
  get form() {
    return this.registerForm.controls;
  }

  createForm() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.submitted = true;
    console.log('registerForm', this.registerForm);
    // stop here if form is invalid
    console.log('Form Status', this.registerForm.invalid);
    if (this.registerForm.invalid) {
      return;
    } else {
      let _this = this;
      var checkEmail = this.validateEmail(this.registerForm.value.username)
      console.log('checkEmail', checkEmail)
      let where = {
        'status': 1,
        password: this.registerForm.value.password
      }
      if (checkEmail) {
        where['email'] = this.registerForm.value.username
      } else {
        where['username'] = this.registerForm.value.username
      }
      let params = {
        model_name: 'users',
        where: where,
        id: true
      };
      _this.loading = true;
      _this.CS.getLoginData(params).subscribe(response => {
        console.log('response', response);
        _this.loading = false;
        if (response && response.status == 'success' && response.result) {
          localStorage.setItem('username', response.result.username);
          localStorage.setItem('sender_pro_id', response.result.id);
          localStorage.setItem('companyXid', response.result.company);
          localStorage.setItem('loggedIn', 'true');
          localStorage.setItem('token', response.token);
          // Encrypt
          let permissions = null;
          permissions = response.result.UR && response.result.UR.length ? response.result.UR[0].permissions : '';
          var permissions_enc = CryptoJS.AES.encrypt(JSON.stringify(permissions), globalConstants.ENC_KEY).toString();
          localStorage.setItem('permissions', permissions_enc);
          _this.success = true;
          let firsttime = response.firsttime;
          localStorage.setItem('checkFirstLogin', firsttime);
          // console.log('start redirection')
          if (firsttime) {
            _this.router.navigate(['/change-password']);
          } else {
            _this.router.navigate(['/dashboard']);
          }
        } else {
          _this.error = true;
        }
      });
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }


}
