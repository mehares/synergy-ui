import { environment } from '../../environments/environment';

export const globalConstants = {
  APP: 'SYNERGY',
  ENC_KEY: 'Synergymarinegroup',
  MSG: {},
  ENDPOINTS: {
    GET_DATA: '/common/getData',
    GET_DATA_UNSAFE: '/api/supplier/get-data',
    SET_DATA: '/common/setData',
    REMOVE_DATA: '/common/remove',
    GET_USER_DATA: '/user/getData',
    SET_USER_DATA: '/user/setData',
    COUNT_USER_DATA: '/user/count',
    LOGIN_USER_DATA: '/login/getLoginData',
    SEND_MAIL: '/user/sendMail',
    GET_COUNTRY_LIST: '/common-data/countries',
    GET_STATE_LIST: '/common-data/states',
    CREATE_VENDORS: '/api/vendor/create',
    GET_VENDOR_LIST: '/api/vendor/list',
    GET_VENDOR_COUNT: '/api/vendor/count',
    GET_VENDOR_DETAILS: '/api/vendor/details',
    POST_UPDATE_VENDOR_DETAILS: '/api/vendor/edit',
    POST_DELETE_ADDRESS: '/api/vendor/deleteAddress',
    POST_CHANGE_VENDOR_STATUS: '/api/vendor/updstatus',
    POST_CREATE_CUSTOMERS: '/api/customer/create',
    GET_CUSTOMER_LIST: '/api/customer/list',
    GET_CUSTOMER_COUNT: '/api/customer/count',
    GET_CUSTOMER_DETAILS: '/api/vendor/details',
    UPDATE_VENDORS: '/api/vendor/edit',
    POST_CHANGE_VESSEL_STATUS: '/api/vessel/updstatusVessel',
    GET_PURCHASE_ORDER_LIST: '/purchase-order/list',
    SEARCH_PURCHASE_ORDERS: '/purchase-order/search',
    SET_USER_STATUS: '/user/setUserStatus',
    SAVE_PO_ORDER: '/purchase-order/savePoOrder',
    GET_ORDER_DETAILS: '/purchase-order/oderDetails',
    UPDATE_PO_ORDER: '/purchase-order/updatePoOrder',
    SAVE_DOCUMENTS: '/purchase-order/saveDocuments',
    SAVE_USER_PROFILE: '/user/saveProfileImage',
    POST_GENERATE_INVOICE: '/invoice/generateInvoice',
    GET_INVOICE_DETAILS: '/invoice/details',
    GET_USER_IMAGE: '/user/getProfileImage',
    POST_COMPANY_DATA: '/api/company/setData',
    GET_PORT_DATA: '/port/getPortData',
    POST_CHANGE_PORT_STATUS: '/port/updstatus',
    POST_PORT_DATA: '/port/setData',
    UPDATE_PORT_DATA: '/port/update',
    COUNT_PORT_DATA: '/port/count',
    GET_COMPANY_DATA: '/api/company/list',
    SET_UOM_DATA: '/api/uom/setData',
    GET_UOM_DATA: '/api/uom/uomlist',
    GET_INVOICE_LIST: '/invoice/list',
    POST_SEARCH_INVOICE: '/invoice/search',
    POST_UPDATE_INVOICE_DETAILS: '/invoice/update-details',
    POST_GROUPS_DATA: '/api/groups/setData',
    GET_GROUPS_DATA: '/api/groups/list',
    UPDATE_GROUPS_DATA: '/api/groups/update',
    COUNT_GROUPS_DATA: '/api/groups/count',
    POST_CURRENCY_DATA: '/api/currency/setData',
    GET_CURRENCY_DATA: '/api/currency/list',
    UPDATE_CURRENCY_DATA: '/api/currency/update',
    COUNT_CURRENCY_DATA: '/api/currency/count',
    PO_MAILER: '/purchase-order/poMailer',
    GENERATE_PURCHASE: '/purchase-order/generatePurchase',
    PO_STATUS_CHANGE: '/purchase-order/statusChange',
    POST_GENERATE_PDF_INVOICE: '/invoice/generate-invoice-pdf',
    POST_SEARCH_CUSTOMER_PO: '/purchase-order/search-customer-po',
    GENERATE_COMBINE_INVOICE: '/invoice/generate-customer-invoice',
    SAVE_INVOICE_DOCUMENTS: '/invoice/saveDocuments',
    GET_PAYMENT_DATA: '/payment/getPaymentData',
    SET_PAYMENT_DATA: '/payment/setPaymentData',
    POST_INSTRUCTION_DATA: '/api/instruction/setData',
    GET_INSTRUCTION_DATA: '/api/instruction/list',
    UPDATE_INSTRUCTION_DATA: '/api/instruction/update',
    COUNT_INSTRUCTION_DATA: '/api/instruction/count',
    POST_INSTRUCTION_STATUS: '/api/instruction/updatestatus',
    GET_VESSSELSUBTYPES: '/common-data/vesselSubTypes',
    GET_LATEST_CURRENCY_EXCHANGE_RATES: '/api/currency/get-latest-currency-exchange-rates',
    GET_LATEST_CURRENCY_EXCHANGE_RATES_UNSAFE: '/api/supplier/get-latest-currency-exchange-rates',
    POST_COUNT_VESSELS_DATA: '/api/vessel/count',
    SEARCH_VENDORS: '/api/vendor/search',
    SEARCH_USERS: '/user/search',
    POST_SAVE_VESSEL: '/api/vessel/create',
    POST_EDIT_VESSEL: '/api/vessel/edit',
    POST_UPDATE_VESSEL: '/api/vessel/update',
    UOM_SET_EXCEL: '/api/uom/setExcel',
    VENDOR_SET_EXCEL: '/api/vendor/setExcel',
    CUSTOMER_SET_EXCEL: '/api/customer/setExcel',
    VESSEL_SET_EXCEL: '/api/vessel/setExcel',
    USER_SET_EXCEL: '/user/setExcel',
    COMPANY_SET_EXCEL: '/api/company/setExcel',
    PORTS_SET_EXCEL: '/port/setExcel',
    ITEMGROUPS_SET_EXCEL: '/api/groups/setExcel',
    CURRENCY_SET_EXCEL: '/api/currency/setExcel',
    INVOICEINSTRUCTION_SET_EXCEL: '/api/instruction/setExcel',
    INVOICE_SET_EXCEL: '/invoice/setExcel',
    PURCHASE_SET_EXCEL: '/purchase-order/setExcel',
    POST_SEARCH_VENDORS: '/api/vessel/search',
    SEARCH_CUSTOMERS: '/api/customer/search',
    LOGOUT_USER: '/user/logout',
    SEARCH_PORTS: '/port/search',
    SEARCH_ITEMGROUPS: '/api/groups/search',
    SEARCH_UOM: '/api/uom/search',
    COUNT_COMPANY_DATA: '/api/company/count',
    UPDATE_COMPANY_DATA: '/api/company/update',
    COUNT_UOM_DATA: '/api/uom/count',
    UPDATE_UOM_DATA: '/api/uom/update',
    CHANGE_PASSWORD: '/api/password-update',
    SAVE_ENQUIRY: '/enquiry/saveEnquiry',
    ENQUIRY_DOCUMENTS: '/enquiry/saveDocuments',
    GET_ENQUIRY_DETAILS: '/enquiry/enquiryDetails',
    UPDATE_ENQUIRY: '/enquiry/updateEnquiry',
    ENQ_MAILER: '/enquiry/enqMailer',
    ENQ_STATUS_CHANGE: '/enquiry/statusChange',
    CANCEL_ENQ_MAILER: '/enquiry/removeVendorMailer',
    GET_ENQUIRY_LIST: '/enquiry/list',
    ENQUIRY_SET_EXCEL: '/enquiry/setExcel',
    POST_ENQUIRY_DETAILS_BYID_FOR_QUOTATION: '/api/quotation/enquiry-details-byid',
    POST_SAVE_QUOTATION_DOCUMENTS: '/api/quotation/saveDocuments',
    POST_SAVE_QUOTATION: '/api/quotation/saveQuotation',
    POST_UPDATE_QUOTATION: '/api/quotation/updateQuotation',
    POST_SEARCH_QUOTATION: '/api/quotation/search',
    QUOTATION_SET_EXCEL: '/api/quotation/setExcel',
    GET_USERMODULES_DATA: '/user/getUserModulesData',
    GET_SUPPLIER_ENQUIRY_DETAILS: '/api/supplier/enquiryDetails',
    UPDATE_SUPPLIER_ENQUIRY: '/api/supplier/updateEnquiry',
    SUPPLIER_DOCUMENTS: '/api/supplier/saveDocuments',
    POST_QUOTATION_DETAILS_BYID_FOR_QUOTATION: '/api/quotation/details-byid',
    ENQUIRY_VENDORS_GET_DATA: '/api/supplier/getData',
    POST_GENERATE_QUOTATION_PDF_OR_MAIL: '/api/quotation/generate-quotation-pdf',
    CANCEL_ENQUIRY: '/enquiry/enquiryCancel',
    POST_GENERATE_CUSTOMER_QUOTATION: '/api/quotation/generate-customer-quotation',
    POST_DECLINE_ENQ_QUOTE: '/api/supplier/decline-enquiry',
    POST_SEARCH_ENQ_FOR_GEN_QUOTATION :'/api/quotation/select-enq-for-quotation',
    POST_SEARCH_QT_FOR_GEN_PO :'/purchase-order/select-qt-for-po',
    POST_ENQUIRY_DETAILS_BYID_FOR_PO: '/purchase-order/enquiry-details-byid',
    POST_SEARCH_ENQ_FOR_GEN_PO :'/purchase-order/select-enq-for-po',
    GET_DASHBOARD : '/api/dashboard',
    FORGOT_PASSWORD: '/login/forgotPassword',
    NEW_PASSWORD: '/login/newPassword',
    GENERATE_REPORT : '/api/report/reportGenerate'

    




  },
  PO_DOC_PATH: environment.SYNERGY_BACKEND + '/uploads/',
  QUOTATION_DOC_PATH: environment.SYNERGY_BACKEND + '/uploads/',
  ENQ_DOC_PATH: environment.SYNERGY_BACKEND + '/uploads/enquiry',
  UPLOADS_DIR: environment.SYNERGY_BACKEND + '/uploads',
  FP: [{ name: 'Flat' }, { name: 'Percentage' }],
  DISCOUNT: 4,
  TAX: 2,
  FLAT: 'Flat',
  PER: 'Percentage',
  PO_STATUS: ['NEW', "PO recv'd from Cust", 'PO sent to Sup' , 'Invoice Generated'],
  //INV_STATUS :['New','Invoice Generated','Invoice Sent','Payment Closed']
  INV_STATUS :['New','Invoice Generated','Invoice Sent','Payment made to supplier', 'Payment received from the customer' ,'Closed'],
  ENQ_STATUS: ['Enq Updated','Rqst sent to Sup', "Qtn recv'd from Sup", 'Qtn sent to Cust', "PO  recv'd from Cust",'PO sent to Sup', "Inv recv'd from Sup", 'Inv sent to Cust', 'Payment made to Sup',  "Payment recv'd from Cust", 'Closed', 'Cancelled', "Enq reject"],
  QUOTATION_STATUS: ['New', 'Qtn Received', 'Qtn sent to cust',"PO recv'd from cust",'PO sent to sup'],
  ENQPayTerms: [{ id: 1, name: '0-DAYS' }, { id: 2, name: '15-DAYS' }, { id: 3, name: '30-DAYS' }, { id: 4, name: '45-DAYS' }, { id: 5, name: '60-DAYS' }, { id: 6, name: '90-DAYS' }, { id: 7, name: 'BEF-DUE-DATE' }],
  ENQPayModes: [{ id: 1, name: 'Cash' }, { id: 2, name: 'Cheque' }, { id: 3, name: 'Direct Debit' }, { id: 4, name: 'Demand Draft' }, { id: 5, name: 'Non Cash' }, { id: 6, name: 'Others' }, { id: 7, name: 'Pay Order' }],
  ENQIncoTerms: [{id:1, name: 'CFS'}, {id:2, name: 'CIF' },{id:3, name: 'EXW'}, {id:4, name: 'FOB' },{id:5, name: 'NONE' }],
};