import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { globalConstants } from '../../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {




  message_content: string = '';
  customerlist = [];
  countryList = [];
  stateList = [];
  currencylist = [];

  currentPage = 1
  totalItems = 0
  offset = 0
  loader = false
  permissions = this.CF.findPermissions()
  itemsPerPage = this.CS.getItemPerPage();
  userQuestionUpdate = new Subject<string>();
  search = '';
  searchToggle = false;
  searchLoader = false;
  customerSearchForm: FormGroup;
  isValidFormSubmitted = null;
  submitted = false;
  UPLOAD_PATH = globalConstants.UPLOADS_DIR
  statuslist = [{ id: 1, name: 'YES' }, { id: 0, name: 'NO' }];
  searchBox = false;
  pageOffset = 0;
  keywordCurrency = 'name';
  firstLoad = false;
  currencyName = '';
  isSearchRequest=false;



  constructor(private http: HttpClient,
    private CS: CommonservicesService,
    private CF: CommonFunctionsService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,private spinner: NgxSpinnerService,) {
    this.userQuestionUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.resetPagination();
        this.onSubmit();
        if (this.search != '') {
          this.searchBox = true
        }
        else {
          this.searchBox = false
        }
      });
  }

  ngOnInit() {
    this.getCustomerData();
    this.getCurrencies();
    this.firstLoad = true;
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    this.customerSearchForm = this.formBuilder.group({
      search: ['', []],
      approvedVendor: [null, []],
      vendorCode: ['', []],
      vendorName: ['', []],
      status: [0],
      currency: ['', []],
      currencyObj: ['', []],

    });


  }


  getCurrencies() {
    let _this = this;
    let params = { model_name: 'currencies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencylist = response.result;
      } else {
        _this.currencylist = [];
      }
    });
  }

  getCountries() {
    let _this = this;
    let params = { model_name: 'country', where: {} };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.countryList = response.result;
        console.log("countriess", response.result)
      } else {
        _this.countryList = [];
      }
    });
  }

  getState(countryId, i) {
    if (countryId > 0) {
      let _this = this;
      _this.stateList[i] = [];
      _this.CS.getStateList({ id: countryId }).subscribe(response => {
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          _this.stateList[i] = response.result[0].CS;
        } else {
          _this.stateList[i] = [];
        }
      });
    }
  }
  getCustomerData() {
    var params = { typeUser: 2, limit: this.itemsPerPage, offset: this.offset };
    let _this = this
    _this.loader = true
    this.pageOffset = this.offset;
    this.isSearchRequest=false;
    _this.CS.getCustomerCount(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == 'success') {
        _this.customerlist = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;

      } else {
        this.toastr.error('Error Occured While Fetching,Try Again!', 'Failed!');
      }
      _this.loader = false
    });
  }

  deleteCustomer(id) {
    if (confirm("Are You Sure To Delete This Customer ? ")) {
      let _this = this
      this.http.post<any>(`api/cutomer-delete`, { id: id, model_name: 'vendor' })
        .subscribe(response => {
          console.log("response", response)
          if (response && response.status == "success") {
            _this.getCustomerData()
            this.toastr.success('Customer Details Deleted.', 'Success!');
          } else {
            this.toastr.error('Something Went Wrong!', 'Failed!');
          }
        });
    }
  }

  changeVendorStatus(id, status,customer) {

    let _this = this;
    let params = { id, status, customer};
    //if (confirm("Are you sure to change status ? ")) {

    this.CS.changeVendorStatus(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == 'success') {
        this.getCustomerData();
        this.toastr.success('Details Updated.', 'Success!');
      } else {
        this.toastr.error('Error Occured While Saving,Try again!', 'Failed!');
      }
    });
    //}
  }

  pageChanged(event) {
    console.log("event", event);
    if (this.submitted == true) {
      this.itemsPerPage = this.CS.getItemPerPage();
      this.currentPage = event
      var setoffset = event - 1
      setoffset = setoffset * this.itemsPerPage
      this.offset = setoffset
      this.customerlist
    }
    else {
      this.itemsPerPage = this.CS.getItemPerPage();
      this.currentPage = event
      var setoffset = event - 1
      setoffset = setoffset * this.itemsPerPage
      this.offset = setoffset
      this.getCustomerData()
    }

  }

  get f() {
    return this.customerSearchForm.controls;
  }

  onSubmit() {

    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;

    if (this.customerSearchForm.invalid) {
      this.toastr.error('Please Enter The Fields!', 'Invalid!');
      return;
    }
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;
    const searchObj = {
      search: this.search,
      vendorCode: this.f.vendorCode.value,
      vendorName: this.f.vendorName.value,
      currency: this.f.currency.value,
      status: this.f.status.value,
      approvedVendor: this.f.approvedVendor.value
    };

      
    this.isSearchRequest=true;
    this.resetPagination();
    this.CS.searchCustomer(searchObj).subscribe(response => {
      console.log("response", response);
      _this.searchLoader = false;
      _this.searchToggle = false;

      _this.loader = false

      if (response && response.status == 'success') {
        _this.customerlist = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
        this.pageOffset = 0;
        //this.pageChanged(event)
      } else {
        this.toastr.error('Error Occured While Search,Try again!', 'Failed!');
      }
    });
  }


  exportCustomer() {
    let _this = this;
    _this.CS.customerSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'customer/' + response.filename, '_blank');
        this.toastr.success('Successfuly Exported Excel.', 'Success!');
      }
      else {
        this.toastr.error(response.msg, 'Failed!');

      }
    });
  }

  clearSearch(type) {
    this.resetPagination()
    this.searchBox = false;
    this.submitted = false;
    this.loader = false
    this.customerSearchForm = this.formBuilder.group({
      approvedVendor: null,
      vendorCode: null,
      vendorName: null,
      status: '',
      currency: null,
      search: null,
      currencyObj:['', []],

    });
    this.getCustomerData();
        if(type=='reset') {
      this.searchToggle = true
    }
    else {
      this.searchToggle = false
      this.firstLoad = false;
    }
  }

  changeSelection(name) {
    this.currencyName = name ? name : this.currencyName;

    var currencyObj = _.find(this.currencylist, { name: this.currencyName });
    this.customerSearchForm.get('currency').setValue(currencyObj.id);

  }

  clearMainSearch(type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.clearSearch(type);
      this.searchToggle = true
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }
  resetPagination(){    
    this.offset=0;
    this.currentPage=1;
  }

    onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container')){
        console.log('wow--->closeee----')
      }
      else {
    this.firstLoad = false
  }
  }
}
