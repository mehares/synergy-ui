import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerAddComponent } from './customer-add/customer-add.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerEditComponent } from './customer-edit/customer-edit.component';
import { PageGuard } from '../../guards/page-guard.service';
const routes: Routes = [
  {
    path: 'list',
    canActivate: [PageGuard],
    component: CustomerListComponent,
    data:{permissions: 'customers_view' }
  },
  {
    path: 'add',
    canActivate: [PageGuard],
    component: CustomerAddComponent,
    data:{permissions: 'customers_add' }
  },
  {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: CustomerEditComponent,
    data:{permissions: 'customers_edit' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
