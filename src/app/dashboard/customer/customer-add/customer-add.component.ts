import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.scss']
})
export class CustomerAddComponent implements OnInit {

  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  custVendorForm: FormGroup;
  isValidFormSubmitted = null;
  submitted = false;
  currencylist = [];
  statuslist = [{ id: 1, name: 'YES' }, { id: 0, name: 'NO' }];
  countryList = [];
  stateList = [];
  selectedAddress = 0;
  keywordCountry = 'name';
  keywordState = 'name';
  permissions = this.CF.findPermissions();
  ToDay = { year: new Date().getFullYear(), month: new Date().getMonth()+1, day: new Date().getDate() };
  loading = false
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient,private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private CS: CommonservicesService,
    private CF: CommonFunctionsService) { }

  ngOnInit() {
    this.getCountries();
    this.getCurrencies();

    this.custVendorForm = this.formBuilder.group({
      vendorCode: ['', [Validators.required,Validators.maxLength(24)]],
      vendorName: ['', Validators.required],
      currency: [null, [Validators.required]],
      approvedDate: [null],
      approvedVendor: [null, [Validators.required]],
      status: [null, [Validators.required]],
      VA: this.formBuilder.array(
        [this.createAddressFormGroup()],
        [Validators.required])
    });

  }

  getCurrencies() {
    let _this = this;
    let params = { model_name: 'currencies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencylist = response.result;
      } else {
        _this.currencylist = [];
      }
    });
  }


  getCountries() {
    let _this = this;
    let params = { model_name: 'country', where: {} };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.countryList = response.result;
        console.log("countriess", response.result)
      } else {
        _this.countryList = [];
      }
    });
  }

 /* getState(countryId, i) {
    if (countryId > 0) {
      let _this = this;
      _this.stateList[i] = [];
      _this.CS.getStateList({ id: countryId }).subscribe(response => {
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          _this.stateList[i] = response.result[0].CS;
        } else {
          _this.stateList[i] = [];
        }
      });
    }
  }*/


  get f() { return this.custVendorForm.controls; }
  // get vendor address "VA"
  get VA(): FormArray {
    return this.custVendorForm.get('VA') as FormArray;

  }

  addNewAddress() {
    const fg = this.createAddressFormGroup();
    this.VA.push(fg);
  }

  deleteAddress(idx: number) {
    this.VA.removeAt(idx);
    this.stateList.splice(idx, 1);
  }

  createAddressFormGroup() {

    return this.formBuilder.group({
      address1: ['', [Validators.required]],
      //address1: ['', [Validators.required, Validators.min(20)]],
      isDefaultAddress: [''],
      address2: ['', []],
      address3: ['', []],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      country: ['', [Validators.required]],
      zip: ['', [Validators.required]],
      phoneNo: ['', [Validators.required, Validators.pattern(this.mobNumberPattern)]],
      salesEmail: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      accountsEmail: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      url: ['', []],
      fax: ['', []],
      status: 1,
      stateName:''
    });
  }
handleDateFIeld(approved) {
  console.log("approved",approved)
    if (approved.id == 1) {
      this.custVendorForm.controls['approvedDate'].enable();
    } else {
      this.custVendorForm.controls['approvedDate'].setValue(null);
      this.custVendorForm.controls['approvedDate'].disable();
    }
  }


  // Form Submit
  onSubmit() {
    let _this = this;
    console.log('FORM', this.custVendorForm);
    console.log('FORM status', this.custVendorForm.invalid);
    this.submitted = true;
    this.isValidFormSubmitted = false;
     this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    let checkIsDefault = 0;
    for (let isDef in this.f.VA.value) {

      if (this.selectedAddress == parseInt(isDef)) {
        this.f.VA.value[isDef].isDefaultAddress = true;
        checkIsDefault = 1;
      } else {
        this.f.VA.value[isDef].isDefaultAddress = false;
      }
    }

    if (checkIsDefault == 0) {      
      this.toastr.error('Choose One Default Address.', 'Failed!');
      return;
    }
    if (this.custVendorForm.invalid) {
      this.toastr.error('Enter Details & Address Fields Marked(*)', 'Invalid!');
      // this.toastr.error('Please Enter The Fields.', 'Invalid!');
      return;
    }

        
    if (this.f.approvedVendor.value == 1 && this.f.approvedDate.value == null) {
      this.toastr.error('Approved Date Is  Required.', 'Failed!');
      return;
    }
    const uniqueValues = new Set(this.f.VA.value.map(v => v.salesEmail));
  const uniquePhone = new Set(this.f.VA.value.map(v => v.phoneNo));
      const uniqueEmail = new Set(this.f.VA.value.map(v => v.accountsEmail));
    if (uniqueValues.size < this.f.VA.value.length || uniquePhone.size < this.f.VA.value.length || uniqueEmail.size < this.f.VA.value.length) {
      console.log('duplicates found')
      this.toastr.error('Duplicate Email/Phone Found.', 'Invalid!');
      _this.loading = false;
      return
    }
       const uniqueAddress = new Set(this.f.VA.value.map(v => v.address1));
      if (uniqueAddress.size < this.f.VA.value.length ) {
      console.log('duplicates found')
      this.toastr.error('Duplicate Address Found.', 'Invalid!');
      _this.loading = false;
      return
    }
    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;
    const custVendorObj = {
      vendorCode: this.f.vendorCode.value,
      vendorName: this.f.vendorName.value,
      currency: this.f.currency.value,
      approvedDate: this.toDateObject(this.f.approvedDate.value),
      approvedVendor: this.f.approvedVendor.value,
      status: this.f.status.value,
      VA: this.f.VA.value,
 
    };
    this.loading = true
    this.CS.createCustomers(custVendorObj).subscribe(response => {
      console.log("response", response)
      if (response && response.status == 'success') {
        
      this.toastr.success('New Customer Added.','Success');
        _this.router.navigate(['/customer/edit', response.id]);
        this.loading = false
      } else {
        let errorMsg = 'Error occured while updating,Try again!';
         _this.loading = false
        if (response.errObject) {
            errorMsg = response.errObject;
          }
          
      this.toastr.error(errorMsg, 'Failed!');
      }
    });

  }

toDateObject(date){
    return this.CF.jsonDatetoDateObject(date)
}
changeState(event,countryId,dataField, to, index?, from?) {
    console.log('event');
    console.log('countryId');
    if (countryId && event) {
      let _this = this;
     _this.stateList = [];
      var getId = typeof event == 'object' ? event.id : event;
      var countryId = _.find(countryId, {id: parseInt(getId, 10)});
      console.log("countryId", countryId);

      
      
      _this.CS.getStateList({ id: countryId.id }).subscribe(response => {
        console.log("response",response);
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          _this.stateList = response.result[0].CS;
          console.log("_this.stateList",_this.stateList);
        } else {
        //  _this.stateList = [];
        }
      })
     /*   for (let ctr in this.form.companyForm.value ) {
      _this.form.companyForm.value[ctr].country) = countryId.id;
      //(<FormArray>this.VA.controls['ctr']).at(index).patchValue(input);
      console.log("_this.form.value[ctr].country",_this.form.value[ctr].country)
    }*/

    if (countryId) {
        if (index || index == 0) {
          var input = {};
          input[to] = countryId[dataField];
          (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          }
        } else {
          this.custVendorForm.get(to).setValue(countryId[dataField]);
          if (typeof event == 'object') {
            this.custVendorForm.get(from).setValue(getId);
          }
          console.log("this.custVendorForm", this.custVendorForm);
        }
      } else {

      }
    
  }
}
changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, {id: parseInt(getId, 10)});
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          }
        } else {
          this.custVendorForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.custVendorForm.get(from).setValue(getId);
          }
          console.log("this.custVendorForm", this.custVendorForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          }
        } else {
          this.custVendorForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.custVendorForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
        }
      } else {
        this.custVendorForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.custVendorForm.get(from).setValue(null);
        }
      }
    }
  }

  clear(fields, index?, checkField?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = checkField && field == checkField ? '' : null;
        (<FormArray>_this.custVendorForm.controls['VA']).at(index).patchValue(data);
      } 

    });
          console.log(this.f);
  }

omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}
}