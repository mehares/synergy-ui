import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnquiryRoutingModule } from './enquiry-routing.module';
import { EnquiryAddComponent } from './enquiry-add/enquiry-add.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgSelectModule } from '@ng-select/ng-select';
import { EnquiryEditComponent } from './enquiry-edit/enquiry-edit.component';
import { EnquiryListComponent } from './enquiry-list/enquiry-list.component';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
  declarations: [EnquiryAddComponent, EnquiryEditComponent, EnquiryListComponent],
  imports: [
    CommonModule,
    EnquiryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    NgbModule,
    NgxSpinnerModule,
    NgSelectModule,
    ClickOutsideModule
  ]
})
export class EnquiryModule { }
