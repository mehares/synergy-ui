import { Component, OnInit, ElementRef } from '@angular/core';
import {NgModule} from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import * as _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { NgxSpinnerService } from "ngx-spinner";
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { globalConstants } from '../../../constants/global-constants';
import { NgbCalendar, NgbDateParserFormatter, NgbDate } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-enquiry-edit',
  templateUrl: './enquiry-edit.component.html',
  styleUrls: ['./enquiry-edit.component.scss']
})
export class EnquiryEditComponent implements OnInit {

	enquiryForm: FormGroup;
	itemsArray: FormArray;
	customers = [];
	keywordVendor = 'vendorName';
	itemGroups = [];
  	itemTypes = [];
  	uoms = [];
  	vendors = [];
  	fileToUpload: File[] = [];
	imageSrc = [];
	keywordUom = 'uomName';
    keywordGroup = 'groupName';
    loading = false;
    submitted = false;
    id = null;
    enquiryDetails = null
    enqNumber = null
    UPLOAD_PATH = globalConstants.UPLOADS_DIR;
    dateArray = null
    loader =false
    DOC_PATH = globalConstants.ENQ_DOC_PATH;
    todayDate:any
    previewFn = 0;
    ENQ_STATUS = globalConstants.ENQ_STATUS
    dateVendor = null
    enqStatus = null
    itemsDocsArray : FormArray
    company = JSON.parse(localStorage.getItem('companyXid'));


  constructor(private formBuilder: FormBuilder,private CS: CommonservicesService, private el: ElementRef,private spinner: NgxSpinnerService,private CF: CommonFunctionsService,private toastr: ToastrService,private router: Router, private activatedroute: ActivatedRoute) { }

  ngOnInit() {
  	this.id = this.activatedroute.snapshot.params.id;
    this.getEnquiryDetails();
  	this.createForm();
  	this.getCustomers();
  	this.getVendors();
    this.todayDate = this.CF.dateObjectToJsonDate(new Date())

  }

   getEnquiryDetails() {
    let _this = this;
    let params = { where: { id: this.id } };
    _this.CS.getEnquiryDetails(params).subscribe(response => {
      console.log("enquiryDetails", response);
      if (response && response.status == "success" && response.result && response.result!=null) {
        _this.enquiryDetails = response.result;
        _this.setForm();

            if (response.result.EV && response.result.EV.length) {
      _.forEach(response.result.EV, function (field, index) {
        if (field.selectVendor ==true ) {
        	        _this.dateArray = []
        	_this.dateArray.push(field.date)
        	console.log("^^^^^^^^^^^^^date",_this.dateArray)
        }
    })
  }
       }  else {
        _this.enquiryDetails = {};
      }
    });
  }

  setForm() {
  	this.enquiryForm = this.formBuilder.group({
  		id: [this.enquiryDetails.id],
      status:1,
		vendorDetails: this.formBuilder.array([]),
    enqDate:[this.toJsonDate(this.enquiryDetails.enqDate)],
    enqStatus:[this.enquiryDetails.enqStatus],
     documents: this.formBuilder.array([]),
  });
  	this.setVendors();
    this.setDocuments()
    console.log("Form", this.form)
  }
  setVendors() {
  	    let _this = this;
    if (_this.enquiryDetails.EV && _this.enquiryDetails.EV.length) {
      _.forEach(_this.enquiryDetails.EV, function (field, index) {

        let item = _this.formBuilder.group({
          id: field.id,
          vendorDate:[_this.toJsonDate(field.date) ],
           details:[field.details],
           vendorXid:[field.vendorXid],
           vendorName:[field.vendorName],
           vendorNameAuto:[field.vendorName],
           status:[field.status],
           enqXid:[field.enqXid],
           selectVendor:[field.selectVendor],
           pdfSentDate:[field.pdfSentDate],
           date:[field.date],
           enquiryStatus:[field.enquiryStatus]

        });
        _this.itemsArray = _this.enquiryForm.get('vendorDetails') as FormArray;
        _this.itemsArray.push(item);
   });
    } else {
      this.createRequests();
    }
  }

  createForm() {
  	this.enquiryForm = this.formBuilder.group({
		vendorDetails: this.formBuilder.array([]),
    status:1,
     documents: this.formBuilder.array([]),
  });
  	this.createRequests();
    this.createDocuments();

}


createRequests() {
let _this =this
	let item = this.formBuilder.group({
			date:[null],
           details:['', ],
           vendorXid:['', ],
           vendorName:['', ],
           status:1,
           enqXid:[''],
           vendorNameAuto:[''],
           selectVendor:['',],
           id:[''],
           pdfSentDate:[''],
           vendorDate:[null],
           enquiryStatus:[null],

       })
    this.itemsArray = this.enquiryForm.get('vendorDetails') as FormArray;
    this.itemsArray.push(item);
    console.log("Form",this.form)

   }





get form() {
    return this.enquiryForm.controls;
  }

  clearSupplier(fields, index?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = null;
        (<FormArray>_this.enquiryForm.controls['vendorDetails']).at(index).patchValue(data);
      } else {
        _this.enquiryForm.get(field).setValue(null);
      }
    });
  }



  getCustomers() {
    let _this = this;

    let params = { model_name: 'vendors', where: { 'status': 1, typeUser: 2, approvedVendor: 1 } };
    _this.CS.getCustomerList(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.customers = response.result;
      } else {
        _this.customers = [];
      }
    });
  }
  getVendors() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'status': 1, approvedVendor: 1 ,  typeUser: 1    } };
    _this.CS.getVendorList(params).subscribe(response => {
      console.log("response getVendorList", response);
      if (response && response.status == "success" && response.result) {
        _this.vendors = response.result;
      } else {
        _this.vendors = [];
      }
    });
  }

  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, { id: parseInt(getId, 10) });
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index).patchValue(input);
          }
        } else {
          this.enquiryForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.enquiryForm.get(from).setValue(getId);
          }
          console.log("this.enquiryForm", this.enquiryForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index).patchValue(input);
          }
        } else {
          this.enquiryForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.enquiryForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index).patchValue(input);
        }
      } else {
        this.enquiryForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.enquiryForm.get(from).setValue(null);
        }
      }
    }
  }

  clearSelect(field){
    this.enquiryForm.get(field).setValue(null);
  }

  triggerFileUpload() {
    this.el.nativeElement.querySelector("#fileUpload").click();
  }

  deleteDoc(index) {
    this.fileToUpload.splice(index, 1);
    this.imageSrc.splice(index, 1)
  }

  upload(files: File[]) {

    let _this = this;
    _.forEach(files, function (value) {
       console.log("reader", value)
       var fileTypes = ['jpg', 'jpeg', 'png', 'xlsx', 'txt', 'gif', 'pdf', 'xls'];  //acceptable file types
        var extension = value.name.split('.').pop().toLowerCase() 
        console.log(extension)
        var Success = fileTypes.indexOf(extension) > -1
       if(Success) {
         if (value.size < 4096000) {
      var reader = new FileReader();
      reader.readAsDataURL(value);
      reader.onload = () => {
        var src = reader.result as string;
        _this.imageSrc.push(src);
        _this.fileToUpload.push(value);
      }
      }
        else {
        alert('Maximum allowed file size is 4 MB!');
        return
      }
      
    } else {
      alert('File type not supported')
    }
    });

    document.getElementById('fileUpload')['value'] = "";
  }
toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date) {
    return this.CF.dateObjectToJsonDate(date)
  }


  makeDecimalPoint(field?, index?, formArray?, point?) {
    let _this = this;
    if (index || index == 0) {
      var element = (<FormArray>this.enquiryForm.controls[formArray]).at(index);
      var value = (<FormArray>element).controls[field].value;
      var data = {};
      data[field] = (parseFloat(value.toString()).toFixed(point ? 2 : 3));
      value && !isNaN(value) ? element.patchValue(data) : '';
    } else {
      var value = _this.enquiryForm.get(field).value;
      value && !isNaN(value) ? _this.enquiryForm.get(field).setValue((parseFloat(value.toString()).toFixed(point ? 2 : 3))) : '';
    }
  }

   onSubmit() {
  	console.log("Form",this.form);
    let _this = this;
    _this.submitted = true;
	let formData = new FormData();
   var imageSize =[]
   var size;
    _.forEach(this.imageSrc, function (field) {
      imageSize.push(field.length)
      console.log(imageSize)
      size = imageSize.reduce((a, b) => a + b)/1000000
      console.log("size",size)
    })
    if(size > 10) {
      this.toastr.error('Total Allowed files is 10 Mb.', 'Invalid!');
      return
    }
    if (this.fileToUpload.length) {
      for (var i = 0; i < this.fileToUpload.length; i++) {
        formData.append("files", this.fileToUpload[i], uuidv4() + this.fileToUpload[i].name);
      }
    }
    if (this.enquiryForm.invalid) {
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      _this.loading = false;
    }
       for(let ct in this.enquiryForm.controls.vendorDetails.value) {
    if(this.enquiryForm.controls.vendorDetails.value.length >1 && this.enquiryForm.controls.vendorDetails.value[ct].vendorXid ==""){
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      _this.loading = false;
      return
    }

}     

    const uniqueValues = new Set(this.enquiryForm.controls.vendorDetails.value.map(v => v.vendorXid));
      if (uniqueValues.size < this.enquiryForm.controls.vendorDetails.value.length) {
      console.log('duplicates found')
      this.toastr.error('Duplicate Vendor Found.', 'Invalid!');
      _this.loading = false;
      return
    }  
    _this.enquiryForm.value['status'] = 1;
 if (this.enquiryForm.status == 'VALID') {
        _this.loading = true
        
        this.spinner.show();
      	setTimeout(() => this.spinner.hide(), 2000);
        //_this.enquiryForm.value['enqDate'] = this.toDateObject(_this.enquiryForm.get('enqDate').value)
        for (let dt in this.form.vendorDetails.value) {
        	this.form.vendorDetails.value[dt].vendorDate = this.toDateObject(_this.form.vendorDetails.value[dt].vendorDate)
          if(this.form.vendorDetails.value[dt].pdfSentDate == "") {
            this.form.vendorDetails.value[dt].pdfSentDate = null
          }
        }
          	console.log("Form",this.form);

        _this.CS.updateEnquiiry(_this.enquiryForm.value).subscribe(response => {
        	console.log("response",response);
          if (response && response.status == "success") {
            _this.CS.enqMailer({'vendor':response.result.EV, 'data':response.result }).subscribe(response2 => {
              console.log("response2", response2)
       if (response2 && response2.status == "success") {
            if (this.fileToUpload.length) {
              formData.append("data", response.result.id);
              _this.CS.saveEnquiryDocuments(formData).subscribe(res => {
                _this.loading = false;
                if (res && res.data && res.data.status == "success") {
                  this.toastr.success('Enquiry Added.', 'Success!');
                  this.resetFileUpload();
                  this.getEnquiryDetails();
                } else {
                  this.toastr.error('Something Went Wrong!', 'Failed!'); 
                }
              });
            } else {
              _this.loading = false;
              this.toastr.success('Enquiry Added.', 'Success!');
              this.getEnquiryDetails();
            }
         } else {
           _this.loading = false;
           this.getEnquiryDetails();
        this.toastr.error(response2.customMessage, 'Failed!');

      }
      

    });            
          } else {
            _this.loading = false;
            this.toastr.error('Something Went Wrong!', 'Failed!');
          }
        });
  }

}

generateRequest(i) {
	 let email = (localStorage.getItem('mdata'));
	 console.log("email");
	 let _this = this
   _this.previewFn =0
    for (let dt in this.form.vendorDetails.value) {
    if(!this.form.vendorDetails.value[dt].vendorName ){
            this.toastr.error('Please Fill Vendor Details', 'Failed!');
            return
          }
        }  
        var choosedVendor =[]
        var vendorId = []
        _.forEach(_this.form.vendorDetails.value, function (field, index) {
          if(!field.pdfSentDate){
          choosedVendor.push(field.selectVendor+'')
          vendorId.push(field.id+'')
          console.log('choosedVendor',choosedVendor.includes('true'));
          console.log('vendorId',vendorId.includes('true'));
        }
        })
         if(!choosedVendor.includes('true')){
            _this.toastr.error('Please Select A Supplier', 'Failed!');
            return
          }
         if(vendorId.includes('')) {
           _this.toastr.error('Please Save Supplier', 'Failed!');
            return
         }
    _this.CS.enqMailer({ 'vendor': _this.enquiryDetails.EV, preview:_this.previewFn,  setterMail: email, 'data':_this.enquiryDetails }).subscribe(response => {
     console.log("_this.enquiryDetails",_this.enquiryDetails);
     console.log("response",response);
      _this.loading = false;
      if (response && response.status == "success") {
          // open file
          
         // window.open(this.UPLOAD_PATH + 'enqPdf/' + response.fileData, '_blank');
           //this.updateEnqStatus(this.id, _this.enquiryDetails.enqStatus);
 

          this.toastr.info('Enquiry Sent!', 'Success');
          this.getEnquiryDetails();



      } else if (response && response.status == "successpreview"){
         window.open(this.UPLOAD_PATH + 'enqPdf/' + response.fileData, '_blank');   
          this.toastr.info('Verify the Details!', 'Info');

       } else {
        this.toastr.error(response.customMessage, 'Failed!');
      }
      

    });
  }

  chooseSupplier(i) {
console.log("Form", this.enquiryForm);
console.log(i);
 var element = (<FormArray>this.enquiryForm.controls['vendorDetails']).at(i);
    console.log("element", element);
    //(<FormGroup>element).get('date').patchValue('date')
    if ((<FormGroup>element).controls['id'].value != "") {
  /*	 for (let dt in this.form.vendorDetails.value) {
        	//this.form.vendorDetails.value[dt].date = this.toDateObject(this.form.vendorDetails.value[dt].date)
          this.form.vendorDetails.value[dt].pdfSentDate = this.toDateObject(this.form.vendorDetails.value[dt].pdfSentDate)
        }*/
   console.log("Form", this.enquiryForm);
  this.CS.updateEnquiiry(this.enquiryForm.value).subscribe(response => {
        	console.log("response",response);
        	  	console.log("***",this.form)
          if (response && response.status == "success") {
            this.getEnquiryDetails()
          }
})
}
else {
console.log("new supplier");
console.log("Form", this.enquiryForm);
}

  }

  changeRequest(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, { id: parseInt(getId, 10) });
      console.log("item**", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index).patchValue(input);
          }
        } else {
        }
      }
    }
  }


    updateEnqStatus(enqiD, status_value) {
    this.CS.updateEnqStatus({ "enqId": enqiD, "enqStatus": status_value }).subscribe(response => {

      if (response && response.status == "success") {
        console.log("Status changed", status_value);
        this.enquiryDetails.poStatus = status_value;
      } else {
        console.log("Status change failed", response);
      }
    });
  }


  deleteVendorItems(index) {
  	console.log("FORM",this.form);
  	let _this =this
    var element = (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index);
    console.log("element", element);
    if ((<FormArray>element).controls['id'].value) {
      console.log(true)
        var data = { status: 4,selectVendor: false };
        console.log("data", data);
        element.patchValue(data);
        _this.CS.cancelEnqMailer({ 'vendor': _this.enquiryDetails.EV,   'data':_this.enquiryDetails }).subscribe(response => {
     console.log("_this.enquiryDetails",_this.enquiryDetails);
     console.log("response",response);
   
  })


    } else  {
      this.itemsArray = this.enquiryForm.get('vendorDetails') as FormArray;
      this.itemsArray.removeAt(index);
    }

  }

convertDate(dateString) {
    //console.log("dateString-", dateString);
    if (dateString == "" || dateString == null) {
      return "";
    }
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMONTHYY(objectDate);
  }


    resetFileUpload(){
    let _this = this;
    _this.fileToUpload = [];
    _this.imageSrc =[];
  }
  deleteDocFromServer(doc, index) {
    console.log("doc", doc);
    var params = { id: doc.id, model_name: 'enquiryDocument' };
    let _this = this;
   // _this.loader[doc.id] = true;
    _this.CS.removeData(params).subscribe(response => {
      //_this.loader[doc.id] = false;
      if (response && response.status == "success" && response.result) {
        _this.enquiryDetails.EDO.splice(index, 1);
      } else {
        this.toastr.error('Something Went Wrong!', 'Failed!');
      }
    });
  }

  viewPdf() {
    this.previewFn = 1;
    let _this = this
    
    if(this.previewFn){
      if (!confirm("Make sure all changes saved before preview,Do you want to continue?")) {
        return false;
      }
    }

     _this.CS.enqMailer({ 'vendor': _this.enquiryDetails.EV, preview:_this.previewFn, 'data':_this.enquiryDetails }).subscribe(response => {
      _this.loading = false;
    if (response && response.status == "successpreview"){
         window.open(this.UPLOAD_PATH + 'enqPdf/' + response.fileData, '_blank');   
          //this.toastr.info('Verify the Details!', 'Info');

       } else {
        this.toastr.error(response.customMessage, 'Failed!');
      }
      

    });
  }

  dateChange(event,i,date: NgbDate, data) {
  console.log("event",event)
  //console.log(this.dateVendor);
  
  console.log(i);
  console.log(data)
   var element = (<FormArray>this.enquiryForm.controls['vendorDetails']).at(i);
    console.log("element", element);
    //var toObject = this.toDateObject(data)
    this.dateVendor = event;

  (<FormGroup>element).get('date').setValue(this.toDateObject(event))
  console.log("Form", this.enquiryForm);
}


 createDocuments() {
    let _this = this;
    let item = this.formBuilder.group({
      enqXid: [''],
      fileName: [''],
      fileDisplayName: [''],
      filePath: [''],
      companyXid: [null],
      status: 1
    });
    _this.itemsDocsArray = this.enquiryForm.get('documents') as FormArray;
    _this.itemsDocsArray.push(item);
  } 


   setDocuments() {
    let _this = this;
    if (_this.enquiryDetails.EDO && _this.enquiryDetails.EDO.length) {
      _.forEach(_this.enquiryDetails.EDO, function (field, index) {
        console.log("documentttttttttttt", field)
        let item = _this.formBuilder.group({
          enqXid: [field.enqXid],
          fileName: [field.fileName],
          fileDisplayName: [field.fileDisplayName],
          filePath: [field.filePath],
          companyXid: [_this.company],
          status: 1
        });
        _this.itemsDocsArray = _this.enquiryForm.get('documents') as FormArray;
        _this.itemsDocsArray.push(item);
      });
    }
  }

  fixSelectedData(name,index){
  console.log("name",name)
  console.log("index",index)

     var element = (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index);
 if(name =='vendor') {
      (<FormArray>element).controls['vendorNameAuto'].setValue( (<FormArray>element).controls['vendorName'].value);
    }
}

}


