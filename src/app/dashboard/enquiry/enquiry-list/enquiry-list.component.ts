import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { globalConstants } from '../../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-enquiry-list',
  templateUrl: './enquiry-list.component.html',
  styleUrls: ['./enquiry-list.component.scss']
})
export class EnquiryListComponent implements OnInit {

  enquirylist = [];
  enquirySeacrhForm: FormGroup;
  isValidFormSubmitted = null;
  submitted = false;
  customers = [];
  vendors = [];
  itemsPerPage = 18;
  currentPage = 1;
  totalItems = 0;
  offset = 0;
  searchToggle = false;
  searchLoader = false;
  search = '';
  clearFilter = false;
  searchFilterObject = null;
  pageOffset = 0;
  isAdvanceSearch = false;
  customerName = '';
  vendorName = '';
  selCustomerId = null;
  selVendorId = null;

  permissions = this.CF.findPermissions();
  loader = false;
  loaderItem = {};
  keywordVendor = 'vendorName'
  keywordCustomer = 'vendorName'
  userQuestionUpdate = new Subject<string>();
  public innerHeight: any;
  UPLOAD_PATH = globalConstants.UPLOADS_DIR  
  ENQ_STATUS = globalConstants.ENQ_STATUS
  status =  [{id:0, name:'Enquiry Updated'},{id:1,name:'Request Sent to Supplier'}, {id:2,name:'Quotation Received from Supplier'}, {id:3, name:'Quotation Sent to Customer'}, {id:4,name:'PO  Received from Customer'}, {id:5, name:'PO Sent to Supplier'}, {id:6, name:'Invoice Received from Supplier'}, {id:7, name:'Invoice Sent to Customer'}, {id:8, name:'Payment Made to Supplier'},  {id:9, name:'Payment Received from Customer'}, {id:10, name:'Closed'}, {id:11, name:'Cancelled'},{id:12,name:'Reject Enquiry'}]

  firstLoad = false
  today = new Date()
  todayDate:any
  closeResult = '';
  cancelModalForm:FormGroup
  enquiryXid = null
  modal = false
  enqDate=null
  dashboardId = null


  constructor(private http: HttpClient,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private CS: CommonservicesService, private CF: CommonFunctionsService,private spinner: NgxSpinnerService,private modalService: NgbModal, private route: ActivatedRoute) {
    this.userQuestionUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.resetPagination();
        this.onSubmit();
      });
  }

  ngOnInit() {
    this.innerHeight = window.innerHeight;
    console.log("innerHeight", innerHeight);
    if (this.innerHeight < 700) {
      this.itemsPerPage = 10;
    }
    if (this.innerHeight > 900) {
      this.itemsPerPage = 20;
    }
    if (this.innerHeight > 1000) {
      this.itemsPerPage = 25;
    }

    this.dashboardId = this.route.snapshot.paramMap.get('id');
    this.getEnquiryList();
    this.getVendors();
    this.getCustomers();
    this.createSearchForm();
    this.createModalForm();
    this.firstLoad = true
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    this.todayDate = this.CF.dateObjectToJsonDate(new Date())
    
    

  }
  createSearchForm() {
    this.enquirySeacrhForm = this.formBuilder.group({
      fromDate: [null, []],
      toDate: [null, []],
      source: [null, []],
      search: ['', []],
      customer: ['', []],
      vendor: ['', []],
      enqno: ['', []],
      customerName: ['', []],
      vendorName: ['', []],
      status:[null,[]]
    });
  }

  get f() {
    return this.enquirySeacrhForm.controls;
  }

  convertDate(dateString) {
    if (dateString == "" || dateString == null) {
      return "";
    }
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMONTHYY(objectDate);
  }


  getCustomers() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'approvedVendor': 1, 'status': 1, typeUser: 2 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.customers = response.result;
      } else {
        _this.customers = [];
      }
    });
  }

  getVendors() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'approvedVendor': 1, 'status': 1, typeUser: 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.vendors = response.result;
      } else {
        _this.vendors = [];
      }
    });
  }


  getEnquiryList() {
    var params = {  limit: this.itemsPerPage, offset: this.offset, filterOption: this.searchFilterObject, enqStatus : this.dashboardId };
    this.pageOffset = this.offset;
    let _this = this;
    _this.loader = true;
    this.CS.getEnquiryList(params).subscribe(response => {
      if (response && response.status == 'success') {
        _this.enquirylist = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
        console.log("enquirylist", response);
      } else {
        _this.loader = false;
        this.toastr.error('Error Occured,Try again!', 'Failed!');
      }
      _this.loader = false;
    });
  }



  preventNavClose(event) {
    console.log(event);
    // $(this).parents('.dropdown').find('button.dropdown-toggle').dropdown('toggle')
    return false;
  }

  onSubmit(from = '') {

    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;
    if (this.enquirySeacrhForm.invalid) {
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      return;
    }
    if(this.enquirySeacrhForm.value.fromDate || this.enquirySeacrhForm.value.toDate){
      console.log(true)
      if(this.enquirySeacrhForm.value.toDate ==null || this.enquirySeacrhForm.value.fromDate ==null) {
      this.toastr.error('Please Provide A Valid Date Range.', 'Invalid!');
      _this.searchLoader = false;
      console.log("FORM",this.enquirySeacrhForm)
      return;
    }
}
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;


    const poSeacrhObj = {
      fromDate: this.toDateObject(this.f.fromDate.value),
      toDate: this.toDateObject(this.f.toDate.value),

      // search: this.f.search.value,
      vendor: this.f.vendor.value,
      customer: this.f.customer.value,
      enqno: this.f.enqno.value,
      search: this.search,
      status : this.f.status.value
    };
    _this.searchLoader = false;
    _this.clearFilter = true;    
    _this.searchToggle = false;
    this.searchFilterObject = poSeacrhObj;
    this.getEnquiryList();


  }

  preventChange(event) {
    //check if needed function
  }

  pageChanged(event) {
    console.log("event", event)
    this.currentPage = event
    var setoffset = event - 1
    setoffset = setoffset * this.itemsPerPage
    this.offset = setoffset
    this.getEnquiryList();
  }

  changeSelection(event, to) {
    console.log("event", event)
    console.log("to", to);
    // this.poSeacrhForm.get(to).setValue(event.id);

    if (to == 'customer') {
      this.customerName = event.vendorName ? event.vendorName : this.customerName;
      this.selCustomerId = event.id ? event.id : this.selCustomerId;
      this.enquirySeacrhForm.get(to).setValue(this.customerName);
    }
    if (to == 'vendor') {
      this.vendorName = event.vendorName ? event.vendorName : this.vendorName;
      this.selVendorId = event.id ? event.id : this.selVendorId;
      this.enquirySeacrhForm.get(to).setValue(this.vendorName);
    }
  }

  clear(to) {
    this.enquirySeacrhForm.get(to).setValue(null);
  }

  exportEnquiry() {
    let _this = this;
    _this.CS.enquirySetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'enquiry/' + response.filename, '_blank');
        this.toastr.success('Successfuly Exported Excel!', 'Success!');
      }
      else {
        this.toastr.error(response.msg, 'Failed!');
      }
    });
  }

  clearSearch(type) {

    if (type =="reset") {
      this.isAdvanceSearch = false;
      this.customerName = null;
      this.vendorName = null;
      this.searchToggle = true;
    }
    else  {
      this.isAdvanceSearch = false;
      this.customerName = null;
      this.vendorName = null;
      this.searchToggle = false
    }
    this.searchFilterObject = null;
    this.createSearchForm();
    this.getEnquiryList();
  }

  clearMainSearch(type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.clearSearch(type);
      this.searchToggle = true
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }
  
  toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }
  resetPagination() {
    this.offset = 0;
    this.currentPage = 1;
  }
  resetCustomerValue(){
    this.enquirySeacrhForm.get('customer').setValue(null);
  }



  changeEnquiryStatus(id, status) {

    console.log("status___", status);
    let _this = this;
    let params = {"enqId": id, "enqStatus": status };
    _this.CS.updateEnqStatus(params).subscribe(response => {
      if (response && response.status == "success") {
        console.log("success");
        this.toastr.success('Enquiry Cancelled', 'Success!');
        this.getEnquiryList();

      } else {
        this.toastr.error('Something Went Wrong', 'Failed!');
      }
    });
  }

   open(content,enqXid) {
     console.log("enqXid", enqXid)
     this.enquiryXid = enqXid 
      var dateEnq =[]
      let _this = this
      _.forEach(_this.enquirylist, function (field, index) {
       if(field.id ==_this.enquiryXid){
         console.log(true);
         dateEnq.push(field.enqDate)
         _this.enqDate = _this.CF.dateObjectToJsonDate(dateEnq)
       }

      })
      console.log("_this.enqDate",_this.enqDate)
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      console.log("result",result)
      //this.enquiryXid = enqXid 
      this.enqRecievedDate()
      this.Submit();
       this.createModalForm()
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
       this.createModalForm()
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  createModalForm() {
    this.cancelModalForm = this.formBuilder.group({
      reason:['',Validators.required],
      cancelDate:['',Validators.required],
      id:[''],
      enqStatus:11
});
  }
 get form() {
    return this.cancelModalForm.controls;
  }

  Submit() {
console.log("enqXid", this.enquiryXid)
     let _this = this;
    this.submitted = true;
    this.isValidFormSubmitted = false;
    if (this.cancelModalForm.invalid) {
      this.modal = true
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      return;
    }
     this.modalService.dismissAll()
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    const modalObj = {
      cancelDate: this.toDateObject(this.form.cancelDate.value),
      reason: this.form.reason.value,
      id: this.enquiryXid,
      enqStatus:11
    };
    console.log("modalObj", modalObj)
    this.CS.cancelEnquiry(modalObj).subscribe(response => {
          console.log("response",response);
              console.log("***",this.form)
          if (response && response.status == "success") {

            this.toastr.success('Enquiry Cancelled!', 'Success!');
            this.getEnquiryList()
            this.createModalForm()
          }
})

  }

  enqRecievedDate(){
    var dateEnq =[]
    let _this = this
    console.log("enqXidss", this.enquiryXid)
     _.forEach(_this.enquirylist, function (field, index) {
       if(field.id ==_this.enquiryXid){
         dateEnq.push(field.enqRecievedDate)
         _this.enqDate = this.CF.dateObjectToJsonDate(dateEnq)
       }

  })
   }


   comaSeperated(vendorArr){
    var vendors =[]
    _.forEach(vendorArr, function (field, index) {
      vendors.push(field.vendorName)
    })
    let vendors_name = (vendors.join(", ")).replace(/,\s*$/, ""); // remove trailing coma and return
    return vendors_name
   }

     onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container') || e.path[5].className.includes('ng-select ng-select') || e.path[6].className.includes('dropdown-menu show')){
        console.log('wow--->closeee----')
      }
      else {
    this.firstLoad = false
    this.searchToggle = false
  }
  }
}