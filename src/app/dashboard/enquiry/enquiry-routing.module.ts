import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnquiryAddComponent } from './enquiry-add/enquiry-add.component';
import { EnquiryEditComponent } from './enquiry-edit/enquiry-edit.component';
import { EnquiryListComponent } from './enquiry-list/enquiry-list.component';
import { PageGuard } from '../../guards/page-guard.service';
const routes: Routes = [
	 {
    path: 'add',
    canActivate: [PageGuard],
    component: EnquiryAddComponent,
    data:{permissions: 'enquiry_add' }
  },
   {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: EnquiryEditComponent,
    data:{permissions: 'enquiry_edit' }
  },
  {
  	path: 'list',
    canActivate: [PageGuard],
  	component: EnquiryListComponent,
    data:{permissions: 'enquiry_view' }
  },
    {
    path: 'list/:id',
    canActivate: [PageGuard],
    component: EnquiryListComponent,
    data:{permissions: 'enquiry_view' }
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnquiryRoutingModule { }
