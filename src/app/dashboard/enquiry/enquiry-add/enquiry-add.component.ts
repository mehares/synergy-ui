import { Component, OnInit, ElementRef } from '@angular/core';
import {NgModule} from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import * as _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { NgxSpinnerService } from "ngx-spinner";
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbCalendar, NgbDateParserFormatter, NgbDate, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2' 
@Component({
  selector: 'app-enquiry-add',
  templateUrl: './enquiry-add.component.html',
  styleUrls: ['./enquiry-add.component.scss']
})
export class EnquiryAddComponent implements OnInit {

	enquiryForm: FormGroup;
	itemsArray: FormArray;
	customers = [];
	keywordVendor = 'vendorName';
	itemGroups = [];
  	itemTypes = [];
  	uoms = [];
  	vendors = [];
  	fileToUpload: File[] = [];
	imageSrc = [];
	keywordUom = 'uomName';
    keywordGroup = 'groupName';
    vessels = [];
  	keywordVessel = 'vessel';
    loading = false;
    submitted = false;
    dateTaken = null
    selectedDate:'';
    todayDate:any;
    dateVendor = null
    closeResult = '';
    customer = ''
    itemsDocsArray: FormArray;
    customerNameAuto = ''
    groupNameAuto = ''
    itemArray = 'Valve'


  constructor(private formBuilder: FormBuilder,private CS: CommonservicesService, private el: ElementRef,private spinner: NgxSpinnerService,private CF: CommonFunctionsService,private toastr: ToastrService,private router: Router,private modalService: NgbModal) { }

  ngOnInit() {
  	this.createForm();
  	this.getCustomers();
  	this.getVendors();
  	this.getUoms();
  	this.getTypes();
  	this.getItemGroups();
  	this.getVessels();
  	this.todayDate = this.CF.dateObjectToJsonDate(new Date())
  }

  createForm() {
  	this.enquiryForm = this.formBuilder.group({
  		enqDate: ['', Validators.required],
  		customerXid: ['', Validators.required],
		customerName: ['', Validators.required],
		customerRefName: ['', Validators.required],
		customerRefNo:['', Validators.required],
		//vesselXid: ['', Validators.required],
      	//vesselName: ['', Validators.required],
      	serviceRequestFor:['', Validators.required],
		status:1,
		details: this.formBuilder.array([]),
		vendorDetails: this.formBuilder.array([]),
     documents: this.formBuilder.array([]),
		vendorNameAuto:[''],
    customerNameAuto:['']
  });
  	this.createItems();
  	this.createRequests();
    this.createDocuments();

}

createItems() {
    let item = this.formBuilder.group({
      groupXid: [''],
      group: ['', Validators.required],
      description: ['', Validators.required],
      typeXid: [null],
      type: ['',Validators.required],
      uomXid: [''],
      groupNameAuto: [''],
      uomNameAuto: [''],
      uom: ['', Validators.required],
      status: 1,
      qtQty:['', Validators.required]

    });
    this.itemsArray = this.enquiryForm.get('details') as FormArray;
    this.itemsArray.push(item);
  
}

createRequests() {

	let item = this.formBuilder.group({
			date:[null],
           details:['' ],
           vendorXid:[''],
           vendorName:[''],
           selectVendor:[''],
           status:1,
           vendorNameAuto:['']

       })
    this.itemsArray = this.enquiryForm.get('vendorDetails') as FormArray;
    this.itemsArray.push(item);
   }

   deleteItems(index) {
    this.itemsArray = this.enquiryForm.get('details') as FormArray;
    this.itemsArray.removeAt(index);
  }

  deleteVendorItems(index) {
    this.itemsArray = this.enquiryForm.get('vendorDetails') as FormArray;
    this.itemsArray.removeAt(index);
  }

get form() {
    return this.enquiryForm.controls;
  }

  clearSupplier(fields, index?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = null;
        (<FormArray>_this.enquiryForm.controls['vendorDetails']).at(index).patchValue(data);
      } else {
        _this.enquiryForm.get(field).setValue(null);
      }
    });
  }

  clear(fields, index?, checkField?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = checkField && field == checkField ? '' : null;
        (<FormArray>_this.enquiryForm.controls['details']).at(index).patchValue(data);
      } else {
        _this.enquiryForm.get(field).setValue(checkField && field == checkField ? '' : null);
      }
    });
  }


  getVessels() {
    let _this = this;
    let params = { model_name: 'vessels', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response aaaaa", response);
      if (response && response.status == "success" && response.result) {
        _this.vessels = response.result;
      } else {
        _this.vessels = [];
      }
    });
  }

  getCustomers() {
    let _this = this;

    let params = { model_name: 'vendors', where: { 'status': 1, typeUser: 2, approvedVendor: 1 } };
    _this.CS.getCustomerList(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.customers = response.result.sort((a,b) => a.vendorName > b.vendorName ? 1 : -1);
       console.log("sort",this.customers)
      } else {
        _this.customers = [];
      }
    });
  }
  getVendors() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'status': 1, approvedVendor: 1 ,  typeUser: 1    } };
    _this.CS.getVendorList(params).subscribe(response => {
      console.log("response getVendorList", response);
      if (response && response.status == "success" && response.result) {
        _this.vendors = response.result;
      } else {
        _this.vendors = [];
      }
    });
  }
getItemGroups() {
    let _this = this;
    let params = { model_name: 'itemGroups', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("itemGroups", response);
      if (response && response.status == "success" && response.result) {
        _this.itemGroups = response.result;
      } else {
        _this.itemGroups = [];
      }
    });
  }
getTypes() {
    let _this = this;

    let params = { model_name: 'poTypes', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("itemTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.itemTypes = response.result;
      } else {
        _this.itemTypes = [];
      }
    });
  }

  getUoms() {
    let _this = this;
    let params = { model_name: 'uom', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("getUoms", response);
      if (response && response.status == "success" && response.result) {
        _this.uoms = response.result;
        console.log(" _this.uoms", _this.uoms);
      } else {
        _this.uoms = [];
      }
    });
  }
  changeSelection(event, data, dataField, to, index?, from?, where?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    console.log("from",from)
    console.log("where", where);
   let _this = this
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, { id: parseInt(getId, 10) });
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.enquiryForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            console.log(true)
            var input = {};
            input[from] = getId;
            (<FormArray>this.enquiryForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.enquiryForm.get(to).setValue(item[dataField]);
          if(where=='customerNameAuto'){
            this.customerNameAuto = item[dataField]

          }
          console.log("event type", event.constructor)

          if (typeof event == 'object' ) {
            this.enquiryForm.get(from).setValue(getId);
            where = item[dataField]
            console.log();
          }
          
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.enquiryForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.enquiryForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.enquiryForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.enquiryForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.enquiryForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.enquiryForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.enquiryForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.enquiryForm.get(from).setValue(null);
        }
      }
    }



  }
  clearSelect(field){
    this.enquiryForm.get(field).setValue(null);
  }

  triggerFileUpload() {
    this.el.nativeElement.querySelector("#fileUpload").click();
  }

  deleteDoc(index) {
    this.fileToUpload.splice(index, 1);
    this.imageSrc.splice(index, 1)
  }

upload(files: File[]) {

    let _this = this;
    _.forEach(files, function (value) {
       console.log("reader", value)
       var fileTypes = ['jpg', 'jpeg', 'png', 'xlsx', 'txt', 'gif', 'pdf', 'xls'];  //acceptable file types
        var extension = value.name.split('.').pop().toLowerCase() 
        console.log(extension)
        var Success = fileTypes.indexOf(extension) > -1
       if(Success) {
         if (value.size < 4096000) {
      var reader = new FileReader();
      reader.readAsDataURL(value);
      reader.onload = () => {
        var src = reader.result as string;
        _this.imageSrc.push(src);
        _this.fileToUpload.push(value);
      }
      }
        else {
        alert('Maximum allowed file size is 4 MB!');
        return
      }
      
    } else {
      alert('File type not supported')
    }
    });

    document.getElementById('fileUpload')['value'] = "";
  }

    changeRequest(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);

    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, { id: parseInt(getId, 10) });
      console.log("item**", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index).patchValue(input);
          }
        } else {
        }
      }
    }
  }
  onSubmit(content) {
  	console.log("Form",this.form);
    let _this = this;
    _this.submitted = true;
	let formData = new FormData();
   var imageSize =[]
   var size;
    _.forEach(this.imageSrc, function (field) {
      imageSize.push(field.length)
      console.log(imageSize)
      size = imageSize.reduce((a, b) => a + b)/1000000
      console.log("size",size)
    })
    if(size > 10) {
      this.toastr.error('Total Allowed files is 10 Mb.', 'Invalid!');
      return
    }
    if (this.fileToUpload.length) {
      for (var i = 0; i < this.fileToUpload.length; i++) {
        formData.append("files", this.fileToUpload[i], uuidv4() + this.fileToUpload[i].name);
      }
    }
    if (this.enquiryForm.invalid ) {
      this.toastr.error('Please Fill Valid Details.', 'Invalid!');
      _this.loading = false;
    }
    for(let ct in this.enquiryForm.controls.vendorDetails.value) {
    if(this.enquiryForm.controls.vendorDetails.value.length >1 && this.enquiryForm.controls.vendorDetails.value[ct].vendorXid ==""){
    	this.toastr.error('Please Fill Valid Details.', 'Invalid!');
    	_this.loading = false;
    	return
    }

}

	const uniqueValues = new Set(this.enquiryForm.controls.vendorDetails.value.map(v => v.vendorXid));

		if (uniqueValues.size < this.enquiryForm.controls.vendorDetails.value.length) {
 		 console.log('duplicates found')
 		 this.toastr.error('Duplicate Vendor Found.', 'Invalid!');
    	_this.loading = false;
    	return
		}
    //this.checkDate();
   	//this.open(content)
    _this.enquiryForm.value['status'] = 1;
 if (this.enquiryForm.status == 'VALID') {
        _this.loading = true
         Swal.fire({ 
              title: 'Confirm Submitting Enquiry',
              text: 'Have you confirmed the details before submit?',
              showCancelButton: true,
              confirmButtonText: 'Yes',
              cancelButtonText: 'No',
              allowOutsideClick: false,
              position: 'center',
              }).then((result) => {

      if (result.value) {
        this.spinner.show();
      	setTimeout(() => this.spinner.hide(), 2000);
      	 _this.enquiryForm.value['enqStatus'] = 0;
        _this.enquiryForm.value['enqDate'] = this.toDateObject(_this.enquiryForm.get('enqDate').value)
 
          	console.log("Form",this.form);
      	//		var vendorRequest = []
      	//if(this.enquiryForm.value.vendorDetails.value.selectVendor == true) {
      		      		/*_.forEach(this.enquiryForm.controls.vendorDetails.value, function (row,index) {
      		      			if(this.enquiryForm.controls.vendorDetails.value.selectVendor == true) {
      			vendorRequest.push(row.vendorXid)
      		}
      		else {
      			console.log("error")
      		}
      	})*/
      	       for (let dt in this.enquiryForm.controls.vendorDetails.value) {
        	this.enquiryForm.controls.vendorDetails.value[dt].date = this.toDateObject(_this.enquiryForm.controls.vendorDetails.value[dt].date)
        }
        _this.CS.saveEnquiry(_this.enquiryForm.value).subscribe(response => {
        	console.log("response",response);
          if (response && response.status == "success") {
          	_this.CS.enqMailer({'vendor':response.result.EV, 'data':response.result }).subscribe(response2 => {
          		console.log("response2", response2)
 			if (response2 && response2.status == "success") {
    			if (this.fileToUpload.length) {
              formData.append("data", response.result.id);
              _this.CS.saveEnquiryDocuments(formData).subscribe(res => {
              	console.log("res", res);
                
                if (res && res.data && res.data.status == "success") {
                  this.toastr.success('Enquiry Added.', 'Success!');
                   _this.router.navigate(['/enquiry/edit', response.result.id]);
                   _this.loading = false;
                } else {
                  this.toastr.error('Something Went Wrong!', 'Failed!'); 
                  _this.loading = false
                }
              });
            } else {
             
              this.toastr.success('Enquiry Added.', 'Success!');
               _this.router.navigate(['/enquiry/edit', response.result.id]);
                _this.loading = false;
            }
             } else {
               _this.loading = false
        this.toastr.error(response2.customMessage, 'Failed!');
      }
      

    });
        
          } else {
            _this.loading = false;
            this.toastr.error('Something Went Wrong!', 'Failed!');
          }
        });
        }     
        else if (result.dismiss === Swal.DismissReason.cancel) {
              _this.loading = false;
              _this.submitted = false;
      /*Swal.fire(
          'Cancelled',
          'Enquiry not added',
          'error'
        )*/
      }})
  }
//}
}

toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date) {
    return this.CF.dateObjectToJsonDate(date)
  }


  makeDecimalPoint(field?, index?, formArray?, point?) {
    let _this = this;
    if (index || index == 0) {
      var element = (<FormArray>this.enquiryForm.controls[formArray]).at(index);
      var value = (<FormArray>element).controls[field].value;
      var data = {};
      data[field] = (parseFloat(value.toString()).toFixed(point ? 2 : 3));
      value && !isNaN(value) ? element.patchValue(data) : '';
    } else {
      var value = _this.enquiryForm.get(field).value;
      value && !isNaN(value) ? _this.enquiryForm.get(field).setValue((parseFloat(value.toString()).toFixed(point ? 2 : 3))) : '';
    }
  }

  checkDate() {
	let today = JSON.stringify(new Date());
     today = today.slice(1,11)
     console.log(today);
      var selectedDate =this.toDateObject(this.enquiryForm.get('enqDate').value)
      console.log(selectedDate);
    if( selectedDate >= today) {
    	this.toastr.error('ENQ Date should be before current date')
      return
    }
  }

dateChange(event,i,date: NgbDate, data) {
	console.log("event",event)
	//console.log(this.dateVendor);
	
	console.log(i);
	console.log(data)
 	var element = (<FormArray>this.enquiryForm.controls['vendorDetails']).at(i);
    console.log("element", element);
    //var toObject = this.toDateObject(data)
    this.dateVendor = event;

	(<FormGroup>element).get('date').patchValue(event)
	console.log("Form", this.enquiryForm);
}


   		/*open(content) {
      let _this = this
		this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
		this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    	});*/

 
 createDocuments() {
    let _this = this;
    let item = this.formBuilder.group({
      enqXid: [''],
      fileName: [''],
      fileDisplayName: [''],
      filePath: [''],
      companyXid: [null],
      status: 1
    });
    _this.itemsDocsArray = this.enquiryForm.get('documents') as FormArray;
    _this.itemsDocsArray.push(item);
  }


fixSelectedData(name,index){
  console.log("name",name)
  console.log("index",index)

     var element = (<FormArray>this.enquiryForm.controls['details']).at(index);
     var element2 = (<FormArray>this.enquiryForm.controls['vendorDetails']).at(index);

     if(name =='group') {
      (<FormArray>element).controls['groupNameAuto'].setValue( (<FormArray>element).controls['group'].value);
    }

    if (name =='customer'){
      this.enquiryForm.get('customerNameAuto').setValue(this.enquiryForm.get('customerName').value)
    }


     if(name =='uom') {
      (<FormArray>element).controls['uomNameAuto'].setValue( (<FormArray>element).controls['uom'].value);
    }

     if(name =='vendor') {
      (<FormArray>element2).controls['vendorNameAuto'].setValue( (<FormArray>element2).controls['vendorName'].value);
    }
}

}
