import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { InvoiceRoutingModule} from './invoice-routing.module';
import { InvoiceAddComponent } from './invoice-add/invoice-add.component';
import { InvoiceGenerateComponent } from './invoice-generate/invoice-generate.component';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgSelectModule } from '@ng-select/ng-select';
import { ClickOutsideModule } from 'ng-click-outside';



@NgModule({
  declarations: [InvoiceAddComponent, InvoiceGenerateComponent, InvoiceListComponent],
  imports: [
    CommonModule,
    InvoiceRoutingModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    AngularFontAwesomeModule,
    NgxSpinnerModule,
    NgSelectModule,
    ClickOutsideModule
  ],
  
})
export class InvoiceModule { }
