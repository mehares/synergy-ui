import { Component, OnInit, ElementRef, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';

import { v4 as uuidv4 } from 'uuid';
import { globalConstants } from '../../../constants/global-constants';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-invoice-generate',
  templateUrl: './invoice-generate.component.html',
  styleUrls: ['./invoice-generate.component.scss']
})

export class InvoiceGenerateComponent implements OnInit {

  loading = false;
  invoiceForm: FormGroup;
  itemsArray: FormArray;
  itemsDetailsArray: FormArray;
  isValidFormSubmitted = null;
  submitted = false;
  // position: NbGlobalPosition = NbGlobalPhysicalPosition.BOTTOM_RIGHT;
  savedItems = [];
  invoiceDetails = null;
  invoicePODetails = null;
  DOC_PATH = globalConstants.PO_DOC_PATH;
  UPLOAD_PATH = globalConstants.UPLOADS_DIR;
  INV_STATUS = globalConstants.INV_STATUS
  sourceType = [{ id: 1, name: "From Customer" }, { id: 2, name: "To Vendor" }];
  invoice_id = null;
  itemTotals = {};
  itemTotalsPageLoad = {};
  itemTotal = 0;
  itemTotalAmountUsd = 0;
  currentPOType = null;
  loader = {};
  itemsPerPage = 200;
  currentPage = 1;
  totalItems = 0;
  offset = 0;
  poDropList = [];
  poDropVal = null;
  invoiceDifference = 0;
  fileToUpload: File[] = [];
  imageSrc = [];
  invoicePOTotalCost = 0;
  invoicePOTotalAmountUsd = 0;
  paymentAllow = false; // later chage
  itemTCDTotal = 0;
  allowInvoiceSend = false;
  currencies = [];
  permissions = this.CF.findPermissions();
 
  constructor(private router: Router, private toastr: ToastrService ,
    private el: ElementRef,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private CS: CommonservicesService, private CF: CommonFunctionsService,private spinner: NgxSpinnerService,) {

  }


  ngOnInit() {
    
    this.getCurrencies();
    this.getPOList();
    //this.createForm();
    this.createInvoiceForm();
    this.getInvoiceDetails();
  }
 
  resetCalculationAmount(){  
    let _this = this;    
    _this.itemTotal = 0;
    _this.itemTotalAmountUsd = 0;  
    _this.invoicePOTotalCost = 0;
    _this.invoicePOTotalAmountUsd = 0;
    _this.itemTCDTotal=0;

    console.log("reset- _this.invoicePOTotalCost************************",_this.invoicePOTotalCost)
  }

  getInvoiceDetails(){
    let _this = this;    
    _this.invoice_id = this.route.snapshot.paramMap.get('id');
    console.log(_this.invoice_id);
    _this.CS.getInvoiceDetails({ id: _this.invoice_id }).subscribe(response => {
      
    _this.resetCalculationAmount();
      console.log("response==============", response)
      if (response && response.result) {
        
        _this.invoicePODetails = response.result.PO;
        _this.invoiceDetails = response.result.invoice;
        _this.itemTotalAmountUsd = response.result.invoice.amountUsdTotal;
        console.log("this.invoiceDetails.invStatus", this.invoiceDetails.invStatus);
        if (_this.invoiceDetails.invStatus == null) {
          _this.invoiceDetails.invStatus = 0;
        }
        _this.invoiceDetails.invStatus = parseInt(_this.invoiceDetails.invStatus);
        
        if (_this.invoiceDetails.invStatus >= 2) {
          _this.paymentAllow = true;
        }
        if (_this.invoiceDetails.invStatus > 0 && _this.invoiceDetails.invStatus < 3) {
          _this.allowInvoiceSend = true;
        }
        
         
        // Attach po details to invoice array
        _.map(_this.invoiceDetails.IMH, function (item) {
          console.log("itemitem  itemitemitem", item);
          let poInfoData = _.filter(_this.invoicePODetails, function (o) { return o.id == item.poHeaderXid; });
          
          console.log("poInfo", poInfoData)
          console.log("poInfo ll", poInfoData.length)
          if (poInfoData.length > 0) {
            let poInfo = poInfoData[0];
            item.poDate = poInfo.poDate;
            item.port = poInfo.port;
            item.vesselName = poInfo.vesselName;
            item.vendorName = poInfo.vendorName;
            item.customerName = poInfo.customerName;
            item.advanceAmountUsd = poInfo.advanceAmountUsd;
            item.poCost = poInfo.poCost;
          }
        });

        this.selectAll(true);
        this.showPODetails(null, "show");
        // calculate total
        console.log("reset aft- _this.invoicePOTotalCost+++++++++++++++++++++++++++",_this.invoicePOTotalCost)
        _.forEach(_this.invoicePODetails, function (poData, index) {
          _this.invoicePOTotalCost = _this.invoicePOTotalCost + poData['poCost'];
          _this.invoicePOTotalAmountUsd = _this.invoicePOTotalAmountUsd + poData['amountUsd'];
          _this.itemTCDTotal = _this.itemTCDTotal + parseFloat(poData['tcd']);
          console.log('----------+>',_this.invoicePOTotalAmountUsd)
        });


        this.setForm();

      } else {
        if (response.result == null) {
          this.toastr.error( 'There is no such invoice exist.','Invalid!');

          _this.router.navigate(['/invoice/list']);
        } else {
            this.toastr.error(  'Error occured while loading,Try again.','Error!');
        }

      }
    });
  }
  getCurrencies() {
    let _this = this;
    let params = { model_name: 'currencies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencies = response.result;
      } else {
        _this.currencies = [];
      }
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.invoiceForm.controls;
  }

  createInvoiceForm() {
    this.invoiceForm = this.formBuilder.group({
      id: [''],
      remarks: ['', Validators.required],
      invSentDate: [''],
      invoicePdf: [''],
      invStatus: [''],
      IMH: this.formBuilder.array([])
    });
    this.createInvoiceHeaderItems();
    console.log(" this.purchaseForm", this.invoiceForm);
  }
  createInvoiceHeaderItems() {
    let item = this.formBuilder.group({
      // id: null,
      ID: this.formBuilder.array([])
    });
    this.itemsArray = this.invoiceForm.get('IMH') as FormArray;
    this.itemsArray.push(item);
  }


  createForm() {
    this.invoiceForm = this.formBuilder.group({
      id: [''],
      remarks: ['', Validators.required],
      invSentDate: [''],
      invoicePdf: [''],
      invStatus: [''],
      IMH: this.formBuilder.array([])
    });
  }
  viewpdf() {
    window.open(this.UPLOAD_PATH + 'invoice/' + this.invoiceDetails.invoicePdf, '_blank');
  }
  setForm() {
    console.log("_this.invoiceDetails FOR SET FORM", this.invoiceDetails);
    let ne_invstatus = this.invoiceDetails.invStatus;
    if (ne_invstatus == "" || ne_invstatus == 0) {
      ne_invstatus = 1;
    }
    this.invoiceForm = this.formBuilder.group({
      id: [this.invoiceDetails.id],
      remarks: [this.invoiceDetails.remarks, Validators.required],
      invSentDate: [this.toJsonDate(this.invoiceDetails.invSentDate)],
      invoicePdf: [this.invoiceDetails.invoicePdf],
      invStatus: [ne_invstatus],
      IMH: this.formBuilder.array([]),
      enqXid: [this.invoicePODetails.enqXid],
      quotationXid: [this.invoicePODetails.quotationXid]
    });
    this.setItems();

    console.log("this.invoiceForm--->", this.invoiceForm);
    //this.calculateInvoiceTotal();
    this.showInvoiceTotal();
  }

  setItems() {
    let _this = this;
    if (_this.invoiceDetails.IMH && _this.invoiceDetails.IMH.length) {
      _.forEach(_this.invoiceDetails.IMH, function (fieldInvDetails, index) {
        let item = _this.formBuilder.group({
          id: fieldInvDetails.id,
          exRate: fieldInvDetails.exRate,
          poNumber: fieldInvDetails.poNumber,
          ID: _this.setInvoiceItems(fieldInvDetails, index)
        });
        _this.itemsArray = _this.invoiceForm.get('IMH') as FormArray;
        _this.itemsArray.push(item);
      });
    }
  }

   toDateObject(date){
    return this.CF.jsonDatetoDateObject(date)
    }
    toJsonDate(date){
      return this.CF.dateObjectToJsonDate(date)
    }

  setInvoiceItems(fieldInvDetails, indexIHM) {

    let _this = this;
    let arr = new FormArray([])
    //fieldInvDetails.ID.forEach(field => {
    _.forEach(fieldInvDetails.ID, function (field, index) {
      arr.push(_this.formBuilder.group({
        //arr.push(this.formBuilder.group({
        id: [field.id],
        groupXid: [field.groupXid],
        group: [field.group, Validators.required],
        description: [field.description, Validators.required],
        typeXid: [field.typeXid],
        type: [field.type],
        uomXid: [field.uomXid],
        uom: [field.uom, Validators.required],
        poQty: [field.poQty ? field.poQty.toFixed(2) : field.poQty, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
        poDiscount: [field.poDiscount ? field.poDiscount.toFixed(2) : 0, [Validators.required, Validators.pattern(/^(([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/)]],
        poPrice: [field.poPrice ? field.poPrice.toFixed(3) : field.poPrice, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
        poCost: [field.poCost ? field.poCost.toFixed(3) : field.poCost, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
        invQty: [field.invQty ? field.invQty.toFixed(2) : (field.poQty ? field.poQty.toFixed(2) : field.poQty), [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
        invPrice: [field.invPrice ? field.invPrice.toFixed(3) : (field.poPrice ? field.poPrice.toFixed(3) : field.poPrice), [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
        invDiscount: [field.invDiscount ? field.invDiscount.toFixed(2) : (field.poDiscount ? field.poDiscount.toFixed(2) : 0), [Validators.required, Validators.pattern(/^(([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/)]],
        invCost: [field.invCost ? field.invCost.toFixed(3) : (field.poCost ? field.poCost.toFixed(3) : field.poCost), [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
        status: field.status,
      }))

      let totalVal = (field.invCost ? field.invCost : field.poCost);
      _this.itemTotalsPageLoad[indexIHM + "-" + index] = parseFloat(totalVal);

    })
    return arr;
  }

  calculateCost(indIMH, index) {
    console.log("=======", indIMH)
    console.log("=======", index)
    var element = (<FormArray>this.invoiceForm.controls['IMH']).at(indIMH);
    var elementDt = (<FormArray>element.get('ID')).at(index);

    if ((<FormArray>elementDt).controls['invQty'].status == 'VALID' && (<FormArray>elementDt).controls['invPrice'].status == 'VALID' && (<FormArray>elementDt).controls['invDiscount'].status == 'VALID') {
      var qty = (<FormArray>elementDt).controls['invQty'].value;
      var price = (<FormArray>elementDt).controls['invPrice'].value;
      var dis = (<FormArray>elementDt).controls['invDiscount'].value;
      var cost = null;
      cost = qty * price;
      dis = dis / 100;
      var totalValue = cost - (cost * dis);
      this.itemTotals[indIMH + "-" + index] = totalValue;
      console.log("_form load itemTotals", this.itemTotals);
      (<FormArray>elementDt).controls['invCost'].setValue(totalValue.toFixed(3));
      this.calculateInvoiceTotal();
    }

  }

  showInvoiceTotal() {
    let _this = this;
    _this.itemTotals = _this.itemTotalsPageLoad;
    this.calculateInvoiceTotal();
  }

  calculateInvoiceTotal() {
    let _this = this;
    console.log("ttttttttt", _this.itemTotals)
    _this.itemTotal = 0;
    _.map(_this.itemTotals, function (item) {
      console.log("_this.itemTotal", item);
      _this.itemTotal = _this.itemTotal + item;
    });
    _this.invoiceDifference = (_this.itemTotal + _this.itemTCDTotal) - _this.invoicePOTotalCost;
    console.log(_this.itemTotal + "------" + _this.invoicePOTotalCost)
    _this.itemTotal = parseFloat(_this.itemTotal.toFixed(3));
    _this.invoiceDifference = parseFloat(_this.invoiceDifference.toFixed(3));

    
    _this.invAmountToUsd(_this.itemTotal);
  }

  invAmountToUsd(amount){
  /*  _this =this;
    var curencyObj = _.find(_this.currencies, { id: _this.purchaseForm.value.currencyXid });
      _this.currencySymbol = curencyObj ? curencyObj.symbol : ''
      console.log("slected rate",rate)
      if (rate) {
        var InUsd = _this.purchaseForm.value.poCost / rate.Rate;
        this.purchaseForm.get('amountUsd').setValue(InUsd.toFixed(3));
        this.purchaseForm.get('exRate').setValue(rate.Rate);
      } else {
        //if no exchange rate       
        this.purchaseForm.get('amountUsd').setValue(0);
        this.purchaseForm.get('exRate').setValue(0);
      }
      */
  }

  show(field) {
    this.el.nativeElement.querySelector('.' + field).classList.remove("hidden");
    this.el.nativeElement.querySelector('.' + field + '_id').classList.add("hidden");
  }

  close(field) {
    this.el.nativeElement.querySelector('.' + field).classList.add("hidden");
    this.el.nativeElement.querySelector('.' + field + '_id').classList.remove("hidden");
  }

  deleteItems(index) {
    this.itemsArray = this.invoiceForm.get('IMH') as FormArray;
    this.itemsArray.removeAt(index);
  }

  deleteItem(index) {
    console.log("index", index);
    this.savedItems.splice(index, 1);
  }

  convertDate(dateString) {
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMMYY(objectDate);
  }

  getPOList() {
    console.log("getPOList")
    //var params = { type: this.currentPOType, limit: this.itemsPerPage, offset: this.offset };
    //var params = {};
    let _this = this;
    _this.loader = true;
    let params = { model_name: 'poHeader', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response**", response);
      if (response && response.status == "success" && response.result) {
        _this.poDropList = response.result;
      } else {
        _this.poDropList = [];
      }
    });
  }

  getPOByType() {

    //this.currentPOType = this.sourceTypeVal;
    this.getPOList();
  }

  changeInvoiceDetailsByPO() {
    console.log(this.poDropVal);
    let _this = this;
    let po_propVal = this.poDropVal;
    let params = { model_name: 'invoiceHeader', where: { 'status': 1, 'poHeaderXid': this.poDropVal } };
    _this.CS.getData(params).subscribe(response => {
      console.log("itemTypes", response);
      if (response && response.status == "success" && response.result) {
        if (response.result.length > 0) {
          console.log("haveeeeeee", response.result[0].poHeaderXid);
          _this.router.navigate(['/invoice/invoice-generate/' + response.result[0].poHeaderXid]);
        } else {
          /// _this.router.navigate(['/pages/purchase/order-edit/' + po_propVal]);
        }
      } else {
    
           this.toastr.error('Failed to load invoice details!', 'Invalid!');
      }
    });

  }

  isInvSendDateLessthanPOdate(_this) {
    let last_PODate = _this.invoicePODetails.map(function (e) { 
      return e.poDate; }).sort().reverse()[0];
    let inv_sentDate = this.toDateObject(this.invoiceForm.get('invSentDate').value);
    if (inv_sentDate) {
      // remove time info from date time,return date only
      let last_PODateObj = new Date(last_PODate).toDateString(); 
      last_PODate = new Date(last_PODateObj);
      let inv_sentDateObj = new Date(inv_sentDate);
      if (inv_sentDateObj < last_PODate) {
        return true;
      }
      return false;
    }
    return false;
  }
  onSubmit(from) {

    let _this = this;
    this.submitted = true;

    // check invoice send date
    if (this.isInvSendDateLessthanPOdate(_this)) {
        this.toastr.error( 'Invoice sent date should greater than po generated date!','Error!');
        return;
    }

    console.log("this.invoiceForm", _this.invoiceForm);
    let formData = new FormData();
    var imageSize =[]
   var size;
    _.forEach(this.imageSrc, function (field) {
      imageSize.push(field.length)
      console.log(imageSize)
      size = imageSize.reduce((a, b) => a + b)/1000000
      console.log("size",size)
    })
    if(size > 10) {
      this.toastr.error('Total Allowed files is 10 Mb.', 'Invalid!');
      return
    }
    if (this.fileToUpload.length) {
      for (var i = 0; i < this.fileToUpload.length; i++) {
        formData.append("files", this.fileToUpload[i], uuidv4() + this.fileToUpload[i].name);
      }
    }

    console.log('FORM status===>', this.invoiceForm.controls['IMH'].value[0]);
    console.log("this.invoiceForm", this.invoiceForm);
    console.log("this._this.itemsArray", _this.itemsArray);


  //  this.invoiceDetails.invSentDate  = (this.toDateObject(params.paid_date) ); 
    if (this.invoiceForm.invalid) {
      this.toastr.error('Please validate fields!', 'Invalid!');

      return;
    }

    if (this.invoiceForm.controls['IMH'].status == 'VALID') {
      this.savedItems.push(this.invoiceForm.controls['IMH'].value[0]);

      //this.invoiceForm.controls.invSentDate.setValue(this.toDateObject(this.invoiceForm.get('invSentDate').value));
      _this.invoiceForm.value.invSentDate = this.toDateObject(this.invoiceForm.get('invSentDate').value)
      _this.CS.updateInvoiceDetails(_this.invoiceForm.value).subscribe(response => {
        
        if (response && response.status == "success") {

          if (this.fileToUpload.length) {
            formData.append("data", _this.invoice_id);
            _this.CS.saveInvoiceDocuments(formData).subscribe(response => {
              _this.loading = false;
              if (response && response.data && response.data.status == "success") {
                //  this.invoiceDetails.invStatus = 2;
                this.resetFileUpload();
                this.getInvoiceDetails();
                if (from == 1) {
                  this.toastr.success('Order details updated successfully!', 'Success!');

                  if (_this.invoiceDetails.invStatus == 0 || _this.invoiceDetails.invStatus == null) {
                    _this.invoiceDetails.invStatus = 1;
                    _this.allowInvoiceSend = true;
                    _this.router.navigate(['/invoice/invoice-generate/' + _this.invoice_id])
                  }
                }
                else {
                  
                  this.generatePdf(0,(from==2 ?'send_invoice':''));
                }
                // sri _this.router.navigate(['/pages/invoice/invoice-generate/' + _this.invoice_id]);
              } else {
                this.toastr.error('Something went wrong!', 'Error!');
                return;
              }
            });
            ////////////////////// Jun 2  _this.router.navigate(['/pages/invoice']);
            // *** stay in same page, _this.router.navigate(['/pages/purchase/order-list']);
          } else {
            _this.loading = false;
            //  this.invoiceDetails.invStatus = 2;
            if (from == 1) {
              this.getInvoiceDetails();
                this.toastr.success( 'Invoice details updated successfully!', 'Success!');
            }
            else {
              this.getInvoiceDetails();
              this.generatePdf(0,(from==2 ?'send_invoice':''));
            }
            
            if (_this.invoiceDetails.invStatus == 0 || _this.invoiceDetails.invStatus == null) {
              _this.invoiceDetails.invStatus = 1;
              _this.allowInvoiceSend = true;
              _this.router.navigate(['invoice/invoice-generate/' + _this.invoice_id])
            }
            // _this.router.navigate(['/pages/purchase/order-list']);
            //// _this.router.navigate(['/pages/invoice']);

            // *** stay in same page, _this.router.navigate(['/pages/purchase/order-list']);
          }
        _this.getInvoiceDetails();
        } else {
          this.toastr.error( 'Failed to update!', 'Failed!');
          return;
        }
      });

    }
  }
  preViewGeneratepdf() {
    this.generatePdf(1,'preview');
  }
  
  //preview =0 , send to Customer/ Vendor
  //preview = 1 , just to show pdf

  generatePdf(preview,genType='') {
    console.log("--------"+genType)
    if (preview == 0) {
      let prMsg ='';
      if (this.isInvSendDateLessthanPOdate(this)) {
  
            this.toastr.error( 'Invoice sent date should greater that po generated date!','Invalid!');

      return;
    }
      if(genType=="preview"){
        prMsg = "Make sure all changes saved before preview.";
      }else if(genType== "send_invoice"){
        if (this.invoiceForm.get('invSentDate').value != null) {
          prMsg = "Details updated,Are you sure want to send invoice?.";
        }else{
          prMsg = "Details updated,Please fill invoice date for send invoice.";
          this.toastr.warning( prMsg ,'Warning!');
          return false;
        }
        
      }else{
        prMsg = "Are you sure want to start invoice generation?";
      }
      
      if (!confirm(prMsg)) {
      return false;
      }
    }
    
    let _this = this
    _this.loading = true;

    // check invoice send date
    

    console.log("_this.invoiceDetails *-*", _this.invoiceDetails);
        let email =   (localStorage.getItem('mdata'));
        this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    _this.CS.generatePDFInvoice({ 'invoice_data': _this.invoiceDetails,'invoicePODetails':_this.invoicePODetails,itemTotal:this.itemTotal ,itemTCDTotal:this.itemTCDTotal,preview : preview, setterMail: email }).subscribe(response => {
      _this.loading = false;
      if (response && response.status == "success") {
        if (this.invoiceForm.get('invSentDate').value != null) {
          this.paymentAllow = true;
        }
        // this.invoiceForm.controls['invCost'].setValue(response.invoicePdf);
        console.log("openining", response);
        if (response.invStatus) {
          this.invoiceDetails.invStatus = response.invStatus;
        }
        window.open(this.UPLOAD_PATH + 'invoice/' + response.invoicePdf, '_blank');
        this.invoiceForm.controls['invoicePdf'].setValue(response.invoicePdf);
        this.invoiceDetails.invoicePdf = response.invoicePdf;
      

      } else {
      
      }
    });
  }
   
  triggerFileUpload() {
    this.el.nativeElement.querySelector("#fileUpload").click();
  }

  deleteDoc(index) {
    this.fileToUpload.splice(index, 1);
    this.imageSrc.splice(index, 1)
  }

  upload(files: File[]) {

    let _this = this;
    _.forEach(files, function (value) {
       console.log("reader", value)
       var fileTypes = ['jpg', 'jpeg', 'png', 'xlsx', 'txt', 'gif', 'pdf', 'xls'];  //acceptable file types
        var extension = value.name.split('.').pop().toLowerCase() 
        console.log(extension)
        var Success = fileTypes.indexOf(extension) > -1
       if(Success) {
      var reader = new FileReader();
      reader.readAsDataURL(value);
      reader.onload = () => {
        var src = reader.result as string;
        if (src.length < 4096000){
        _this.imageSrc.push(src);
        _this.fileToUpload.push(value);
      }
        else {
        alert('Maximum allowed file size is 4 MB!');
        return
      }
      };
    } else {
      alert('File type not supported')
    }
    });

    document.getElementById('fileUpload')['value'] = "";
  }

    deleteDocFromServer(doc, index) {
    console.log("doc", doc);
    var params = { id: doc.id, model_name: 'invoiceDocument' };
    let _this = this;
    _this.CS.removeData(params).subscribe(response => {
      if (response && response.status == "success" && response.result) {
        for (let doc in _this.invoiceDetails.IMH) {
        _this.invoiceDetails.IMH[doc].IDO.splice(index, 1);
      }
      } else {
        this.toastr.error('Something Went Wrong!', 'Failed!');
      }
    });
  }
  resetFileUpload(){
    let _this = this;
    _this.fileToUpload = [];
    _this.imageSrc =[];
  }
  getLabelColor(amount) {
    if (amount == 0) {
      return 'alert-primary';
    } else if (amount > 0) {
      return 'alert-danger';
    } else {
      return 'alert-success';
    }

  }

  selectAll(event) {
    let checked = false;
    if (event == true) {
      checked = true;
    } else {
      let inp = (event.target as HTMLInputElement);
      checked = inp.checked;
      console.log("checked", checked);
    }
    let _this = this;
    if (_this.invoiceDetails.IMH.length > 0) {
      if (checked) {
        _this.invoiceDetails.IMH.forEach(item => item.expanded = true);
        this.showPODetails(null,"show") 
      } else {
        _this.invoiceDetails.IMH.forEach(item => item.expanded = false);
        this.showPODetails(null,null) 
      }
    }

  }

  showPODetails(index=null,type=null) {
    let _this = this;
    if(index != null){
      //_this.invoicePODetails[index].show = !_this.invoicePODetails[index].show;
      _this.invoiceDetails.IMH[index].show = !_this.invoiceDetails.IMH[index].show;
    }else if(type=="show"){
        //_.map(_this.invoicePODetails, function (item) { item.show = true; });
        _.map(_this.invoiceDetails.IMH, function (item) { item.show = true; });
    }else{
      //_.map(_this.invoicePODetails, function (item) { item.show = false; });
      _.map(_this.invoiceDetails.IMH, function (item) { item.show = false; });
    }
  }

  showFloatVal(val,toFixNo){
    return (val && val != null) ? val.toFixed(toFixNo) :val;
  }
}
