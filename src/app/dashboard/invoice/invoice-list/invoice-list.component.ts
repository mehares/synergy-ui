import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { globalConstants } from '../../../constants/global-constants';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { NgbCalendar, NgbDateParserFormatter, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss']
})


export class InvoiceListComponent implements OnInit {
  message_content: string = '';
  mdlSampleIsOpen: any;
  modal: any;
  display = 'none';
  invSearchForm: FormGroup;
  searchBox = false;
  INV_STATUS = globalConstants.INV_STATUS
  invoice_no = "";
  customerList = [];
  invoiceList = [];
  cardIcon: string = 'toggle-left-outline';
  cardIcon_inactive: string = 'trash-2-outline';

  currentPage = 1;
  totalItems = 0;
  offset = 0;
  itemsPerPage = 10
  processing = true;

  customers = [];
  vendors = [];
  searchToggle = false;
  searchLoader = false;
  search = '';
  keywordVendor = 'vendorName';


  isValidFormSubmitted = null;
  submitted = false;
  sourceType = [{ id: 1, name: "From Customer" }, { id: 2, name: "To Vendor" }];
  sourceTypeVal = this.sourceType[0]['id'];
  currentPOType = this.sourceType[0]['id'];
  userQuestionUpdate = new Subject<string>();


  permissions = this.CF.findPermissions()

  loader = false;
  loaderItem = {};
  public innerHeight: any;
  UPLOAD_PATH = globalConstants.UPLOADS_DIR
  firstLoad = false
  pageOffset = 0
  customerName = '';
  vendorName = '';
  isSearchRequest=false;

  

  hoveredDateNew: NgbDate | null = null;
  fromDateNew: NgbDate | null;
  toDateNew: NgbDate | null;

  


  constructor(private route: ActivatedRoute,
    private CS: CommonservicesService,
    private CF: CommonFunctionsService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    
    private calendar: NgbCalendar, public formatter: NgbDateParserFormatter,private spinner: NgxSpinnerService,) {
      this.fromDateNew = calendar.getPrev(calendar.getToday(), 'd', 10 );
      this.toDateNew = calendar.getToday();

    this.mdlSampleIsOpen = false;
    this.modal = true;
    this.userQuestionUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.resetPagination();
        this.resetDateRange();
        this.onSubmit();
        if (this.search != '') {
          this.searchBox = true
        }
        else {
          this.searchBox = false
        }
      });
  }


  ngOnInit() {

    this.itemsPerPage = this.CS.getItemPerPage();
    this.getInvoiceList();
    this.getVendors();
    this.getCustomers();
    this.firstLoad = true;
    this.searchToggle = false;
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    this.invSearchForm = this.formBuilder.group({
      fromDate: ['', []],
      toDate: ['', []],
      source: [null, []],
      search: ['', []],
      customer: ['', []],
      vendor: ['', []],
      invNumber: ['', []],
      enqno: ['', []],
      vendorname: ['', []],
      customername : ['', []],
    });
  }
  preventChange(event) {
    //check if needed function
  }
  fixNext() {
    //set to date fill
  }
  clearSearch(type) {
    if (type =='reset') {
          this.searchToggle = true;
    }
    else {
          this.searchToggle = false;
              this.firstLoad = false;

    }
    this.resetPagination()
    this.loader = false
    this.searchBox = false;
    this.fromDateNew =null;
    this.toDateNew = null;
    this.customerName = null;
    this.vendorName = null;
    this.invSearchForm = this.formBuilder.group({
      fromDate: ['', []],
      toDate: ['', []],
      source: ['', []],
      search: ['', []],
      customer: ['', []],
      vendor: ['', []],
      invNumber: ['', []],
      enqno: ['', []],
      vendorname: ['', []],
      customername :['', []],
    });
    this.getInvoiceList();
    
  }
  getVendors() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'status': 1, approvedVendor: 1 ,  typeUser: 1    } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.vendors = response.result;
      } else {
        _this.vendors = [];
      }
    });
  }
  conertDate(dateString) {
    if (dateString == "" || dateString == null) {
      return "";
    }
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMONTHYY(objectDate);
  }

  getCustomers() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'status': 1, typeUser: 2 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.customers = response.result;
      } else {
        _this.customers = [];
      }
    });
  }

  // getCustomers() {
  //   let _this = this;
  //   let params = { model_name: 'vendor', where: {} };
  //   _this.CS.getVendorList(params).subscribe(response => {
  //     console.log("response", response);
  //     if (response && response.status == "success" && response.result) {
  //       _this.customerList = response.result;
  //       console.log("customers", response.result)
  //     } else {
  //       _this.customerList = [];
  //     }
  //   });
  // }
  getInvoiceList() {
    let _this = this;
    _this.loader = true;
    var params = { limit: this.itemsPerPage, offset: this.offset };
    this.pageOffset = this.offset;
    this.isSearchRequest=false;
    _this.CS.getInvoiceList(params).subscribe(response => {
      this.processing = false;

      console.log("getInvoiceList", response);
      if (response && response.status == "success" && response.result) {
        // console.log("InvoiceList", response.result)
        _this.invoiceList = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
      } else {
        this.toastr.error('Error occured while saving,Try again!', "Failed!");
      }
      _this.loader = false;

    });
  }

  pageChanged(event) {
    console.log("event", event);
    if (this.submitted == true) {
      this.currentPage = event;
      var setoffset = event - 1;
      setoffset = setoffset * this.itemsPerPage;
      this.offset = setoffset;
      this.invoiceList
    }
    else {
      this.currentPage = event;
      var setoffset = event - 1;
      setoffset = setoffset * this.itemsPerPage;
      this.offset = setoffset;
      this.getInvoiceList();
    }


  }

  preventNavClose(event) {
    console.log(event);
    // $(this).parents('.dropdown').find('button.dropdown-toggle').dropdown('toggle')
    return false;
  }
  get f() {
    return this.invSearchForm.controls;
  }
  onSubmit() {
    //this.processing = true;
    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;
    console.log("this.invSearchForm",this.invSearchForm)
    if (this.invSearchForm.invalid) {
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      this.searchLoader = false;
      return;
    }
     if(this.invSearchForm.value.fromDate || this.invSearchForm.value.toDate){
      console.log(true)
      if(this.invSearchForm.value.toDate ==null || this.invSearchForm.value.fromDate ==null) {
      this.toastr.error('Please Validate Date.', 'Invalid!');
      _this.searchLoader = false;
      console.log("FORM",this.invSearchForm)
      return;
    }
}
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    this.currentPage = 1;
    this.totalItems = 0;
    this.offset = 0;
    this.isValidFormSubmitted = true;
    this.sourceTypeVal = this.f.source.value;
    console.log('FORM', this.f.source.value);
    const poSeacrhObj = {
      //fromDate: this.toDateObject(this.f.fromDate.value),
      //toDate: this.toDateObject(this.f.toDate.value),
      fromDate: this.toDateObject(this.fromDateNew),
      toDate: this.toDateObject(this.toDateNew),
      source: this.f.source.value,
      vendor: this.f.vendor.value,
      customer: this.f.customer.value,
      invNumber: this.f.invNumber.value,
      search: this.search
    };
    this.resetPagination();   
    this.isSearchRequest=true;
    this.CS.searchInvoice(poSeacrhObj).subscribe(response => {
      console.log("response", response);
      _this.searchLoader = false;
      _this.searchToggle = false;
      _this.loader = false;
      if (response && response.status == 'success') {
        _this.invoiceList = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
        this.pageOffset = 0;
      } else {
        this.toastr.error('Error occured while search,Try again.', 'Failed!');
      }
      this.processing = false;
    });
  }
  changeSelection(vendorName, dataField) {
    if (dataField == 'customer') {
      this.customerName = vendorName ? vendorName : this.customerName;
      var customerObj = _.find(this.customers, { vendorName: this.customerName });
      this.invSearchForm.get('customer').setValue(customerObj.id);
    }
    if (dataField == 'vendor') {
      this.vendorName = vendorName ? vendorName : this.vendorName;
      var vendorObj = _.find(this.vendors, { vendorName: this.vendorName });
      this.invSearchForm.get('vendor').setValue(vendorObj.id);
    }
  }

  clear(to) {
    this.invSearchForm.get(to).setValue(null);
  }
  exportInvoice() {
    let _this = this;
    _this.CS.invoiceSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'invoice/' + response.filename, '_blank');
        this.toastr.success('Successfuly exported excel.', 'Success!');
      }
      else {
        this.toastr.error(response.msg, 'Failed!');
      }
    });
  }

  clearMainSearch(type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.clearSearch(type);
      this.searchToggle = true
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }

 resetPagination(){    
    this.offset=0;
    this.currentPage=1;
  }

  toDateObject(date){
    return this.CF.jsonDatetoDateObject(date)
  }

  
  onDateSelection(date: NgbDate) {
    if (!this.fromDateNew && !this.toDateNew) {
      this.fromDateNew = date;
    } else if (this.fromDateNew && !this.toDateNew && date && date.after(this.fromDateNew)) {
      this.toDateNew = date;
    } else {
      this.toDateNew = null;
      this.fromDateNew = date;
    }

    console.log("fromDateNew",this.fromDateNew)
    console.log("toDateNew",this.toDateNew)
  }

  isHovered(date: NgbDate) {
    return this.fromDateNew && !this.toDateNew && this.hoveredDateNew && date.after(this.fromDateNew) && date.before(this.hoveredDateNew);
  }

  isInside(date: NgbDate) {
    return this.toDateNew && date.after(this.fromDateNew) && date.before(this.toDateNew);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDateNew) || (this.toDateNew && date.equals(this.toDateNew)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  resetDateRange(){
    this.fromDateNew = null;
    this.toDateNew =null;
  }

     onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container') || e.path[5].className.includes('ng-select ng-select') || e.path[6].className.includes('dropdown-menu show')){
        console.log('wow--->closeee----')
      }
      else {
    this.firstLoad = false
    this.searchToggle = false
  }
  }

}

