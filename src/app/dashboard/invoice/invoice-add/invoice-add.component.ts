import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { group } from '@angular/animations';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { ToastrService } from 'ngx-toastr';

// import {globalConstants} from '../../../constants/global-constants';
// import { Observable, Subject } from 'rxjs';
// import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import * as _ from 'lodash';
import { NgbDate, NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { globalConstants } from '../../../constants/global-constants';
@Component({
  selector: 'app-invoice-add',
  templateUrl: './invoice-add.component.html',
  styleUrls: ['./invoice-add.component.scss']
})

export class InvoiceAddComponent implements OnInit {

  loading = false;
  isValidFormSubmitted = null;
  submitted = false;

  invoiceAddForm: FormGroup;
  poDropVal = null;
  loader = {};
  poDropList = [];
  customers = [];
  vendors = [];
  poSerachList = [];
  selectedPO = [];
  poTotal = 0;
  poCategories = [{ id: 1, name: "From Customer" }, { id: 2, name: "To Vendor" }];

  keywordVendor = 'vendorName';
  keywordCustomer = 'vendorName';
  hoveredDateNew: NgbDate | null = null;
  fromDateNew: NgbDate | null;
  toDateNew: NgbDate | null;
  toDate : NgbDate | null = null;
  isAllChecked=false;
  fromDate : NgbDate | null = null;  
  PO_STATUS = globalConstants.PO_STATUS;
  
  constructor(private router: Router,
    private el: ElementRef,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private CS: CommonservicesService,
    private CF: CommonFunctionsService,
    private toastr: ToastrService,
    private calendar: NgbCalendar, public formatter: NgbDateParserFormatter,private spinner: NgxSpinnerService,) {
    this.fromDateNew = calendar.getPrev(calendar.getToday(), 'd', 10);
    this.toDateNew = calendar.getToday();
  }

  ngOnInit() {
    this.getPOList();

    this.getCustomers();
    this.getVendors();
    this.createForm();
  }

  onSubmit() {
  }

  get form() {
    return this.invoiceAddForm.controls;
  }

  createForm() {

    this.invoiceAddForm = this.formBuilder.group({
      customerXid: ['',],
      vendorXid: ['',],
      poCategory: [null, Validators.required],
      fromToDate: ['', Validators.required]
    });
  }

  getCustomers() {
    let _this = this;

    let params = { model_name: 'vendors', where: { 'status': 1, typeUser: 2 } };
    _this.CS.getCustomerList(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.customers = response.result;
      } else {
        _this.customers = [];
      }
    });
  }


  getVendors() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'status': 1, approvedVendor: 1 ,  typeUser: 1    } };
    _this.CS.getVendorList(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.vendors = response.result;
      } else {
        _this.vendors = [];
      }
    });
  }




  getPOList() {
    console.log("getPOList")
    //var params = { type: this.currentPOType, limit: this.itemsPerPage, offset: this.offset };
    //var params = {};
    let _this = this;
    _this.loader = true;
    let params = { model_name: 'poHeader', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response**", response);
      if (response && response.status == "success" && response.result) {
        _this.poDropList = response.result;
      } else {
        _this.poDropList = [];
      }
    });
  }

  conertDate(dateString) {
    if (dateString == "") {
      return "";
    }
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMMYY(objectDate);
  }

  selectPO(event: any, index, poItem) {
    this.isAllChecked = false;
    let _this = this;
    if (event.target.checked) {
      _this.poSerachList[index].selected = true;
    } else {
      _this.poSerachList[index].selected = false;
    }
    this.calculatePOTotal();
  }
  toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }

  selectAll(event: any) {
    this.isAllChecked = true;
    let _this = this;
    //console.log("_this.poSerachList",_this.poSerachList)
    if (event.target.checked) {
      _this.poSerachList.forEach(item => item.selected = true);
    } else {
      _this.poSerachList.forEach(item => item.selected = false);
    }

    console.log("_this.poSerachList", _this.poSerachList)
    this.calculatePOTotal();
  }

  calculatePOTotal() {
    console.log("calculateTcdAmount");
    let _this = this;
    _this.poTotal = 0;
    _this.selectedPO = [];
    _.map(_this.poSerachList, function (item) {
      if (item && item.selected) {
        _this.selectedPO.push(item);
        console.log("====", item)
        _this.poTotal = _this.poTotal + item.poCost;
      }
    });

    console.log("-------selected POS----", _this.selectedPO)
  }

  searchCustomerPO() {
    let _this = this;
    this.submitted = true;
    //let frmToDt = null;
    //if(this.fromDateNew != null && this.toDateNew != null){      
    let frmToDt = {
      "start": this.toDateObject(this.fromDateNew),
      "end": this.toDateObject(this.toDateNew)
    }
    //}
    this.invoiceAddForm.get('fromToDate').setValue(frmToDt);

    console.log("this.invoiceAddForm", this.invoiceAddForm)

    if (this.invoiceAddForm.invalid) {

      this.toastr.error('Please Add Valid Data', "Error");
      return;
    } else {
      _this.CS.searchCustomerPO(_this.invoiceAddForm.value).subscribe(response => {
        if (response && response.status == "success") {

          if (response.result.length > 0) {
            this.poSerachList = response.result;
            _.map(this.poSerachList, function (item) {
              item.selected = false;
            });

          } else {
            this.toastr.error('No PO Availabl To This Search', "No PO Found");
          }

        } else {
          if (response && response.status == "failed" && response.error == "Please Fill From And To Date.") {
            this.toastr.error(response.error, "Failed");
          } else {
            this.toastr.error('Failed To Process', "Failed");
          }
          return;
        }
      });
    }


  }
  changeInvoiceDetailsByPO() {
    console.log(this.poDropVal);
    let _this = this;
    let po_propVal = this.poDropVal;
    let params = { model_name: 'invoiceHeader', where: { 'status': 1, 'poHeaderXid': this.poDropVal } };
    _this.CS.getData(params).subscribe(response => {
      console.log("itemTypes", response);
      if (response && response.status == "success" && response.result) {
        if (response.result.length > 0) {
          this.toastr.info('Invoice Already Generated!', "Redirecting..");
          _this.router.navigate(['/invoice/invoice-generate/' + response.result[0].poHeaderXid]);
        } else {
          _this.router.navigate(['/pages/purchase/order-edit/' + po_propVal]);
        }
      } else {
        this.toastr.error('Failed To Load Invoice Details,Try Again!', "Failed!");
      }
    });

  }


  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, { id: parseInt(getId, 10) });
      console.log("item**", item);
      if (item) {
        this.invoiceAddForm.get(to).setValue(item[dataField]);
        if (typeof event == 'object') {
          this.invoiceAddForm.get(from).setValue(getId);
        }
      }
    }
  }

  clearSelection(to) {
    this.invoiceAddForm.get(to).setValue(null);
  }

  generateNewInvoice(params) {
    console.log("generate", params);
    let _this = this;
    let poIds = [];
    let custIds = [];

    _.map(_this.selectedPO, function (item) {
      poIds.push(item.id);
      // select unique customerids
      if (custIds.indexOf(item.customerXid) === -1) {
        custIds.push(item.customerXid);
      }
    });

    if (custIds.length == 1) {
      let poDetails = {
        customerId: custIds[0],
        poIds: poIds,
        data: params
      }
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
      _this.CS.generateNewInvoice(poDetails).subscribe(response => {
        console.log("itemTypes", response);
        if (response && response.status == "success" && response.result) {
          this.toastr.success('Invoice generated successfully!', "Success");

          _this.router.navigate(['/invoice/invoice-generate/' + response.result.id]);

        } else {
          let msg = "Failed To Generate Invoice!";
          if (response && response.message) {
            msg = response.message;
          }
          this.toastr.error(msg, "Error");
        }
      });


    } else {
      this.toastr.error("Can't Generate, Selected Purchase Order Are From Different Customers!", "Error");
    }
  }
  resetCustomerValue(){
    this.invoiceAddForm.get('customerXid').setValue(null);
  }
  onDateSelection(date: NgbDate) {
    if (!this.fromDateNew && !this.toDateNew) {
      this.fromDateNew = date;
    } else if (this.fromDateNew && !this.toDateNew && date && date.after(this.fromDateNew)) {
      this.toDateNew = date;
    } else {
      this.toDateNew = null;
      this.fromDateNew = date;
    }

    console.log("fromDateNew", this.fromDateNew)
    console.log("toDateNew", this.toDateNew)
  }

  isHovered(date: NgbDate) {
    return this.fromDateNew && !this.toDateNew && this.hoveredDateNew && date.after(this.fromDateNew) && date.before(this.hoveredDateNew);
  }

  isInside(date: NgbDate) {
    return this.toDateNew && date.after(this.fromDateNew) && date.before(this.toDateNew);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDateNew) || (this.toDateNew && date.equals(this.toDateNew)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  

}
