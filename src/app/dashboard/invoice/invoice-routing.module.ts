import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 
import { InvoiceAddComponent } from './invoice-add/invoice-add.component';
import { InvoiceGenerateComponent } from './invoice-generate/invoice-generate.component';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { PageGuard } from '../../guards/page-guard.service';

const routes: Routes = [
  {
    path: 'list',
    canActivate: [PageGuard],
    component: InvoiceListComponent,
    data:{permissions: 'invoice_view' }
  },
    {
    path: 'add',
    canActivate: [PageGuard],
    component: InvoiceAddComponent,
    data:{permissions: 'invoice_add' }
  },
    {
    path: 'invoice-generate/:id',
    canActivate: [PageGuard],
    component: InvoiceGenerateComponent,
    data:{permissions: 'invoice_edit' }

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceRoutingModule { }
