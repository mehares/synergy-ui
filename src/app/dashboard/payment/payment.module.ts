 
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {PaymentRoutingModule } from './payment-routing.module';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PaymentGenerateComponent } from './payment-generate/payment-generate.component';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [ PaymentGenerateComponent],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    AngularFontAwesomeModule,
    FormsModule, 
    ReactiveFormsModule,
    NgbModule,
    NgxSpinnerModule
  ]
  
})
export class PaymentModule { }
