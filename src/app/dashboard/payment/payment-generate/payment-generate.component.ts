import { Component, OnInit, ElementRef, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import * as _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { globalConstants } from '../../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-payment-generate',
  templateUrl: './payment-generate.component.html',
  styleUrls: ['./payment-generate.component.scss']
})


export class PaymentGenerateComponent implements OnInit {
  invoice_id = null;
  payForm: FormGroup;
  loading:boolean;
  isValidFormSubmitted = null;
  submitted = false;
  paymentData = null;
  dateValid = true;
  invoiceDetails = null;
  invoicePODetails = null;
  UPLOAD_PATH = globalConstants.UPLOADS_DIR;
  permissions = this.CF.findPermissions()
 

  constructor(private router: Router,
    private el: ElementRef,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private CS: CommonservicesService, private CF: CommonFunctionsService ,  private toastr: ToastrService,private spinner: NgxSpinnerService,) {

  }

  ngOnInit() {
    let _this = this;
    _this.invoice_id = this.route.snapshot.paramMap.get('id');
    console.log(_this.invoice_id);
     this.getPaymentData();
    
    this.createForm();
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);

    
  }
  createForm() {
  this.payForm = this.formBuilder.group({
        id: [''],
        amount_paid: ['', Validators.required],
        paid_date: ['',Validators.required],
        payment_details: [''],
        payment_ref_no: ['', Validators.required],
        invoice_id: [this.invoice_id, Validators.required],
        poCategory:[""],
        enqXid:[""]
  });
  }

  get form() {
    return this.payForm.controls;
  }
  
  
  onSubmit() {
    this.submitted = true;
   let _this = this;
    console.log("payForm", this.payForm);
    //check if date is okay
    if (!_this.dateValid) {
     this.toastr.error( 'Please generate the invoice,and add payment', 'Error!');
        return;
    }
    
    if (this.payForm.invalid) {
      this.toastr.error( 'Fill the details', 'Error!');
      return;
    } else {
      _this.loading = true;
      let params = this.payForm.value;
     params.paid_date = (this.toDateObject(params.paid_date) ); 
    this.spinner.show();
    //_this.invoiceDetails.poCategory
    params.poCategory = _this.invoiceDetails.poCategory;
    var enqXidPO = []
     _.forEach(_this.invoicePODetails, function (row, index) {
       enqXidPO.push(row.enqXid)
     })
     params.enqXid = enqXidPO
    setTimeout(() => this.spinner.hide(), 2000);
      _this.CS.setPaymentData(params).subscribe(response => {
        console.log("params", params);
        _this.loading = false;
        if (response && response.status == "success") {
          this.getPaymentData(true);
          this.toastr.success( 'Payment details added successfully!', 'Success!');
        } else {
          this.toastr.error( 'Failed to Add Payment', 'Success!');
        }
      });
    }
  }
  
  toDateObject(date){
    return this.CF.jsonDatetoDateObject(date)
    }
    toJsonDate(date){
      return this.CF.dateObjectToJsonDate(date)
    }
  
  viewPdf() {
    this.toastr.success('Opening Invoice PDF', 'Success!');
    window.open(this.UPLOAD_PATH + 'invoice/' + this.invoiceDetails.invoicePdf, '_blank');
  }
  
  getPaymentData(isSecondLoad=false) {
    let _this = this;
    let params = { model_name: 'payment', where: { 'invoice_id': this.invoice_id }, invoice_id: this.invoice_id };
    this.getInvoiceDetails(this.invoice_id);
    _this.CS.getPaymentData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.paymentData = response.result;
        let paid_date = null;
        if (_this.paymentData.status == 2) {
          let payment_msg = (isSecondLoad) ? 'Payment closed!' : 'Payment Already Closed';
          _this.toastr.info('', payment_msg); 
        }
        
        if (_this.paymentData.paid_date) {
          paid_date = this.toJsonDate(_this.paymentData.paid_date);
         }
        console.log("_this.paid_date", paid_date);
        this.payForm = this.formBuilder.group({
        id: [_this.paymentData.id],
        amount_paid: [_this.paymentData.amount_paid],
        paid_date: [paid_date ],
        payment_details: [_this.paymentData.payment_details],
        payment_ref_no: [_this.paymentData.payment_ref_no ],
        invoice_id :  [_this.paymentData.invoice_id],
        poCategory: []
  });
      }
      else {
        _this.toastr.info( 'New Payment', 'Info!');
      }
    });
  }

getInvoiceDetails(invoice_id){
    // get invoice details
  let _this =this;
  _this.CS.getInvoiceDetails({ id: invoice_id }).subscribe(response => {
    if (response && response.result) {
      
      _this.invoicePODetails = response.result.PO;
      _this.invoiceDetails = response.result.invoice;

      // Attach po details to invoice array
      _.map(_this.invoiceDetails.IMH, function (item) {
        let poInfoData = _.filter(_this.invoicePODetails, function(o) { return o.id == item.poHeaderXid; });
        
        console.log("poInfo",poInfoData)
        console.log("poInfo ll",poInfoData.length)
        if(poInfoData.length > 0){
          let poInfo = poInfoData[0];
          item.poDate = poInfo.poDate;
          item.poNumber = poInfo.poNumber;
          item.port = poInfo.port;
          item.vesselName = poInfo.vesselName;
          item.vendorName = poInfo.vendorName;
          item.customerName = poInfo.customerName;
          item.advanceAmountUsd = poInfo.advanceAmountUsd;
          item.poCost = poInfo.poCost;
        }
      });

      this.selectAll(true);
      this.showPODetails(null,"show");
      // calculate total
      

      console.log("**_this.invoicePODetails",_this.invoicePODetails);
      console.log("**_this.invoiceDetails",_this.invoiceDetails);
      console.log("invSentDate",_this.invoiceDetails.invSentDate);

    } else {
      if (response.result == null) {
     /*   _this.toastrService.show(
          'Error',
          'There is no such invoice exist.',
          { position: _this.position, status: 'danger' }); */
        _this.router.navigate(['/pages/invoice']);
      } else {
      /*  _this.toastrService.show(
          'Error',
          'Error occured while loading,Try again.',
          { position: _this.position, status: 'danger' }); */
      }

    }
  });

}

convertDate(dateString) {
  let _this = this;
  let objectDate = new Date(dateString);
  //console.log("objectDate",objectDate);
  console.log("dateString",dateString)
  if(dateString == null){
  objectDate == null
  }
  else {
  return _this.CF.convertDateObjectToDDMMYY(objectDate);
}
}
  
  selectAll(event) {
    
    let checked = false;
    console.log(event);
    if (event == true) {
      checked = true;
    } else {
      let inp = (event.target as HTMLInputElement);
      checked = inp.checked;
      console.log("checked", checked);
    }
    let _this = this;
    if (_this.invoiceDetails.IMH.length > 0) {
      if (checked) {
        _this.invoiceDetails.IMH.forEach(item => item.expanded = true);
        this.showPODetails(null,"show") 
      } else {
        _this.invoiceDetails.IMH.forEach(item => item.expanded = false);
        this.showPODetails(null,null) 
      }
    }

  }
  
  
  showPODetails(index=null,type=null) {
    let _this = this;
    if(index != null){
      _this.invoiceDetails.IMH[index].show = !_this.invoiceDetails.IMH[index].show;
    }else if(type=="show"){
        _.map(_this.invoiceDetails.IMH, function (item) { item.show = true; });
    }else{
      _.map(_this.invoiceDetails.IMH, function (item) { item.show = false; });
    }
  }

}
