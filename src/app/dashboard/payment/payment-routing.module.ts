import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 
import { PaymentGenerateComponent } from './payment-generate/payment-generate.component';


const routes: Routes = [
  {
    path: 'generate/:id',
    component: PaymentGenerateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
