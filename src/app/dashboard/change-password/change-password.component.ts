import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators ,FormControl} from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {CommonservicesService} from '../../helper/commonservices/commonservices.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  selectedId=0;
  changePasswordForm: FormGroup;
  message_content: string = '';
  submitted: boolean = false;
  checkFirstLogin = false;
 

  constructor(private formBuilder: FormBuilder, private router: Router,private http: HttpClient ,private route: ActivatedRoute ,
    private CS: CommonservicesService,private toastr: ToastrService,private spinner: NgxSpinnerService,) { }

  
   ngOnInit() {
     this.checkFirstLogin = JSON.parse(localStorage.getItem('checkFirstLogin'));
     localStorage.setItem('checkFirstLogin','false');


    this.changePasswordForm = this.formBuilder.group({
      c_password: ['', [Validators.required]],
      new_password: ['', [Validators.required,Validators.pattern('(?=.*[a-z])(?=.*)(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')]],
      conf_new_password: ['', [Validators.required,Validators.pattern('(?=.*[a-z])(?=.*)(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')]], 
    });
  }
   
  get f() { return this.changePasswordForm.controls; }
   
  onSubmit() {
    // this.message_content ='';
    this.submitted = true;
    let _this = this;
    
    // stop here if form is invalid
    console.log('Form Status', this.changePasswordForm.invalid);
    if (this.changePasswordForm.invalid) {
      // this.message_content="Please fill the details";
      this.toastr.error('Please Fill The Details', 'Error!');
      return;
    }
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    //this.message_content="Server Error ,Failed to Update your password";
    var userCurrent =JSON.parse (localStorage.getItem('sender_pro_id'));
    const pObj = {
      userCurrent :userCurrent ,
      c_password:this.f.c_password.value ,
      new_password: this.f.new_password.value,
      conf_new_password:this.f.conf_new_password.value,
    };  
    if(pObj.conf_new_password!=pObj.new_password  ){
      // this.message_content = "New and Confirm password doesn't match";
      this.toastr.error('New and Confirm Password doesnt match', 'Error!');
      return ;
    }
    
    if(pObj.c_password==pObj.new_password  ){
      // this.message_content = "Current password and New passwords are same";
      this.toastr.error('Current Password and New Password are same', 'Error!');
      return ;
    }

     this.CS.changePassword(pObj).subscribe(response=> {
        console.log("response from node",response);
        if(response.status ){
          // this.message_content = "Password Updated";
          this.toastr.success('Password Updated', 'Success!');
      return ;
        }else{
          // this.message_content = "Failed to Update your password";
          this.toastr.error('Failed to Update your Password', 'Failed!');
        } 
             
      });
  }

}