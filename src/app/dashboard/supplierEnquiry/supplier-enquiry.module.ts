import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SupplierEnquiryRoutingModule } from './supplier-enquiry-routing.module';
import { SupplierEnquiryComponent } from './supplier-enquiry.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [SupplierEnquiryComponent],
  imports: [
    CommonModule,
    SupplierEnquiryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    NgbModule,
    NgxSpinnerModule,
    NgSelectModule
    
  ]
})
export class SupplierEnquiryModule { }
