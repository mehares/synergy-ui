import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupplierEnquiryComponent } from './supplier-enquiry.component';

const routes: Routes = [
	{
		path: ':id/:vendorId',
		component: SupplierEnquiryComponent
	}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupplierEnquiryRoutingModule { }
