import { Component, OnInit, ElementRef } from '@angular/core';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonservicesService } from './../../helper/commonservices/commonservices.service';
import * as _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { NgxSpinnerService } from "ngx-spinner";
import { CommonFunctionsService } from './../../helper/commonFunctions/common-functions.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { globalConstants } from './../../constants/global-constants';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-supplier-enquiry',
  templateUrl: './supplier-enquiry.component.html',
  styleUrls: ['./supplier-enquiry.component.scss']
})
export class SupplierEnquiryComponent implements OnInit {

  enquirySupplierForm: FormGroup;
  itemsArray: FormArray;
  itemsTcdArray: FormArray;
  customers = [];
  keywordVendor = 'vendorName';
  itemGroups = [];
  itemTypes = [];
  uoms = [];
  vendors = [];
  tcdTypes = [];
  fileToUpload: File[] = [];
  imageSrc = [];
  keywordUom = 'uomName';
  keywordGroup = 'groupName';
  loading = false;
  submitted = false;
  tcdSubmitted = false;
  enqXid: any
  enquiryDetails = null
  quotationDetails = null;
  enqNumber = null
  dateArray = null
  loader = false
  itemTotals = {};
  itemTotal = 0;
  itemsError = null;
  items = true
  vendorId: any;
  vendorIdDetails = null
  exchangeRate = [];
  currencies = [];
  currencySymbol = ''
  currencyShortKey = ''
  currencyDetails = null
  urlParams = null;
  vendor_itemTotals = {};
  vendor_itemTotal: any = 0;
  vend_tcdTotal = 0;
  vend_tcdDiscount = 0;
  tcdError = null;
  ENQ_STATUS = globalConstants.ENQ_STATUS
  FPtypes = globalConstants.FP;
  PayTerms = globalConstants.ENQPayTerms;
  PayModes = globalConstants.ENQPayModes;
  IncoTerms = globalConstants.ENQIncoTerms;
  keywordCurrency = 'name';
  openActiveIds = ['toggle-1', 'toggle-2', 'toggle-3', 'toggle-4'];
  UPLOAD_PATH = globalConstants.ENQ_DOC_PATH;
  custCurrency = null
  itemsDocsArray: FormArray;


  constructor(private formBuilder: FormBuilder, private CS: CommonservicesService, private el: ElementRef, private spinner: NgxSpinnerService, private CF: CommonFunctionsService, private toastr: ToastrService, private router: Router, private activatedroute: ActivatedRoute) {
    this.urlParams = this.activatedroute.snapshot.params;

  }

  ngOnInit() {
    this.createForm();
    this.getEnquiryDetails();
    this.items = true
    this.getItemGroups();
    this.getTypes();
    this.getUoms();
    this.getCurrencies();
    this.getExchangeRate();
    this.getTcdTypes();
  }


  getEnquiryDetails() {
    let _this = this;
    console.log("_this.urlParams", _this.urlParams)
    let params = _this.urlParams;
    _this.CS.getSupplierEnquiryDetails(params).subscribe(response => {
      console.log("enquiryDetails", response);
      if (response && response.status == "success" && response.result) {
        _this.enquiryDetails = response.result;
        _this.enqXid = _this.enquiryDetails.id;
        _this.currencyDetails = response.currency
        _this.quotationDetails = response.quotationData
        _this.custCurrency = response.custCurrency
        _this.setForm();

        if (response.result.EV && response.result.EV.length > 0) {
          _this.vendorIdDetails = response.result.EV;

          //_this.vendorId = _this.vendorIdDetails
          _this.setVendors();
        } else {
          _this.vendorIdDetails = {};
        }


      } else {
        _this.enquiryDetails = {};
      }
    });
  }

  setForm() {
    this.enquirySupplierForm = this.formBuilder.group({
      enqXid: [this.enquiryDetails.id],
      enqDate: [this.enquiryDetails.enqDate],
      customerXid: [this.enquiryDetails.customerXid],
      customerName: [this.enquiryDetails.customerName],
      customerRefName: [this.enquiryDetails.customerRefName],
      customerRefNo: [this.enquiryDetails.customerRefNo],
      vesselXid: [this.enquiryDetails.vesselXid],
      vesselName: [this.enquiryDetails.vesselName],
      status: [this.enquiryDetails.status],
      enqNumber: [this.enquiryDetails.enqNumber],
      details: this.formBuilder.array([]),
      vendorDetails: this.formBuilder.array([]),
      tcdDetails: this.formBuilder.array([]),
      quotationDate: [new Date],
      fromVendorCost: [0, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      fromVendorCostWithTCD: [0, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      quotationFromLink: [1, Validators.required],
      quotationCategory: [1, Validators.required],
      vendCurrencyXid: [this.currencyDetails.id, Validators.required],
      vendExRate: [this.currencyDetails.ER[0].Rate, Validators.required],
      vendCurrency: [this.currencyDetails.name, Validators.required],
      currencyNameAuto: [this.currencyDetails.name, Validators.required],
      vendAmountUsd: 0,
      deliveryDate: ['', Validators.required],
      payTermId: [null],
      payTerm: ['',Validators.required],
      payModeId: [null],
      payMode: ['',Validators.required],
      incoTermId: [null],
      incoPlace: ['',Validators.required],
      incoTerm: ['',Validators.required],
      remarks: ['', Validators.required],
      custCurrencyXid:[this.custCurrency.id],
      custCurrency: [this.custCurrency.name],
      custExRate:[this.custCurrency.ER[0].Rate],
      documents: this.formBuilder.array([])
    });
    this.setItems();
    this.setVendors();
    this.setTcdItems();
    this.setDocuments();
    console.log("FORM", this.enquirySupplierForm);
  }

  get form() {
    return this.enquirySupplierForm.controls;
  }

  setItems() {
    let _this = this;
    if (_this.enquiryDetails.EDE && _this.enquiryDetails.EDE.length) {
      _.forEach(_this.enquiryDetails.EDE, function (field, index) {
        let item = _this.formBuilder.group({
          groupXid: [field.groupXid],
          group: [field.group, Validators.required],
          description: [field.description, Validators.required],
          typeXid: [field.typeXid],
          type: [field.type],
          uomXid: [field.uomXid],
          uom: [field.uom, Validators.required],
          qtQty: [field.qtQty ? field.qtQty.toFixed(2) : field.qtQty, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
          venQtUnitPrice: [field.poPrice ? field.poPrice.toFixed(3) : field.poPrice, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          vendQtCost: [field.poCost ? field.poCost.toFixed(3) : field.poCost, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          status: field.status,
          groupNameAuto: [field.group],
          uomNameAuto: [field.uom],
          enqXid: [_this.enqXid],
        });
        _this.itemsArray = _this.enquirySupplierForm.get('details') as FormArray;
        _this.itemsArray.push(item);
        _this.itemTotals[index] = ((field.vendQtCost && field.vendQtCost > 0) ? field.vendQtCost : 0);
        _this.calculateCost(index);
        console.log("item", item);
      });
    } else {
      this.createItems();
    }
  }

  createItems() {
    let item = this.formBuilder.group({
      id: null,
      groupXid: [null],
      group: ['', Validators.required],
      description: ['', Validators.required],
      typeXid: [null],
      type: ['', Validators.required],
      uomXid: [null],
      uom: ['', Validators.required],
      qtQty: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
      venQtUnitPrice: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      vendQtCost: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      status: 1,
      enqXid: this.enqXid,
      groupNameAuto: [''],
      uomNameAuto: [''],

    });
    this.itemsArray = this.enquirySupplierForm.get('details') as FormArray;
    this.itemsArray.push(item);
  }
  createTcdItems(flag?) {

    console.log("this.quotationForm.controls.tcdDetails");
    if (flag) {
      if (this.enquirySupplierForm.controls.tcdDetails.status == 'INVALID') {
        this.tcdSubmitted = true;
        return false;
      }
    }
    let item = this.formBuilder.group({
      tcdType: [null, Validators.required],
      tcdName: [null],
      tcdDesc: [null],
      flatOrPercentage: [null, Validators.required],
      amount: [null, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      tcdAmount: [null, [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      status: 1,

    });
    this.itemsTcdArray = this.enquirySupplierForm.get('tcdDetails') as FormArray;
    this.itemsTcdArray.push(item);
  }

  createForm() {
    this.enquirySupplierForm = this.formBuilder.group({
      details: this.formBuilder.array([]),
      vendorDetails: this.formBuilder.array([]),
      tcdDetails: this.formBuilder.array([]),
      quotationDate: [''],
      fromVendorCost: [0, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      fromVendorCostWithTCD: [0, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      quotationFromLink: [1, Validators.required],
      quotationCategory: [1, Validators.required],
      vendAmountUsd: [''],
      currencyNameAuto: [''],
      vendCurrencyXid: [''],
      vendExRate: [''],
      vendCurrency: [''],
      deliveryDate: ['', Validators.required],
      payTermId: [null],
      payTerm: ['',Validators.required],
      payModeId: [''],
      payMode: ['',Validators.required],
      incoTermId: [''],
      incoPlace: ['',Validators.required],
      incoTerm: ['',Validators.required],
      remarks: ['', Validators.required],
      documents: this.formBuilder.array([])

    });
    this.createItems();
    this.createTcdItems();
    this.createDocuments();

  }

  setVendors() {
    let _this = this;
    console.log("this.vendorDetails", this.vendorIdDetails);
    //if (_this.vendorIdDetails && _this.vendorIdDetails.length) {
    _.forEach(_this.vendorIdDetails, function (field, index) {

      let item = _this.formBuilder.group({
        date: [field.date, Validators.required],
        details: [field.details, Validators.required],
        vendorXid: [field.vendorXid, Validators.required],
        vendorName: [field.vendorName, Validators.required],
        vendorNameAuto: [field.vendorName, Validators.required],
        status: [field.status],
        enqXid: [field.enqXid],
        selectVendor: [field.selectVendor]

      });
      _this.itemsArray = _this.enquirySupplierForm.get('vendorDetails') as FormArray;
      _this.itemsArray.push(item);
    });
    //} else {
    //this.createRequests();
    //}
  }
  setTcdItems() {
    let _this = this;
    this.createTcdItems();
  }
  createRequests() {

    let item = this.formBuilder.group({
      date: ['', Validators.required],
      details: ['', Validators.required],
      vendorXid: ['', Validators.required],
      vendorName: ['', Validators.required],
      status: 1,
      enqXid: [''],
      vendorNameAuto: [''],
      selectVendor: ['', Validators.required]

    })
    this.itemsArray = this.enquirySupplierForm.get('vendorDetails') as FormArray;
    this.itemsArray.push(item);
  }
  deleteItems(index) {
    this.itemsArray = this.enquirySupplierForm.get('details') as FormArray;
    this.itemsArray.removeAt(index);
    this.resetAndReCalculate();
  }


  deleteTcdItems(index) {
    this.itemsTcdArray = this.enquirySupplierForm.get('tcdDetails') as FormArray;
    this.itemsTcdArray.removeAt(index);
    this.resetAndReCalculate();
  }
  validateTcdValue(index) {
    this.itemsTcdArray = this.enquirySupplierForm.get('tcdDetails') as FormArray;
    console.log(this.itemsTcdArray)
    console.log(this.itemsTcdArray.controls[index].get('flatOrPercentage').value)
    console.log(this.itemsTcdArray.controls[index].get('tcdAmount').value)

    if (this.itemsTcdArray.controls[index].get('flatOrPercentage').value == globalConstants.PER && this.itemsTcdArray.controls[index].get('amount').value > 100) {
      this.itemsTcdArray.controls[index].get('amount').setValue('100.000');
    }
  }

  clear(fields, index?, checkField?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = checkField && field == checkField ? '' : null;
        (<FormArray>_this.enquirySupplierForm.controls['details']).at(index).patchValue(data);
      } else {
        _this.enquirySupplierForm.get(field).setValue(checkField && field == checkField ? '' : null);
      }
    });

  }
  makeDecimalPoint(field?, index?, formArray?, point?) {
    let _this = this;
    if (index || index == 0) {
      var element = (<FormArray>this.enquirySupplierForm.controls[formArray]).at(index);
      var value = (<FormArray>element).controls[field].value;
      if (value == null) { return; }
      var data = {};
      data[field] = (parseFloat(value.toString()).toFixed(point ? 2 : 3));
      value && !isNaN(value) ? element.patchValue(data) : '';
      if(value && !isNaN(value) && parseFloat(value)  <= 0){
        element.get(field).setValue("")
      }
    } else {
      var value = _this.enquirySupplierForm.get(field).value;
      console.log("*****value",value)
      if (value != "") {
        _this.enquirySupplierForm.get(field).setValue((parseFloat(value.toString()).toFixed(point ? 2 : 3)));
      }else {
        _this.enquirySupplierForm.get(field).setValue("");
      }
    }
  }
  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("from", from);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      console.log("getId", getId);
      if (isNaN(getId)) {
        if (index || index == 0) {
          var element = (<FormArray>this.enquirySupplierForm.controls['details']).at(index);
          if ((<FormArray>element).controls[to].value == getId) {
            return false;
          }
        } else {
          if (this.enquirySupplierForm.controls[to].value == getId) {
            return false;
          }
        }
        var check = {};
        check[dataField] = getId;
        var item = _.find(data, check);
      } else {
        console.log("is num");
        var item = _.find(data, { id: parseInt(getId, 10) });
      }
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.enquirySupplierForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.enquirySupplierForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.enquirySupplierForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.enquirySupplierForm.get(from).setValue(getId);
          }
          console.log("this.enquirySupplierForm", this.enquirySupplierForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.enquirySupplierForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.enquirySupplierForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.enquirySupplierForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.enquirySupplierForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.enquirySupplierForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.enquirySupplierForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.enquirySupplierForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.enquirySupplierForm.get(from).setValue(null);
        }
      }
    }
  }

  resetAndReCalculate() {
    let _this = this;
    _this.itemTotals= {};
    _this.vendor_itemTotals = {};
    _this.vendor_itemTotal = 0;
    _.forEach(_this.enquirySupplierForm.get('details')['controls'], function (value, index) {
      _this.calculateCost(index);
    });
  }


  calculateCost(index) {
    let _this = this
    _this.itemTotal = 0;
    var element = (<FormArray>this.enquirySupplierForm.controls['details']).at(index);

    if ((<FormGroup>element).controls['qtQty'].status == 'VALID' && (<FormGroup>element).controls['venQtUnitPrice'].status == 'VALID') {
      var qty = (<FormArray>element).controls['qtQty'].value;
      var price = (<FormArray>element).controls['venQtUnitPrice'].value;
      var cost = null;
      cost = qty * price;
      _this.itemTotals[index] = cost;
      (<FormArray>element).controls['vendQtCost'].setValue(cost.toFixed(3));
    } else {
      _this.itemTotals[index] = 0;
      (<FormArray>element).controls['vendQtCost'].setValue(null)
    }
    _.map(_this.itemTotals, function (item) {
      item = (item) ? item : 0;
      _this.itemTotal = _this.itemTotal + item;
    });
    var fromVendorCost = _this.itemTotal;
    _this.enquirySupplierForm.get('fromVendorCost').setValue(fromVendorCost.toFixed(3));
    console.log("_this.itemTotals", _this.itemTotals);
    console.log("fromVendorCost", fromVendorCost);
    console.log("Form", _this.enquirySupplierForm)
    this.calculateTotalAmount();
  }

  resetCalculation(_this) {
    _this.vendor_itemTotal = 0;
    _this.vend_tcdTotal = 0;
    _this.vend_tcdDiscount = 0;
    _this.vend_tcdTotal = 0;

  }
  calculateTotalAmount() {
    let _this = this;
    this.resetCalculation(_this)
    var vendor_FinalCost = 0;

    _.map(_this.itemTotals, function (item) {
      _this.vendor_itemTotal = _this.vendor_itemTotal + item;
    });
    vendor_FinalCost = _this.vendor_itemTotal;
    _this.enquirySupplierForm.get('fromVendorCostWithTCD').setValue(vendor_FinalCost.toFixed(3));

    if (this.enquirySupplierForm.controls.details.status == 'VALID' && _this.itemTotals) {
      _this.enquirySupplierForm.get('fromVendorCost').setValue(_this.vendor_itemTotal.toFixed(3));
    }

    // calculate TCD 
    _.forEach(_this.enquirySupplierForm.value.tcdDetails, function (value, index) {
      var element = (<FormArray>_this.enquirySupplierForm.controls['tcdDetails']).at(index);
      var rate = 0;
      var v_rate = 0;
      if ((<FormGroup>element).controls['tcdType'].status == 'VALID' && (<FormGroup>element).controls['flatOrPercentage'].status == 'VALID' && (<FormGroup>element).controls['amount'].status == 'VALID') {
        rate = value.status == 1 ? parseFloat(value.amount) : 0;
        if (value.flatOrPercentage == globalConstants.PER) {
          rate = rate / 100;
          v_rate = (_this.vendor_itemTotal * rate);
        } else {
          v_rate = rate;
        }

        if (value.tcdType == globalConstants.DISCOUNT) {
          // vendor discount details
          vendor_FinalCost = vendor_FinalCost - parseFloat(v_rate.toString());
          _this.vend_tcdDiscount = parseFloat(_this.vend_tcdDiscount.toString()) + parseFloat(v_rate.toString());
          _this.vend_tcdDiscount = parseFloat((_this.vend_tcdDiscount).toFixed(3));
        } else {
          // vendor tax/charge details
          vendor_FinalCost = vendor_FinalCost + parseFloat(v_rate.toString());
          _this.vend_tcdTotal = parseFloat(_this.vend_tcdTotal.toString()) + parseFloat(v_rate.toString());
          _this.vend_tcdTotal = parseFloat((_this.vend_tcdTotal).toFixed(3));
        }
        _this.enquirySupplierForm.get('fromVendorCostWithTCD').setValue(vendor_FinalCost.toFixed(3));
        (<FormArray>element).controls['tcdAmount'].setValue(v_rate.toFixed(3));
      } else {
        (<FormArray>element).controls['tcdAmount'].setValue(null);
      }
      console.log("vendor_FinalCost", vendor_FinalCost)
      console.log("v_rate", v_rate)
    });
    this.toUsd()
  }
  calculateTcdAmount() {
    this.calculateTotalAmount();
  }

  getItemGroups() {
    let _this = this;
    let params = { model_name: 'itemGroups', where: { 'status': 1 } };
    _this.CS.getDataUnsafe(params).subscribe(response => {
      console.log("getItemGroups", response);
      if (response && response.status == "success" && response.result) {
        _this.itemGroups = response.result;
      } else {
        _this.itemGroups = [];
      }
    });
  }

  getTypes() {
    let _this = this;

    let params = { model_name: 'poTypes', where: { 'status': 1 } };
    _this.CS.getDataUnsafe(params).subscribe(response => {
      console.log("itemTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.itemTypes = response.result;
      } else {
        _this.itemTypes = [];
      }
    });
  }
  getUoms() {
    let _this = this;
    let params = { model_name: 'uom', where: { 'status': 1 } };
    _this.CS.getDataUnsafe(params).subscribe(response => {
      console.log("getUoms", response);
      if (response && response.status == "success" && response.result) {
        _this.uoms = response.result;
        console.log(" _this.uoms", _this.uoms);
      } else {
        _this.uoms = [];
      }
    });
  }

  getTcdTypes() {
    let _this = this;
    _this.CS.getDataUnsafe({ model_name: 'tcdTypes', where: { 'status': 1 } }).subscribe(response => {
      if (response && response.status == "success" && response.result) {
        _this.tcdTypes = response.result;
      } else {
        _this.tcdTypes = [];
      }
    });
  }


  onSubmit() {
    console.log("Form", this.form);
    let _this = this;
    _this.submitted = true;
    _this.tcdSubmitted = true;
    let formData = new FormData();
    if (this.fileToUpload.length) {
      for (var i = 0; i < this.fileToUpload.length; i++) {
        formData.append("files", this.fileToUpload[i], uuidv4() + this.fileToUpload[i].name);
      }
    }

    // check enq date
    if (this.isEnquiryDateLessthanDeliverydate(_this)) {
      this.toastr.error('Delivery date must be greater than request received date!', 'Error!');
      return;
    }

    // check TCD details valid
    if (_this.enquirySupplierForm.value.tcdDetails.length > 1 && this.enquirySupplierForm.controls.tcdDetails.status == 'VALID') {
      var checkDiscount = _.countBy(_this.enquirySupplierForm.value.tcdDetails, function (tcd) {
        return tcd.tcdType == globalConstants.DISCOUNT;
      });
      if (checkDiscount.true > 1) {
        _this.tcdError = 'Only one discount row allowed!';
        return false;
      }
    } else if (_this.enquirySupplierForm.value.tcdDetails.length == 1) {
      console.log("length1");
      if (!_this.enquirySupplierForm.value.tcdDetails[0].tcdType && !_this.enquirySupplierForm.value.tcdDetails[0].flatOrPercentage && !_this.enquirySupplierForm.value.tcdDetails[0].amount && !_this.enquirySupplierForm.value.tcdDetails[0].tcdAmount) {
        this.itemsArray = this.enquirySupplierForm.get('tcdDetails') as FormArray;
        this.itemsArray.removeAt(0);
      } else {
        _this.tcdSubmitted = true;
      }
    } else {
      _this.tcdSubmitted = true;
    }
    
    // check total amout greater than 0
    if (_this.enquirySupplierForm.value.fromVendorCostWithTCD < 0) {
      _this.tcdError = 'Discount value should be less than total cost!';
      return false;
    }

    if (this.enquirySupplierForm.invalid) {
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      _this.loading = false;
    }

    _this.enquirySupplierForm.value['status'] = 1;
    if (this.enquirySupplierForm.status == 'VALID') {
      _this.loading = true
      this.spinner.show();
      setTimeout(() => this.spinner.hide(), 2000);
      console.log("Form", this.form);
      _this.enquirySupplierForm.value['quotationStatus'] = 1;
      _this.enquirySupplierForm.value['deliveryDate'] = this.toDateObject(_this.enquirySupplierForm.get('deliveryDate').value)
      _this.CS.updateSupplierEnquiiry(_this.enquirySupplierForm.value).subscribe(response => {
        console.log("response", response);
        if (response && response.status == "success") {

          /*if (this.fileToUpload.length) {
            formData.append("data", response.result.id);
            _this.CS.saveSupplierDocuments(formData).subscribe(res => {
              _this.loading = false;
              if (res && res.data && res.data.status == "success") {
                this.toastr.success('Quotation Added.', 'Success!');
              } else {
                this.toastr.error('Something Went Wrong!', 'Failed!'); 
              }
            });
          } else {
            _this.loading = false;
            this.toastr.success('Quotation Added.', 'Success!');
          }*/
          this.getEnquiryDetails();
          _this.loading = false;
          _this.submitted = false;
          _this.tcdSubmitted = false;
          this.toastr.success('Quotation Added.', 'Success!');

        } else if (response.status == "show_message") {
          _this.loading = false;
          this.toastr.error(response.message, 'Failed!');

        } else if (response.status == "quotation_exist") {
          _this.loading = false;
          this.toastr.warning(response.message, 'Warning!');
        } else {
          _this.loading = false;
          this.toastr.error('Something Went Wrong!', 'Failed!');
        }
      });
    }

  }

  isEnquiryDateLessthanDeliverydate(_this) {
    let enqDate = (_this.vendorIdDetails.length > 0) ? _this.vendorIdDetails[0].date : null; // using vendor enq date
    //let enqDate = _this.enquiryDetails.enqDate;
    let deliveryDate = this.toDateObject(this.enquirySupplierForm.get('deliveryDate').value);
    if (deliveryDate) {
      // remove time info from date time,return date only
      let enqDateObj = new Date(enqDate).toDateString();
      enqDate = new Date(enqDateObj);
      let deliveryDateObj = new Date(deliveryDate);
      if (deliveryDateObj < enqDate) {
        return true;
      }
      return false;
    }
    return false;
  }

  declineQuote() {
    let _this = this;
    _this.CS.declineEnqQuote(_this.urlParams).subscribe(response => {
      if (response && response.status == "success") {
        _this.loading = false;
        this.toastr.success('Quotation request declined.', 'Success!');
        this.getEnquiryDetails();
      } else {
        _this.loading = false;
        if (response.status == "show_message") {
          this.toastr.error(response.message, 'Failed!');
        } else {
          this.toastr.error('Something Went Wrong!', 'Failed!');
        }
      }
    });
  }

  convertDate(dateString) {
    if (dateString == "" || dateString == null) {
      return "";
    }
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMONTHYY(objectDate);
  }
  toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }

  deleteDocFromServer(doc, index) {
    console.log("doc", doc);
    var params = { id: doc.id, model_name: 'enquiryDocument' };
    let _this = this;
    _this.loader[doc.id] = true;
    _this.CS.removeData(params).subscribe(response => {
      _this.loader[doc.id] = false;
      if (response && response.status == "success" && response.result) {
        _this.enquiryDetails.EDO.splice(index, 1);
      } else {
        this.toastr.error('Something Went Wrong!', 'Failed!');
      }
    });
  }
  clearSelect(field) {
    this.enquirySupplierForm.get(field).setValue(null);
  }

  triggerFileUpload() {
    this.el.nativeElement.querySelector("#fileUpload").click();
  }

  deleteDoc(index) {
    this.fileToUpload.splice(index, 1);
  }

  upload(files: File[]) {

    let _this = this;
    _.forEach(files, function (value) {
       console.log("reader", value)
       var fileTypes = ['jpg', 'jpeg', 'png', 'xlsx', 'txt', 'gif', 'pdf', 'xls'];  //acceptable file types
        var extension = value.name.split('.').pop().toLowerCase() 
        console.log(extension)
        var Success = fileTypes.indexOf(extension) > -1
       if(Success) {
         if (value.size < 4096000) {
      var reader = new FileReader();
      reader.readAsDataURL(value);
      reader.onload = () => {
        var src = reader.result as string;
        _this.imageSrc.push(src);
        _this.fileToUpload.push(value);
      }
      }
        else {
        alert('Maximum allowed file size is 4 MB!');
        return
      }
      
    } else {
      alert('File type not supported')
    }
    });

    document.getElementById('fileUpload')['value'] = "";
  }

  toUsd() {
    let _this = this;
    if (_this.enquirySupplierForm.value.vendCurrencyXid && _this.exchangeRate && _this.exchangeRate.length) {
      var rate = _.find(_this.exchangeRate, { currencyXId: _this.enquirySupplierForm.value.vendCurrencyXid });
      console.log("slected rate", rate)
      var curencyObj = _.find(_this.currencies, { id: _this.enquirySupplierForm.value.vendCurrencyXid });
      _this.currencySymbol = curencyObj ? curencyObj.symbol : ''
      _this.currencyShortKey = curencyObj ? curencyObj.shortKey : ''
      if (rate) {
        let vendrCost = _this.enquirySupplierForm.value.fromVendorCostWithTCD
        var InUsd = ((vendrCost && vendrCost > 0) ? vendrCost : 0) / rate.Rate;
        console.log("InUsd", InUsd);
        this.enquirySupplierForm.get('vendAmountUsd').setValue(InUsd.toFixed(3));
        this.enquirySupplierForm.get('vendExRate').setValue(rate.Rate);
      } else {
        //if no exchange rate       
        this.enquirySupplierForm.get('vendAmountUsd').setValue(0);
        this.enquirySupplierForm.get('vendExRate').setValue(0);
      }

    }
  }
  getExchangeRate() {
    let _this = this;
    let params = {};
    _this.CS.getLatestCurrencyExchangeRatesUnsafe(params).subscribe(response => {
      console.log("exchangeRate", response);
      if (response && response.status == "success" && response.result) {
        let exchangeRates = [];
        _.map(response.result, function (item) {
          if (item.ER && item.ER.length > 0) {
            exchangeRates.push(item.ER[0]);
          }
        });
        _this.exchangeRate = exchangeRates;
        console.log(" _this.exchangeRate", _this.exchangeRate);
      } else {
        _this.exchangeRate = [];
      }
    });
  }

  getCurrencies() {
    let _this = this;
    let params = { model_name: 'currencies', where: { 'status': 1 } };
    _this.CS.getDataUnsafe(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencies = response.result;
      } else {
        _this.currencies = [];
      }
    });
  }

  fixSelectedData(name,index){
  console.log("name",name)
  console.log("index",index)

     var element = (<FormArray>this.enquirySupplierForm.controls['details']).at(index);

     if(name =='group') {
      (<FormArray>element).controls['groupNameAuto'].setValue( (<FormArray>element).controls['group'].value);
    }

     if(name =='uom') {
      (<FormArray>element).controls['uomNameAuto'].setValue( (<FormArray>element).controls['uom'].value);
    }

}

createDocuments() {
    let _this = this;
    let item = this.formBuilder.group({
      enqXid: [''],
      fileName: [''],
      fileDisplayName: [''],
      filePath: [''],
      companyXid: [null],
      status: 1
    });
    _this.itemsDocsArray = this.enquirySupplierForm.get('documents') as FormArray;
    _this.itemsDocsArray.push(item);
  }
setDocuments() {
    let _this = this;
    if (_this.enquiryDetails.EDO && _this.enquiryDetails.EDO.length) {
      _.forEach(_this.enquiryDetails.EDO, function (field, index) {
        console.log("documentttttttttttt", field)
        let item = _this.formBuilder.group({
          enqXid: [field.enqXid],
          fileName: [field.fileName],
          fileDisplayName: [field.fileDisplayName],
          filePath: [field.filePath],
          //companyXid: [_this.company],
          status: 1
        });
        _this.itemsDocsArray = _this.enquirySupplierForm.get('documents') as FormArray;
        _this.itemsDocsArray.push(item);
      });
    }
  }


}
