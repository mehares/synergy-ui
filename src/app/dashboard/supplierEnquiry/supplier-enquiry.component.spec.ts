import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierEnquiryComponent } from './supplier-enquiry.component';

describe('SupplierEnquiryComponent', () => {
  let component: SupplierEnquiryComponent;
  let fixture: ComponentFixture<SupplierEnquiryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierEnquiryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierEnquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
