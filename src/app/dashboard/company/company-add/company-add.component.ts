import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { group } from '@angular/animations';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-company-add',
  templateUrl: './company-add.component.html',
  styleUrls: ['./company-add.component.scss']
})
export class CompanyAddComponent implements OnInit {

  i:any;
  keyword = 'name';
  key = 'name';
  companyForm: FormGroup;
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  countryList = [];
  stateList = [];
  submitted = false;
  isSSLEnabled = [{ id: 1, name: 'YES' }, { id: 0, name: 'NO' }];
  enableEmail = [{ id: 1, name: 'YES' }, { id: 0, name: 'NO' }];
  isSSLEnabledRead = [{ id: 1, name: 'YES' }, { id: 0, name: 'NO' }];
  loading = false;
  keywordCountry = 'name';
  keywordState = 'name';

  constructor(private router: Router,private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private CS: CommonservicesService,private toastr: ToastrService) {
  }

  ngOnInit() {
    this.createForm();
    this.getCountries();
    //this.getState();
  }

  createForm() {
    this.companyForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', Validators.required],
      state: ['', [Validators.required]],
      country: ['', [Validators.required]],
      address1: ['', [Validators.required]],
      address2: [''],
      address3: [''],
      zipCode: ['', [Validators.required]],
      phoneNo: ['', [Validators.required, Validators.pattern(this.mobNumberPattern)]],
      website: ['', [Validators.required]],
      fromEmail: [''],
      emailPassword: [''],
      emailServer: [''],
      isSSLEnabled: [null],
      mailPort: [''],
      enableEmail: [null],
      emailServerRead: [''],
      isSSLEnabledRead: [null],
      mailPortRead: [''],
      emailUserName:[''],
      companyName:[''],
      parentId: 0,
      status:1
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.companyForm.controls;
  }
  fieldUpdate(value, type) {
    let updatedRes = [];
    if (value) {
      value.forEach(element => {
        let groupElement = {
          [type]: element,
        }
        updatedRes.push(groupElement);
      });
      return updatedRes;
    } else {
      return updatedRes;
    }
  }
  onSubmit() {
    this.submitted = true;
    let _this = this;
    
    console.log("companyForm", this.companyForm);
    // stop here if form is invalid
    console.log('Form Status', this.companyForm.invalid);
    if (this.companyForm.invalid) {
      this.toastr.error('Please Fill The Details', 'Error!');
      return;
    } else {
    this.spinner.show();
      
      _this.loading = true;
      let params = this.companyForm.value;
      _this.CS.createCompanyData(params).subscribe(response => {
      	console.log("response",response);
        _this.loading = false;
        if (response && response.status == "success") {
          this.toastr.success('New Company Added', 'Success!');
          _this.router.navigate(['/company/edit',response.result.id]);
        } else {
        	this.toastr.error('Error Occured While Saving', 'Failed!');
        }
		setTimeout(() => {
        this.spinner.hide();
   		}, 2000);
      });
    }
  }


getCountries() {
    let _this = this;
    let params = { model_name: 'country', where: {} };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.countryList = response.result;
        console.log("countriess",response.result)
      } else {
        _this.countryList = [];
      }
    });
  }


   changeState(event,countryId,dataField, to, index?, from?) {
    console.log('event');
    console.log('countryId');
    if (countryId && event) {
      let _this = this;
     _this.stateList = [];
      var getId = typeof event == 'object' ? event.id : event;
      var countryId = _.find(countryId, {id: parseInt(getId, 10)});
      console.log("countryId", countryId);

      
      
      _this.CS.getStateList({ id: countryId.id }).subscribe(response => {
        console.log("response",response);
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          _this.stateList = response.result[0].CS;
          console.log("_this.stateList",_this.stateList);
        } else {
        //  _this.stateList = [];
        }
      })
     /*   for (let ctr in this.form.companyForm.value ) {
      _this.form.companyForm.value[ctr].country) = countryId.id;
      //(<FormArray>this.VA.controls['ctr']).at(index).patchValue(input);
      console.log("_this.form.value[ctr].country",_this.form.value[ctr].country)
    }*/

    if (countryId) {
        if (index || index == 0) {
          var input = {};
          input[to] = countryId[dataField];
          (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.companyForm.get(to).setValue(countryId[dataField]);
          if (typeof event == 'object') {
            this.companyForm.get(from).setValue(getId);
          }
          console.log("this.companyForm", this.companyForm);
        }
      } else {

      }
    
  }
}


    changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, {id: parseInt(getId, 10)});
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.companyForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.companyForm.get(from).setValue(getId);
          }
          console.log("this.companyForm", this.companyForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.companyForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.companyForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.companyForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.companyForm.get(from).setValue(null);
        }
      }
    }
  }

 omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}

}