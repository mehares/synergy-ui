import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { globalConstants } from '../../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent implements OnInit {

  company_list=[];
  currentPage = 1
  totalItems = 0
  offset = 0
  UPLOAD_PATH = globalConstants.UPLOADS_DIR
  userQuestionUpdate = new Subject<string>();
  companySearchForm : FormGroup

  submitted = false
  isValidFormSubmitted = null;
  searchToggle = false;
  searchLoader = false;
  search = '';
  clearFilter = false;
  searchFilterObject = null;
  isAdvanceSearch = false;
  pageOffset = 0;
  countryName = '';
  stateName = '';
  countryId = null;
  stateId = null;


  keywordCountry = 'name'
  keywordState = 'name'

  firstLoad = false
  loader = false
  countryList = []
  stateList = []

    

  constructor( private spinner: NgxSpinnerService, private formBuilder: FormBuilder, private http: HttpClient,private CF: CommonFunctionsService ,private CS: CommonservicesService,private toastr: ToastrService ) {
      this.userQuestionUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.resetPagination();
        this.onSubmit();
      });
     }




  permissions = this.CF.findPermissions()
  itemsPerPage = this.CS.getItemPerPage();
  ngOnInit() {
  this.spinner.show();
  setTimeout(() => this.spinner.hide(), 2000);
  this.getCompany();
  this.createSearchForm()
  this.getCountries();
   
  }

  getPage() {


  }

  getCompany() {
    this.spinner.show();
    let _this = this;
    var params = {  limit: this.itemsPerPage, offset: this.offset, filterOption: this.searchFilterObject };
    this.pageOffset = this.offset;
    _this.loader = true;
    this.CS.countCompanyData(params)
    .subscribe((response) => { 
      console.log("ALl list",response.result)
      if(response.status && response.status == 'success'){
        _this.loader = false
        this.company_list = response.result && response.result.rows ? response.result.rows : [];
         this.totalItems = response.result && response.result.count ? response.result.count : 0;
      }else{
        _this.loader = false
      	this.toastr.error('Error Occured While Saving', 'Failed!');
      }
      setTimeout(function () {
       _this.spinner.hide();
      },100)
    });
  }
  pageChanged(event) {
    console.log("event", event)
    this.currentPage = event
    var setoffset = event - 1
    setoffset = setoffset * this.itemsPerPage
    this.offset = setoffset
    this.getCompany();
  }

exportCompany() {
  let _this = this;
  _this.CS.companySetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'company/' + response.filename, '_blank');
        this.toastr.success('Successfuly Exported Excel', 'Success!');
      }
      else {
      	this.toastr.error(response.msg, 'Failed!');

      }
    });
}


  resetPagination() {
    this.offset = 0;
    this.currentPage = 1;
  }

  createSearchForm() {
    this.companySearchForm = this.formBuilder.group({
      name: ['', []],
      address1: ['', []],
      country: ['', []],
      search: ['', []],
      state: ['', []],
      zipCode: ['', []],
      countryName: ['', []],
      stateName: ['', []],
    });
  }

  get f() {
    return this.companySearchForm.controls;
  }
onSubmit(from = '') {

    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;
    if (this.companySearchForm.invalid) {
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      return;
    }

    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;


    const companySeacrhObj = {
      country: this.f.country.value,
      state: this.f.state.value,
      name: this.f.name.value,
      search: this.search,
      address1 : this.f.address1.value,
      zipCode : this.f.zipCode.value
    };
    _this.searchLoader = false;
    _this.clearFilter = true;    
    _this.searchToggle = false;
    this.searchFilterObject = companySeacrhObj;
    this.getCompany();


  }

changeSelection(event, to) {
    console.log("event", event)
    console.log("to", to);
    // this.poSeacrhForm.get(to).setValue(event.id);

    if (to == 'country') {
      this.countryName = event.name ? event.name : this.countryName;
      this.countryId = event.id ? event.id : this.countryId;
      this.companySearchForm.get(to).setValue(this.countryName);
       this.CS.getStateList({ id: this.countryId }).subscribe(response => {
        console.log("response",response);
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          this.stateList = response.result[0].CS;
          console.log("_this.stateList",this.stateList);
        } else {
        //  _this.stateList = [];
        }
      })
    }
       if (to == 'state') {
      this.stateName = event.name ? event.name : this.stateName;
      this.stateId = event.id ? event.id : this.stateId;
      this.companySearchForm.get(to).setValue(this.stateName);
    }
  }
 clearSearch(type) {

    if (type =="reset") {
      this.isAdvanceSearch = false;
      this.countryName = null;
      this.stateName = null;
      this.searchToggle = true;
    }
    else  {
      this.isAdvanceSearch = false;
      this.countryName = null;
      this.stateName = null;
      this.searchToggle = false
    }
    this.searchFilterObject = null;
    this.createSearchForm();
    this.getCompany();
  }

  clearMainSearch(type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.clearSearch(type);
      this.searchToggle = true
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }

    clear(to) {
    this.companySearchForm.get(to).setValue(null);
  }

getCountries() {
    let _this = this;
    let params = { model_name: 'country', where: {} };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.countryList = response.result;
        console.log("countriess",response.result)
      } else {
        _this.countryList = [];
      }
    });
  }

    onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container')){
        console.log('wow--->closeee----')
      }
      else {
    this.firstLoad = false
  }
  }
}