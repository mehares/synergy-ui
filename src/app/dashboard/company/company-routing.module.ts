import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyAddComponent } from './company-add/company-add.component';
import { CompanyEditComponent } from './company-edit/company-edit.component';
import { PageGuard } from '../../guards/page-guard.service';
const routes: Routes = [
  {
    path: 'list',
    canActivate: [PageGuard],
    component: CompanyListComponent,
    data:{permissions: 'companies_view' }
  },
  {
    path: 'add',
    canActivate: [PageGuard],
    component: CompanyAddComponent,
    data:{permissions: 'companies_add' }
  },
  {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: CompanyEditComponent,
    data:{permissions: 'companies_edit' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
