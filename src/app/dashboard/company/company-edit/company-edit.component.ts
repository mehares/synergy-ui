import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { globalConstants } from '../../../constants/global-constants';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as _ from 'lodash';
import { v1 as uuidv1 } from 'uuid';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.scss']
})
export class CompanyEditComponent implements OnInit {

httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  i: any;
  companyForm: FormGroup;
  submitted = false;
  id = null;
  companyList = null;
  userData = null;
  loading = false;
  isSSLEnabled = [{ id: 1, name: 'YES' }, { id: 0, name: 'NO' }];
  enableEmail = [{ id: 1, name: 'YES' }, { id: 0, name: 'NO' }];
  isSSLEnabledRead = [{ id: 1, name: 'YES' }, { id: 0, name: 'NO' }];
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  countryList = [];
  stateList = [];
  keywordCountry = 'name';
  keywordState = 'name';

  constructor(private router: Router,private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private CS: CommonservicesService, private activatedroute: ActivatedRoute,
    private http: HttpClient,
    private location: Location,
    private el: ElementRef,private toastr: ToastrService) {

  }

  ngOnInit() {
    this.id = this.activatedroute.snapshot.params.id;
    console.log('this.activatedroute.snapshot.params', this.activatedroute.snapshot.params);
    this.createCompanyForm();
    this.getCompanyData();
    this.getCountries();
  }




  getCompanyData() {
    let _this = this;
    let params = { model_name: 'companies', where: { 'id': this.id }, id: this.id };
    _this.CS.getCompanyData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.companyList = response.result;
        if (this.companyList.CASR != null) {
          _this.CS.getStateList({ id: this.companyList.CASR.id }).subscribe(response => {
            console.log("response", response);
            if (response && response.status == "success" && response.result && response.result.length > 0) {
              _this.stateList = response.result[0].CS;
              console.log("_this.stateList", _this.stateList);
            } else {
              //  _this.stateList = [];
            }
            _this.setForm();
          })
        } else {
          _this.setForm();
        }


        console.log("_this.companyList", _this.companyList);
      } else {
        this.toastr.error('Something Went Wrong', 'Failed!');
      }
    });
  }
  createCompanyForm() {
    this.companyForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', Validators.required],
      state: ['', [Validators.required]],
      country: ['', [Validators.required]],
      address1: ['', [Validators.required]],
      address2: [''],
      address3: [''],
      zipCode: ['', [Validators.required]],
      phoneNo: ['', [Validators.required, Validators.pattern(this.mobNumberPattern)]],
      website: ['', [Validators.required]],
      fromEmail: [''],
      emailPassword: [''],
      emailServer: [''],
      isSSLEnabled: [null],
      mailPort: [''],
      enableEmail: [null],
      emailServerRead: [''],
      isSSLEnabledRead: [null],
      mailPortRead: [''],
      emailUserName: [''],
      companyName: [''],
      parentId: 0,
      status: 1,
      countryName: '',
      stateName: ''
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.companyForm.controls;
  }
  fieldSelect(value, type) {
    let updatedRes = [];
    if (value) {
      value.forEach(element => {
        updatedRes.push(element[type]);
      });
      updatedRes = _.sortedUniq(updatedRes);
      return updatedRes;
    } else {
      return updatedRes;
    }

  }
  fieldUpdate(value, type) {
    let updatedRes = [];
    if (value) {
      value.forEach(element => {
        let fieldElement = {
          [type]: element,
        }
        updatedRes.push(fieldElement);
      });
      return updatedRes;
    } else {
      return updatedRes;
    }
  }



  onSubmit() {
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
  
    let _this = this;
    this.submitted = true;

    if (this.companyForm.invalid) {
      this.toastr.error('Please Validate Fields', 'Invalid!');
      return;
    }

    const companyObj = {
      email: this.form.email.value,
      name: this.form.name.value,
      state: this.form.state.value,
      country: this.form.country.value,
      address1: this.form.address1.value,
      address2: this.form.address2.value,
      address3: this.form.address3.value,
      zipCode: this.form.zipCode.value,
      phoneNo: this.form.phoneNo.value,
      website: this.form.website.value,
      fromEmail: this.form.fromEmail.value,
      emailPassword: this.form.emailPassword.value,
      emailServer: this.form.emailServer.value,
      isSSLEnabled: this.form.isSSLEnabledRead.value,
      mailPort: this.form.mailPort.value,
      enableEmail: this.form.enableEmail.value,
      emailServerRead: this.form.emailServerRead.value,
      isSSLEnabledRead: this.form.isSSLEnabledRead.value,
      mailPortRead: this.form.mailPortRead.value,
      emailUserName: this.form.emailUserName.value,
      companyName: this.form.companyName.value,
      parentId: 0,
      status: 1,
      id: this.id,
    };

        _this.CS.companyUpdate(companyObj).subscribe(response => {
      	console.log("response",response);
        _this.loading = false;
        if (response && response.status == true) {
          this.toastr.success('Company Details Updated', 'Success!');
        } else {
          this.toastr.error('Something Went Wrong', 'Failed!');
        }

      });
  }







  setForm() {
    this.companyForm = this.formBuilder.group({
      email: [this.companyList.email, [Validators.required, Validators.email]],
      name: [this.companyList.name, Validators.required],
      state: [this.companyList.CAST.id, [Validators.required]],
      country: [this.companyList.CASR.id, [Validators.required]],
      address1: [this.companyList.address1, [Validators.required]],
      address2: [this.companyList.address2],
      address3: [this.companyList.address3],
      zipCode: [this.companyList.zipCode, [Validators.required]],
      phoneNo: [this.companyList.phoneNo, [Validators.required, Validators.pattern(this.mobNumberPattern)]],
      website: [this.companyList.website, [Validators.required]],
      fromEmail: [this.companyList.fromEmail],
      emailPassword: [this.companyList.emailPassword],
      emailServer: [this.companyList.emailServer],
      isSSLEnabled: [(this.companyList.isSSLEnabledRead ? 1 : 0)],
      mailPort: [this.companyList.mailPort],
      enableEmail: [(this.companyList.enableEmail ? 1 : 0)],
      emailServerRead: [this.companyList.emailServerRead],
      isSSLEnabledRead: [(this.companyList.isSSLEnabledRead ? 1 : 0)],
      mailPortRead: [this.companyList.mailPortRead],
      emailUserName: [this.companyList.emailUserName],
      companyName: [this.companyList.companyName],
      parentId: 0,
      status: 1,
      countryName: this.companyList.CASR.name,
      stateName: this.companyList.CAST.name,
    });
  }

  getCountries() {
    let _this = this;
    let params = { model_name: 'country', where: {} };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.countryList = response.result;
        console.log("countriess", response.result)
      } else {
        _this.countryList = [];
      }
    });
  }



  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, { id: parseInt(getId, 10) });
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.companyForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.companyForm.get(from).setValue(getId);
          }
          console.log("this.companyForm", this.companyForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.companyForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.companyForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.companyForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.companyForm.get(from).setValue(null);
        }
      }
    }
  }

  changeState(event, countryId, dataField, to, index?, from?) {
    console.log('event');
    console.log('countryId');
    if (countryId && event) {
      let _this = this;
      _this.stateList = [];
      var getId = typeof event == 'object' ? event.id : event;
      var countryId = _.find(countryId, { id: parseInt(getId, 10) });
      console.log("countryId", countryId);



      _this.CS.getStateList({ id: countryId.id }).subscribe(response => {
        console.log("response", response);
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          _this.stateList = response.result[0].CS;
          console.log("_this.stateList", _this.stateList);
        } else {
          //  _this.stateList = [];
        }
      })
      /*   for (let ctr in this.form.companyForm.value ) {
       _this.form.companyForm.value[ctr].country) = countryId.id;
       //(<FormArray>this.VA.controls['ctr']).at(index).patchValue(input);
       console.log("_this.form.value[ctr].country",_this.form.value[ctr].country)
     }*/

      if (countryId) {
        if (index || index == 0) {
          var input = {};
          input[to] = countryId[dataField];
          (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.companyForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.companyForm.get(to).setValue(countryId[dataField]);
          if (typeof event == 'object') {
            this.companyForm.get(from).setValue(getId);
          }
          console.log("this.companyForm", this.companyForm);
        }
      } else {

      }

    }
  }

   omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}

}
