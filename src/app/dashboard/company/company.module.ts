import { NgModule ,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyRoutingModule } from './company-routing.module';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyAddComponent } from './company-add/company-add.component';
import { CompanyEditComponent } from './company-edit/company-edit.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutocompleteLibModule } from "angular-ng-autocomplete";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgSelectModule } from '@ng-select/ng-select';
import { ClickOutsideModule } from 'ng-click-outside';



@NgModule({
  declarations: [CompanyListComponent, CompanyAddComponent, CompanyEditComponent],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    AngularFontAwesomeModule,
    FormsModule, 
    ReactiveFormsModule,
    AutocompleteLibModule,
    NgbModule,
    NgxSpinnerModule,
    NgSelectModule,
    ClickOutsideModule
  ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class CompanyModule { }
