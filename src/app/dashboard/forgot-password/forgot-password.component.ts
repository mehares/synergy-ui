import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators ,FormControl} from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {CommonservicesService} from '../../helper/commonservices/commonservices.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  selectedId=0;
  forgotPasswordForm: FormGroup;
  message_content: string = '';
  submitted = false;
  userDetails = null
  mailVerified = false
  pwdShow = false
 

  constructor(private formBuilder: FormBuilder, private router: Router,private http: HttpClient ,private route: ActivatedRoute ,
    private CS: CommonservicesService,private toastr: ToastrService,private spinner: NgxSpinnerService,) { }

  
   ngOnInit() {
   	this.createForgotForm()


  }

  createForgotForm(){

    this.forgotPasswordForm = this.formBuilder.group({
      email: ['',[Validators.required]],
      new_password: ['', [Validators.required,Validators.pattern('(?=.*[a-z])(?=.*)(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')]],
      conf_new_password: ['', [Validators.required,Validators.pattern('(?=.*[a-z])(?=.*)(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')]],
      otp:['',[Validators.required]],
    });
  }
   
  get f() { return this.forgotPasswordForm.controls; }



   
  onSubmit() {
        

    this.submitted = true;
    let _this = this;


    if ( _this.f.new_password.value =='' && _this.mailVerified == false){

    	if (_this.f.email.value == ''){
    		_this.toastr.error('Please add email address')
    		return
    	}

    	 	/*_this.forgotPasswordForm.get("new_password").setErrors(null);
			_this.forgotPasswordForm.get("new_password").setValidators(null);
			_this.forgotPasswordForm.get("conf_new_password").setErrors(null);
			_this.forgotPasswordForm.get("conf_new_password").setValidators(null);*/
			let params = _this.forgotPasswordForm.value
			_this.CS.forgotPassword(params).subscribe(response => {
				console.log('---->response',response)
				if (response && response.status == 'success'){
				_this.userDetails = response.result
				_this.mailVerified = true
				_this.pwdShow = true
				_this.toastr.success('Otp sent successfully')
				_this.submitted = false

			}
			else {
				_this.mailVerified = false
				_this.toastr.error('No user found')
			}

		})

		} else {
			_this.submitted = true
			_this.mailVerified = true
			_this.pwdShow = true
			console.log("_this.forgotPasswordForm",_this.forgotPasswordForm)
			if(_this.forgotPasswordForm.status =='INVALID'){
				_this.toastr.error('Please fill details')
				return
			}

			    if(_this.forgotPasswordForm.value.conf_new_password!=_this.forgotPasswordForm.value.new_password  ){
      
      				this.toastr.error('Password Mismatch', 'Error!');
      										return ;
   					 }
			let params = _this.forgotPasswordForm.value
			params['id'] = _this.userDetails.id
			_this.CS.newPassword(params).subscribe(response => {
				console.log('---->response',response)
				if (response && response.status == 'success'){
					_this.toastr.success('Password Updated')
					 _this.router.navigate(['/']);

			}

			else {
				_this.toastr.error('Invalid OTP')
			}
		})

		}



    }
    
  
  

}
