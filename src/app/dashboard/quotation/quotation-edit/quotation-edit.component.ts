import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { QuotationServicesService } from '../quotation-services.service';
import { globalConstants } from '../../../constants/global-constants';
import { v4 as uuidv4 } from 'uuid';
import * as _ from 'lodash';
import Swal from 'sweetalert2' 

@Component({
  selector: 'app-quotation-edit',
  templateUrl: './quotation-edit.component.html',
  styleUrls: ['./quotation-edit.component.scss']
})
export class QuotationEditComponent implements OnInit {

  selectedQuotationId = null;
  selectedEnqId = null;
  submitted = false;
  loading = false;
  tcdError = null;
  selectedEnqNumber = '';
  enqDetails: any = null;
  quotationDetails = null;
  quotationReceivedVendors = null;
  quotationForm: FormGroup;
  itemsArray: FormArray;
  itemsVendArray: FormArray;
  itemsTcdArray: FormArray;
  itemsDocsArray: FormArray;
  customer_itemTotals = {};
  vendor_itemTotals = {};
  profit_itemTotals = {};
  customer_itemTotal: any = 0;
  vendor_itemTotal: any = 0;
  profit_itemTotal: any = 0;
  cust_tcdTotal = 0;
  cust_tcdDiscount = 0;
  vend_tcdTotal = 0;
  vend_tcdDiscount = 0;
  converted_toCustomerCost = 0;
  converted_cust_tcdTotal = 0;
  converted_cust_tcdDiscount = 0;
  converted_toCustomerCostWithTCD = 0;
  company = JSON.parse(localStorage.getItem('companyXid'));
  permissions = this.CF.findPermissions();
  itemGroups: any = [];
  itemTypes: any = [];
  tcdTypes: any = [];
  uoms: any = [];
  exchangeRate = [];
  currencies = [];
  fileToUpload: File[] = [];
  imageSrc = [];
  keywordUom = 'uomName';
  keywordGroup = 'groupName';
  FPtypes = globalConstants.FP;
  DOC_PATH = globalConstants.QUOTATION_DOC_PATH;
  QUOTATION_STATUS = globalConstants.QUOTATION_STATUS;
  UPLOAD_PATH = globalConstants.UPLOADS_DIR;
  inclCustQuotation = false;
  cust_currency_rate = null;
  cust_curencyObj = null;
  cust_currencySymbol = null;
  cust_currencyShortKey = null;
  cust_currencyFullName = null;
  vend_currency_rate = null;
  vend_currencyObj = null;
  vend_currencySymbol = null;
  vend_currencyShortKey = null;
  vend_currencyFullName = null;
  deletedItems = [];
  loader = {};
  loaderItem = {};
  previewFn = 0;
  tcdSubmitted = false;
  todayDate: any;
  min_enqDate: any;
  PayTerms = globalConstants.ENQPayTerms;
  PayModes = globalConstants.ENQPayModes;
  IncoTerms = globalConstants.ENQIncoTerms;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private http: HttpClient, private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private CS: CommonservicesService,
    private CF: CommonFunctionsService,
    private el: ElementRef,
    private QuotService: QuotationServicesService
  ) {

    this.route.params.subscribe(params => {
      this.selectedQuotationId = params['id'];
      this.ngOnInit(); // reset and set based on new parameter this time
    });

  }

  ngOnInit() {

    this.selectedQuotationId = this.route.snapshot.paramMap.get('id');
    this.getCurrencies();
    this.getExchangeRate();
    this.createForm();
    this.getTypes();
    this.getItemGroups();
    this.getUoms();
    this.getTcdTypes();
    this.getQuotationDetails();
    this.todayDate = this.CF.dateObjectToJsonDate(new Date())

  }


  onSubmit() {
    let _this = this;
    this.submitted = true;
    console.log("this.quotationForm", _this.quotationForm);
    console.log("_this.quotationForm.details...", _this.quotationForm.get('details'))
    let formData = new FormData();


    // check invoice send date
    if (this.isQuotationDateLessthanEnqDate(_this)) {
      this.toastr.error('Quotation date should greater than enquiry date!', 'Error!');
      return;
    }
    // check TCD details valid
    if (_this.quotationForm.value.tcdDetails.length > 1 && this.quotationForm.controls.tcdDetails.status == 'VALID') {
      var checkDiscount = _.countBy(_this.quotationForm.value.tcdDetails, function (tcd) {
        return tcd.tcdType == globalConstants.DISCOUNT;
      });
      if (checkDiscount.true > 1) {
        _this.tcdError = 'Only one discount row allowed!';
        return false;
      }
    } else if (_this.quotationForm.value.tcdDetails.length == 1) {
      console.log("length1");
      if (!_this.quotationForm.value.tcdDetails[0].tcdType && !_this.quotationForm.value.tcdDetails[0].flatOrPercentage && !_this.quotationForm.value.tcdDetails[0].amount && !_this.quotationForm.value.tcdDetails[0].tcdAmount) {
        this.itemsArray = this.quotationForm.get('tcdDetails') as FormArray;
        this.itemsArray.removeAt(0);
      } else {
        _this.tcdSubmitted = true;
      }
    } else {
      _this.tcdSubmitted = true;
    }

    console.log("this.quotationForm", _this.quotationForm);
    console.log("_this.quotationForm.details...", _this.quotationForm.get('details'))
    console.log("this.quotationForm.status", this.quotationForm.status)

    // check total amout greater than 0
    if (_this.quotationForm.value.fromVendorCostWithTCD < 0 || _this.quotationForm.value.toCustomerCostWithTCD < 0) {
      _this.tcdError = 'Discount value should be less than total cost!';
      return false;
    }

    _this.quotationForm.value['quotationDate'] = this.toDateObject(_this.quotationForm.get('quotationDate').value)
    _this.quotationForm.value['deliveryDate'] = this.toDateObject(_this.quotationForm.get('deliveryDate').value)
    _this.quotationForm.value['custQtGenDate'] = this.toDateObject(_this.quotationForm.get('custQtGenDate').value)
    console.log("**********************");
    if (this.fileToUpload.length) {
      for (var i = 0; i < this.fileToUpload.length; i++) {
        formData.append("files", this.fileToUpload[i], uuidv4() + this.fileToUpload[i].name);
      }
    }

    if (this.quotationForm.status == 'VALID') {
      _this.loading = true;
      _this.spinner.show();
      setTimeout(() => this.spinner.hide(), 2000);
             _this.quotationForm.value['details'] =  _this.quotationForm.getRawValue().details
          _this.quotationForm.value['tcdDetails'] =  _this.quotationForm.getRawValue().tcdDetails
      let params = _this.quotationForm.value;
      params['deletedItems'] = _this.deletedItems;
      _this.CS.updateQuotation(params).subscribe(response => {
        console.log("after update response 1", response)
        if (response && response.status == "success") {
          if (this.fileToUpload.length) {
            //formData.append("data selectedEnqId", _this.selectedEnqId);
            formData.append("data", _this.selectedQuotationId);
            _this.CS.saveQuotationDocuments(formData).subscribe(response => {
              _this.loading = false;
              _this.submitted = false;
              _this.tcdSubmitted = false;
              console.log("after update response", response)
              if (response && response.data && response.data.status == "success") {
                console.log(response);
                this.toastr.success('Quotation Updated.', 'Success!');
                this.resetFileUpload();
                this.getQuotationDetails();
              } else {
                this.toastr.error('Something Went Wrong!', 'Failed!');
              }
            });
            // stay in same page, _this.router.navigate(['/purchase-order/list']);
          } else {
            _this.loading = false;
            _this.submitted = false;
            _this.tcdSubmitted = false;
            this.toastr.success('Quotation Updated.', 'Success!');
            this.getQuotationDetails();
          }

          if (response.warning_message) {
            this.toastr.warning(response.warning_message, 'Warning!');
          }

        } else if (response && response.status == "quotation_exist") {
          _this.loading = false;
          this.toastr.warning(response.message, 'Already Generated!');
        } else if (response && response.status == "show_message") {
          _this.loading = false;
          this.toastr.warning(response.message, 'Warning!');
        } else {
          _this.loading = false;
          this.toastr.error('Something Went Wrong!', 'Failed!');
        }
        _this.submitted = false;
      });

      _this.submitted = false;

    } else {
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
    }


  }

  generatePurchaseOrder() {
    alert("ssss")
  }

  generateCustomerQuotation() {
    let _this = this
    _this.loading = true;

    if (_this.quotationDetails.id == null) {
      this.toastr.error("Please select valid quotation", 'Failed!');
      return false;
    } else {
        if (_this.inclCustQuotation == false) {
      this.toastr.error('Please select customer quotation', 'Invalid')
      return
    }

    if(_this.quotationForm.get('custQtGenDate').value ==null){
      _this.toastr.error('Please select quotation send date')
      _this.loading = false
      return
    }

            Swal.fire({ 
            title: 'Generate Customer Quotation',
            text: 'Are you sure you want to generate this quotation to customer?',
          showCancelButton: true,
          confirmButtonText: 'Yes',
          cancelButtonText: 'No',
          allowOutsideClick: false,
          position: 'center',
          focusConfirm: false,
          showLoaderOnConfirm: true,
          }).then((result) => {

      if (result.value) {
        var params = { id: _this.quotationDetails.id };
        _this.CS.generateCustomerQuotation(params).subscribe(response => {
          _this.loaderItem[_this.quotationDetails.id] = false;
          if (response && response.status == "success" && response.result) {
            _this.loading = false;
            this.toastr.success('Customer Quotation Generated!', 'Success!');
            _this.router.navigate(['/quotation/edit/' + response.result.id]);

            if (response.warning_message) {
              this.toastr.warning(response.warning_message, 'Warning!');
            }
          } else if (response && response.status == "show_error") {
            _this.loading = false;
            this.toastr.error(response.message, 'Error');
          } else {
            _this.loading = false;
            this.toastr.error('Something Went Wrong!', 'Failed!');
          }
        });

      }
          else if (result.dismiss === Swal.DismissReason.cancel) {
              _this.loading = false;
              _this.submitted = false;
              _this.tcdSubmitted = false;
      /*Swal.fire(
          'Cancelled',
          'No change in vendor',
          'error'
        )*/
      }
    })


    }
  }

  viewPdf() {
    this.previewFn = 1;


    /*if (this.previewFn) {
      if (!confirm("Make sure all the changes are saved before preview. Do you want to continue?")) {
        return false;
      }
    }*/

    this.toastr.info('Loading Preview!', 'Loading..');
    this.generateQuotationPdfOrMail();
  }
  generateQuotationPdfOrMail() {

    let _this = this
    _this.loading = true;

    if (this.quotationDetails.quotationDate == null) {
      alert("Please enter Quotation Date");
      return false;
    }


    let email = (localStorage.getItem('mdata'));
    _this.CS.generateQuotationPdfOrMail({ 'quotation': _this.quotationDetails, preview: this.previewFn, setterMail: email }).subscribe(response => {
      console.log("_this.quotationDetails", _this.quotationDetails);
      _this.loading = false;
      if (response && response.status == "success") {
        if (this.previewFn) {
          // open file
          window.open(this.UPLOAD_PATH + 'quotationPdf/' + response.result.documentName, '_blank');
          this.toastr.info('Verify The Details!', 'Info');

        } else {
          this.toastr.success('Successfuly Sent PO to Vendor!', 'Success!');
          alert("updateee....... status here")
          // this.updateQuotationStatus(this.id, 2);
          if (typeof (response.customMessage) != 'undefined') {
            this.toastr.error(response.customMessage, 'Failed!');
          }
        }

      } else {
        this.toastr.error(response.customMessage, 'Failed!');
      }
      this.previewFn = 0;

    });
  }


  isQuotationDateLessthanEnqDate(_this) {
    let enq_date = (_this.enqDetails) ? _this.enqDetails.enqDate : null;
    let quot_date = _this.toDateObject(_this.quotationForm.get('quotationDate').value);
    if (quot_date) {
      // remove time info from date time,return date only
      let enq_dateDateObj = new Date(enq_date).toDateString();
      enq_date = new Date(enq_dateDateObj);
      let quot_dateDateObj = new Date(quot_date);
      if (quot_dateDateObj < enq_date) {
        return true;
      }
      return false;
    }
  }

  includeCustomerQuotation(event) {
    let _this = this;
    let checked = false;
    if (event == true) {
      checked = true;
    } else {
      let inp = (event.target as HTMLInputElement);
      checked = inp ? inp.checked: event;
    }

    console.log("checked---->", checked);
    _this.inclCustQuotation = checked;
    this.validateItems();
  }

  validateItems() {
    let _this = this;
    _this.QuotService.validateItems(_this, _this.inclCustQuotation);
  }

  toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date) {
    return this.CF.dateObjectToJsonDate(date)
  }
  createForm() {
    let _this = this;
    this.quotationForm = this.formBuilder.group({
      id: _this.selectedQuotationId,
      enqXid: [_this.selectedEnqId],
      //enqNumber: _this.selectedEnqNumber,
      quotationDate: [null, Validators.required],
      quotationCategory: [1, Validators.required],
      //vendorXid: ['', Validators.required],
      //vendorName: ['', Validators.required],
      //customerXid: [''],
      //customerName: ['', Validators.required],
      //currencyXid: [null],
      //currency: [null],
      //exRate: [null],
      custCurrencyXid: [null, Validators.required],
      custCurrency: [null, Validators.required],
      custExRate: [null, Validators.required],
      vendCurrencyXid: [null, Validators.required],
      vendCurrency: [null, Validators.required],
      vendExRate: [null, Validators.required],
      custAmountUsd: [null],
      vendAmountUsd: [null],
      fromVendorCost: [0],
      fromVendorCostWithTCD: [0],
      toCustomerCost: [0],
      toCustomerCostWithTCD: [0],
      custTCD: [null],
      vendTCD: [null],
      companyXid: [this.company],
      includeCustomerQuotation: [false],
      deliveryDate: ['', Validators.required],
      payTermId: [''],
      payTerm: ['',Validators.required],
      payModeId: [null],
      payMode: ['',Validators.required],
      incoTermId: [null],
     incoPlace: ['',Validators.required],
     incoTerm: ['',Validators.required],
      remarks: ['', Validators.required],
      custQtGenDate:[''],
      convertedToCustomerCost :[''],
      convertedToCustomerCostWithTCD:[''],
      convertedcustTCD:[''],
      details: this.formBuilder.array([]),
      tcdDetails: this.formBuilder.array([]),
      vendors: this.formBuilder.array([]),
      documents: this.formBuilder.array([])
    });
    this.createItems();
    this.createVendors();
    this.createDocuments();
    this.createTcdItems();
    console.log(" this.purchaseForm", this.quotationForm);
  }

  createItems(flag?) {
    let _this = this;
    if (flag) {
      if (this.quotationForm.controls.details.status == 'INVALID') {
        this.submitted = true;
        return false;
      }
    }
    let item = this.formBuilder.group({
      id: [null],
      groupXid: [null],
      group: ['', Validators.required],
      description: ['', Validators.required],
      typeXid: [null],
      type: ['', Validators.required],
      uomXid: [null],
      uom: ['', Validators.required],
      qtQty: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
      smeProfit: [{ value: '', disabled: true }, [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)]],
      venQtUnitPrice: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      vendQtCost: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      custQtUnitPrice: [{ value: '', disabled: true }, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      custQtCost: [{ value: '', disabled: true }, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      status: 1,
      enqXid: [_this.selectedEnqId],
      groupNameAuto: [''],
      uomNameAuto: [''],
    });
    this.itemsArray = this.quotationForm.get('details') as FormArray;
    this.itemsArray.push(item);

    this.validateItems();
  }

  createVendors() {
    let item = this.formBuilder.group({
      id: null,
      date: [''],
      details: [''],
      vendorXid: [null],
      vendorName: [null],
      quotationNumber: [null],
      quotationXid: [null],
      status: 1
    });
    this.itemsVendArray = this.quotationForm.get('vendors') as FormArray;
    this.itemsVendArray.push(item);
  }
  createTcdItems(flag?) {

    console.log("this.quotationForm.controls.tcdDetails");
    if (flag) {
      if (this.quotationForm.controls.tcdDetails.status == 'INVALID') {
        this.tcdSubmitted = true;
        return false;
      }
    }
    let item = this.formBuilder.group({
      tcdType: [null, Validators.required],
      tcdName: [null],
      tcdDesc: [null],
      flatOrPercentage: [null, Validators.required],
      amount: [null, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      tcdAmount: [null, [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      status: 1
    });
    this.itemsTcdArray = this.quotationForm.get('tcdDetails') as FormArray;
    this.itemsTcdArray.push(item);
  }
  createDocuments() {
    let _this = this;
    let item = this.formBuilder.group({
      enqXid: [_this.selectedEnqId],
      fileName: [''],
      fileDisplayName: [''],
      filePath: [''],
      companyXid: [null],
      status: 1
    });
    _this.itemsDocsArray = this.quotationForm.get('documents') as FormArray;
    _this.itemsDocsArray.push(item);
  }



  setForm() {
    let _this = this;

    this.quotationForm = this.formBuilder.group({
      id: _this.selectedQuotationId,
      enqXid: _this.selectedEnqId,
      // enqNumber: _this.selectedEnqNumber,
      quotationDate: [this.quotationDetails.quotationDate ? this.toJsonDate(this.quotationDetails.quotationDate) : null, Validators.required],
      quotationCategory: [this.quotationDetails.quotationCategory],
      //vendorXid: ['', Validators.required],
      //vendorName: ['', Validators.required],
      //customerXid: [this.quotationDetails.customerXid, Validators.required],
      //customerName: [this.quotationDetails.customerName, Validators.required],
      //currencyXid: [null],
      //currency: [null],
      //exRate: [null],
      custCurrencyXid: [this.quotationDetails.custCurrencyXid, Validators.required],
      custCurrency: [this.quotationDetails.custCurrency, Validators.required],
      custExRate: [this.quotationDetails.custExRate, Validators.required],
      vendCurrencyXid: [this.quotationDetails.vendCurrencyXid, Validators.required],
      vendCurrency: [this.quotationDetails.vendCurrency, Validators.required],
      vendExRate: [this.quotationDetails.vendExRate, Validators.required],
      custAmountUsd: [this.quotationDetails.custAmountUsd],
      vendAmountUsd: [this.quotationDetails.vendAmountUsd],
      fromVendorCost: [this.quotationDetails.fromVendorCost],
      fromVendorCostWithTCD: [this.quotationDetails.fromVendorCostWithTCD],
      toCustomerCost: [this.quotationDetails.toCustomerCost],
      toCustomerCostWithTCD: [this.quotationDetails.toCustomerCostWithTCD],
      custTCD: [this.quotationDetails.custTCD],
      vendTCD: [this.quotationDetails.vendTCD],
      companyXid: [this.company],
      includeCustomerQuotation: [(this.quotationDetails.toCustomerCost > 0) ? true : false],
      deliveryDate: [this.quotationDetails.deliveryDate ? this.toJsonDate(this.quotationDetails.deliveryDate) : null, Validators.required],
      payTermId: [JSON.parse(this.quotationDetails.payTermId)],
      payTerm: [this.quotationDetails.payTerm,Validators.required],
      payModeId: [JSON.parse(this.quotationDetails.payModeId)],
      payMode: [this.quotationDetails.payMode,Validators.required],
      incoTermId: [JSON.parse(this.quotationDetails.incoTermId)],
      incoPlace: [this.quotationDetails.incoPlace,Validators.required],
      incoTerm: [this.quotationDetails.incoTerm,Validators.required],
      remarks: [this.quotationDetails.remarks, Validators.required],
      custQtGenDate:[this.toJsonDate(this.quotationDetails.custQtGenDate)],
      convertedToCustomerCost:[this.quotationDetails.convertedToCustomerCost],
      convertedToCustomerCostWithTCD:[this.quotationDetails.convertedToCustomerCostWithTCD],
      convertedcustTCD:[this.quotationDetails.convertedcustTCD],
      details: this.formBuilder.array([]),
      tcdDetails: this.formBuilder.array([]),
      vendors: this.formBuilder.array([]),
      documents: this.formBuilder.array([]),
    });
    this.setItems();
    this.setVendors();
    this.setDocuments();
    this.setTcdItems();
    if (this.quotationDetails.toCustomerCost > 0) {
      this.includeCustomerQuotation(true);
    }
    else {
      this.includeCustomerQuotation(false);
    }
    console.log(" this.purchaseForm", this.quotationForm);
    this.resetAndReCalculate();
  }

  setItems() {
    let _this = this;
    if (_this.quotationDetails && _this.quotationDetails.QD && _this.quotationDetails.QD.length) {
      let canAddAmount = (_this.quotationDetails.toCustomerCost) ? false : true;
      _.forEach(_this.quotationDetails.QD, function (field, index) {
        let item = _this.formBuilder.group({
          id: [field.id, Validators.required],
          groupXid: [field.groupXid],
          group: [field.group, Validators.required],
          description: [field.description, Validators.required],
          typeXid: [field.typeXid],
          type: [field.type, Validators.required],
          uomXid: [field.uomXid],
          uom: [field.uom, Validators.required],
          qtQty: [field.qtQty ? field.qtQty.toFixed(2) : field.qtQty, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
          smeProfit: [{
            value: field.smeProfit ? field.smeProfit.toFixed(2) : field.smeProfit,
            disabled: canAddAmount
          }, [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)]],
          venQtUnitPrice: [field.venQtUnitPrice ? field.venQtUnitPrice.toFixed(3) : field.venQtUnitPrice, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          vendQtCost: [field.vendQtCost ? field.vendQtCost.toFixed(3) : field.vendQtCost, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          custQtUnitPrice: [{
            value: field.custQtUnitPrice ? field.custQtUnitPrice.toFixed(3) : field.custQtUnitPrice,
            disabled: canAddAmount
          }, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          custQtCost: [{ value: field.custQtCost ? field.custQtCost.toFixed(3) : field.custQtCost, disabled: true }, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          status: 1,
          enqXid: _this.selectedEnqId,
          groupNameAuto: [field.group],
          uomNameAuto: [field.uom]
        });
        _this.itemsArray = _this.quotationForm.get('details') as FormArray;
        _this.itemsArray.push(item);
        //_this.itemTotals[index] = field.vendQtCost;
        //_this.calculateCost(index);
        console.log("item", item);
      });
    }
    else {
      this.createItems();
    }
  }

  setVendors() {
    let _this = this;
    if (_this.quotationDetails && _this.quotationDetails.QV && _this.quotationDetails.QV.length) {
      _.forEach(_this.quotationDetails.QV, function (field, index) {

        let item = _this.formBuilder.group({
          id: [field.id],
          date: [field.date],
          details: [field.details],
          vendorXid: [field.vendorXid],
          vendorName: [field.vendorName],
          quotationNumber: [field.quotationNumber],
          quotationXid: [field.quotationXid],
          status: 1
        });
        _this.itemsVendArray = _this.quotationForm.get('vendors') as FormArray;
        _this.itemsVendArray.push(item);

      });
    }
    else {
      this.createVendors();
    }
  }

  setDocuments() {
    let _this = this;
    if (_this.quotationDetails && _this.quotationDetails.QDO && _this.quotationDetails.QDO.length) {
      _.forEach(_this.quotationDetails.QDO, function (field, index) {
        console.log("documentttttttttttt", field)
        let item = _this.formBuilder.group({
          enqXid: [field.enqXid],
          fileName: [field.fileName],
          fileDisplayName: [field.fileDisplayName],
          filePath: [field.filePath],
          companyXid: [_this.company],
          status: 1
        });
        _this.itemsDocsArray = _this.quotationForm.get('documents') as FormArray;
        _this.itemsDocsArray.push(item);
      });
    }
  }

  setTcdItems() {
    let _this = this;
    if (_this.quotationDetails.QT && _this.quotationDetails.QT.length) {
      _.forEach(_this.quotationDetails.QT, function (field, index) {
        let item = _this.formBuilder.group({
          id: [field.id],
          tcdType: [field.tcdType, Validators.required],
          tcdName: [field.tcdName],
          tcdDesc: [field.tcdDesc],
          flatOrPercentage: [field.flatOrPercentage, [Validators.required]],
          amount: [field.amount ? field.amount.toFixed(3) : field.amount, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          tcdAmount: [field.tcdAmount ? field.tcdAmount.toFixed(3) : field.tcdAmount, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          status: 1,
        });
        _this.itemsTcdArray = _this.quotationForm.get('tcdDetails') as FormArray;
        _this.itemsTcdArray.push(item);

      });
    }
    else {
      this.createTcdItems();
    }

  }

  // convenience getter for easy access to form fields
  get form() {
    return this.quotationForm.controls;
  }
  deleteItems(index) {
    this.itemsArray = this.quotationForm.get('details') as FormArray;
    var element = (<FormArray>this.quotationForm.controls['details']).at(index);
    if ((<FormArray>element).controls['id'].value) {
      this.deletedItems.push((<FormArray>element).controls['id'].value);
    }
    this.itemsArray.removeAt(index);
    this.resetAndReCalculate();
  }

  deleteTcdItems(index) {
    this.itemsTcdArray = this.quotationForm.get('tcdDetails') as FormArray;
    this.itemsTcdArray.removeAt(index);
    this.resetAndReCalculate();
  }

  validateTcdValue(index) {
    this.itemsTcdArray = this.quotationForm.get('tcdDetails') as FormArray;
    if (this.itemsTcdArray.controls[index].get('flatOrPercentage').value == globalConstants.PER && this.itemsTcdArray.controls[index].get('amount').value > 100) {
      this.itemsTcdArray.controls[index].get('amount').setValue('100.000');
    }
  }
  validateSMEprofitValue(index) {
    this.itemsArray = this.quotationForm.get('details') as FormArray;
    if (this.itemsArray.controls[index].get('smeProfit').value > 100) {
      this.itemsArray.controls[index].get('smeProfit').setValue('100.00');
    }
  }
  deleteDocFromServer(doc, index) {
    console.log("doc", doc);
    var params = { id: doc.id, model_name: 'quotationDocument' };
    let _this = this;
    _this.loader[doc.id] = true;
    _this.CS.removeData(params).subscribe(response => {
      _this.loader[doc.id] = false;
      if (response && response.status == "success" && response.result) {
        _this.quotationDetails.QDO.splice(index, 1);
      } else {
        this.toastr.error('Something Went Wrong!', 'Failed!');
      }
    });
  }
  getQuotationDetails() {
    let _this = this;
    let params = { 'id': this.selectedQuotationId };
    _this.CS.getQuotationDetailsById(params).subscribe(response => {
      console.log("quotationDetails", response);
      if (response && response.status == "success" && response.result) {

        _this.enqDetails = response.result.QHEH;
        _this.selectedEnqNumber = response.result.enqNumber;
        _this.quotationDetails = response.result;
        _this.quotationDetails.customerCurrency = response.result.currencyXid;
        _this.selectedEnqId = _this.quotationDetails.enqXid;
        // remove quotation vendor from list
        _this.quotationReceivedVendors = _.filter(response.quotationReceivedVendors, function (row, index) {
          if(_this.quotationDetails && _this.quotationDetails.quotationCategory == 2){
            // if this customer quotation then select its vendor quotation details
            return response.result.vendorQuotationXid == row.id
          }else{
            // select all other quotations from other vendors
            return response.result.vendorXid != row.vendorXid           
          }
          
        });
        _this.min_enqDate = this.CF.dateObjectToJsonDate(new Date(_this.enqDetails.enqDate))

        this.setForm();
        this.selectVenorCustomerCurrencyRate();

        console.log("_this.enqDetails", _this.enqDetails);
      } else {
        let erMsg = (response.result == null) ? "No Such Quotation Exist" : 'Something Went Wrong';
        this.toastr.error(erMsg, 'Failed!');
        if (response.result == null) {
          _this.router.navigate(['/quotation/list']);
        }
      }
    });
  }

  getCurrencies() {
    let _this = this;
    let params = { model_name: 'currencies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencies = response.result;
      } else {
        _this.currencies = [];
      }
      _this.selectVenorCustomerCurrencyRate();
    });
  }

  getItemGroups() {
    let _this = this;
    _this.QuotService.getItemGroups(_this).subscribe(response => {
      _this.itemGroups = response;
    });
  }

  getTypes() {
    let _this = this;
    _this.QuotService.getTypes(_this).subscribe(response => {
      _this.itemTypes = response;
    });
  }

  getTcdTypes() {
    let _this = this;
    _this.CS.getData({ model_name: 'tcdTypes', where: { 'status': 1 } }).subscribe(response => {
      if (response && response.status == "success" && response.result) {
        _this.tcdTypes = response.result;
      } else {
        _this.tcdTypes = [];
      }
    });
  }

  getUoms() {
    let _this = this;
    _this.QuotService.getUoms(_this).subscribe(response => {
      _this.uoms = response;
    });
  }

  getExchangeRate() {
    let _this = this;
    let params = {};
    _this.CS.getLatestCurrencyExchangeRates(params).subscribe(response => {
      console.log("exchangeRate", response);
      if (response && response.status == "success" && response.result) {
        let exchangeRates = [];
        _.map(response.result, function (item) {
          if (item.ER && item.ER.length > 0) {
            exchangeRates.push(item.ER[0]);
          }
        });
        _this.exchangeRate = exchangeRates;
        console.log(" _this.exchangeRate", _this.exchangeRate);
      } else {
        _this.exchangeRate = [];
      }
      _this.selectVenorCustomerCurrencyRate();
    });
  }

  selectVenorCustomerCurrencyRate() {
    // select customer & vendor currency rates
    let _this = this;

    // select & set Customer currency | if already set then use that rate
    let cust_currency_id = 0;

    if (_this.quotationDetails && _this.quotationDetails.custCurrencyXid && _this.quotationDetails.custExRate) {
      cust_currency_id = _this.quotationDetails.custCurrencyXid;
      //_this.cust_currencyShortKey = _this.quotationDetails.custCurrency;
      _this.cust_currency_rate = _this.quotationDetails.custExRate;
    } else {
      console.log("---------------c _this.quotationDetails", _this.quotationDetails)
      cust_currency_id = (_this.quotationDetails && _this.quotationDetails.customerCurrency) ? parseInt(_this.quotationDetails.customerCurrency) : 0;
      var currency_rate_c = _.find(_this.exchangeRate, { currencyXId: cust_currency_id });
      _this.cust_currency_rate = (currency_rate_c && currency_rate_c.Rate) ? currency_rate_c.Rate : 0;

      console.log("---------------cust_currency_idcust_currency_id", cust_currency_id)
      console.log("---------------cust_currency_  bbbbbb currency_rate_c", currency_rate_c)
    }
   
    _this.cust_curencyObj = _.find(_this.currencies, { id: cust_currency_id });
    _this.cust_currencySymbol = _this.cust_curencyObj ? _this.cust_curencyObj.symbol : ''
    _this.cust_currencyShortKey = _this.cust_curencyObj ? _this.cust_curencyObj.shortKey : ''
    _this.cust_currencyFullName = _this.cust_curencyObj ? _this.cust_curencyObj.name : ''

    this.quotationForm.get('custCurrencyXid').setValue(cust_currency_id);
    this.quotationForm.get('custCurrency').setValue(_this.cust_currencyShortKey);
    this.quotationForm.get('custExRate').setValue(_this.cust_currency_rate);

    // select & set vendor currency | if already set then use that rate
    let vend_currency_id = 0;

    if (_this.quotationDetails && _this.quotationDetails.vendCurrencyXid && _this.quotationDetails.vendExRate) {
      vend_currency_id = _this.quotationDetails.vendCurrencyXid;
      //_this.cust_currencyShortKey = _this.quotationDetails.custCurrency;
      _this.vend_currency_rate = _this.quotationDetails.vendExRate;
    } else {
      // if no vendor currency then use customer currency for quotation
      vend_currency_id = (_this.quotationDetails && _this.quotationDetails.customerCurrency) ? parseInt(_this.quotationDetails.customerCurrency) : 0;
      var currency_rate_v = _.find(_this.exchangeRate, { currencyXId: vend_currency_id });
      _this.vend_currency_rate = (currency_rate_v && currency_rate_v.Rate) ? currency_rate_v.Rate : 0;
    }

    _this.vend_currencyObj = _.find(_this.currencies, { id: vend_currency_id });
    _this.vend_currencySymbol = _this.vend_currencyObj ? _this.vend_currencyObj.symbol : ''
    _this.vend_currencyShortKey = _this.vend_currencyObj ? _this.vend_currencyObj.shortKey : ''
    _this.vend_currencyFullName = _this.vend_currencyObj ? _this.vend_currencyObj.name : ''

    _this.quotationForm.get('vendCurrencyXid').setValue(vend_currency_id);
    _this.quotationForm.get('vendCurrency').setValue(_this.vend_currencyShortKey);
    _this.quotationForm.get('vendExRate').setValue(_this.vend_currency_rate);

    this.resetAndReCalculate();
  }


  resetAndReCalculate() {
    let _this = this;
    _this.customer_itemTotals = {};
    _this.vendor_itemTotals = {};
    _this.profit_itemTotals = {};
    _this.customer_itemTotal = 0;
    _this.vendor_itemTotal = 0;
    _this.profit_itemTotal = 0;

    _.forEach(_this.quotationForm.get('details')['controls'], function (value, index) {
      _this.calculateCost(index);
    });
  }
  calculateCost(index) {
    var _this = this;
    var element = (<FormArray>this.quotationForm.controls['details']).at(index);
    if ((<FormArray>element).controls['qtQty'].status == 'VALID' && (<FormArray>element).controls['venQtUnitPrice'].status == 'VALID') {

      var qty = (<FormArray>element).controls['qtQty'].value;
      var v_price = (<FormArray>element).controls['venQtUnitPrice'].value;
      var c_price = (<FormArray>element).controls['custQtUnitPrice'].value;
      var sme_profit = (<FormArray>element).controls['smeProfit'].value ? (<FormArray>element).controls['smeProfit'].value : 0.00;

      var customer_cost = null;
      var vendor_cost = null;
      sme_profit = sme_profit / 100;
      vendor_cost = qty * v_price;
      if (_this.inclCustQuotation && (<FormArray>element).controls['custQtUnitPrice'].status == 'DISABLED') {
        // Unit Price (Customer)= (Unit Price (Vendor)* SME profit) 
        // Cost (Customer) = (Unit Price(customer)* Qty)
        if (sme_profit > 0) {
          customer_cost = vendor_cost + (vendor_cost * sme_profit);
          c_price = customer_cost / qty;
          (<FormArray>element).controls['custQtUnitPrice'].setValue(c_price.toFixed(3));
        } else {
          customer_cost = qty * c_price;
        }
        this.customer_itemTotals[index] = customer_cost;
        (<FormArray>element).controls['custQtCost'].setValue(customer_cost.toFixed(3));
      }


      this.vendor_itemTotals[index] = vendor_cost;
      this.profit_itemTotals[index] = (vendor_cost * sme_profit);
      (<FormArray>element).controls['vendQtCost'].setValue(vendor_cost.toFixed(3));
      this.calculateTotalAmount();
    }
  }
  calculateTcdAmount() {
    this.calculateTotalAmount();
  }

  resetCalculation(_this) {
    _this.vendor_itemTotal = 0;
    _this.customer_itemTotal = 0;
    _this.profit_itemTotal = 0;
    _this.vend_tcdTotal = 0;
    _this.cust_tcdTotal = 0;
    _this.vend_tcdDiscount = 0;
    _this.cust_tcdDiscount = 0;
  }

  calculateTotalAmount() {
    let _this = this;
    this.resetCalculation(_this)
    var vendor_FinalCost = 0;
    var customer_FinalCost = 0;

    _.map(_this.vendor_itemTotals, function (item) {
      console.log("_this.vendor_itemTotal", item);
      _this.vendor_itemTotal = _this.vendor_itemTotal + item;
    });
    vendor_FinalCost = _this.vendor_itemTotal;
    _this.quotationForm.get('fromVendorCostWithTCD').setValue(vendor_FinalCost.toFixed(3));

    _.map(_this.customer_itemTotals, function (item) {
      console.log("_this.customer_itemTotal", item);
      _this.customer_itemTotal = _this.customer_itemTotal + item;
    });
    customer_FinalCost = _this.customer_itemTotal;
    _this.quotationForm.get('toCustomerCostWithTCD').setValue(customer_FinalCost.toFixed(3));

    _.map(_this.profit_itemTotals, function (item) {
      console.log("_this.profit_itemTotal", item);
      _this.profit_itemTotal = _this.profit_itemTotal + item;
    });

    //if (this.quotationForm.controls.details.status == 'VALID' && _this.vendor_itemTotals) {
    _this.quotationForm.get('fromVendorCost').setValue(_this.vendor_itemTotal.toFixed(3));
    _this.quotationForm.get('toCustomerCost').setValue(_this.customer_itemTotal.toFixed(3));
    //}

    // calculate TCD 
    //if (this.quotationForm.controls.tcdDetails.status == 'VALID' && _this.vendor_itemTotal) {
    _.forEach(_this.quotationForm.value.tcdDetails, function (value, index) {
      var element = (<FormArray>_this.quotationForm.controls['tcdDetails']).at(index);
      var rate = 0;
      var c_rate = 0;
      var v_rate = 0;
      if ((<FormGroup>element).controls['tcdType'].status == 'VALID' && (<FormGroup>element).controls['flatOrPercentage'].status == 'VALID' && (<FormGroup>element).controls['amount'].status == 'VALID') {

        rate = value.status == 1 ? parseFloat(value.amount) : 0;
        if (value.flatOrPercentage == globalConstants.PER) {
          rate = rate / 100;
          c_rate = (_this.inclCustQuotation) ? (_this.customer_itemTotal * rate) : 0; // calculate rate if checked include customer quotation 
          v_rate = (_this.vendor_itemTotal * rate);
        } else {
          v_rate = rate;
          c_rate = (_this.inclCustQuotation) ? (rate) : 0;  // calculate rate if checked include customer quotation 
        }
        console.log("vendor_itemTotal ----->", _this.vendor_itemTotal)
        console.log("customer_itemTotal ----->", _this.customer_itemTotal)
        console.log("c_rate ----->", c_rate)
        console.log("v_rate ----->", v_rate)
        console.log("value ----->", value)
        console.log("rate ----->", rate)

        if (value.tcdType == globalConstants.DISCOUNT) {
          // vendor discount details
          vendor_FinalCost = vendor_FinalCost - parseFloat(v_rate.toString());
          _this.vend_tcdDiscount = parseFloat(_this.vend_tcdDiscount.toString()) + parseFloat(v_rate.toString());
          _this.vend_tcdDiscount = parseFloat((_this.vend_tcdDiscount).toFixed(3));
          // customer discount details
          customer_FinalCost = customer_FinalCost - parseFloat(c_rate.toString());
          _this.cust_tcdDiscount = parseFloat(_this.cust_tcdDiscount.toString()) + parseFloat(c_rate.toString());
          _this.cust_tcdDiscount = parseFloat((_this.cust_tcdDiscount).toFixed(3));

        } else {
          // vendor tax/charge details
          vendor_FinalCost = vendor_FinalCost + parseFloat(v_rate.toString());
          _this.vend_tcdTotal = parseFloat(_this.vend_tcdTotal.toString()) + parseFloat(v_rate.toString());
          _this.vend_tcdTotal = parseFloat((_this.vend_tcdTotal).toFixed(3));
          // customer tax/charge details
          customer_FinalCost = customer_FinalCost + parseFloat(c_rate.toString());
          _this.cust_tcdTotal = parseFloat(_this.cust_tcdTotal.toString()) + parseFloat(c_rate.toString());
          _this.cust_tcdTotal = parseFloat((_this.cust_tcdTotal).toFixed(3));
        }
        _this.quotationForm.get('fromVendorCostWithTCD').setValue(vendor_FinalCost.toFixed(3));
        _this.quotationForm.get('toCustomerCostWithTCD').setValue(customer_FinalCost.toFixed(3));
        // quotation 
        (<FormArray>element).controls['tcdAmount'].setValue(_this.quotationDetails.quotationCategory ==1 ? v_rate.toFixed(3):c_rate.toFixed(3));
      } else {
        (<FormArray>element).controls['tcdAmount'].setValue(null);
      }

    });
    // }


    _this.toUsd();


    let custTCDVal = (_this.cust_tcdTotal - _this.cust_tcdDiscount);
    let vendTCDVal = (_this.vend_tcdTotal - _this.vend_tcdDiscount);
    this.quotationForm.get('custTCD').setValue(custTCDVal.toFixed(3));
    this.quotationForm.get('vendTCD').setValue(vendTCDVal.toFixed(3));

  }
  toUsd() {
    // convert toCustomerCost rate in USD
    let _this = this;
    var custCurencyObj = _.find(_this.currencies, { id: _this.quotationForm.value.custCurrencyXid });
    var vend_rate = _.find(_this.exchangeRate, { currencyXId: _this.quotationForm.value.vendCurrencyXid });
    var cust_rate = _.find(_this.exchangeRate, { currencyXId: _this.quotationForm.value.custCurrencyXid });
    _this.cust_currencySymbol = custCurencyObj ? custCurencyObj.symbol : ''
    this.QuotService.toUsd(_this.quotationForm, _this.exchangeRate, vend_rate, cust_rate)

    // customer currency conversion
    let v_curr_rate = (vend_rate) ? vend_rate.Rate : 0;
    let c_curr_rate = (cust_rate) ? cust_rate.Rate : 0;
    let v_toCustomerCost = this.quotationForm.get('toCustomerCost').value ? parseFloat(this.quotationForm.get('toCustomerCost').value) : 0;
    let v_toCustomerCostWithTCD = this.quotationForm.get('toCustomerCostWithTCD').value ? parseFloat(this.quotationForm.get('toCustomerCostWithTCD').value) : 0;

    if (v_curr_rate > 0 && c_curr_rate > 0) {
      _this.converted_toCustomerCost = parseFloat(((v_toCustomerCost / v_curr_rate) * c_curr_rate).toFixed(3));
      _this.converted_toCustomerCostWithTCD = parseFloat(((v_toCustomerCostWithTCD / v_curr_rate) * c_curr_rate).toFixed(3));
      _this.converted_cust_tcdTotal = parseFloat(((_this.cust_tcdTotal / v_curr_rate) * c_curr_rate).toFixed(3));
      _this.converted_cust_tcdDiscount = parseFloat(((_this.cust_tcdDiscount / v_curr_rate) * c_curr_rate).toFixed(3));
       _this.quotationForm.get('convertedToCustomerCost').setValue( _this.converted_toCustomerCost );
        _this.quotationForm.get('convertedToCustomerCostWithTCD').setValue(_this.converted_toCustomerCostWithTCD );
        _this.quotationForm.get('convertedcustTCD').setValue( _this.converted_cust_tcdTotal.toFixed(3));
    }

  }

  convertDate(dateString) {
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMONTHYY(objectDate);
  }
  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("from", from);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      console.log("getId", getId);
      if (isNaN(getId)) {
        if (index || index == 0) {
          var element = (<FormArray>this.quotationForm.controls['details']).at(index);
          if ((<FormArray>element).controls[to].value == getId) {
            return false;
          }
        } else {
          if (this.quotationForm.controls[to].value == getId) {
            return false;
          }
        }
        var check = {};
        check[dataField] = getId;
        var item = _.find(data, check);
      } else {
        console.log("is num");
        var item = _.find(data, { id: parseInt(getId, 10) });
      }
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.quotationForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.quotationForm.get(from).setValue(getId);
          }
          console.log("this.quotationForm", this.quotationForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.quotationForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.quotationForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.quotationForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.quotationForm.get(from).setValue(null);
        }
      }
    }
  }

  makeDecimalPoint(field?, index?, formArray?, point?) {
    let _this = this;
    if (index || index == 0) {
      var element = (<FormArray>this.quotationForm.controls[formArray]).at(index);
      var value = (<FormArray>element).controls[field].value;
      if (value == null) { return; }
      var data = {};
      data[field] = (parseFloat(value.toString()).toFixed(point ? 2 : 3));
      value && !isNaN(value) ? element.patchValue(data) : '';
      if(value && !isNaN(value) && parseFloat(value)  <= 0){
        element.get(field).setValue("")
      }
    } else {
      var value = _this.quotationForm.get(field).value;
      value && !isNaN(value) ? _this.quotationForm.get(field).setValue((parseFloat(value.toString()).toFixed(point ? 2 : 3))) : '';
    }
  }

  triggerFileUpload() {
    this.el.nativeElement.querySelector("#fileUpload").click();
  }

  deleteDoc(index) {
    this.fileToUpload.splice(index, 1);
    this.imageSrc.splice(index, 1)
  }
  upload(files: File[]) {

    let _this = this;
    _.forEach(files, function (value) {
       console.log("reader", value)
       var fileTypes = ['jpg', 'jpeg', 'png', 'xlsx', 'txt', 'gif', 'pdf', 'xls'];  //acceptable file types
        var extension = value.name.split('.').pop().toLowerCase() 
        console.log(extension)
        var Success = fileTypes.indexOf(extension) > -1
       if(Success) {
         if (value.size < 4096000) {
      var reader = new FileReader();
      reader.readAsDataURL(value);
      reader.onload = () => {
        var src = reader.result as string;
        _this.imageSrc.push(src);
        _this.fileToUpload.push(value);
      }
      }
        else {
        alert('Maximum allowed file size is 4 MB!');
        return
      }
      
    } else {
      alert('File type not supported')
    }
    });

    document.getElementById('fileUpload')['value'] = "";
  }

  clear(fields, index?, checkField?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = checkField && field == checkField ? '' : null;
        (<FormArray>_this.quotationForm.controls['details']).at(index).patchValue(data);
      } else {
        _this.quotationForm.get(field).setValue(checkField && field == checkField ? '' : null);
      }
    });

  }

  resetFileUpload() {
    let _this = this;
    _this.fileToUpload = [];
    _this.imageSrc = [];
  }


  fixSelectedData(name,index){
  console.log("name",name)
  console.log("index",index)

     var element = (<FormArray>this.quotationForm.controls['details']).at(index);

     if(name =='group') {
      (<FormArray>element).controls['groupNameAuto'].setValue( (<FormArray>element).controls['group'].value);
    }

     if(name =='uom') {
      (<FormArray>element).controls['uomNameAuto'].setValue( (<FormArray>element).controls['uom'].value);
    }

}

}
