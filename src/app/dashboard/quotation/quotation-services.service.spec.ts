import { TestBed } from '@angular/core/testing';

import { QuotationServicesService } from './quotation-services.service';

describe('QuotationServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuotationServicesService = TestBed.get(QuotationServicesService);
    expect(service).toBeTruthy();
  });
});
