import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuotationAddComponent } from './quotation-add/quotation-add.component';
import { QuotationListComponent } from './quotation-list/quotation-list.component';
import { QuotationEditComponent } from './quotation-edit/quotation-edit.component';
import { QuotationRoutingModule } from './quotation-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgSelectModule } from '@ng-select/ng-select';
import { ClickOutsideModule } from 'ng-click-outside';
@NgModule({
  declarations: [QuotationAddComponent, QuotationListComponent, QuotationEditComponent],
  imports: [
    CommonModule,
    QuotationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    NgbModule,
    NgxSpinnerModule,
    NgSelectModule,
    ClickOutsideModule
  ]
})
export class QuotationModule { }
