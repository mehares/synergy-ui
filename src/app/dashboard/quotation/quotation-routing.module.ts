import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 
import { QuotationAddComponent } from './quotation-add/quotation-add.component';
import { QuotationEditComponent } from './quotation-edit/quotation-edit.component';
import { QuotationListComponent } from './quotation-list/quotation-list.component';

const routes: Routes = [
  {
    path: 'list',
    component: QuotationListComponent,
    data:{permissions: 'quotation_edit' }
  },
  {
    path: 'add/enq/:enqid',
    component: QuotationAddComponent,
    data:{permissions: 'quotation_add' }
  },
  {
    path: 'add',
    component: QuotationAddComponent,
    data:{permissions: 'quotation_add' }
  },
    {
    path: 'edit/:id',
    component: QuotationEditComponent,
    data:{permissions: 'quotation_edit' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuotationRoutingModule { }
