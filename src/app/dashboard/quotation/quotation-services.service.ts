import { Injectable } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { CommonservicesService } from '../../helper/commonservices/commonservices.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuotationServicesService {

  quotationForm: FormGroup;
  itemGroups = []


  constructor(
    private CS: CommonservicesService,) { }

  toUsd(quotationForm, exchangeRate = [], vend_rate,cust_rate) {
    let _this = this;
    _this.quotationForm = quotationForm
    // convert toCustomerCostWithTCD rate in USD
    if (_this.quotationForm.value.custCurrencyXid && exchangeRate && exchangeRate.length) {
      console.log("slected rate----------*", cust_rate)
      if (vend_rate && cust_rate) {
        let v_curr_rate = (vend_rate) ? vend_rate.Rate : 0;
        let c_curr_rate = (cust_rate) ? cust_rate.Rate : 0;  
        
        //var custInUsd = (_this.quotationForm.value.toCustomerCostWithTCD / v_curr_rate) * c_curr_rate;
        var custInUsd = (_this.quotationForm.value.toCustomerCostWithTCD / v_curr_rate)
        var vendInUsd = _this.quotationForm.value.fromVendorCostWithTCD / v_curr_rate;
        this.quotationForm.get('custAmountUsd').setValue(custInUsd.toFixed(3));
        this.quotationForm.get('vendAmountUsd').setValue(vendInUsd.toFixed(3));
        this.quotationForm.get('custExRate').setValue(c_curr_rate);
      } else {
        //if no exchange rate       
        this.quotationForm.get('custAmountUsd').setValue(0);
        this.quotationForm.get('vendAmountUsd').setValue(0);
        this.quotationForm.get('custExRate').setValue(0);
      }
    }
    return true;
  }

  getItemGroups(_this) {
    return new Observable<any>(observer => {
      let params = { model_name: 'itemGroups', where: { 'status': 1 } };
      _this.CS.getData(params).subscribe(response => {
        console.log("return getItemGroups -->", response);
        if (response && response.status == "success" && response.result) {
          observer.next(response.result);
          observer.complete();
        } else {
          observer.next([]);
          observer.complete();
        }
      });
    });
  }
  
  getTypes(_this) {
    return new Observable<any>(observer => {
      let params = { model_name: 'poTypes', where: { 'status': 1 } };
      _this.CS.getData(params).subscribe(response => {
        console.log("return itemTypes -->", response);
        if (response && response.status == "success" && response.result) {
          observer.next(response.result);
          observer.complete();
        } else {
          observer.next([]);
          observer.complete();
        }
      });
    });
  }

  getUoms(_this) {
    return new Observable<any>(observer => {
      let params = { model_name: 'uom', where: { 'status': 1 } };
      _this.CS.getData(params).subscribe(response => {
        console.log("return UOM -->", response);
        if (response && response.status == "success" && response.result) {
          observer.next(response.result);
          observer.complete();
        } else {
          observer.next([]);
          observer.complete();
        }
      });
    });
  }

  validateItems(_this,inclCustQuotation){
    
    _.forEach(_this.quotationForm.get('details')['controls'], function (itemData, index) {
      console.log("itemData", itemData)
      
      itemData.get("smeProfit").setErrors(null);
      itemData.get("custQtCost").setErrors(null);
      itemData.get("custQtUnitPrice").setErrors(null);
      itemData.get("smeProfit").setValidators(null);
      itemData.get("custQtCost").setValidators(null);
      itemData.get("custQtUnitPrice").setValidators(null);

      if (inclCustQuotation) {
        itemData.get("smeProfit").setValidators([Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]);
        itemData.get("custQtCost").setValidators([Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]);
        itemData.get("custQtUnitPrice").setValidators([Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]);

        
        if (itemData.get('smeProfit').value == "") {
          itemData.get("smeProfit").setErrors({ required: true });
        }
        if (itemData.get('custQtCost').value == "") {
          itemData.get("custQtCost").setErrors({ required: true });
        }
        if (itemData.get('custQtUnitPrice').value == "") {
          itemData.get("custQtUnitPrice").setErrors({ required: true });
        }
        
        itemData.get("smeProfit").enable();
        itemData.get("custQtUnitPrice").disable();
      } else {
        itemData.get("smeProfit").setValidators([Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]);
        itemData.get("custQtCost").setValidators([Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]);
        itemData.get("custQtUnitPrice").setValidators([Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]);

        itemData.get("smeProfit").disable();
        itemData.get("custQtUnitPrice").disable();

        itemData.get("smeProfit").setValue(null);
        itemData.get("custQtCost").setValue(null);
        itemData.get("custQtUnitPrice").setValue(null);
      }
      
      console.log("_this.quotationForm.details...", _this.quotationForm.get('details'))
    });
  }

  
}
