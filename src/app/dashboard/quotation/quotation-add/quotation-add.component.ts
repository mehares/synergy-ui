import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';

import { QuotationServicesService } from '../quotation-services.service';
import { v4 as uuidv4 } from 'uuid';
import * as _ from 'lodash';
import { globalConstants } from '../../../constants/global-constants';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import Swal from 'sweetalert2' 

@Component({
  selector: 'app-quotation-add',
  templateUrl: './quotation-add.component.html',
  styleUrls: ['./quotation-add.component.scss']
})
export class QuotationAddComponent implements OnInit {

  submitted = false;
  showNoVendorMsg = false;
  tcdSubmitted = false;
  loading = false;
  tcdError = null;
  selectedEnqId = '';
  selectedEnqNumber = '';
  enqDetails = null;
  quotationForm: FormGroup;
  itemsArray: FormArray;
  itemsVendArray: FormArray;
  itemsTcdArray: FormArray;
  itemsDocsArray: FormArray;
  customer_itemTotals = {};
  vendor_itemTotals = {};
  profit_itemTotals = {};
  customer_itemTotal: any = 0;
  vendor_itemTotal: any = 0;
  profit_itemTotal: any = 0;
  cust_tcdTotal = 0;
  cust_tcdDiscount = 0;
  vend_tcdTotal = 0;
  vend_tcdDiscount = 0;
  converted_toCustomerCost = 0;
  converted_cust_tcdTotal = 0;
  converted_cust_tcdDiscount = 0;
  converted_toCustomerCostWithTCD = 0;
  company = JSON.parse(localStorage.getItem('companyXid'));
  permissions = this.CF.findPermissions();
  itemGroups: any = [];
  itemTypes: any = [];
  tcdTypes: any = [];
  uoms: any = [];
  exchangeRate: any = [];
  currencies: any = [];
  fileToUpload: File[] = [];
  imageSrc = [];
  enquiryList = [];
  keywordUom = 'uomName';
  keywordGroup = 'groupName';
  keywordEnquiry = 'enqNumber';
  initialKeywordEnquiry = 'SME/ENQ/';
  search_enq = 'SME/ENQ/';
  DOC_PATH = globalConstants.ENQ_DOC_PATH;
  UPLOAD_PATH = globalConstants.UPLOADS_DIR;
  FPtypes = globalConstants.FP;
  inclCustQuotation = false;
  cust_currency_rate = null;
  cust_curencyObj = null;
  cust_currencySymbol = null;
  cust_currencyShortKey = null;
  cust_currencyFullName = null;
  vend_currency_rate = null;
  vend_currencyObj = null;
  vend_currencySymbol = null;
  vend_currencyShortKey = null;
  vend_currencyFullName = null;
  searchEnquiryList = new Subject<string>();
  todayDate: any;
  min_enqDate: any;
  showQuotationMessage = false
  vendorSel =null
  vendorIdSelected = null
   @ViewChild('auto') auto;
   showPage = null
  PayTerms = globalConstants.ENQPayTerms;
  PayModes = globalConstants.ENQPayModes;
  IncoTerms = globalConstants.ENQIncoTerms;
  vendorChoose = false
  vendorDate = null
  first = true

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private http: HttpClient, private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private CS: CommonservicesService,
    private CF: CommonFunctionsService,
    private el: ElementRef,
    private QuotService: QuotationServicesService
  ) {

    this.searchEnquiryList.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.searchEnquiry(value);
      });

  }

  ngOnInit() {

    let year_str = new Date().getFullYear();
    this.search_enq = this.search_enq + year_str.toString().slice(2) + '/';
    this.selectedEnqId = this.route.snapshot.paramMap.get('enqid');
    this.todayDate = this.CF.dateObjectToJsonDate(new Date())

    if (this.selectedEnqId) {
      this.showPage = this.selectedEnqId
      this.getCurrencies();
      this.getExchangeRate();
      this.createForm();
      this.getTypes();
      this.getItemGroups();
      this.getUoms();
      this.getTcdTypes();
      this.getEnquiryDetails();
    }


    this.todayDate = this.CF.dateObjectToJsonDate(new Date())
    this.searchEnquiry(this.search_enq)
    setTimeout(()=>{
        this.auto.focus();
        })

  }

  searchEnquiry(val) {
    console.log("serach val", val)

    let _this = this;
    _this.CS.searchEnquiryForGenQuotation({ data: val }).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.enquiryList = response.result;
      } else {
        _this.enquiryList = [];
      }
    })
  }
  selectEnquiryFromList(event) {
    let _this = this;
    if (_this.initialKeywordEnquiry != event) {

      if (event.EV && event.EV.length > 0) {
        if (event.id && event.id > 0) {
          if (event.EV.length == 1 && (event.EV[0].vendorXid == 0 || event.EV[0].vendorXid == null)) {
            _this.showNoVendorMsg = true;
            _this.showQuotationMessage = false
          } else {
            var vendorStat = []
             _.forEach(event.EV, function (field, index) {
               vendorStat.push(field.enquiryStatus +'')
             })
             console.log("vendorStat",vendorStat)
             if(!vendorStat.includes('1') && !vendorStat.includes('null')) {
               console.log('vendor quotation Already Generated')
               _this.showQuotationMessage = true
               _this.showNoVendorMsg = false;
               return
             }
            _this.showNoVendorMsg = false;
            _this.showQuotationMessage = false
            _this.selectedEnqId = event.id;
            _this.router.navigate(['/quotation/add/enq/' + event.id])
              .then(() => {
                this.ngOnInit()
                
                
              });

          }
        } else {
          _this.showNoVendorMsg = true;
        }
      } else {
        _this.showNoVendorMsg = true;
      }
    }



  }
  onSubmit() {
    let _this = this;
    this.submitted = true;
    let formData = new FormData();
    // check invoice send date
    if (this.isQuotationDateLessthanEnqDate(_this)) {
      this.toastr.error('Quotation date should greater than enquiry date!', 'Error!');
      return;
    }
    // check TCD details valid
    if (_this.quotationForm.value.tcdDetails.length > 1 && this.quotationForm.controls.tcdDetails.status == 'VALID') {
      var checkDiscount = _.countBy(_this.quotationForm.value.tcdDetails, function (tcd) {
        return tcd.tcdType == globalConstants.DISCOUNT;
      });
      if (checkDiscount.true > 1) {
        _this.tcdError = 'Only one discount row allowed!';
        return false;
      }
    } else if (_this.quotationForm.value.tcdDetails.length == 1) {
      console.log("length1");
      if (!_this.quotationForm.value.tcdDetails[0].tcdType && !_this.quotationForm.value.tcdDetails[0].flatOrPercentage && !_this.quotationForm.value.tcdDetails[0].amount && !_this.quotationForm.value.tcdDetails[0].tcdAmount) {
        this.itemsArray = this.quotationForm.get('tcdDetails') as FormArray;
        this.itemsArray.removeAt(0);
      } else {
        _this.tcdSubmitted = true;
      }
    } else {
      _this.tcdSubmitted = true;
    }

    // check total amout greater than 0
    if (_this.quotationForm.value.fromVendorCostWithTCD < 0 || _this.quotationForm.value.toCustomerCostWithTCD < 0) {
      _this.tcdError = 'Discount value should be less than total cost!';
      return false;
    }

    console.log("this.quotationForm", _this.quotationForm);
    console.log("_this.quotationForm.details...", _this.quotationForm.get('details'))
    console.log("this.quotationForm.status", this.quotationForm.status)

    _this.quotationForm.value['quotationDate'] = this.toDateObject(_this.quotationForm.get('quotationDate').value)
     _this.quotationForm.value['deliveryDate'] = this.toDateObject(_this.quotationForm.get('deliveryDate').value)
    
    console.log("**********************");

    
    if (this.fileToUpload.length) {
      for (var i = 0; i < this.fileToUpload.length; i++) {
        formData.append("files", this.fileToUpload[i], uuidv4() + this.fileToUpload[i].name);
      }
    }


    if (this.quotationForm.status == 'VALID') {
      console.log("quotationForm",_this.quotationForm.controls)

      _this.loading = true;
      Swal.fire({ 
      title: 'Confirm Submitting Quotation',
      text: 'Have you confirmed the details before submit?',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    allowOutsideClick: false,
    position: 'center',
    }).then((result) => {

      if (result.value) {
      _this.spinner.show();
      setTimeout(() => this.spinner.hide(), 2000);
       _this.quotationForm.value['details'] =  _this.quotationForm.getRawValue().details
       _this.quotationForm.value['tcdDetails'] =  _this.quotationForm.getRawValue().tcdDetails
      _this.CS.createQuotation(_this.quotationForm.value).subscribe(response => {
        if (response && response.status == "success") {
          if (this.fileToUpload.length) {
            //formData.append("data selectedEnqId", _this.selectedEnqId);
            formData.append("data", response.result.id);
            _this.CS.saveQuotationDocuments(formData).subscribe(response => {
              _this.loading = false;
              _this.submitted = false;
              _this.tcdSubmitted = false;
              if (response && response.data && response.data.status == "success") {
                console.log(response);
                this.toastr.success('Quotation Generated.', 'Success!');
                //this.resetFileUpload();
                this.getEnquiryDetails();
                _this.vend_tcdTotal =0
               _this.vend_tcdDiscount = 0
               _this.converted_cust_tcdTotal =0
               _this.converted_cust_tcdDiscount = 0
               _this.converted_toCustomerCostWithTCD = 0
                _this.converted_toCustomerCost = 0
              } else {
                this.toastr.error('Something Went Wrong!', 'Failed!');
              }
            });
            // stay in same page, _this.router.navigate(['/quotation/list']);
          } else {
            _this.loading = false;
            _this.submitted = false;
            _this.tcdSubmitted = false;
            this.toastr.success('Quotation Generated.', 'Success!');
            this.getEnquiryDetails();
            _this.vend_tcdTotal =0
             _this.vend_tcdDiscount = 0
             _this.converted_cust_tcdTotal =0
             _this.converted_cust_tcdDiscount = 0
             _this.converted_toCustomerCostWithTCD = 0
               _this.converted_toCustomerCost = 0
          }

          if (response.warning_message) {
            this.toastr.warning(response.warning_message, 'Warning!');
          }
        } else if (response && response.status == "quotation_exist") {
          _this.loading = false;
          this.toastr.warning(response.message, 'Already Generated!');
        } else if (response && response.status == "warning_message") {
          _this.loading = false;
          this.toastr.warning(response.message, 'Warning!');
        } else if (response && response.status == "show_message") {
          _this.loading = false;
          this.toastr.warning(response.message, 'Warning!');
        } else {
          _this.loading = false;
          this.toastr.error('Something Went Wrong!', 'Failed!');
        }
      });

      _this.submitted = false;
      _this.tcdSubmitted = false;
    }
    else if (result.dismiss === Swal.DismissReason.cancel) {
              _this.loading = false;
              _this.submitted = false;
              _this.tcdSubmitted = false;
      /*Swal.fire(
          'Cancelled',
          'No change in vendor',
          'error'
        )*/
      }
    })

    } else {
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
    }


  }


  isQuotationDateLessthanEnqDate(_this) {
    let enq_date = (_this.enqDetails) ? _this.enqDetails.enqDate : null;
    let quot_date = _this.toDateObject(_this.quotationForm.get('quotationDate').value);
    if (quot_date) {
      // remove time info from date time,return date only
      let enq_dateDateObj = new Date(enq_date).toDateString();
      enq_date = new Date(enq_dateDateObj);
      let quot_dateDateObj = new Date(quot_date);
      if (quot_dateDateObj < enq_date) {
        return true;
      }
      return false;
    }
  }

  includeCustomerQuotation(event) {
    let _this = this;
    let checked = false;
    if (event == true) {
      checked = true;
    } else {
      let inp = (event.target as HTMLInputElement);
      checked = inp.checked;
      console.log("checked", checked);
    }
    _this.inclCustQuotation = checked;
    this.validateItems();
    this.resetAndReCalculate();
  }

  validateItems() {
    let _this = this;
    _this.QuotService.validateItems(_this, _this.inclCustQuotation);
  }


  toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date) {
    return this.CF.dateObjectToJsonDate(date)
  }

  createForm() {
    let _this = this;
    this.quotationForm = this.formBuilder.group({
      enqXid: _this.selectedEnqId,
      enqNumber: _this.selectedEnqNumber,
      quotationDate: [null, Validators.required],
      quotationCategory: [1, Validators.required],
      vendorXid: ['', Validators.required],
      vendorName: ['', Validators.required],
      customerXid: [''],
      customerName: ['', Validators.required],
      custCurrencyXid: [null, Validators.required],
      custCurrency: [null, Validators.required],
      custExRate: [null, Validators.required],
      vendCurrencyXid: [null, Validators.required],
      vendCurrency: [null, Validators.required],
      vendExRate: [null, Validators.required],
      custAmountUsd: [null],
      vendAmountUsd: [null],
      fromVendorCost: [0],
      fromVendorCostWithTCD: [0],
      toCustomerCost: [0],
      toCustomerCostWithTCD: [0],
      custTCD: [null],
      vendTCD: [null],
      companyXid: [this.company],
      includeCustomerQuotation: [false],
      deliveryDate: ['', Validators.required],
      payTermId: [null],
      payTerm: ['',Validators.required],
      payModeId: [null],
      payMode: ['',Validators.required],
      incoTermId: [null],
      incoPlace: ['',Validators.required],
      incoTerm: ['',Validators.required],
      remarks: ['', Validators.required],
      convertedToCustomerCost: [''],
      convertedToCustomerCostWithTCD:[''],
      convertedcustTCD:[''],
      details: this.formBuilder.array([]),
      tcdDetails: this.formBuilder.array([]),
      vendors: this.formBuilder.array([]),
      documents: this.formBuilder.array([])
    });
    this.createItems();
    this.createVendors();
    this.createDocuments();
    this.createTcdItems();
    console.log(" this.purchaseForm", this.quotationForm);
  }

  createItems(flag?) {
    let _this = this;
    if (flag) {
      if (this.quotationForm.controls.details.status == 'INVALID') {
        this.submitted = true;
        return false;
      }
    }
    let item = this.formBuilder.group({
      groupXid: [null],
      group: ['', Validators.required],
      description: ['', Validators.required],
      typeXid: [null],
      type: ['', Validators.required],
      uomXid: [null],
      uom: ['', Validators.required],
      qtQty: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
      smeProfit: [{ value: '', disabled: (_this.quotationForm.controls.includeCustomerQuotation.value ==false) }, [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)]],
      venQtUnitPrice: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      vendQtCost: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      custQtUnitPrice: [{ value: '', disabled: (_this.quotationForm.controls.includeCustomerQuotation.value ==false) }, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      custQtCost: [{ value: '', disabled: true }, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      status: 1,
      enqXid: _this.selectedEnqId,
      groupNameAuto: [''],
      uomNameAuto: [''],
    });
    this.itemsArray = this.quotationForm.get('details') as FormArray;
    this.itemsArray.push(item);

    this.validateItems();
  }

  createVendors() {
    let item = this.formBuilder.group({
      id: null,
      date: [''],
      details: [''],
      vendorXid: [null],
      vendorName: [null],
      quotationNumber: [null],
      quotationXid: [null],
      status: 1,
      selectVendor:[null]
          });
    this.itemsVendArray = this.quotationForm.get('vendors') as FormArray;
    this.itemsVendArray.push(item);
  }

  createTcdItems(flag?) {

    console.log("this.quotationForm.controls.tcdDetails");
    if (flag) {
      if (this.quotationForm.controls.tcdDetails.status == 'INVALID') {
        this.tcdSubmitted = true;
        return false;
      }
    }
    let item = this.formBuilder.group({
      tcdType: [null, Validators.required],
      tcdName: [null],
      tcdDesc: [null],
      flatOrPercentage: [null, Validators.required],
      amount: [null, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      tcdAmount: [null, [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      status: 1,

    });
    this.itemsTcdArray = this.quotationForm.get('tcdDetails') as FormArray;
    this.itemsTcdArray.push(item);
  }

  createDocuments() {
    let _this = this;
    let item = this.formBuilder.group({
      enqXid: [''],
      fileName: [''],
      fileDisplayName: [''],
      filePath: [''],
      companyXid: [null],
      status: 1
    });
    _this.itemsDocsArray = this.quotationForm.get('documents') as FormArray;
    _this.itemsDocsArray.push(item);
  }



  setForm() {
    let _this = this;

    this.quotationForm = this.formBuilder.group({
      enqXid: _this.selectedEnqId,
      enqNumber: _this.selectedEnqNumber,
      quotationDate: [null, Validators.required],
      quotationCategory: [1],
      vendorXid: ['', Validators.required],
      vendorName: ['', Validators.required],
      customerXid: [this.enqDetails.customerXid, Validators.required],
      customerName: [this.enqDetails.customerName, Validators.required],
      custCurrencyXid: [null],
      custCurrency: [null],
      custExRate: [null],
      vendCurrencyXid: [null],
      vendCurrency: [null],
      vendExRate: [null],
      custAmountUsd: [null],
      vendAmountUsd: [null],
      fromVendorCost: [0],
      fromVendorCostWithTCD: [0],
      toCustomerCost: [0],
      toCustomerCostWithTCD: [0],
      custTCD: [null],
      vendTCD: [null],
      companyXid: [this.company],
      includeCustomerQuotation: [false],
      deliveryDate: ['', Validators.required],
      payTermId: [null],
      payTerm: ['',Validators.required],
      payModeId: [null],
      payMode: ['',Validators.required],
      incoTermId: [null],
      incoPlace: ['',Validators.required],
      incoTerm: ['',Validators.required],
      remarks: ['', Validators.required],
      convertedToCustomerCost: [''],
      convertedToCustomerCostWithTCD:[''],
      convertedcustTCD:[''],
      details: this.formBuilder.array([]),
      tcdDetails: this.formBuilder.array([]),
      vendors: this.formBuilder.array([]),
      documents: this.formBuilder.array([])
    });
    this.setItems();
    this.setVendors();
    this.setDocuments();
    this.setTcdItems();
    console.log(" this.purchaseForm", this.quotationForm);


  }

  setItems() {
    let _this = this;
    if (_this.enqDetails.EDE && _this.enqDetails.EDE.length) {
      _.forEach(_this.enqDetails.EDE, function (field, index) {
        let item = _this.formBuilder.group({
          groupXid: [field.groupXid],
          group: [field.group, Validators.required],
          description: [field.description, Validators.required],
          typeXid: [field.typeXid],
          type: [field.type, Validators.required],
          uomXid: [field.uomXid],
          uom: [field.uom, Validators.required],
          qtQty: [field.qtQty ? field.qtQty.toFixed(2) : field.qtQty, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
          smeProfit: [{ value: '', disabled: (_this.quotationForm.controls.includeCustomerQuotation.value ==false) }, [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)]],
          venQtUnitPrice: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          vendQtCost: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          custQtUnitPrice: [{ value: '', disabled:(_this.quotationForm.controls.includeCustomerQuotation.value ==false) }, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          custQtCost: [{ value: '', disabled: true }, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          status: 1,
          enqXid: _this.selectedEnqId,
          groupNameAuto: [field.group],
          uomNameAuto: [field.uom]
        });
        _this.itemsArray = _this.quotationForm.get('details') as FormArray;
        _this.itemsArray.push(item);
        //_this.itemTotals[index] = field.vendQtCost;
        //_this.calculateCost(index);
        console.log("item", item);
      });
    }
    else {
      this.createItems();
    }
  }

  setVendors() {
    let _this = this;
    if (_this.enqDetails.EV && _this.enqDetails.EV.length) {
      _.forEach(_this.enqDetails.EV, function (field, index) {
        let item = _this.formBuilder.group({
          id: [field.id],
          date: [field.date],
          details: [field.details],
          vendorXid: [field.vendorXid],
          vendorName: [field.vendorName],
          quotationNumber: [field.quotationNumber],
          quotationXid: [field.quotationXid],
          status: 1,
          selectVendor:[ field.selectVendor]
        });
        _this.itemsVendArray = _this.quotationForm.get('vendors') as FormArray;
        _this.itemsVendArray.push(item);

      });
    }
    else {
      this.createVendors();
    }
  }

  setTcdItems() {
    let _this = this;
    this.createTcdItems();
  }



  setDocuments() {
    let _this = this;
    if (_this.enqDetails.EDO && _this.enqDetails.EDO.length) {
      _.forEach(_this.enqDetails.EDO, function (field, index) {
        console.log("documentttttttttttt", field)
        let item = _this.formBuilder.group({
          enqXid: [field.enqXid],
          fileName: [field.fileName],
          fileDisplayName: [field.fileDisplayName],
          filePath: [field.filePath],
          companyXid: [_this.company],
          status: 1
        });
        _this.itemsDocsArray = _this.quotationForm.get('documents') as FormArray;
        _this.itemsDocsArray.push(item);
      });
    }
  }

  alertVendor(vendor,valueRadio) {
    let _this = this
    const arr = <FormArray>this.quotationForm.controls.details;
        arr.controls = [];
        this.quotationForm.get('quotationDate').setValue(null)
        this.setItems()
        const arrTcd = <FormArray>this.quotationForm.controls.tcdDetails;
        arrTcd.controls = []
        this.setTcdItems()

        _this.quotationForm.get('vendorName').setValue(vendor.controls.vendorName.value);
        _this.quotationForm.get('vendorXid').setValue(vendor.controls.vendorXid.value);
       _this.quotationForm.get('fromVendorCost').setValue(null);
       _this.quotationForm.get('vendAmountUsd').setValue(null);
       _this.quotationForm.get('custAmountUsd').setValue(null);
       _this.quotationForm.get('fromVendorCostWithTCD').setValue(null);
       _this.quotationForm.get('incoTermId').setValue(null);
       _this.quotationForm.get('incoPlace').setValue(null);
       _this.quotationForm.get('remarks').setValue(null);
       _this.quotationForm.get('payTermId').setValue(null);
       _this.quotationForm.get('payModeId').setValue(null);
       _this.quotationForm.get('deliveryDate').setValue(null);
       _this.converted_toCustomerCost =0
       _this.vend_tcdTotal =0
       _this.vend_tcdDiscount = 0
       _this.converted_cust_tcdTotal =0
       _this.converted_cust_tcdDiscount = 0
       _this.converted_toCustomerCostWithTCD = 0
        // select vendor currency
        console.log("vendor",_this.quotationForm)
        console.log("valueRadio", valueRadio);
        _.forEach(_this.enqDetails.vendorsCurrency, function (value, index) {
          // set selected vendor currency details
          if (vendor.controls.vendorXid.value == value.id) {
            let vend_currency_id = (value.currency && value.currency > 0) ? parseInt(value.currency) : 0;
            _this.vend_currencyObj = _.find(_this.currencies, { id: vend_currency_id });
            let currency_rate = _.find(_this.exchangeRate, { currencyXId: vend_currency_id });
            _this.vend_currency_rate = (currency_rate && currency_rate.Rate) ? currency_rate.Rate : 0;
            _this.vend_currencySymbol = _this.vend_currencyObj ? _this.vend_currencyObj.symbol : ''
            _this.vend_currencyShortKey = _this.vend_currencyObj ? _this.vend_currencyObj.shortKey : ''
            _this.vend_currencyFullName = _this.vend_currencyObj ? _this.vend_currencyObj.name : ''
            // set data     
            _this.quotationForm.get('vendCurrencyXid').setValue(vend_currency_id);
            _this.quotationForm.get('vendCurrency').setValue(_this.vend_currencyShortKey);
            _this.quotationForm.get('vendExRate').setValue(_this.vend_currency_rate);
          }
        });
        _this.resetAndReCalculate();
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.quotationForm.controls;
  }
  selectVendor(valueRadio,vendor,index,vendorId,length,date) {
    
    let _this = this;
    console.log("length",vendor)
    this.vendorDate =  this.CF.dateObjectToJsonDate(date)
    if(length == 1 || _this.first || vendor.value.vendorName ==_this.quotationForm.get('vendorName').value){
      _this.alertVendor(vendor,valueRadio);
      _this.first = false
    }
    else {
      let _this = this;

    Swal.fire({ 
      title: 'Confirm Changing Vendor',
    text: 'This action will affect the details entered. Are you sure you want to continue?',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    allowOutsideClick: false
    }).then((result) => {

      if (result.value) {
        //click yes reset values
        // change checkbox selection and reset
        this.vendorIdSelected = vendorId
        _this.alertVendor(vendor,valueRadio);

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        this.vendorSel = this.vendorIdSelected
        console.log("vendorSel",this.vendorSel)
        //this.quotationForm.controls.vendors.value[4].selectVendor = true
        //this.quotationForm.controls.vendors.value[2].selectVendor = 'false'
        console.log("vendor",_this.quotationForm)
       /*Swal.fire(
          'Cancelled',
          'No change in vendor',
          'error'
        )*/
      }
    })
  }
  }

  resetVendorDetailsAndCurrency(_this) {
    _this.quotationForm.get('vendCurrencyXid').setValue(null);
    _this.quotationForm.get('vendCurrency').setValue(null);
    _this.quotationForm.get('vendExRate').setValue(null);
    this.quotationForm.get('vendorName').setValue(null);
    this.quotationForm.get('vendorXid').setValue(null);
  }
  deleteItems(index) {
    this.itemsArray = this.quotationForm.get('details') as FormArray;
    this.itemsArray.removeAt(index);
    this.resetAndReCalculate();
  }
  deleteTcdItems(index) {
    this.itemsTcdArray = this.quotationForm.get('tcdDetails') as FormArray;
    this.itemsTcdArray.removeAt(index);
    this.resetAndReCalculate();
  }

  validateTcdValue(index) {
    this.itemsTcdArray = this.quotationForm.get('tcdDetails') as FormArray;
    if (this.itemsTcdArray.controls[index].get('flatOrPercentage').value == globalConstants.PER && this.itemsTcdArray.controls[index].get('amount').value > 100) {
      this.itemsTcdArray.controls[index].get('amount').setValue('100.000');
    }
  }
  validateSMEprofitValue(index) {
    this.itemsArray = this.quotationForm.get('details') as FormArray;
    if (this.itemsArray.controls[index].get('smeProfit').value > 100) {
      this.itemsArray.controls[index].get('smeProfit').setValue('100.00');
    }
  }

  deleteVendorItems(index,vendorName) {
    console.log("vendorName",vendorName)
    let _this = this
    this.itemsVendArray = this.quotationForm.get('vendors') as FormArray;
    this.itemsVendArray.removeAt(index);
    if(this.quotationForm.get('vendorName').value == vendorName){
      console.log("vendor",this.quotationForm.get('vendorName').value)
      this.quotationForm.get('vendorName').setValue(null)
    const arr = <FormArray>this.quotationForm.controls.details;
        arr.controls = [];
        this.quotationForm.get('quotationDate').setValue(null)
        this.setItems()
        const arrTcd = <FormArray>this.quotationForm.controls.tcdDetails;
        arrTcd.controls = []
        this.setTcdItems()
       _this.quotationForm.get('fromVendorCost').setValue(null);
       _this.quotationForm.get('vendAmountUsd').setValue(null);
       _this.quotationForm.get('custAmountUsd').setValue(null);
       _this.quotationForm.get('fromVendorCostWithTCD').setValue(null);
       _this.converted_toCustomerCost =0
       _this.vend_tcdTotal =0
       _this.vend_tcdDiscount = 0
       _this.converted_cust_tcdTotal =0
       _this.converted_cust_tcdDiscount = 0
       _this.converted_toCustomerCostWithTCD = 0
       _this.quotationForm.get('incoTermId').setValue(null);
       _this.quotationForm.get('remarks').setValue(null);
       _this.quotationForm.get('incoPlace').setValue(null);
       _this.quotationForm.get('payTermId').setValue(null);
       _this.quotationForm.get('payModeId').setValue(null);
       _this.quotationForm.get('deliveryDate').setValue(null);   
    }

  }
  getEnquiryDetails() {
    let _this = this;
    this.resetVendorDetailsAndCurrency(_this);
    let params = { 'enqid': this.selectedEnqId };
    _this.CS.getEnquiryDetailsById(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.enqDetails = response.result;
        _this.enqDetails.customerCurrency = response.customerCurrency;
        _this.enqDetails.vendorsCurrency = response.vendorsCurrency;
        _this.selectedEnqNumber = response.result.enqNumber;

        _this.min_enqDate = this.CF.dateObjectToJsonDate(new Date(_this.enqDetails.enqDate))

        this.setForm();
        this.selectCurrencyRate();

        console.log("_this.enqDetails", _this.enqDetails);
      }else if(response && response.result == null){
        this.toastr.error('No such enquiry found', 'Failed!');
        _this.router.navigate(['/quotation/list']);
        
      } else {
        this.toastr.error('Something Went Wrong', 'Failed!');
      }
    });
  }

  selectCurrencyRate() {
    let _this = this;
    let cust_currency_id = (_this.enqDetails && _this.enqDetails.customerCurrency) ? parseInt(_this.enqDetails.customerCurrency) : 0;
    var currency_rate = _.find(_this.exchangeRate, { currencyXId: cust_currency_id });
    _this.cust_curencyObj = _.find(_this.currencies, { id: cust_currency_id });
    _this.cust_currencySymbol = _this.cust_curencyObj ? _this.cust_curencyObj.symbol : ''
    _this.cust_currencyShortKey = _this.cust_curencyObj ? _this.cust_curencyObj.shortKey : ''
    _this.cust_currencyFullName = _this.cust_curencyObj ? _this.cust_curencyObj.name : ''
    _this.cust_currency_rate = (currency_rate && currency_rate.Rate) ? currency_rate.Rate : 0;

    // set data     
    this.quotationForm.get('custCurrencyXid').setValue(cust_currency_id);
    this.quotationForm.get('custCurrency').setValue(_this.cust_currencyShortKey);
    this.quotationForm.get('custExRate').setValue(_this.cust_currency_rate);
  }



  getCurrencies() {
    let _this = this;
    let params = { model_name: 'currencies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencies = response.result;
      } else {
        _this.currencies = [];
      }
      _this.selectCurrencyRate();
    });
  }

  getItemGroups() {
    let _this = this;
    _this.QuotService.getItemGroups(_this).subscribe(response => {
      _this.itemGroups = response;
    });
  }

  getTypes() {
    let _this = this;
    _this.QuotService.getTypes(_this).subscribe(response => {
      _this.itemTypes = response;
    });
  }

  getTcdTypes() {
    let _this = this;
    _this.CS.getData({ model_name: 'tcdTypes', where: { 'status': 1 } }).subscribe(response => {
      if (response && response.status == "success" && response.result) {
        _this.tcdTypes = response.result;
      } else {
        _this.tcdTypes = [];
      }
    });
  }

  getUoms() {
    let _this = this;
    _this.QuotService.getUoms(_this).subscribe(response => {
      _this.uoms = response;
    });
  }

  getExchangeRate() {
    let _this = this;
    let params = {};
    _this.CS.getLatestCurrencyExchangeRates(params).subscribe(response => {
      console.log("exchangeRate", response);
      if (response && response.status == "success" && response.result) {
        let exchangeRates = [];
        _.map(response.result, function (item) {
          if (item.ER && item.ER.length > 0) {
            exchangeRates.push(item.ER[0]);
          }
        });
        _this.exchangeRate = exchangeRates;
        console.log(" _this.exchangeRate", _this.exchangeRate);
      } else {
        _this.exchangeRate = [];
      }
      _this.selectCurrencyRate();
    });
  }

  resetAndReCalculate() {
    let _this = this;
    _this.customer_itemTotals = {};
    _this.vendor_itemTotals = {};
    _this.profit_itemTotals = {};
    _this.customer_itemTotal = 0;
    _this.vendor_itemTotal = 0;
    _this.profit_itemTotal = 0;

    _.forEach(_this.quotationForm.get('details')['controls'], function (value, index) {
      _this.calculateCost(index);
    });
  }
  calculateCost(index) {
    var _this = this;
    if (this.quotationForm.controls.vendorName.status == 'INVALID' || this.quotationForm.controls.vendorXid.status == 'INVALID' ) {
      this.vendorChoose = true
      setTimeout(function(){
      _this.vendorChoose = false
      },7000)
      /*if(this.quotationForm.controls.includeCustomerQuotation.value ==false) {
        this.vendorChoose = true
      }*/
    }
    else {
      this.vendorChoose = false
    }

    var element = (<FormArray>this.quotationForm.controls['details']).at(index);
    if ((<FormArray>element).controls['qtQty'].status == 'VALID' && (<FormArray>element).controls['venQtUnitPrice'].status == 'VALID') {

      var qty = (<FormArray>element).controls['qtQty'].value;
      var v_price = (<FormArray>element).controls['venQtUnitPrice'].value;
      var c_price = (<FormArray>element).controls['custQtUnitPrice'].value;
      var sme_profit = (<FormArray>element).controls['smeProfit'].value ? (<FormArray>element).controls['smeProfit'].value : 0.00;

      var customer_cost = null;
      var vendor_cost = null;
      sme_profit = sme_profit / 100;
      vendor_cost = qty * v_price;
      console.log("quotationForm",_this.quotationForm)
      if (_this.inclCustQuotation && (<FormArray>element).controls['custQtUnitPrice'].status == 'DISABLED') {
        // Unit Price (Customer)= (Unit Price (Vendor)* SME profit) 
        // Cost (Customer) = (Unit Price(customer)* Qty)
        if (sme_profit > 0) {
          customer_cost = vendor_cost + (vendor_cost * sme_profit);
          c_price = customer_cost / qty;
          (<FormArray>element).controls['custQtUnitPrice'].setValue(c_price.toFixed(3));
        } else {
          customer_cost = qty * c_price;
        }
        this.customer_itemTotals[index] = customer_cost;
        (<FormArray>element).controls['custQtCost'].setValue(customer_cost.toFixed(3));
      } else {
        _this.customer_itemTotals[index] = 0;
        (<FormArray>element).controls['custQtCost'].setValue(null)
      }
      console.log("<FormArray>element).controls['custQtCost'])", (<FormArray>element).controls['custQtCost'])
      console.log("after set valueeeeeeee", _this.quotationForm.value)

      this.vendor_itemTotals[index] = vendor_cost;
      this.profit_itemTotals[index] = (vendor_cost * sme_profit);
      (<FormArray>element).controls['vendQtCost'].setValue(vendor_cost.toFixed(3));
      this.calculateTotalAmount();
    } else {
      _this.vendor_itemTotals[index] = 0;
      (<FormArray>element).controls['vendQtCost'].setValue(null)
    }
    _this.validateItems();
  }

  logAmount(_this) {
    console.log("_this.vendor_itemTotal --->", _this.vendor_itemTotal);
    console.log("_this.customer_itemTotal ->", _this.customer_itemTotal);
    console.log("_this.profit_itemTotal---->", _this.profit_itemTotal);
    console.log("_this.vendor_itemTotal --->", _this.vendor_itemTotals);
    console.log("_this.customer_itemTotal ->", _this.customer_itemTotals);
    console.log("_this.profit_itemTotal---->", _this.profit_itemTotals);
  }
  calculateTcdAmount() {
    this.calculateTotalAmount();
  }

  resetCalculation(_this) {
    _this.vendor_itemTotal = 0;
    _this.customer_itemTotal = 0;
    _this.profit_itemTotal = 0;
    _this.vend_tcdTotal = 0;
    _this.cust_tcdTotal = 0;
    _this.vend_tcdDiscount = 0;
    _this.cust_tcdDiscount = 0;
    
  }
  calculateTotalAmount() {
    let _this = this;
    this.resetCalculation(_this)
    var vendor_FinalCost = 0;
    var customer_FinalCost = 0;

    _.map(_this.vendor_itemTotals, function (item) {
      console.log("_this.vendor_itemTotal", item);
      _this.vendor_itemTotal = _this.vendor_itemTotal + item;
    });
    vendor_FinalCost = _this.vendor_itemTotal;
    _this.quotationForm.get('fromVendorCostWithTCD').setValue(vendor_FinalCost.toFixed(3));

    _.map(_this.customer_itemTotals, function (item) {
      console.log("_this.customer_itemTotal", item);
      _this.customer_itemTotal = _this.customer_itemTotal + item;
    });
    customer_FinalCost = _this.customer_itemTotal;
    _this.quotationForm.get('toCustomerCostWithTCD').setValue(customer_FinalCost.toFixed(3));

    _.map(_this.profit_itemTotals, function (item) {
      console.log("_this.profit_itemTotal", item);
      _this.profit_itemTotal = _this.profit_itemTotal + item;
    });

    this.logAmount(_this); // to show data


    //if (this.quotationForm.controls.details.status == 'VALID' && _this.vendor_itemTotals) {
    _this.quotationForm.get('fromVendorCost').setValue(_this.vendor_itemTotal.toFixed(3));
    _this.quotationForm.get('toCustomerCost').setValue(_this.customer_itemTotal.toFixed(3));
    //}

    // calculate TCD 
    //if (this.quotationForm.controls.tcdDetails.status == 'VALID' && _this.vendor_itemTotal) {
    _.forEach(_this.quotationForm.value.tcdDetails, function (value, index) {
      var element = (<FormArray>_this.quotationForm.controls['tcdDetails']).at(index);
      var rate = 0;
      var c_rate = 0;
      var v_rate = 0;
      if ((<FormGroup>element).controls['tcdType'].status == 'VALID' && (<FormGroup>element).controls['flatOrPercentage'].status == 'VALID' && (<FormGroup>element).controls['amount'].status == 'VALID') {

        rate = value.status == 1 ? parseFloat(value.amount) : 0;
        if (value.flatOrPercentage == globalConstants.PER) {
          rate = rate / 100;
          c_rate = (_this.inclCustQuotation) ? (_this.customer_itemTotal * rate) : 0; // calculate rate if checked include customer quotation 
          v_rate = (_this.vendor_itemTotal * rate);
        } else {
          v_rate = rate;
          c_rate = (_this.inclCustQuotation) ? (rate) : 0;  // calculate rate if checked include customer quotation 
        }



        console.log("vendor_itemTotal ----->", _this.vendor_itemTotal)
        console.log("customer_itemTotal ----->", _this.customer_itemTotal)
        console.log("c_rate ----->", c_rate)
        console.log("v_rate ----->", v_rate)
        console.log("value ----->", value)
        console.log("rate ----->", rate)



        if (value.tcdType == globalConstants.DISCOUNT) {
          // vendor discount details
          vendor_FinalCost = vendor_FinalCost - parseFloat(v_rate.toString());
          _this.vend_tcdDiscount = parseFloat(_this.vend_tcdDiscount.toString()) + parseFloat(v_rate.toString());
          _this.vend_tcdDiscount = parseFloat((_this.vend_tcdDiscount).toFixed(3));
          // customer discount details
          customer_FinalCost = customer_FinalCost - parseFloat(c_rate.toString());
          _this.cust_tcdDiscount = parseFloat(_this.cust_tcdDiscount.toString()) + parseFloat(c_rate.toString());
          _this.cust_tcdDiscount = parseFloat((_this.cust_tcdDiscount).toFixed(3));

        } else {
          // vendor tax/charge details
          vendor_FinalCost = vendor_FinalCost + parseFloat(v_rate.toString());
          _this.vend_tcdTotal = parseFloat(_this.vend_tcdTotal.toString()) + parseFloat(v_rate.toString());
          _this.vend_tcdTotal = parseFloat((_this.vend_tcdTotal).toFixed(3));
          // customer tax/charge details
          customer_FinalCost = customer_FinalCost + parseFloat(c_rate.toString());
          _this.cust_tcdTotal = parseFloat(_this.cust_tcdTotal.toString()) + parseFloat(c_rate.toString());
          _this.cust_tcdTotal = parseFloat((_this.cust_tcdTotal).toFixed(3));
        }
        _this.quotationForm.get('fromVendorCostWithTCD').setValue(vendor_FinalCost.toFixed(3));
        _this.quotationForm.get('toCustomerCostWithTCD').setValue(customer_FinalCost.toFixed(3));
        // quotation 
        (<FormArray>element).controls['tcdAmount'].setValue(v_rate.toFixed(3));
      } else {
        (<FormArray>element).controls['tcdAmount'].setValue(null);
      }

    });
    // }


    _this.toUsd();


    let custTCDVal = (_this.cust_tcdTotal - _this.cust_tcdDiscount);
    let vendTCDVal = (_this.vend_tcdTotal - _this.vend_tcdDiscount);
    this.quotationForm.get('custTCD').setValue(custTCDVal.toFixed(3));
    this.quotationForm.get('vendTCD').setValue(vendTCDVal.toFixed(3));

  }
  toUsd() {

    // convert toCustomerCost rate in USD
    let _this = this;
    var custCurencyObj = _.find(_this.currencies, { id: _this.quotationForm.value.custCurrencyXid });
    var vend_rate = _.find(_this.exchangeRate, { currencyXId: _this.quotationForm.value.vendCurrencyXid });
    var cust_rate = _.find(_this.exchangeRate, { currencyXId: _this.quotationForm.value.custCurrencyXid });
    _this.cust_currencySymbol = custCurencyObj ? custCurencyObj.symbol : ''
    this.QuotService.toUsd(_this.quotationForm, _this.exchangeRate, vend_rate, cust_rate)

    // customer currency conversion
    let v_curr_rate = (vend_rate) ? vend_rate.Rate : 0;
    let c_curr_rate = (cust_rate) ? cust_rate.Rate : 0;
    let v_toCustomerCost = this.quotationForm.get('toCustomerCost').value ? parseFloat(this.quotationForm.get('toCustomerCost').value) : 0;
    let v_toCustomerCostWithTCD = this.quotationForm.get('toCustomerCostWithTCD').value ? parseFloat(this.quotationForm.get('toCustomerCostWithTCD').value) : 0;

    if (v_curr_rate > 0 && c_curr_rate > 0) {
      _this.converted_toCustomerCost = parseFloat(((v_toCustomerCost / v_curr_rate) * c_curr_rate).toFixed(3));
      _this.converted_toCustomerCostWithTCD = parseFloat(((v_toCustomerCostWithTCD / v_curr_rate) * c_curr_rate).toFixed(3));
      _this.converted_cust_tcdTotal = parseFloat(((_this.cust_tcdTotal / v_curr_rate) * c_curr_rate).toFixed(3));
      _this.converted_cust_tcdDiscount = parseFloat(((_this.cust_tcdDiscount / v_curr_rate) * c_curr_rate).toFixed(3));
       _this.quotationForm.get('convertedToCustomerCost').setValue( _this.converted_toCustomerCost );
        _this.quotationForm.get('convertedToCustomerCostWithTCD').setValue(_this.converted_toCustomerCostWithTCD );
        _this.quotationForm.get('convertedcustTCD').setValue( _this.converted_cust_tcdTotal.toFixed(3));
    }

  }

  convertDate(dateString) {
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMONTHYY(objectDate);
  }
  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("from", from);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      console.log("getId", getId);
      if (isNaN(getId)) {
        if (index || index == 0) {
          var element = (<FormArray>this.quotationForm.controls['details']).at(index);
          if ((<FormArray>element).controls[to].value == getId) {
            return false;
          }
        } else {
          if (this.quotationForm.controls[to].value == getId) {
            return false;
          }
        }
        var check = {};
        check[dataField] = getId;
        var item = _.find(data, check);
      } else {
        console.log("is num");
        var item = _.find(data, { id: parseInt(getId, 10) });
      }
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.quotationForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.quotationForm.get(from).setValue(getId);
          }
          console.log("this.quotationForm", this.quotationForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.quotationForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.quotationForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.quotationForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.quotationForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.quotationForm.get(from).setValue(null);
        }
      }
    }
  }

  makeDecimalPoint(field?, index?, formArray?, point?) {
    let _this = this;
    if (index || index == 0) {
      var element = (<FormArray>this.quotationForm.controls[formArray]).at(index);
      var value = (<FormArray>element).controls[field].value;
      if (value == null) { return; }
      var data = {};
      data[field] = (parseFloat(value.toString()).toFixed(point ? 2 : 3));
      value && !isNaN(value) ? element.patchValue(data) : '';
      if (value && !isNaN(value) && parseFloat(value) <= 0) {
        element.get(field).setValue("")
      }
    } else {
      var value = _this.quotationForm.get(field).value;
      value && !isNaN(value) ? _this.quotationForm.get(field).setValue((parseFloat(value.toString()).toFixed(point ? 2 : 3))) : '';
    }
  }

  triggerFileUpload() {
    this.el.nativeElement.querySelector("#fileUpload").click();
  }

  deleteDoc(index) {
    this.fileToUpload.splice(index, 1);
    this.imageSrc.splice(index, 1)
  }

    upload(files: File[]) {

    let _this = this;
    _.forEach(files, function (value) {
       console.log("reader", value)
       var fileTypes = ['jpg', 'jpeg', 'png', 'xlsx', 'txt', 'gif', 'pdf', 'xls'];  //acceptable file types
        var extension = value.name.split('.').pop().toLowerCase() 
        console.log(extension)
        var Success = fileTypes.indexOf(extension) > -1
       if(Success) {
         if (value.size < 4096000) {
      var reader = new FileReader();
      reader.readAsDataURL(value);
      reader.onload = () => {
        var src = reader.result as string;
        _this.imageSrc.push(src);
        _this.fileToUpload.push(value);
      }
      }
        else {
        alert('Maximum allowed file size is 4 MB!');
        return
      }
      
    } else {
      alert('File type not supported')
    }
    });

    document.getElementById('fileUpload')['value'] = "";
  }

  clear(fields, index?, checkField?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = checkField && field == checkField ? '' : null;
        (<FormArray>_this.quotationForm.controls['details']).at(index).patchValue(data);
      } else {
        _this.quotationForm.get(field).setValue(checkField && field == checkField ? '' : null);
      }
    });

  }

  fixSelectedData(name,index){
  console.log("name",name)
  console.log("index",index)

     var element = (<FormArray>this.quotationForm.controls['details']).at(index);

     if(name =='group') {
      (<FormArray>element).controls['groupNameAuto'].setValue( (<FormArray>element).controls['group'].value);
    }

     if(name =='uom') {
      (<FormArray>element).controls['uomNameAuto'].setValue( (<FormArray>element).controls['uom'].value);
    }

}



}
