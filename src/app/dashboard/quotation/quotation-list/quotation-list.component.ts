import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { globalConstants } from '../../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-quotation-list',
  templateUrl: './quotation-list.component.html',
  styleUrls: ['./quotation-list.component.scss']
})
export class QuotationListComponent implements OnInit {


  company_list = [];
  currentPage = 1
  totalItems = 0
  offset = 0
  UPLOAD_PATH = globalConstants.UPLOADS_DIR
  pageOffset = 0;  
  searchFilterObject = null;
  loader = false;
  quotationList = [];
  QUOTATION_STATUS = globalConstants.QUOTATION_STATUS;

  searchToggle = false;
  searchLoader = false;
  search='';
  isAdvanceSearch=false;
  customerName = '';
  vendorName = '';
  quotationSearchForm: FormGroup;

  selCustomerId = null;
  selVendorId = null;
  submitted = false
  isValidFormSubmitted = false
  clearFilter = false;

  userQuestionUpdate = new Subject<string>();

  firstLoad = false

  customers = [];
  vendors = [];
  keywordVendor = 'vendorName'
  keywordCustomer = 'vendorName'
  sourceType = [{ id: 1, name: "From Vendor" }, { id: 2, name: "To Customer" }];
  QUOTATION_SORCE = ['','From Vendor','To Customer']
  sourceTypeVal = this.sourceType[0]['id'];


  constructor(private spinner: NgxSpinnerService, private formBuilder: FormBuilder, private http: HttpClient, private CF: CommonFunctionsService, private CS: CommonservicesService, private toastr: ToastrService) {
   this.userQuestionUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.resetPagination();
        this.onSubmit();
      });
    }
  permissions = this.CF.findPermissions()
  itemsPerPage = this.CS.getItemPerPage();
  ngOnInit() {
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    this.getQuotationList();
    this.firstLoad = true
    this.getVendors();
    this.getCustomers();
    this.createSearchForm();

  }

  createSearchForm(){
  this.quotationSearchForm = this.formBuilder.group({
      fromDate: [null, []],
      toDate: [null, []],
      source: [null, []],
      search: ['', []],
      customer: ['', []],
      vendor: ['', []],
      enqno: ['', []],
      customerName: ['', []],
      vendorName: ['', []],
    });
  }

    get f() {
    return this.quotationSearchForm.controls;
  }
    preventChange(event) {
    //check if needed function
  }


  getQuotationList() {
    var params = { limit: this.itemsPerPage, offset: this.offset, filterOption: this.searchFilterObject };
    this.pageOffset = this.offset;
    let _this = this;
    _this.loader = true;
    this.CS.getQuotationList(params).subscribe(response => {
      console.log("quotationList", response);
      console.log("quotationList.status", response.result);
      if (response && response.status == 'success') {
        _this.quotationList = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
        console.log("quotationList", response);
      } else {
        this.toastr.error('Error Occured,Try again!', 'Failed!');
      }
      _this.loader = false;
    });
  }

  convertDate(dateString) {
    console.log("dateString-", dateString);
    if (dateString == "" || dateString == null) {
      return "";
    }
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMONTHYY(objectDate);
  }
  amountDecimal(amt) {
    if (amt != "" && amt!=null) {
      amt = amt.toFixed(3);
    }
    return amt;
  }
  pageChanged(event) {

    this.currentPage = event;
    var setoffset = event - 1;
    setoffset = setoffset * this.itemsPerPage;
    this.offset = setoffset;
    this.getQuotationList();

  }

  exportQuotation() {
    let _this = this;
    _this.CS.quotationSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'quotation/' + response.filename, '_blank');
        this.toastr.success('Successfuly Exported Excel', 'Success!');
      }
      else {
        this.toastr.error(response.msg, 'Failed!');

      }
    });
  }

  
  clearSearch(type) {

    if (type =="reset") {
      this.isAdvanceSearch = false;
      this.customerName = null;
      this.vendorName = null;
      this.searchToggle = true;
    }
    else  {
      this.isAdvanceSearch = false;
      this.customerName = null;
      this.vendorName = null;
      this.searchToggle = false
     // this.firstLoad = false
    }
    this.searchFilterObject = null;
    this.createSearchForm();
    this.getQuotationList();
  }

  clearMainSearch(type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.clearSearch(type);
      this.searchToggle = true
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }
  changeSelection(event, to) {
    console.log("event", event)
    console.log("to", to);

    if (to == 'customer') {
      this.customerName = event.vendorName ? event.vendorName : this.customerName;
      this.selCustomerId = event.id ? event.id : this.selCustomerId;
      this.quotationSearchForm.get(to).setValue(this.selCustomerId);
    }
    if (to == 'vendor') {
      this.vendorName = event.vendorName ? event.vendorName : this.vendorName;
      this.selVendorId = event.id ? event.id : this.selVendorId;
      this.quotationSearchForm.get(to).setValue(this.selVendorId);
    }
  }

  clear(to) {
    this.quotationSearchForm.get(to).setValue(null);
  }


  onSubmit(from = '') {

    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;
    if (this.quotationSearchForm.invalid) {
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      return;
    }
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;
    this.sourceTypeVal = this.f.source.value;


    const poSeacrhObj = {
      fromDate: this.toDateObject(this.f.fromDate.value),
      toDate: this.toDateObject(this.f.toDate.value),
      source: this.f.source.value,

      // search: this.f.search.value,
      vendor: this.f.vendor.value,
      customer: this.f.customer.value,
      enqno: this.f.enqno.value,
      search: this.search,
    };
    _this.searchLoader = false;
    _this.clearFilter = true;    
    _this.searchToggle = false;
    this.searchFilterObject = poSeacrhObj;
    this.getQuotationList();


  }

   toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }

  resetPagination() {
    this.offset = 0;
    this.currentPage = 1;
  }
  resetCustomerValue(){
    this.quotationSearchForm.get('customer').setValue(null);
  }

  getCustomers() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'approvedVendor': 1, 'status': 1, typeUser: 2 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.customers = response.result;
      } else {
        _this.customers = [];
      }
    });
  }

  getVendors() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'approvedVendor': 1, 'status': 1, typeUser: 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.vendors = response.result;
      } else {
        _this.vendors = [];
      }
    });
  }

     onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container') || e.path[5].className.includes('ng-select ng-select') || e.path[6].className.includes('dropdown-menu show')){
        console.log('wow--->closeee----')
      }
      else {
    this.firstLoad = false
    this.searchToggle = false
  }
  }

}
