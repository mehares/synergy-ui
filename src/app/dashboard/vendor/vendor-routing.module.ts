import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorListComponent } from './vendor-list/vendor-list.component';
import { VendorAddComponent } from './vendor-add/vendor-add.component';
import { VendorEditComponent } from './vendor-edit/vendor-edit.component';
import { PageGuard } from '../../guards/page-guard.service';

const routes: Routes = [
  {
    path: 'list',
    canActivate: [PageGuard],
    component: VendorListComponent,
    data:{permissions: 'vendors_view' }
  },
  {
    path: 'add',
    canActivate: [PageGuard],
    component: VendorAddComponent,
    data:{permissions: 'vendors_add' }
  },
  {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: VendorEditComponent,
    data:{permissions: 'vendors_edit' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorRoutingModule { }
