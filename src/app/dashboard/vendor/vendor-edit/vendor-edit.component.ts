import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-vendor-edit',
  templateUrl: './vendor-edit.component.html',
  styleUrls: ['./vendor-edit.component.scss']
})
export class VendorEditComponent implements OnInit {


  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  custVendorForm: FormGroup;
  isValidFormSubmitted = null;
  submitted = false;
  currencylist = [];
  statuslist = [{ id: 1, name: 'YES' }, { id: 0, name: 'NO' }];
  countryList = [];
  stateList = [];
  selectedAddress = 0;
  currentVendor = '';
  countryLoader = false;
  stateLoader = {};
  keywordCountry = 'name';
  keywordState = 'name';
  appdate = null;
  permissions = this.CF.findPermissions();
  ToDay = { year: new Date().getFullYear(), month: new Date().getMonth()+1, day: new Date().getDate() };
  loading = false

  constructor(private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private toastr: ToastrService,
    private CS: CommonservicesService,
    private CF: CommonFunctionsService,private spinner: NgxSpinnerService,) {
  }

  

handleDateFIeld(approved) {
  console.log("approved",approved)
    if (approved.id == 1) {
      this.custVendorForm.controls['approvedDate'].enable();
    } else {
      this.custVendorForm.controls['approvedDate'].setValue(null);
      this.custVendorForm.controls['approvedDate'].disable();
    }
  }
  ngOnInit() {
    this.currentVendor = this.route.snapshot.paramMap.get('id');
    let _this = this;
    this.getCountries();
    this.getCurrencies();
    this.getVendorDetails()

    this.custVendorForm = this.formBuilder.group({
      vendorCode: ['', [Validators.required, Validators.maxLength(24)]],
      vendorName: ['', Validators.required],
      currency: [null, [Validators.required]],
      approvedDate: [null],
      approvedVendor: [null, [Validators.required]],
      status: [null, [Validators.required]],
      VA: this.formBuilder.array(
        [this.createAddressFormGroup()],
        [Validators.required])
    });


  }

  getVendorDetails() {
    let _this = this
        _this.CS.getVendorDetails({ vendorId: this.currentVendor }).subscribe(response => {
      console.log("response", response);
      if (response && response.result) {
        let vendorDetails = response.result;
        let formatedApprovedDate = null;

        if (vendorDetails.approvedDate != null) {
          //formatedApprovedDate = new Date(vendorDetails.approvedDate);
          formatedApprovedDate = this.toJsonDate(vendorDetails.approvedDate)
        }

        this.appdate = formatedApprovedDate;
        for (let isDef in vendorDetails.address) {
          this.stateList[isDef] = [];
          console.log("isDef", isDef);
          this.getState(vendorDetails.address[isDef].country, isDef);
          if (vendorDetails.address[isDef].isDefaultAddress) {
            this.selectedAddress = parseInt(isDef);
          }

        }

        this.custVendorForm = this.formBuilder.group({
          vendorCode: [vendorDetails.vendorCode, [Validators.required, Validators.maxLength(24)]],
          vendorName: [vendorDetails.vendorName, Validators.required],
          currency: [ parseInt(vendorDetails.currency),  Validators.required ],
          approvedDate: [formatedApprovedDate],
          approvedVendor: [(vendorDetails.approvedVendor ? 1 : 0), []],
          status: [parseInt(vendorDetails.status), []],
          VA: this.formBuilder.array(
            vendorDetails.address.map(addr => this.createAddressFormGroup(addr)
            ))
        });

        if (formatedApprovedDate == null) {
          this.custVendorForm.controls['approvedDate'].disable();
        }

      } else {
        this.toastr.error('Error Occured!', 'Failed!');
      }
    });
  }

  toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date) {
    return this.CF.dateObjectToJsonDate(date)
  }

  loadSelectList() {
    for (let isDef in this.f.VA.value) {
      this.stateList[isDef] = [];
      console.log("isDef", isDef);
      this.getState(this.f.VA.value[isDef].country, isDef);
      if (this.f.VA.value[isDef].isDefaultAddress) {
        this.selectedAddress = parseInt(isDef);
      }

    }
    this.setStateId();
  }

  setStateId() {
    // for (let isDef in this.f.VA.value) {
    //   this.stateList[isDef] = [];
    //   console.log("isDef", isDef);
    //   this.getState(this.f.VA.value[isDef].country, isDef);
    // }
  }

  getCurrencies() {
    let _this = this;
    let params = { model_name: 'currencies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencylist = response.result;
      } else {
        _this.currencylist = [];
      }
    });
  }

  getCountries() {
    let _this = this;
    let params = { model_name: 'country', where: {} };
    _this.countryLoader = true;
    _this.CS.getData(params).subscribe(response => {
      _this.countryLoader = false;
      if (response && response.status == "success" && response.result) {
        _this.countryList = response.result;
        console.log("countryList", response.result);
      } else {
        _this.countryList = [];
      }
    });
  }

  getState(countryId, i) {
    console.log("******", i);
    console.log("countryId", countryId);
    let _this = this;
    if (countryId > 0) {
      _this.stateLoader[i] = true;
      _this.stateList[i] = [];
      _this.CS.getStateList({ id: countryId }).subscribe(response => {
        console.log("response.result[0].CS", response.result[0].CS)
        _this.stateLoader[i] = false;
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          _this.stateList[i] = response.result[0].CS;
        } else {
          _this.stateList[i] = [];
        }
      });
    }

    console.log("_this.stateList", _this.stateList);
  }

  get f() {
    return this.custVendorForm.controls;
  }

  // get vender address VA
  get VA(): FormArray {
    return this.custVendorForm.get('VA') as FormArray;
  }

  addNewAddress() {
    const fg = this.createAddressFormGroup();
    this.VA.push(fg);
  }

  deleteAddress(idx: number, add_id: number) {
    console.log(add_id);
    let _this = this;
    let deleteAdrObj = { id: add_id, vendorId: this.currentVendor };

    if (idx == this.selectedAddress) {
      this.toastr.error('Please Choose A Different Defaut Address Before Delete!', 'Failed!');

      return;
    }
    this.CS.deleteVendorAddress(deleteAdrObj).subscribe(response => {
      console.log("response", response);
      if (response && response.status == 'success') {
        //updates data
        this.toastr.success('Address Deleted.', 'Success!');
        //_this.router.navigate(['/pages/vendors']);
      } else {
        this.toastr.error('Error Occured While Deleting Address,Try Again!', 'Failed!');
      }
    });
    this.VA.removeAt(idx);
    this.stateList.splice(idx, 1);
  }

  createAddressFormGroup(addr: any = {}) {
    return this.formBuilder.group({
      id: [addr.id, []],
      isDefaultAddress: [addr.isDefaultAddress, []],
      address1: [addr.address1, [Validators.required]],
      address2: [addr.address2, []],
      address3: [addr.address3, []],
      city: [addr.city, [Validators.required]],
      country: [addr.country, [Validators.required]],
      state: [addr.state, [Validators.required]],
      zip: [addr.zip, [Validators.required]],
      phoneNo: [addr.phoneNo, [Validators.required, Validators.pattern(this.mobNumberPattern)]],
      salesEmail: [addr.salesEmail, [Validators.required, Validators.pattern(this.emailPattern)]],
      accountsEmail: [addr.accountsEmail, [Validators.required, Validators.pattern(this.emailPattern)]],
      fax: [addr.fax, []],
      url: [addr.url, []],
      status: [addr.status, []],
      countryName: [addr.countryName],
      stateName: [addr.stateName]
    });
  }

  // Form Submit
  onSubmit() {
    console.log('FORM', this.f);
    console.log('FORM status', this.custVendorForm.invalid);
    let _this = this;
    this.submitted = true;
    this.isValidFormSubmitted = false;
    let checkIsDefault = 0;
    for (let isDef in this.f.VA.value) {

      if (this.f.VA.value[isDef].status == null) {
        this.f.VA.value[isDef].status = 1;
      }
      if (this.selectedAddress == parseInt(isDef)) {
        this.f.VA.value[isDef].isDefaultAddress = true;
        checkIsDefault = 1;
      } else {
        this.f.VA.value[isDef].isDefaultAddress = false;
      }
      // if (this.f.VA.value[isDef].isDefaultAddress == 1) {
      //   checkIsDefault = 1;
      // }
    }

    if (checkIsDefault == 0) {
      this.toastr.error('Choose One Default Address.', 'Failed!');
      return;
    }

    //if approved vendor ,,then date required 
    if (this.f.approvedVendor.value == 1 && this.f.approvedDate.value == null) {
      this.toastr.error('Approved Date Is Required.', 'Invalid!');
      return;
    }

      //if not approved vendor ,remove date
    if (this.f.approvedVendor.value == 0 ) {
       this.custVendorForm.controls['approvedDate'].setValue(null);
    }


    console.log("this.f.VA.value Address", this.f.VA.value);
    if (this.custVendorForm.invalid) {
      this.toastr.error('Enter Details & Address Fields Marked(*)', 'Invalid!');
      return;
    }
      const uniqueValues = new Set(this.f.VA.value.map(v => v.salesEmail));
      const uniquePhone = new Set(this.f.VA.value.map(v => v.phoneNo));
     const uniqueEmail = new Set(this.f.VA.value.map(v => v.accountsEmail));
    if (uniqueValues.size < this.f.VA.value.length || uniquePhone.size < this.f.VA.value.length || uniqueEmail.size < this.f.VA.value.length) {
      console.log('duplicates found')
      this.toastr.error('Duplicate Email/Phone Found.', 'Invalid!');
      _this.loading = false;
      return
    }
       const uniqueAddress = new Set(this.f.VA.value.map(v => v.address1));
      if (uniqueAddress.size < this.f.VA.value.length ) {
      console.log('duplicates found')
      this.toastr.error('Duplicate Address Found.', 'Invalid!');
      _this.loading = false;
      return
    }
    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;

    const custVendorObj = {
      vendorCode: this.f.vendorCode.value,
      vendorName: this.f.vendorName.value,
      currency: this.f.currency.value,
      approvedDate: this.toDateObject(this.f.approvedDate.value),
      approvedVendor: this.f.approvedVendor.value,
      status: this.f.status.value,
      VA: this.f.VA.value,
      id: this.route.snapshot.paramMap.get('id')
    };

    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    this.loading = true
    this.CS.updateVendors(custVendorObj).subscribe(response => {
      console.log("response", response);
      if (response && response.status == 'success') {
        this.getVendorDetails()
        this.loading = false
        this.toastr.success('Vendor Details Updated.', 'Success!');
        //_this.router.navigate(['/pages/vendors']);
      } else {
        let errorMsg = 'Error Occured While Updating,Try Again!';
        if (response.errObject) {
          errorMsg = response.errObject;
        }

        this.toastr.error(errorMsg, 'Failed!');
      }
    });
  }



  changeState(event, countryId, dataField, to, index?, from?) {
    console.log('event');
    console.log('countryId');
    if (countryId && event) {
      let _this = this;
      _this.stateList = [];
      var getId = typeof event == 'object' ? event.id : event;
      var countryId = _.find(countryId, { id: parseInt(getId, 10) });
      console.log("countryId", countryId);



      _this.CS.getStateList({ id: countryId.id }).subscribe(response => {
        console.log("response", response);
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          _this.stateList = response.result[0].CS;
          console.log("_this.stateList", _this.stateList);
        } else {
          //  _this.stateList = [];
        }
      })
      /* for (let ctr in this.f.VA.value ) {
      _this.f.VA.value[ctr].country = countryId.id;
      //(<FormArray>this.VA.controls['ctr']).at(index).patchValue(input);
      console.log("_this.f.VA.value[ctr].country",_this.f.VA.value[ctr].country);
    }*/

      if (countryId) {
        if (index || index == 0) {
          var input = {};
          input[to] = countryId[dataField];
          (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          }
        } else {
          this.custVendorForm.get(to).setValue(countryId[dataField]);
          if (typeof event == 'object') {
            this.custVendorForm.get(from).setValue(getId);
          }
          console.log("this.custVendorForm", this.custVendorForm);
        }
      } else {

      }

    }
  }
  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      console.log("getId", getId);
      if (isNaN(getId)) {
        if (index || index == 0) {
          var element = (<FormArray>this.custVendorForm.controls['VA']).at(index);
          if ((<FormArray>element).controls[to].value == getId) {
            return false;
          }
        } else {
          if (this.custVendorForm.controls[to].value == getId) {
            return false;
          }
        }
        var check = {};
        check[dataField] = getId;
        var item = _.find(data, check);
      } else {
        console.log("is num");
        var item = _.find(data, { id: parseInt(getId, 10) });
      }
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          }
        } else {
          this.custVendorForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.custVendorForm.get(from).setValue(getId);
          }
          console.log("this.custVendorForm", this.custVendorForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
          }
        } else {
          this.custVendorForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.custVendorForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.custVendorForm.controls['VA']).at(index).patchValue(input);
        }
      } else {
        this.custVendorForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.custVendorForm.get(from).setValue(null);
        }
      }
    }
  }

  clear(fields, index?, checkField?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = checkField && field == checkField ? '' : null;
        (<FormArray>_this.custVendorForm.controls['VA']).at(index).patchValue(data);
      } 
    });
          console.log(this.f);
  }


omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}
}
