import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { globalConstants } from '../../../constants/global-constants';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-vendor-list',
  templateUrl: './vendor-list.component.html',
  styleUrls: ['./vendor-list.component.scss']
})
export class VendorListComponent implements OnInit {

  message_content: string = '';
  vendorlist = [];
  currentPage = 1
  totalItems = 0
  offset = 0
  loader = false
  search = '';
  searchToggle = false;
  searchLoader = false;
  vendorSearchForm: FormGroup;
  isValidFormSubmitted = null;
  submitted = false;

  permissions = this.CF.findPermissions();
  itemsPerPage = this.CS.getItemPerPage();

  userQuestionUpdate = new Subject<string>();
  UPLOAD_PATH = globalConstants.UPLOADS_DIR
  currencylist = [];
  statuslist = [{ id: "", name: "ALL" }, { id: 1, name: 'YES' }, { id: 0, name: 'NO' }];
  searchBox = false;
  pageOffset = 0;
  keywordCurrency = 'name'
  statusListValue = this.statuslist[0]['id'];
  firstLoad = false
  currencyName=''
  isSearchRequest=false;

  constructor(private http: HttpClient,
    private CS: CommonservicesService,     
    private toastr: ToastrService,
    private CF: CommonFunctionsService,
    private formBuilder: FormBuilder,private spinner: NgxSpinnerService,) {

    this.userQuestionUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.resetPagination();
        this.onSubmit();
        if (this.search != '') {
          this.searchBox = true
        }
        else {
          this.searchBox = false
        }

      });
  }

  ngOnInit() {
    this.getVendorData();
    this.getCurrencies();
    this.vendorForm();
    this.firstLoad = true;
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);




  }

  vendorForm() {
    this.vendorSearchForm = this.formBuilder.group({
      approvedVendor: ['', []],
      vendorCode: ['', []],
      vendorName: ['', []],
      status: [''],
      currency: ['', []],
      search: ['', []],
      currencyObj: ['', []],

    });
  }

  getVendorData() {
    var params = { limit: this.itemsPerPage, offset: this.offset };
    let _this = this
    _this.loader = true
    this.pageOffset = this.offset;
    this.isSearchRequest=false;
    _this.CS.getVendorCount(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == 'success') {
        _this.vendorlist = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
        console.log("vendorlist", _this.vendorlist);
      } else {        
        this.toastr.error('Error Occured!', 'Failed!');
      }
      _this.loader = false
    });
  }

  deleteVendor(id) {
    if (confirm("Are you sure to delete this vendor ? " + id)) {

      let _this = this;
      this.http.post<any>(`api/vendor-delete`, { id: id, model_name: 'vendor' })
        .subscribe(response => {
          console.log("response", response);
          if (response && response.status == "success") {
            _this.getVendorData();
            
        this.toastr.success('Vendor Details Deleted.', 'Success!');
          } else {
            this.toastr.error('Something Went Wrong!', 'Failed!');
          }
        });
    }
  }

  changeVendorStatus(id, status,vendor) {

    let _this = this;
    vendor = 'vendor'
    let params = { id, status , vendor};
    //if (confirm("Are you sure to change status ? ")) {

    this.CS.changeVendorStatus(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == 'success') {
        this.getVendorData();
        this.toastr.success('Details Saved.', 'Success!');
      } else {
        this.toastr.error('Error Occured While Saving,Try again!', 'Failed!');
      }
    });
    //}
  }

  pageChanged(event) {
    console.log("event", event)
    if (this.submitted == true) {
      this.pageOffset = 0
      this.itemsPerPage = this.CS.getItemPerPage();
      this.currentPage = event
      var setoffset = event - 1
      setoffset = setoffset * this.itemsPerPage
      this.offset = setoffset
      this.vendorlist
    }
    else {
      this.itemsPerPage = this.CS.getItemPerPage();
      this.currentPage = event
      var setoffset = event - 1
      setoffset = setoffset * this.itemsPerPage
      this.offset = setoffset
      this.getVendorData()
    }
  }
  get f() {
    return this.vendorSearchForm.controls;
  }

  onSubmit() {

    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;

    if (this.vendorSearchForm.invalid) {
      
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      return;
    }
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    console.log("this.search",this.search);
    this.isValidFormSubmitted = true;

    const searchObj = {
      search: this.search,
      vendorCode: this.f.vendorCode.value,
      vendorName: this.f.vendorName.value,
      currency: this.f.currency.value,
      status: this.f.status.value,
      approvedVendor: this.f.approvedVendor.value,
    };
    //var params = {};
    this.resetPagination();   
    this.isSearchRequest=true;
    this.CS.searchVendors(searchObj).subscribe(response => {
      console.log("response", response);
      _this.searchLoader = false;
      _this.searchToggle = false;
      _this.loader = false;
      if (response && response.status == 'success') {
        _this.vendorlist = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
        this.pageOffset = 0
        //this.pageChanged(event);
      } else {
        this.toastr.error('Error Occured While Search,Try Again!', 'Failed!');
      }
    });

  }

  exportVendor() {
    let _this = this;
    _this.CS.vendorSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'vendor/' + response.filename, '_blank');
        this.toastr.success('Successfuly Exported Excel!', 'Success!');
      }
      else {
        this.toastr.error(response.msg, 'Failed!');
      }
    });
  }

  getCurrencies() {
    let _this = this;
    let params = { model_name: 'currencies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencylist = response.result;
      } else {
        _this.currencylist = [];
      }
    });
  }

  clearSearch(type) {
    this.resetPagination()
    this.searchBox = false;
    this.submitted = false;
    this.loader = false
    this.vendorSearchForm = this.formBuilder.group({
      approvedVendor: null,
      vendorCode: null,
      vendorName: null,
      status: '',
      currency: null,
      search: null,
      currencyObj:['', []],

    });
    this.getVendorData();
        if(type=='reset') {
      this.searchToggle = true
    }
    else {
      this.searchToggle = false
      this.firstLoad = false
    }
  }


  changeSelection(name,event) {
    this.currencyName = name ? name : this.currencyName;
    
    var currencyObj = _.find(this.currencylist, { name: this.currencyName });
    console.log("currencyObj",currencyObj);
    this.vendorSearchForm.get('currency').setValue(currencyObj.id);
    if (typeof event == 'object') {
          this.vendorSearchForm.get('currency').setValue(currencyObj.id);
        }

  }

  clearMainSearch(type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.clearSearch(type);
      this.searchToggle = true
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }

  resetPagination(){    
    this.offset=0;
    this.currentPage=1;
  }
  resetApprovedVendorValue(){
    console.log("-----")
    this.vendorSearchForm.get('approvedVendor').setValue("");
  }
  

  onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container')){
        console.log('wow--->closeee----')
      }
      else {
    this.firstLoad = false
  }
  }

 

}



export interface Vendor {

  id: number;
  vendorCode: string;
  vendorName: string;
  currency: number;
  approvedDate: string;
  approvedVendor: string;
  status: string;
  vendorId: number;
  createdAt: string;
  updatedAt: string;
}

