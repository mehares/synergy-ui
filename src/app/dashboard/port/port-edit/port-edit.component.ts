 import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import {CommonservicesService} from '../../../helper/commonservices/commonservices.service';
import {CommonFunctionsService} from '../../../helper/commonFunctions/common-functions.service';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';

import { Router ,ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import {globalConstants} from '../../../constants/global-constants';
import * as moment from 'moment';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-port-edit',
  templateUrl: './port-edit.component.html',
  styleUrls: ['./port-edit.component.scss']
})

export class PortEditComponent implements OnInit {
  portForm: FormGroup;
  submitted = false;
  countries = [];
  // position: NbGlobalPosition = NbGlobalPhysicalPosition.BOTTOM_RIGHT;
  loading = false;
  itemSubmitted = false;
  id=null;
  portList=null;
  keywordCountry = 'name';

    constructor(private formBuilder: FormBuilder, private toastr: ToastrService ,private router: Router,private http: HttpClient ,private activatedroute: ActivatedRoute,private CS: CommonservicesService,private spinner: NgxSpinnerService, ) { }

  ngOnInit() {
    this.id = this.activatedroute.snapshot.params.id;
    console.log('this.activatedroute.snapshot.params',this.activatedroute.snapshot.params);
    this.createForm();
    this.getCountries();
    this.getPortData();
  }

getPortData() {
    let _this = this;
    let params = { model_name: 'port', where: { 'id': this.id }, id: this.id };
    _this.CS.gePortData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.portList = response.result;
        
        _this.setPortForm();
        console.log("_this.portList", _this.portList);
      } else {
        this.toastr.error('Port Details Not available', 'Failed!');
      }
    });
  }
  createForm() {
    this.portForm = this.formBuilder.group({
      name: ['', Validators.required],
      shortName: ['', Validators.required],
      isoCode: ['', Validators.required],
      unCode: ['', Validators.required],
      countryXid: ['', Validators.required],
      latitude: [null],
      latitudeDirection: [null],
      longitude: [null],
      longitudeDirection: [null],
      draft: ['0.00'],
      berth: ['0.00'],
      berthType: [null],
      status: '',
      countryName:''
    });
    this.createItems();
    console.log(" this.portForm", this.portForm);
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.portForm.controls;
  }

  createItems(flag?) {
    if (flag) {
      if (this.portForm.controls.details.status == 'INVALID') {
        this.submitted = true;
        return false;
      }
    }
    let item = this.formBuilder.group({
      name: ['', Validators.required],
      shortName: ['', Validators.required],
      isoCode: ['', Validators.required],
      unCode: ['', Validators.required],
      countryXid: ['', Validators.required],
      latitude: [null],
      latitudeDirection: [null],
      longitude: [null],
      longitudeDirection: [null],
      draft: ['0.00'],
      berth: ['0.00'],
      berthType: [null],
      status: '',
      countryName:''
    });
  }


  getCountries() {
    let _this = this;
    let params = { model_name: 'country' };
    _this.CS.getData(params).subscribe(response => {
      console.log("getCountries", response);
      if (response && response.status == "success" && response.result) {
        _this.countries = response.result;
      } else {
        _this.countries = [];
      }
    });
  }


  onSubmit() {
    let _this = this;
    console.log("this.portForm Data", _this.portForm);
    let formData = new FormData();
    if (this.portForm.status == 'VALID') {
      const portObj = {
        name:this.form.name.value,
        shortName:this.form.shortName.value,
        isoCode:this.form.isoCode.value,
        unCode:this.form.unCode.value,
        countryXid:this.form.countryXid.value,
        latitude:this.form.latitude.value,
        latitudeDirection: this.form.latitudeDirection.value,
        longitude:  this.form.longitude.value,
        longitudeDirection:  this.form.longitudeDirection.value,
        draft:  this.form.draft.value,
        berth:  this.form.berth.value,
        berthType:   this.form.berthType.value,
        status: this.form.status.value,
        id:this.id
    }

      _this.loading = true;
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
      _this.CS.updatePortData(portObj).subscribe(response => {
        if ( response.status ) {
          _this.loading = false;
          this.toastr.success('Port Details Updated', 'Success!');
        } else {
          _this.loading = false;
           this.toastr.error('Failed to Save', 'Failed!');
        }
      });


    } else {
      _this.submitted = true;
    }

  }

 setPortForm() {
    this.portForm = this.formBuilder.group({
      name: [this.portList.name, Validators.required],
      shortName: [this.portList.shortName, Validators.required],
      isoCode: [this.portList.isoCode, Validators.required],
      unCode: [this.portList.unCode, Validators.required],
      countryXid: [this.portList.countryXid, Validators.required],
      latitude: [this.portList.latitude],
      latitudeDirection: [this.portList.longitudeDirection],
      longitude: [this.portList.longitude],
      longitudeDirection: [this.portList.longitudeDirection],
      draft: [this.portList.draft],
      berth: [this.portList.berth],
      berthType: [this.portList.berthType],
      status: [this.portList.status],
      countryName:this.portList.PUSR.name


    })
  }


      changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, {id: parseInt(getId, 10)});
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.portForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.portForm.get(from).setValue(getId);
          }
          console.log("this.portForm", this.portForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.portForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.portForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.portForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.portForm.get(from).setValue(null);
        }
      }
    }
  }
allownsew(event){
  console.log("event",event)
  var key = event.charCode
  return((key ==110 || key ==119 || key == 115 || key ==101 || key ==83 || key ==78 || key ==69 || key == 87))
}


omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}

}
