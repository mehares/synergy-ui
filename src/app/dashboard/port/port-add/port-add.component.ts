import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import {CommonservicesService} from '../../../helper/commonservices/commonservices.service';
import {CommonFunctionsService} from '../../../helper/commonFunctions/common-functions.service';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
 
import { Router } from '@angular/router';
import * as _ from 'lodash';
import {globalConstants} from '../../../constants/global-constants';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-port-add',
  templateUrl: './port-add.component.html',
  styleUrls: ['./port-add.component.scss']
})

export class PortAddComponent implements OnInit {
  portForm: FormGroup;
  submitted = false;
  countries = [];
  // position: NbGlobalPosition = NbGlobalPhysicalPosition.BOTTOM_RIGHT;
  loading = false;
  itemSubmitted = false;
  keywordCountry = 'name';
  
  constructor(private http: HttpClient,
    private router: Router,
    private formBuilder: FormBuilder,
    private CS: CommonservicesService, private toastr: ToastrService , private CF: CommonFunctionsService, private calendar: NgbCalendar,private spinner: NgxSpinnerService,) { }
  
  ngOnInit() {
    this.createForm();
    this.getCountries();
  }


  createForm() {
    this.portForm = this.formBuilder.group({
      name: ['', Validators.required],
      shortName: ['', Validators.required],
      isoCode: ['', Validators.required],
      unCode: ['', Validators.required],
      countryXid: ['', Validators.required],
      latitude: [null],
      latitudeDirection: [null],
      longitude: [null],
      longitudeDirection: [null],
      draft: ['0.00'],
      berth: ['0.00'],
      berthType: [null],
      status: 1,
    });
    this.createItems();
    console.log(" this.portForm", this.portForm);
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.portForm.controls;
  }

  createItems(flag?) {
    if (flag) {
      if (this.portForm.controls.details.status == 'INVALID') {
        this.submitted = true;
        return false;
      }
    }
    let item = this.formBuilder.group({
      name: ['', Validators.required],
      shortName: ['', Validators.required],
      isoCode: ['', Validators.required],
      unCode: ['', Validators.required],
      countryXid: ['', Validators.required],
      latitude: [null],
      latitudeDirection: [null],
      longitude: [null],
      longitudeDirection: [null],
      draft: ['0.00'],
      berth: ['0.00'],
      berthType: [null],
      status: 1,
    });
  }


  getCountries() {
    let _this = this;
    let params = { model_name: 'country' };
    _this.CS.getData(params).subscribe(response => {
      console.log("getCountries", response);
      if (response && response.status == "success" && response.result) {
        _this.countries = response.result;
      } else {
        _this.countries = [];
      }
    });
  }


  onSubmit() {
    let _this = this;
    console.log("this.portForm", _this.portForm);
    let formData = new FormData();

    if (this.portForm.status == 'VALID') {
      _this.loading = true;
     this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
      _this.CS.savePortData(_this.portForm.value).subscribe(response => {
        if (response && response.status == "success") {
          _this.loading = false;
          this.toastr.success('Port Details Saved', 'Success!');
          
          // _this.toastrService.show(
          //   'Success',
          //   'Port details added successfully!',
          //   { position: _this.position, status: 'success' });
          _this.router.navigate(['/port/edit', response.result.id]);
        } else {
          _this.loading = false;
          _this.toastr.error('Failed to Save', 'Fail!');
          // _this.toastrService.show(
          //   'Danger',
          //   'Something went wrong!',
          //   { position: _this.position, status: 'danger' });
        }
      });


    } else {
      _this.submitted = true;
    }

  }

    changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, {id: parseInt(getId, 10)});
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.portForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.portForm.get(from).setValue(getId);
          }
          console.log("this.portForm", this.portForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.portForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.portForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.portForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.portForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.portForm.get(from).setValue(null);
        }
      }
    }
  }

allownsew(event){
  console.log("event",event)
  var key = event.charCode
  return((key ==110 || key ==119 || key == 115 || key ==101 || key ==83 || key ==78 || key ==69 || key == 87))
}

omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}
}
