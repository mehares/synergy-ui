import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {PortRoutingModule} from './port-routing.module';

import { PortListComponent } from './port-list/port-list.component';
import { PortAddComponent } from './port-add/port-add.component';
import { PortEditComponent } from './port-edit/port-edit.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { ClickOutsideModule } from 'ng-click-outside';


@NgModule({
  declarations: [PortListComponent, PortAddComponent, PortEditComponent],
  imports: [
    CommonModule,
    PortRoutingModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    AngularFontAwesomeModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    ClickOutsideModule
  ],
  
})
export class PortModule { }
