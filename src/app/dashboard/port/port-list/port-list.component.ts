import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {CommonservicesService} from '../../../helper/commonservices/commonservices.service';
import {CommonFunctionsService} from '../../../helper/commonFunctions/common-functions.service';
 
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {globalConstants} from '../../../constants/global-constants';
import * as _ from 'lodash';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-port-list',
  templateUrl: './port-list.component.html',
  styleUrls: ['./port-list.component.scss']
})
export class PortListComponent implements OnInit {

  // constructor(private router: Router, private CS: CommonservicesService, private CF: CommonFunctionsService,
  //   private toastrService: NbToastrService, private formBuilder: FormBuilder) {
      
constructor(private http: HttpClient,
              private router: Router,
              private formBuilder: FormBuilder,
              private CS: CommonservicesService, private CF: CommonFunctionsService,private toastr: ToastrService,private spinner: NgxSpinnerService,) {
    this.userQuestionUpdate.pipe(

      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.resetPagination();
        this.onSubmit();
        if (this.search != '') {
          this.searchBox = true;
        }
        else {
          this.searchBox = false;
        }
      });
  }

  ports = [];

  users = [];
  currentPage = 1;
  totalItems = 0;
  offset = 0;
  // position: NbGlobalPosition = NbGlobalPhysicalPosition.BOTTOM_RIGHT;
  loader = {};
  permissions = this.CF.findPermissions();
  itemsPerPage = this.CS.getItemPerPage();
  UPLOAD_PATH = globalConstants.UPLOADS_DIR;
  userQuestionUpdate = new Subject<string>();
  search = '';
  searchToggle = false;
  searchLoader = false;
  isValidFormSubmitted = null;
  submitted = false;
  searchBox = false;
  portSearchForm: FormGroup;
  keywordCountry = 'name';
  countries = [];
  pageOffset = 0;
  firstLoad = false;
  countryName = '';
  isSearchRequest=false;

  ngOnInit() {
    this.getPortData();
    this.getCountries();
    this.firstLoad = true;
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    this.portSearchForm = this.formBuilder.group({
      search: ['', []],
      name: ['', []],
      shortName: ['', []],
      isoCode: ['', []],
      unCode: ['', []],
      countryXid: ['', []],
      countryName:['', []]

    });


  }

  getPortData() {
    let _this = this;
    var params = {limit: this.itemsPerPage, offset: this.offset};
    _this.loader = true
    this.pageOffset = this.offset;
    this.isSearchRequest=false;
    _this.CS.countPortData(params).subscribe(response => {
      console.log("gePortDatagePortData", response);
       this.loader = false;
      if (response.status && response.status == 'success') {
        _this.ports = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
       
      } else {
      /*  this.toastrService.show(
          'Failed',
          'Error occured while saving,Try again!',
          {position: this.position, status: 'danger'}); */
        this.toastr.error('Port List Not available', 'Failed!');
      }
    });
  }

  pageChanged(event) {
    console.log("event", event);
    //this.loader = true;
    if (this.submitted == true) {
      this.itemsPerPage = this.CS.getItemPerPage();
      this.currentPage = event;
      var setoffset = event - 1;
      setoffset = setoffset * this.itemsPerPage;
      this.offset = setoffset;
      this.ports;
    }
    else {
      this.itemsPerPage = this.CS.getItemPerPage();
      this.currentPage = event;
      var setoffset = event - 1;
      setoffset = setoffset * this.itemsPerPage;
      this.offset = setoffset;
      this.loader = true;
      this.getPortData();
    }

  }


  changeportsStatus(id, status) {

    let _this = this;
    let params = {id, status};
    this.CS.changePortStatus(params).subscribe(response => {
      console.log("changePortStatus", response);
      if (response && response.status == 'success') {
        this.getPortData();
       this.toastr.success('Status Updated', 'Success!', {
       timeOut: 2000
      });

       /* _this.toastrService.show(
          'Success',
          'Succesfully updated.',
          {position: _this.position, status: 'success'}); */
      } else {
        this.toastr.error('Failed to Update', 'Failed!');
      /*  _this.toastrService.show(
          'Failed',
          'Error occured while saving,Try again!',
          {position: _this.position, status: 'danger'}); */
      }
    });
    //}
  }

  exportPorts() {
    let _this = this;
    _this.CS.portsSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'ports/' + response.filename, '_blank');
      /*  _this.toastrService.show(
          'Success',
          'Successfuly exported excel!',
          {position: this.position, status: 'success'}); */
      }
      else {
      /*  _this.toastrService.show(
          'danger',
          response.msg,
          {position: this.position, status: 'danger'}); */

      }
    });
  }

  getCountries() {
    let _this = this;
    let params = {model_name: 'country'};
    _this.CS.getData(params).subscribe(response => {
      console.log("getCountries", response);
      if (response && response.status == "success" && response.result) {
        _this.countries = response.result;
      } else {
        _this.countries = [];
      }
    });
  }

  get f() {
    return this.portSearchForm.controls;
  }

  onSubmit() {
    console.log("this.portSearchForm", this.portSearchForm.value);
    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;
    if (this.portSearchForm.invalid) {
     /* _this.toastrService.show(
        'Invalid',
        'Please validate fields!',
        {position: _this.position, status: 'danger'}); */
      return;
    }
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;
    const searchObj = {
      search: this.search,
      name: this.f.name.value,
      shortName: this.f.shortName.value,
      isoCode: this.f.isoCode.value,
      unCode: this.f.unCode.value,
      countryXid: this.f.countryXid.value
    };
    this.resetPagination();   
    this.isSearchRequest=true;
    this.CS.searchPort(searchObj).subscribe(response => {
      console.log("response", response);
      _this.searchLoader = false;
      _this.searchToggle = false;
      _this.loader = false;
      if (response && response.status == 'success') {
        _this.ports = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
        this.pageOffset = 0;
        //this.pageChanged(event)
      } else {
       /* _this.toastrService.show(
          'Failed',
          'Error occured while search,Try again!',
          {position: _this.position, status: 'danger'}); */
      }
    });
  }

  changeSelection(name) {
    // console.log("changeSelection", name);
    // this.portSearchForm.get('countryXid').setValue(name);
    this.countryName = name ? name : this.countryName;
    
    var contryObj = _.find(this.countries, { name: this.countryName });
    this.portSearchForm.get('countryXid').setValue(contryObj.id);

  }
 
  clearSearch(type) {
    if (type =='reset') {
      this.searchToggle =true
    }
    else {
      this.searchToggle = false
      this.firstLoad = false;
    }
    this.resetPagination()
    this.loader = false;
    this.searchBox = false;
    this.portSearchForm = this.formBuilder.group({
      name: ['', []],
      shortName: ['', []],
      isoCode: ['', []],
      unCode: ['', []],
      countryXid: ['', []],
      search: ['', []],
      countryName : ['', []],
    });
    
    this.getPortData();
  }

  clearMainSearch(type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.clearSearch(type);
      this.searchToggle = true;
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }
 resetPagination(){    
    this.offset=0;
    this.currentPage=1;
  }

    onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container')){
        console.log('wow--->closeee----')
      }
      else {
    this.firstLoad = false
  }
  }
}
