import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {PortListComponent } from './port-list/port-list.component';
import { PortAddComponent } from './port-add/port-add.component';
import { PortEditComponent } from './port-edit/port-edit.component';
import { PageGuard } from '../../guards/page-guard.service';

const routes: Routes = [
  {
    path: 'list',
    canActivate: [PageGuard],
    component: PortListComponent,
    data:{permissions: 'ports_view' }
  },
    {
    path: 'add',
    canActivate: [PageGuard],
    component: PortAddComponent,
    data:{permissions: 'ports_add' }
  },
    {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: PortEditComponent,
    data:{permissions: 'ports_edit' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortRoutingModule { }
