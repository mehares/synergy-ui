import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {ItemGroupRoutingModule} from './item-routing.module';

import { ItemgroupListComponent } from './itemgroup-list/itemgroup-list.component';
import { ItemgroupAddComponent } from './itemgroup-add/itemgroup-add.component';
import { ItemgroupEditComponent } from './itemgroup-edit/itemgroup-edit.component';
import { NgxSpinnerModule } from "ngx-spinner";

import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
  declarations: [ ItemgroupListComponent, ItemgroupAddComponent, ItemgroupEditComponent],
  imports: [
    CommonModule,
    ItemGroupRoutingModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    AngularFontAwesomeModule,
    NgxSpinnerModule,
    ClickOutsideModule
  ],
  
})
export class ItemGroupModule { }
