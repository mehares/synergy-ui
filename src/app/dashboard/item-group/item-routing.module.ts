import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ItemgroupListComponent } from './itemgroup-list/itemgroup-list.component';
import { ItemgroupAddComponent } from './itemgroup-add/itemgroup-add.component';
import { ItemgroupEditComponent } from './itemgroup-edit/itemgroup-edit.component';
import { PageGuard } from '../../guards/page-guard.service';


const routes: Routes = [
  {
    path: 'list',
    canActivate: [PageGuard],
    component: ItemgroupListComponent,
    data:{permissions: 'itemGroups_view' }
  },
    {
    path: 'add',
    canActivate: [PageGuard],
    component: ItemgroupAddComponent,
    data:{permissions: 'itemGroups_add' }
  },
    {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: ItemgroupEditComponent,
    data:{permissions: 'itemGroups_edit' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemGroupRoutingModule { }
