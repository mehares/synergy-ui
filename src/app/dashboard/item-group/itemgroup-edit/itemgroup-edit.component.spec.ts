import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemgroupEditComponent } from './itemgroup-edit.component';

describe('ItemgroupEditComponent', () => {
  let component: ItemgroupEditComponent;
  let fixture: ComponentFixture<ItemgroupEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemgroupEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemgroupEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
