import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import {globalConstants} from '../../../constants/global-constants';
import {v1 as uuidv1} from 'uuid';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-itemgroup-edit',
  templateUrl: './itemgroup-edit.component.html',
  styleUrls: ['./itemgroup-edit.component.scss']
})
export class ItemgroupEditComponent implements OnInit {

  submitted = false;
  id = null;
  groupList = null;
  loading = false;
  groupForm: FormGroup;
  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private activatedroute: ActivatedRoute,
    private CS: CommonservicesService,private toastr: ToastrService,private spinner: NgxSpinnerService,
    ) {

  }

  ngOnInit() {
    this.id = this.activatedroute.snapshot.params.id;
    console.log('this.activatedroute.snapshot.params',this.activatedroute.snapshot.params);
    this.createGroupForm();
    this.getGroupData();
  }

  


   getGroupData() {
    let _this = this;
    let params = { model_name: 'groups', where: { 'id': this.id }, id: this.id };
    _this.CS.getGroupsData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.groupList = response.result;
        
        _this.setGroupForm();
        console.log("_this.groupList", _this.groupList);
      } else {
      	this.toastr.error('Something Went Wrong', 'Failed!');
      }
    });
  }
  createGroupForm() {
    this.groupForm = this.formBuilder.group({
      groupName: ['', Validators.required],
      status:1
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.groupForm.controls;
  }


    onSubmit() {
    let _this = this;
    this.submitted = true;
  
     if (this.groupForm.invalid) {
     	this.toastr.error('Please Fill The Details', 'Error!');
      return;
    }
    
    const groupObj = {
      groupName: this.form.groupName.value,
      status:1,
      id : this.id,
    };
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
     _this.CS.updateGroupsData(groupObj)
      .subscribe(response => {
        console.log("response",response);
        if(response && response.status == true){
        	this.toastr.success('Group Details Updated Successfully', 'Success!');
        }else{
        	this.toastr.error('Error Occured While Saving', 'Failed!');

        }
       
      });
  }





  

  setGroupForm() {
    this.groupForm = this.formBuilder.group({
      groupName: [this.groupList.groupName, Validators.required],
      status:1,
    });
  }



omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}

  
}
