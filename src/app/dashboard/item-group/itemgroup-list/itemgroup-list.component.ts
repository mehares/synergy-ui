import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { globalConstants } from '../../../constants/global-constants';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-itemgroup-list',
  templateUrl: './itemgroup-list.component.html',
  styleUrls: ['./itemgroup-list.component.scss']
})
export class ItemgroupListComponent implements OnInit {

groupList=[];
  currentPage = 1
  totalItems = 0
  offset = 0
  UPLOAD_PATH = globalConstants.UPLOADS_DIR
  userQuestionUpdate = new Subject<string>();
  search = '';
  searchToggle = false;
  searchLoader = false;
  isValidFormSubmitted = null;
  submitted = false;
  searchBox = false;
  groupSearchForm:FormGroup;
  pageOffset = 0;
  firstLoad = false;
  isSearchRequest=false;
  
  constructor( private http: HttpClient,private CF: CommonFunctionsService ,private CS: CommonservicesService,private formBuilder: FormBuilder,private toastr: ToastrService,private spinner: NgxSpinnerService,) {
  this.userQuestionUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.resetPagination();
        this.onSubmit();
        if (this.search !='') {
          this.searchBox = true
            }
        else {
          this.searchBox = false
          }
      }); 
   }
  permissions = this.CF.findPermissions()
  itemsPerPage = this.CS.getItemPerPage();
  ngOnInit() {
    this.getGroup();
    this.firstLoad = true;
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    this.groupSearchForm = this.formBuilder.group({
      search: ['', []],
      groupName: ['', []],
    });
    
  }

getGroup() {
  let _this = this;
  let params = {limit: this.itemsPerPage, offset: this.offset };
  this.pageOffset = this.offset;
  this.isSearchRequest=false;
    _this.CS.countGroupsData(params)
    .subscribe((response) => { 
      console.log("groups",response.result)
      if(response.status && response.status == 'success'){
        this.groupList = response.result && response.result.rows ? response.result.rows : [];
         this.totalItems = response.result && response.result.count ? response.result.count : 0;
      }else{
      	this.toastr.error('Error Occured While Saving', 'Failed!');
      }
    });
  }
pageChanged(event) {
    console.log("event", event)
    if(this.submitted == true) {
    this.itemsPerPage = this.CS.getItemPerPage();
     this.currentPage = event
    var setoffset = event - 1
    setoffset = setoffset * this.itemsPerPage
    this.offset = setoffset
    this.groupList
  }
  else {
    this.itemsPerPage = this.CS.getItemPerPage();
     this.currentPage = event
    var setoffset = event - 1
    setoffset = setoffset * this.itemsPerPage
    this.offset = setoffset
    this.getGroup()
  }
}

exportItemGroup() {
  let _this = this;
  _this.CS.itemGroupsSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'itemGroups/' + response.filename, '_blank');
        this.toastr.success('Successfuly exported excel', 'Success!');
      }
      else {
      	this.toastr.error(response.msg, 'Failed!');

      }
    });
}

 get f() {
    return this.groupSearchForm.controls;
  }

  onSubmit() {

    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;
    if (this.groupSearchForm.invalid) {
    	this.toastr.error('Please Validate Fields', 'Invalid!');
      return;
    }
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;
    const searchObj = {
      search: this.search,
      groupName: this.f.groupName.value,
    };
    this.isSearchRequest=true;
    this.resetPagination();																																																																																																																																																																	
    this.CS.searchItemGroup(searchObj).subscribe(response => {
      console.log("response", response);
      _this.searchLoader = false;
      _this.searchToggle = false;
      if (response && response.status == 'success') {
        _this.groupList = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
        this.pageOffset = 0;
        //this.pageChanged(event)
      } else {
      	this.toastr.error('Error Occured While Search', 'Failed!');
      }
    });
  }
 clearSearch(type) {
   if(type =="reset") {
     this.searchToggle=true;
   }
   else {
    this.searchToggle = false;
       this.firstLoad = false;
     }
   this.searchBox = false;
   this.resetPagination();
     this.groupSearchForm = this.formBuilder.group({
      groupName: ['', []],
      search: ['', []]

    });
    this.getGroup()
  }

  clearMainSearch(type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.clearSearch(type);
      this.searchToggle = true
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }
   resetPagination(){    
    this.offset=0;
    this.currentPage=1;
  }

  
    onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container')){
        console.log('wow--->closeee----')
      }
      else {
    this.searchToggle = false
  }
  }
}
