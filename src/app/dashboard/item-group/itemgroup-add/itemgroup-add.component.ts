import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { group } from '@angular/animations';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-itemgroup-add',
  templateUrl: './itemgroup-add.component.html',
  styleUrls: ['./itemgroup-add.component.scss']
})
export class ItemgroupAddComponent implements OnInit {

  groupForm: FormGroup;
  submitted = false;
  loading = false;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private CS: CommonservicesService,
   private http: HttpClient,private toastr: ToastrService,private spinner: NgxSpinnerService,) {
  }

  ngOnInit() {
    this.createGroupForm();
  }

  createGroupForm() {
    this.groupForm = this.formBuilder.group({
      groupName: ['', Validators.required],
      status:1
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.groupForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    console.log("groupForm", this.groupForm);
    if (this.groupForm.invalid) {
    	this.toastr.error('Please Fill The Details', 'Error!');
      return;
    } else {
      let _this = this;
      _this.loading = true;
      let params = this.groupForm.value;
     this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
      _this.CS.setGroupsData(params).subscribe(response => {
        _this.loading = false;
        if (response && response.status == "success") {
        	this.toastr.success('Group Details Added Successfully', 'Success!');
          _this.router.navigate(['/item-group/edit', response.result.id]);
        } else {
        	this.toastr.error('Something Went Wrong', 'Failed!');
        }
      });
    }
  }


omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}

}
