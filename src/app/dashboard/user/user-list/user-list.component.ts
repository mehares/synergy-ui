import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { IfStmt } from '@angular/compiler';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { globalConstants } from '../../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

constructor(private router: Router, private CS: CommonservicesService, private CF: CommonFunctionsService,
    private formBuilder: FormBuilder,private toastr: ToastrService,private spinner: NgxSpinnerService,) {
    this.userQuestionUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.onSubmit();
        if (this.search != '') {
          this.searchBox = true
        }
        else {
          this.searchBox = false
        }
      });

  }

  users = [];
  currentPage = 1
  totalItems = 0
  offset = 0
  loader = {}
  permissions = this.CF.findPermissions()
  itemsPerPage = this.CS.getItemPerPage();
  searchToggle = false;
  searchLoader = false;
  userSearchForm: FormGroup;
  isValidFormSubmitted = null;
  submitted = false;
  userQuestionUpdate = new Subject<string>();
  search = '';
  UPLOAD_PATH = globalConstants.UPLOADS_DIR;
  selectedType = [];
  selectedGroup = [];
  ranks = [];
  types = [];
  companies = [];
  groups = [];
  serachType = 'all';
  serachObject = {};
  searchBox = false



  ngOnInit() {
    this.createForm();
    this.getUserData();
    this.getRanks();
    this.getTypes();
    this.getCompanies();
    this.getGroups();
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);


  }

  createForm() {
    this.userSearchForm = this.formBuilder.group({
      username: [''],
      name: [''],
      company: [null],
      types: [null],
      group: [''],
      rank: [null],
      mobileNumber: [''],
      email: [''],
      search: ['']
    });
  }

  getUserData() {

    console.log("")
    let _this = this
    var params = { limit: this.itemsPerPage, offset: this.offset, }

    if (_this.serachType == "search" && _this.serachObject) {
      params['search'] = _this.serachObject
    }

    params['status'] = [0,1,2]

    console.log("params----", params)
    _this.loader = true
    _this.CS.countUserData(params).subscribe(response => {

      _this.searchLoader = false;
      console.log("response", response)
      if (response.status && response.status == 'success') {
        _this.users = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
      } else {
      	this.toastr.error('Error Occured While Saving', 'Failed!');
      }
    });
  }

  getRanks() {
    let _this = this;
    let params = { model_name: 'ranks', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.ranks = response.result;
      } else {
        _this.ranks = [];
      }
    });
  }

  getTypes() {
    let _this = this;
    let params = { model_name: 'types', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.types = response.result;
      } else {
        _this.types = [];
      }
    });
  }

  getCompanies() {
    let _this = this;
    let params = { model_name: 'companies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.companies = response.result;
      } else {
        _this.companies = [];
      }
    });
  }

  getGroups() {
    let _this = this;
    let params = { model_name: 'groups', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.groups = response.result;
      } else {
        _this.groups = [];
      }
    });
  }

  editUser(item) {
    this.router.navigate(['/user/edit', item.id]);
  }
  changeUserStatus(id, status) {

    console.log("status___", status);
    let _this = this;
    let params = { data: { id: id, status: status }, model_name: 'users', id: id };
    _this.CS.setUserStatus(params).subscribe(response => {
      if (response && response.status == "success") {
        console.log("success");
        this.toastr.success('User Status Changed Successfully', 'Success!');

      } else {
      	this.toastr.error('Something Went Wrong', 'Failed!');
      }
    });
  }

  deleteUser(item) {
    var params = { id: item.id, model_name: 'users' }
    let _this = this
    _this.loader[item.id] = true
    _this.CS.removeData(params).subscribe(response => {
      _this.loader[item.id] = false
      if (response && response.status == "success" && response.result) {
        _this.getUserData()
        this.toastr.success('User Details Deleted Successfully', 'Success!');
      } else {
      	this.toastr.error('Something Went Wrong', 'Failed!');
      }
    });
  }
  pageChanged(event) {
    console.log("event", event)
    this.currentPage = event
    var setoffset = event - 1
    setoffset = setoffset * this.itemsPerPage
    this.offset = setoffset
    this.getUserData()

  }

  get f() {
    return this.userSearchForm.controls;
  }

  onSubmit() {

    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.searchBox = true;
    this.isValidFormSubmitted = false;

    if (this.userSearchForm.invalid) {
    	this.toastr.error('Please Validate Fields', 'Invalid!');
      return;
    }

    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM*', this.f);
    this.isValidFormSubmitted = true;


    _this.searchToggle = !_this.searchToggle;
    _this.offset = 0;
    _this.currentPage=1;
    _this.serachType = 'search';
    _this.itemsPerPage = this.CS.getItemPerPage();
    _this.serachObject = {
      search: (this.search ? this.search : null),
      advance: (this.search ? null : this.userSearchForm.value)
    };


    console.log('_this.serachObject', _this.serachObject);
    console.log('search', this.search)
    this.currentPage = 1;
    _this.searchToggle = false;
    this.getUserData();
    _this.loader = false;
    /*
    this.CS.searchUsers(searchObj).subscribe(response => {
      console.log("response", response);
      _this.searchLoader = false;
      _this.searchToggle = false;
      if (response && response.status == 'success') {
        _this.users = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
      } else {
        _this.toastrService.show(
          'Failed',
          'Error occured while search,Try again!',
          { position: _this.position, status: 'danger' });
      }
    });

    */
  }
  
  resetSearchForm(type) {
    if(type=='reset') {
    this.createForm();
    this.serachObject = {
      search: null,
      advance: null
    };
    this.serachType = 'all';
    this.getUserData();
    this.searchToggle=true;
    this.searchBox =false
    this.selectedGroup = []
    this.selectedType = []
    console.log("---",this.userSearchForm);
  }
  else {
    this.createForm();
    this.serachObject = {
      search: null,
      advance: null
    };
    this.serachType = 'all';
    this.getUserData();
    this.searchToggle=false;
    this.searchBox =false
    this.selectedGroup = []
    this.selectedType = []
    console.log("form",this.selectedGroup)
  }
  }
    clearMainSearch(type) {
    if (this.search != '') {
      console.log("Clearing Data");
      this.search = '';
      this.resetSearchForm(type);
    }
  }

  exportUser() {
    let _this = this;
    _this.CS.userSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'user/' + response.filename, '_blank');
        this.toastr.success('Successfuly Exported Excel', 'Success!');
      }
      else {
      	this.toastr.error(response.msg, 'Failed!');

      }
    });
  }

    clearSelection(to) {
    this.userSearchForm.get(to).setValue(null);
  }

    onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container')){
        console.log('wow--->closeee----')
      }
      else {
    this.searchToggle = false
  }
  }

}
