import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { group } from '@angular/animations';
import * as _ from 'lodash';
import {v1 as uuidv1} from 'uuid';
import { ToastrService } from 'ngx-toastr';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import {CommonFunctionsService} from '../../../helper/commonFunctions/common-functions.service';
// import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSpinnerService } from "ngx-spinner";
import { globalConstants } from '../../../constants/global-constants';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  registerForm: FormGroup;
  submitted = false;
  selectedType = [];
  selectedGroup = [];
  ranks = [];
  types = [];
  companies = [];
  groups = [];
  usersList =[];
  reportToUser = null;
  loading = false;
  keywordReport = 'username';
  model: NgbDateStruct;  
  fileToUpload: File = null;
  imageSrc = null;
  UPLOAD_PATH = globalConstants.UPLOADS_DIR;

  

  constructor(private router: Router,private el: ElementRef,
    private formBuilder: FormBuilder,
    private CS: CommonservicesService,private toastr: ToastrService,private calendar: NgbCalendar, private CF: CommonFunctionsService,private spinner: NgxSpinnerService,) {
  }

  ngOnInit() {
    this.createForm();
    this.getRanks();
    this.getTypes();
    this.getCompanies();
    this.getGroups();
    this.getUserData();
  }

  createForm() {
    this.registerForm = this.formBuilder.group({
      password: 'test@123',
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      username: ['', Validators.required],
      name: ['', Validators.required],
      dateOfJoining: ['', Validators.required],
      company: [null, Validators.required],
      types: [null],
      reportTo: ['', Validators.required],
      group: [null],
      rank: [null,Validators.required],
      mobileNumber: ['', [Validators.required,Validators.pattern(this.mobNumberPattern)]],
      permanentAddress: ['', Validators.required],
      temporaryAddress: ['', Validators.required],
      // active: [''],
      status: [0],
      isSuperAdmin:0
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.registerForm.controls;
  }
  fieldUpdate(value, type) {
    let updatedRes = [];
    if (value) {                                        
      value.forEach(element => {
        let groupElement = {
          [type]: element,
        }
        updatedRes.push(groupElement);
      });
      return updatedRes;
    } else {
      return updatedRes;
    }
  }

   toDateObject(date){
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date){
    return this.CF.dateObjectToJsonDate(date)
  }
  onSubmit() {
    this.submitted = true;

    console.log("registerForm", this.registerForm);
    console.log("registerForm################", this.registerForm.value);
    // stop here if form is invalid
    console.log('Form Status', this.registerForm.invalid);
    if (this.registerForm.invalid) {
    	this.toastr.error('Please Fill The Details', 'Error!');
      return;
    } else {
      let _this = this;
      _this.loading = true;
      
      let formData = new FormData();
      console.log('this.fileToUpload---',this.fileToUpload);
      
      if (this.fileToUpload && !this.fileToUpload['id']) {
          formData.append("files", this.fileToUpload, uuidv1() + this.fileToUpload.name);
      }

      if (this.registerForm.value.status) {
      this.registerForm.get("status").setValue(1);
      }
      let params =  {
      	password: this.form.password.value,
				email: this.form.email.value,
				username: this.form.username.value,
 				name: this.form.name.value,
				dateOfJoining: this.toDateObject(this.form.dateOfJoining.value),
				company: this.form.company.value,
				reportTo: this.form.reportTo.value,
				rank: this.form.rank.value,
				mobileNumber: this.form.mobileNumber.value,
 				permanentAddress: this.form.permanentAddress.value,
				temporaryAddress: this.form.temporaryAddress.value,
				status: this.form.status.value,
				isSuperAdmin: this.form.isSuperAdmin.value
      }
      var groupId = this.fieldUpdate(this.selectedGroup, 'groupId');
      console.log(groupId);
      const typeId = this.fieldUpdate(this.selectedType, 'typeId');
      params['UG'] = this.fieldUpdate(this.selectedGroup, 'groupId');
      params['UT'] = this.fieldUpdate(this.selectedType, 'typeId');
      params['types'] = this.selectedType;
      params['group'] = this.selectedGroup;
      let user_id = 0;
          this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
      _this.CS.setUserData(params).subscribe(response => {
      	console.log(response);
        _this.loading = false;
        
        if (response && response.status == "success") {
           user_id = response.result.id;
          _this.CS.sendMail(_this.registerForm.value).subscribe(responseMl => {


                       
                if (this.fileToUpload && !this.fileToUpload['id']) {            
                  formData.append("data", response.result.id);                        
                  _this.CS.saveUserProfile(formData).subscribe(response => {
                    _this.loading = false;
                    if (response && response.data && response.data.status == "success") {
                      this.toastr.success('User Details Updated', 'Success!');
                    } else {
                      this.toastr.error('Something Went Wrong on image upload.', 'Failed!');
                    }
                  });
                }
          });
          this.toastr.success('User Details Added Successfully', 'Success!');
          _this.router.navigate(['/user/edit', user_id]);
        } else {
          
         let errorMsg = 'Failed to Add User.!';
        if (response.messageTxt) {
            errorMsg = response.messageTxt;
          }
          this.toastr.error(errorMsg, 'Failed!');
        }
      });
    }
  }

  copyClick(event: Event) {
    const input = (event.target as HTMLInputElement);
    if (input.checked) {
      this.registerForm.get("temporaryAddress").setValue(this.registerForm.value.permanentAddress);
    }
  }

  getRanks() {
    let _this = this;
    let params = { model_name: 'ranks', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.ranks = response.result;
      } else {
        _this.ranks = [];
      }
    });
  }

  getTypes() {
    let _this = this;
    let params = { model_name: 'types', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.types = response.result;
      } else {
        _this.types = [];
      }
    });
  }

  getCompanies() {
    let _this = this;
    let params = { model_name: 'companies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.companies = response.result;
      } else {
        _this.companies = [];
      }
    });
  }

  getGroups() {
    let _this = this;
    let params = { model_name: 'groups', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.groups = response.result;
      } else {
        _this.groups = [];
      }
    });
  }

  getUserData() {
    let _this = this
    var params = {}
    _this.CS.getUserData(params).subscribe(response => {
      console.log("response", response)
      if (response && response.status == "success" && response.result) {
        _this.usersList = response.result
        console.log('_this.usersList',_this.usersList);
        
      } else {
        _this.usersList = []
      }
    });
  }

  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, {id: parseInt(getId, 10)});
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.registerForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.registerForm.get(from).setValue(getId);
          }
          console.log("this.registerForm", this.registerForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.registerForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.registerForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.registerForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.registerForm.get(from).setValue(null);
        }
      }
    }
  }

  deleteDoc() {
    delete this.fileToUpload;
  }

  upload(files: File) {
    let _this = this;


    _.forEach(files, function (value) {
      var fileTypes = ['jpg', 'jpeg', 'png'];  //acceptable file types
      var extension = value.name.split('.').pop().toLowerCase()
      console.log(extension)
      var Success = fileTypes.indexOf(extension) > -1
      if (Success) {
         if (value.size < 4060000 ) {
        var reader = new FileReader();
        reader.readAsDataURL(value);
        reader.onload = () => {
          var src = reader.result as string;
         
            _this.imageSrc = src;
            _this.fileToUpload = value;
          } }
           else {
            alert('Maximum allowed file size is 4 MB!');
            return
          
        };
      } else {
        _this.toastr.error('File type not supported')
      }


    });
    document.getElementById('fileUpload')['value'] = "";
  }

  triggerFileUpload() {
    this.el.nativeElement.querySelector("#fileUpload").click();
  }


  omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}

}
