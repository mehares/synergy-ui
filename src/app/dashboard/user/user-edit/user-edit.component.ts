import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { globalConstants } from '../../../constants/global-constants';
import * as _ from 'lodash';
import { v1 as uuidv1 } from 'uuid';
import { ToastrService } from 'ngx-toastr';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
// import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  registerForm: FormGroup;
  submitted = false;
  selectedType = [];
  selectedGroup = [];
  ranks = [];
  types = [];
  companies = [];
  groups = [];
  id = null;
  userData = null;
  usersList = [];
  loading = false;
  fileToUpload: File = null;
  imageSrc = null;
  DOC_PATH = globalConstants.UPLOADS_DIR;
  profileEdit = false;
  user_edit_or_profile = "User Edit";
  keywordReport = 'username';
  usersReportTo = null;
  isSuper = false;
  model: NgbDateStruct;


  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private CS: CommonservicesService, private activatedroute: ActivatedRoute,
    private location: Location,
    private el: ElementRef, private toastr: ToastrService, private calendar: NgbCalendar, private CF: CommonFunctionsService, private spinner: NgxSpinnerService,) {

  }

  ngOnInit() {
    if (this.activatedroute.snapshot.params.id) {
      this.id = this.activatedroute.snapshot.params.id;
      this.profileEdit = false;
    } else {
      this.id = localStorage.getItem('sender_pro_id');
      this.profileEdit = true;
      this.user_edit_or_profile = "Update Profile";
    }


    console.log('this.activatedroute.snapshot.params', this.activatedroute.snapshot.params);
    // this.profileEdit = this.activatedroute.snapshot.params.profileEdit;
    this.createForm();
    this.getRanks();
    this.getTypes();
    this.getCompanies();
    this.getGroups();
    this.getUserData();
    this.getProfileImage();
    this.getData();
  }

  getData() {
    let _this = this;
    let params = { model_name: 'users', where: { 'id': this.id }, id: this.id };
    _this.CS.getUserData(params).subscribe(response => {
      console.log("User Data", response);
      if (response && response.status == "success" && response.result) {
        _this.userData = response.result;

        _this.selectedGroup = _this.fieldSelect(response.result.UG, 'groupId');
        _this.selectedType = _this.fieldSelect(response.result.UT, 'typeId');
        //console.log('selectedType', _this.selectedType);
        //console.log('selectedGroup', _this.selectedGroup);
        /*if (this.userData.isSuperAdmin) {
          this.isSuper = true;
        }*/

        if (this.userData.reportTo != null) {
          _this.CS.getUserData({ where: { 'id': this.userData.reportTo }, id: this.userData.reportTo }).subscribe(response => {
            console.log("this.userData.reportTo", this.userData.reportTo);
            console.log("res", response);
            if (response && response.status == "success" && response.result) {
              _this.usersReportTo = response.result;
              console.log("_this.usersReportTo", _this.usersReportTo);
            } else {
              //  _this.usersList = [];
            }
            _this.setForm();
          })
        } else {
          _this.setForm();
        }
        console.log("_this.userData", _this.userData);
      } else {
        this.toastr.error('No such user exist', 'Danger!');
        _this.router.navigate(['/user/list']);
      }
    });
  }

  createForm() {
    this.registerForm = this.formBuilder.group({
      // password: 'test@123',
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      username: ['', Validators.required],
      name: ['', Validators.required],
      dateOfJoining: ['', Validators.required],
      company: [null, Validators.required],
      types: [null],
      reportTo: ['', Validators.required],
      group: [null],
      rank: [null, Validators.required],
      mobileNumber: ['', [Validators.required, Validators.pattern(this.mobNumberPattern)]],
      permanentAddress: ['', Validators.required],
      temporaryAddress: ['', Validators.required],
      active: [''],
      status: 1,
      reportToName: [''],
      isSuperAdmin: ['']
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.registerForm.controls;
  }
  fieldSelect(value, type) {
    let updatedRes = [];
    if (value) {
      value.forEach(element => {
        updatedRes.push(element[type]);
      });
      updatedRes = _.sortedUniq(updatedRes);
      return updatedRes;
    } else {
      return updatedRes;
    }

  }
  fieldUpdate(value, type) {
    let updatedRes = [];
    if (value) {
      value.forEach(element => {
        let fieldElement = {
          [type]: element,
        }
        updatedRes.push(fieldElement);
      });
      return updatedRes;
    } else {
      return updatedRes;
    }
  }

  toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date) {
    return this.CF.dateObjectToJsonDate(date)
  }

  onSubmit() {
    this.submitted = true;

    console.log("registerForm", this.registerForm);
    console.log("registerForm######", this.registerForm.value);
    // stop here if form is invalid
    console.log('Form Status', this.registerForm.invalid);
    if (this.registerForm.invalid) {
      this.toastr.error('Please Fill The Details', 'Error!');
      return;
    } else {
      let _this = this;
      _this.loading = true;
      let formData = new FormData();
      console.log('this.fileToUpload---', this.fileToUpload);

      if (this.fileToUpload && !this.fileToUpload['id']) {
        // for (var i = 0; i < this.fileToUpload.length; i++) {
        formData.append("files", this.fileToUpload, uuidv1() + this.fileToUpload.name);
        // }
      }
      this.registerForm.value['id'] = this.id;
      this.registerForm.value['dateOfJoining'] = this.toDateObject(this.form.dateOfJoining.value);
      let params = { data: this.registerForm.value, model_name: 'users', id: this.id };
      params.data['UG'] = this.fieldUpdate(this.selectedGroup, 'groupId');
      params.data['UT'] = this.fieldUpdate(this.selectedType, 'typeId');
      params.data['types'] = this.selectedType;
      params.data['group'] = this.selectedGroup;
      this.spinner.show();
      setTimeout(() => this.spinner.hide(), 2000);
      _this.CS.setUserData(params).subscribe(response => {
        _this.loading = false;
        console.log("response", response);
        if (response && response.status == "success") {
          console.log('this.fileToUpload', this.fileToUpload);

          if (this.fileToUpload && !this.fileToUpload['id']) {
            formData.append("data", this.id);
            _this.CS.saveUserProfile(formData).subscribe(response => {
              _this.loading = false;
              if (response && response.data && response.data.status == "success") {
                console.log("succes __s");
                this.toastr.success('User Details Updated', 'Success!');
              } else {
                this.toastr.error('Something Went Wrong', 'Failed!');
              }
            });
            // _this.location.back();
          } else {

            if (response && response.status == "success") {
              console.log("succes __s");
              this.toastr.success('User Details Updated', 'Success!');
            } else {
              this.toastr.error('Something Went Wrong', 'Failed!');
            }


          }
        } else {
          let errorMsg = 'Something Went Wrong!';
          if (response.errorMsg) {
            errorMsg = response.errorMsg;
          }
          this.toastr.error(errorMsg, 'Failed!');
        }
      });
    }
  }

  copyClick(event: Event) {
    const input = (event.target as HTMLInputElement);
    if (input.checked) {
      this.registerForm.get("temporaryAddress").setValue(this.registerForm.value.permanentAddress);
    }
  }

  getRanks() {
    let _this = this;
    let params = { model_name: 'ranks', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.ranks = response.result;
      } else {
        _this.ranks = [];
      }
    });
  }

  getTypes() {
    let _this = this;
    let params = { model_name: 'types', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.types = response.result;
      } else {
        _this.types = [];
      }
    });
  }

  getCompanies() {
    let _this = this;
    let params = { model_name: 'companies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.companies = response.result;
      } else {
        _this.companies = [];
      }
    });
  }

  getGroups() {
    let _this = this;
    let params = { model_name: 'groups', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.groups = response.result;
      } else {
        _this.groups = [];
      }
    });
  }

  setForm() {
    this.registerForm = this.formBuilder.group({
      // password: 'test@123',
      email: [this.userData.email, [Validators.required, Validators.email]],
      username: [this.userData.username, Validators.required],
      name: [this.userData.name, Validators.required],
      dateOfJoining: [this.toJsonDate(this.userData.dateOfJoining), Validators.required],
      company: [this.userData.company, Validators.required],
      types: [this.selectedType],
      reportTo: [this.usersReportTo.id, Validators.required],
      group: [this.selectedGroup],
      rank: [this.userData.rank, Validators.required],
      mobileNumber: [this.userData.mobileNumber, Validators.required],
      permanentAddress: [this.userData.permanentAddress, Validators.required],
      temporaryAddress: [this.userData.temporaryAddress, Validators.required],
      status: [this.userData.status],
      reportToName: [this.usersReportTo.username],
      isSuperAdmin: [this.userData.isSuperAdmin]
    });
  }

  getUserData() {
    let _this = this
    var params = {}
    _this.CS.getUserData(params).subscribe(response => {
      console.log("response", response)
      if (response && response.status == "success" && response.result) {
        _this.usersList = response.result
        console.log('_this.usersList', _this.usersList);

      } else {
        _this.usersList = []
      }
    });
  }

  deleteDoc() {
    delete this.fileToUpload;
  }

  upload(files: File) {
    let _this = this;


    _.forEach(files, function (value) {
      var fileTypes = ['jpg', 'jpeg', 'png'];  //acceptable file types
      var extension = value.name.split('.').pop().toLowerCase()
      console.log(extension)
      var Success = fileTypes.indexOf(extension) > -1
      if (Success) {
         if (value.size < 4060000 ) {
        var reader = new FileReader();
        reader.readAsDataURL(value);
        reader.onload = () => {
          var src = reader.result as string;
         
            _this.imageSrc = src;
            _this.fileToUpload = value;
          } }
           else {
            alert('Maximum allowed file size is 4 MB!');
            return
          
        };
      } else {
        _this.toastr.error('File type not supported')
      }


    });
    document.getElementById('fileUpload')['value'] = "";
  }

  triggerFileUpload() {
    this.el.nativeElement.querySelector("#fileUpload").click();
  }

  getProfileImage() {
    let _this = this;
    let params = { model_name: 'userDocument', where: { 'userId': this.id }, userId: this.id };
    _this.CS.getProfileImage(params).subscribe(response => {
      if (response && response.status == "success" && response.result) {
        this.imageSrc = this.DOC_PATH + 'profileImages/' + response.result.filePath;
        this.fileToUpload = response.result;
      } else {
        console.log("No image Uploaded");
        //this.toastr.info('No Profile Image Found', 'No image!');
      }
    });
  }



  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, { id: parseInt(getId, 10) });
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.registerForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.registerForm.get(from).setValue(getId);
          }
          console.log("this.registerForm", this.registerForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.registerForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.registerForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.registerForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.registerForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.registerForm.get(from).setValue(null);
        }
      }
    }
  }


  omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}
}
