import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAddComponent } from './user-add/user-add.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { PageGuard } from '../../guards/page-guard.service';

const routes: Routes = [
{
    path: 'list',
    canActivate: [PageGuard],
    component: UserListComponent,
    data:{permissions: 'users_view' }
  },
    {
    path: 'add',
    canActivate: [PageGuard],
    component: UserAddComponent,
    data:{permissions: 'users_add' }
  },
    {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: UserEditComponent,
    data:{permissions: 'users_edit' }
  },
     {
      path: 'edit-profile',
      component: UserEditComponent,

    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
