import { Component, OnInit } from '@angular/core';
import { CommonservicesService } from '../../helper/commonservices/commonservices.service';
import {globalConstants} from '../../constants/global-constants';
import { Router } from '@angular/router';
import { CommonFunctionsService } from '../../helper/commonFunctions/common-functions.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
  
export class LayoutComponent implements OnInit {
  id='';
  imageSrc = 'assets/images/user-login.png';
  username = "Anonymous User";
  permissions = this.CF.findPermissions()
  constructor( private CS: CommonservicesService, private router: Router,private CF: CommonFunctionsService) { }

  ngOnInit() {
    this.id = localStorage.getItem('sender_pro_id');
    this.getProfileImage();
    this.username = localStorage.getItem('username'); 
   }
  
  getProfileImage() {
    let _this = this;
    let UPLOADS_DIR = globalConstants.UPLOADS_DIR;
    
    let params = { model_name: 'userDocument', where: { 'userId': this.id }, userId: this.id };
    _this.CS.getProfileImage(params).subscribe(response => {
      console.log("response.resultresponse.result",response.result)
      if (response && response.status == "success" && response.result) {
          this.imageSrc =  UPLOADS_DIR + 'profileImages/' + response.result.filePath;
      } else {
        
      }
    });
  }

  navbarOpen = false;

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

logout() {
var user =JSON.parse (localStorage.getItem('justfortemp_purposedeletelater'));
console.log("user",user);
let params = {

userId: user
}
console.log("params", params);
this.CS.logout(params).subscribe(response => {
// console.log("response", response);
// if (response.status == "success") {
// }
localStorage.clear();
this.router.navigate(['/']);
})
}
}
