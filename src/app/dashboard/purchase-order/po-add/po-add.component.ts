import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { group } from '@angular/animations';
import * as _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { globalConstants } from '../../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import Swal from 'sweetalert2' 

@Component({
  selector: 'app-po-add',
  templateUrl: './po-add.component.html',
  styleUrls: ['./po-add.component.scss']
})
export class PoAddComponent implements OnInit {

  constructor(private router: Router, private el: ElementRef,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private CS: CommonservicesService,
    private CF: CommonFunctionsService,private spinner: NgxSpinnerService, private route: ActivatedRoute) 
      {
         this.searchQuotationList.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.searchQuotation(value);
      });
    }

  loading = false;
  purchaseForm: FormGroup;
  submitted = false;
  itemSubmitted = false;
  itemsArray: FormArray;
  vessels = [];
  vendors = [];
  customers = [];
  port = [];
  companies = [];
  tcdTypes = [];
  itemGroups = [];
  itemTypes = [];
  uoms = [];
  savedItems = [];
  currencies = [];
  edit = null;
  fileToUpload: File[] = [];
  poCategories = [{ id: 2, name: "From Customer" }, { id: 1, name: "To Vendor" }];
  imageSrc = [];
  tcdSubmitted = false;
  toggle = true;
  tcdError = null;
  FPtypes = globalConstants.FP;
  itemTotals = {};
  itemTotal: any = 0;
  exchangeRate = [];
  currencySymbol = ''
  customError = { advanceAmountUsd: null };

  keywordVessel = 'vessel';
  keywordVendor = 'vendorName';
  keywordCurrency = 'name';
  keywordPort = 'name';
  keywordUom = 'uomName';
  keywordGroup = 'groupName';

  tcdTotal = 0;
  tcdDiscount = 0;

  company = JSON.parse(localStorage.getItem('companyXid'));

  searchQuotationList = new Subject<string>();
  selectedQtId = ''
  keywordQuotation = 'enqNumber';
  search_enq = 'SME/ENQ/';
   @ViewChild('auto') auto;
   quotationList = []
   orderDetails = null
   customerChecked = false
    initialKeywordQuotation = 'SME/ENQ/';
    selectedEnqId = ''
    vendorSel =null
  vendorIdSelected = null
  vendorDate = null
  permissions = this.CF.findPermissions();
  venItemTotals = {}
  arrQuot = null
  poVendorDate = null
  vendorChoose = false
  qtFound = true
  showNoVendorMsg = false
  newPo = null
  firstTimeSerach = true
  todayDate = null
  DOC_PATH = globalConstants.PO_DOC_PATH;
  itemsDocsArray :FormArray

  ngOnInit() {
    this.selectedQtId = this.route.snapshot.paramMap.get('qtid');
    this.selectedEnqId = this.route.snapshot.paramMap.get('qtid');
    this.newPo = this.route.snapshot.paramMap.get('nid');
    let year_str = new Date().getFullYear();
    this.search_enq  = this.search_enq + year_str.toString().slice(2) + '/';
    this.todayDate = this.CF.dateObjectToJsonDate(new Date())
    if( this.selectedEnqId || this.newPo == 'new') {
    this.createForm();
    this.getCompanies();
    this.getVessels();
    this.getVendors();
    this.getCurrencies();
    this.getCustomers();
    this.getPort();
    this.getTypes();
    this.getItemGroups();
    this.getTcdTypes();
    this.getExchangeRate();
    this.getUoms();
    //this.getQuotationDetails()
  }
    this.searchQuotation(this.search_enq)
    setTimeout(()=>{
        this.auto.focus();
        this.auto.close();
        })
    
  }


    searchQuotation(val) {
    //console.log("search val", val)
    console.log("serach",this.firstTimeSerach)

    let _this = this;
    //_this.CS.searchQuotationForGenPO({ data: val }).subscribe(response => {
      /*if(_this.firstTimeSerach ==true){
        _this.firstTimeSerach = false
        //_this.quotationList = [];
         console.log("serach",_this.firstTimeSerach)
      }

      else {*/
        console.log("serach")
      _this.CS.searchEnquiryForGenPO({ data: val }).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.quotationList = response.result;
      } else {
        _this.quotationList = [];
      }
    })
    //}
          console.log("serach",_this.firstTimeSerach)
  }

    selectQuotationFromList(event) {
      console.log("event",event)
    let _this = this;
    if (_this.initialKeywordQuotation != event) {
      _this.CS.getEnquiryDetailsByIdPO({'enqid':event.id}).subscribe(response => {
            if(response.quotationVendors.length == 0){
              console.log("resQt",response.quotationVendors)
               _this.qtFound = false
               _this.showNoVendorMsg = false

                
              
            }

              else{
             var vendorStat = []
             _.forEach(response.quotationVendors, function (field, index) {
               vendorStat.push(field.quotationStatus +'')
             })
             console.log("vendorStat",vendorStat)
             if(!vendorStat.includes('2')) {
               console.log('vendor purchase Already Generated')
               _this.qtFound = true
               _this.showNoVendorMsg = true;
               return
             }
                
                this.selectedEnqId = event.id;
                _this.showNoVendorMsg = false
              _this.qtFound = true
            _this.router.navigate(['/purchase-order/add/qt/' + event.id])
              .then(() => {
                this.ngOnInit()
                });
              }
            })
           } 
}

  createForm() {
    // formControl = new FormControl(new Date());
    let defDate = new Date();
    let _this = this
    this.purchaseForm = this.formBuilder.group({
      poDate: [this.toJsonDate(defDate)],
      poCategory: ['', Validators.required],
      currencyXid: [null],
      currencyNameAuto: [''],
      currency: ['', Validators.required],
      poCost: 0,
      advanceAmountUsd: [null, [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]], //Validators.required,
      remarks: ['', Validators.required],
      vesselXid: ['', Validators.required],
      vesselName: ['', Validators.required],
      vendorXid: ['', Validators.required],
      vendorName: ['', Validators.required],
      customerXid: ['', Validators.required],
      customerName: ['', Validators.required],
      customerNameAuto: [''],
      vendorNameAuto:[''],
      portNameAuto: [''],
      portXid: [''],
      port: ['', Validators.required],
      companyXid: [this.company],
      tcdDetails: this.formBuilder.array([]),
      details: this.formBuilder.array([]),
      vendors: this.formBuilder.array([]),
      documents: this.formBuilder.array([]),
      status: 1,
      amountUsd: 0,
      exRate: 0,
      amountLocal: 0,
      tcd: 0,
      quotationXid:[''],
      enqNumber:[''],
      quotationNumber:[''],
      enqXid:[''],
      isDirectPO:[_this.newPo =='new' ? 0 : 1],
      poStatus:[''],
      vesselNameAuto:['']


    });
    this.createItems();
    this.createDocuments();
    if( this.selectedEnqId ) {
        this.getQuotationDetails()
       }
    console.log(" this.purchaseForm", this.purchaseForm);
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.purchaseForm.controls;
  }



  createTcdItems(flag?) {
    console.log("this.purchaseForm.controls.tcdDetails");
    if (flag) {
      if (this.purchaseForm.controls.tcdDetails.status == 'INVALID') {
        this.tcdSubmitted = true;
        return false;
      }
    }
    let item = this.formBuilder.group({
      tcdType: [null, Validators.required],
      tcdName: [null],
      tcdDesc: [null],
      flatOrPercentage: [null, Validators.required],
      amount: [null, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      tcdAmount: [null, [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      companyXid: [this.company],
      status: 1,

    });
    this.itemsArray = this.purchaseForm.get('tcdDetails') as FormArray;
    this.itemsArray.push(item);
  }

  createItems(flag?) {
    if (flag) {
      if (this.purchaseForm.controls.details.status == 'INVALID') {
        this.submitted = true;
        return false;
      }
    }
    let _this = this
    let item = this.formBuilder.group({
      groupXid: [''],
      group: ['', Validators.required],
      description: ['', Validators.required],
      typeXid: [null],
      type: ['',Validators.required],
      uomXid: [''],
      groupNameAuto: [''],
      uomNameAuto: [''],
      uom: ['', Validators.required],
      poTypeXid: [''],
      poType: [''],
      poQty: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
      poDiscount: [(parseFloat(0.00.toString()).toFixed(2)), [Validators.pattern(/^(([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/)]],
      poPrice: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      poCost: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      status: 1,
      qtPrice:['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      qtCost:['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      qtQty:['',[Validators.required,Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
      supPrice:[''],
      supQty:[''],
      supCost:[''],

    });
    this.itemsArray = this.purchaseForm.get('details') as FormArray;
    this.itemsArray.push(item);
  }

  show(field) {
    this.el.nativeElement.querySelector('.' + field).classList.remove("hidden");
    this.el.nativeElement.querySelector('.' + field + '_id').classList.add("hidden");
  }

  close(field) {
    this.el.nativeElement.querySelector('.' + field).classList.add("hidden");
    this.el.nativeElement.querySelector('.' + field + '_id').classList.remove("hidden");
  }

  deleteItems(index) {
    this.itemsArray = this.purchaseForm.get('details') as FormArray;
    this.itemsArray.removeAt(index);
  }

  deleteTcdItems(index) {
    this.itemsArray = this.purchaseForm.get('tcdDetails') as FormArray;
    this.itemsArray.removeAt(index);
  }


  calculateCost(index) {
      this.vendorChoose = false
    var element = (<FormArray>this.purchaseForm.controls['details']).at(index);
    if ((<FormArray>element).controls['poQty'].status == 'VALID' && (<FormArray>element).controls['poPrice'].status == 'DISABLED' && (<FormArray>element).controls['poDiscount'].status == 'DISABLED' || (<FormArray>element).controls['poQty'].status == 'VALID' && (<FormArray>element).controls['poPrice'].status == 'VALID' && (<FormArray>element).controls['poDiscount'].status == 'VALID') {
      var qty = (<FormArray>element).controls['poQty'].value;
      var price = (<FormArray>element).controls['poPrice'].value;
      //var dis = (<FormArray>element).controls['poDiscount'].value ? (<FormArray>element).controls['poDiscount'].value : 0.00;
      var cost = null;
      cost = qty * price;
      //dis = 1 / 100;
      var totalValue = cost
      this.itemTotals[index] = totalValue;
      (<FormArray>element).controls['poCost'].setValue(totalValue.toFixed(3));
      console.log("purchaseForm",this.purchaseForm)
      this.calculateTcdAmount();
    }
    var element = (<FormArray>this.purchaseForm.controls['details']).at(index);
 if ((<FormArray>element).controls['qtQty'].status == 'DISABLED' && (<FormArray>element).controls['qtPrice'].status == 'DISABLED' || (<FormArray>element).controls['qtQty'].status == 'VALID' && (<FormArray>element).controls['qtPrice'].status == 'VALID')  {
          var custQty = (<FormArray>element).controls['qtQty'].value;
          var v_price = (<FormArray>element).controls['qtPrice'].value;
          var cost = null;
          cost = custQty * v_price;
         // dis = 100 / 100;
          var totalValue = cost ;
              this.venItemTotals[index] = totalValue;
              (<FormArray>element).controls['qtCost'].setValue(totalValue.toFixed(3));
              this.calculateTcdAmount();
    }

    if(this.newPo){
      console.log(true)
      let _this = this
    _.forEach(_this.purchaseForm.get('details')['controls'], function (itemData, index) {
      console.log("itemData", itemData)
      itemData.get("qtCost").setErrors(null);
      itemData.get("qtPrice").setErrors(null);
      itemData.get("qtQty").setErrors(null);
      itemData.get("qtCost").setValidators(null);
      itemData.get("qtPrice").setValidators(null);
      itemData.get("qtQty").setValidators(null);
      //(<FormArray>element).controls['custQtUnitPrice'].status == 'VALID'
    })
    }
  }

  getCompanies() {
    let _this = this;
    let params = { model_name: 'companies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.companies = response.result;
      } else {
        _this.companies = [];
      }
    });
  }

  getVessels() {
    let _this = this;
    let params = { model_name: 'vessels', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response aaaaa", response);
      if (response && response.status == "success" && response.result) {
        _this.vessels = response.result;
      } else {
        _this.vessels = [];
      }
    });
  }

  getVendors() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'status': 1, approvedVendor: 1 ,  typeUser: 1    } };
    _this.CS.getVendorList(params).subscribe(response => {
      console.log("response getVendorList", response);
      if (response && response.status == "success" && response.result) {
        _this.vendors = response.result;
      } else {
        _this.vendors = [];
      }
    });
  }

  getPort() {
    let _this = this;
    let params = { model_name: 'port', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("port", response);
      if (response && response.status == "success" && response.result) {
        _this.port = response.result;
      } else {
        _this.port = [];
      }
    });
  }

  getItemGroups() {
    let _this = this;
    let params = { model_name: 'itemGroups', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("itemGroups", response);
      if (response && response.status == "success" && response.result) {
        _this.itemGroups = response.result;
      } else {
        _this.itemGroups = [];
      }
    });
  }

  getCurrencies() {
    let _this = this;
    let params = { model_name: 'currencies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencies = response.result;
      } else {
        _this.currencies = [];
      }
    });
  }

  getCustomers() {
    let _this = this;

    let params = { model_name: 'vendors', where: { 'status': 1, typeUser: 2, approvedVendor: 1 } };
    _this.CS.getCustomerList(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.customers = response.result;
      } else {
        _this.customers = [];
      }
    });
  }

  getTypes() {
    let _this = this;

    let params = { model_name: 'poTypes', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("itemTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.itemTypes = response.result;
      } else {
        _this.itemTypes = [];
      }
    });
  }

  getUoms() {
    let _this = this;
    let params = { model_name: 'uom', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("getUoms", response);
      if (response && response.status == "success" && response.result) {
        _this.uoms = response.result;
        console.log(" _this.uoms", _this.uoms);
      } else {
        _this.uoms = [];
      }
    });
  }

  getTcdTypes() {
    let _this = this;
    let params = { model_name: 'tcdTypes', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("tcdTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.tcdTypes = response.result;
        console.log(" _this.tcdTypes", _this.tcdTypes);
      } else {
        _this.tcdTypes = [];
      }
    });
  }

  getExchangeRate() {
    let _this = this;
    let params = {};
    _this.CS.getLatestCurrencyExchangeRates(params).subscribe(response => {
      console.log("exchangeRate", response);
      if (response && response.status == "success" && response.result) {
        let exchangeRates = [];
        _.map(response.result, function (item) {
          if (item.ER && item.ER.length > 0) {
            exchangeRates.push(item.ER[0]);
          }
        });
        _this.exchangeRate = exchangeRates;
        console.log(" _this.exchangeRate", _this.exchangeRate);
      } else {
        _this.exchangeRate = [];
      }
    });
  }


  clearTcd(fields, index?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = null;
        (<FormArray>_this.purchaseForm.controls['tcdDetails']).at(index).patchValue(data);
      } else {
        _this.purchaseForm.get(field).setValue(null);
      }
    });
  }

  clear(fields, index?, checkField?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = checkField && field == checkField ? '' : null;
        (<FormArray>_this.purchaseForm.controls['details']).at(index).patchValue(data);
      } else {
        _this.purchaseForm.get(field).setValue(checkField && field == checkField ? '' : null);
      }
    });
  }

  saveItems() {
    if (this.edit || this.edit == 0) {
      this.savedItems[this.edit] = this.purchaseForm.controls['details'].value[0];
      this.edit = null;
    } else {
      if (this.purchaseForm.controls['details'].status == 'VALID') {
        this.savedItems.push(this.purchaseForm.controls['details'].value[0]);
      } else {
        this.itemSubmitted = true;
      }
    }
  }

  makeDecimalPoint(field?, index?, formArray?, point?) {
    let _this = this;
    if (index || index == 0) {
      var element = (<FormArray>this.purchaseForm.controls[formArray]).at(index);
      var value = (<FormArray>element).controls[field].value;
      var data = {};
      data[field] = (parseFloat(value.toString()).toFixed(point ? 2 : 3));
      value && !isNaN(value) ? element.patchValue(data) : '';
    } else {
      var value = _this.purchaseForm.get(field).value;
      value && !isNaN(value) ? _this.purchaseForm.get(field).setValue((parseFloat(value.toString()).toFixed(point ? 2 : 3))) : '';
    }
  }

  changeSelection(event, data, dataField, to, index?, from?,chooses?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    let _this = this
    if(this.purchaseForm.get('poCategory').value ==chooses) {
    _.forEach(_this.currencies, function(row,index){
      if(row.id == event.currency) {
        let currencyName = row.name
        console.log(row.name)
    _this.purchaseForm.get('currencyNameAuto').setValue(currencyName)
    _this.purchaseForm.get('currency').setValue(row.name)
    _this.purchaseForm.get('currencyXid').setValue(row.id)
    _this.currencySymbol = row.symbol
      }
    })      


  }
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, { id: parseInt(getId, 10) });
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.purchaseForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.purchaseForm.get(from).setValue(getId);
          }
          console.log("this.purchaseForm", this.purchaseForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.purchaseForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.purchaseForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.purchaseForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.purchaseForm.get(from).setValue(null);
        }
      }
    }

  }

  clearSelect(field){
    this.purchaseForm.get(field).setValue(null);
  }

  triggerFileUpload() {
    this.el.nativeElement.querySelector("#fileUpload").click();
  }

  deleteDoc(index) {
    this.fileToUpload.splice(index, 1);
    this.imageSrc.splice(index, 1)
  }

  upload(files: File[]) {

    let _this = this;
    _.forEach(files, function (value) {
       console.log("reader", value)
       var fileTypes = ['jpg', 'jpeg', 'png', 'xlsx', 'txt', 'gif', 'pdf', 'xls'];  //acceptable file types
        var extension = value.name.split('.').pop().toLowerCase() 
        console.log(extension)
        var Success = fileTypes.indexOf(extension) > -1
       if(Success) {
         if (value.size < 4096000) {
      var reader = new FileReader();
      reader.readAsDataURL(value);
      reader.onload = () => {
        var src = reader.result as string;
        _this.imageSrc.push(src);
        _this.fileToUpload.push(value);
      }
      }
        else {
        alert('Maximum allowed file size is 4 MB!');
        return
      }
      
    } else {
      alert('File type not supported')
    }
    });

    document.getElementById('fileUpload')['value'] = "";
  }

  saveList(from?, to?, model?, index?, form?) {
    let _this = this;
    this.toggle = false;
    if (index || index == 0) {
      var element = (<FormArray>this.purchaseForm.controls[form]).at(index);
      if ((<FormArray>element).controls[from].value) {
        var value = (<FormArray>element).controls[from].value;
        var params = { model_name: model, data: {} };
        params.data[from] = value;
        params.data['status'] = 1;
        _this.CS.setData(params).subscribe(response => {
          console.log("response", response);
          if (response && response.status == 'success') {
            _this.tcdTypes.push(response.result);
            var data = {};
            data[to] = response.result.id;
            element.patchValue(data);
            _this.toggle = true;
            _this.close('tcdType' + index);
            _this.calculateTcdAmount();
          } else {
            this.toastr.error('Something went wrong!', 'Failed!');
          }
        });
      } else {
        this.tcdSubmitted = true;
      }
    }
  }

  calculateTcdAmount() {
    console.log("calculateTcdAmount");
    let _this = this;
    _this.itemTotal = 0;
    _this.tcdTotal = 0;
    _this.tcdDiscount = 0;
    _.map(_this.itemTotals, function (item) {
      console.log("_this.itemTotal", item);
      if(item == null) {
      _this.itemTotal = _this.itemTotal + 0;
    }
    else {
       _this.itemTotal = _this.itemTotal + item;
    }
    });
    var poCost = _this.itemTotal;
    _this.purchaseForm.get('poCost').setValue(poCost.toFixed(3));
    console.log("poCost", poCost);
    if (this.purchaseForm.controls.tcdDetails.status == 'VALID' && _this.itemTotal) {
      _.forEach(_this.purchaseForm.getRawValue().tcdDetails, function (value, index) {
        var rate = 0;
        rate = value.status == 1 ? parseFloat(value.amount) : 0;
        console.log("rate",value)
        if (value.flatOrPercentage == globalConstants.PER) {
          rate = rate / 100;
          rate = (_this.itemTotal * rate);
        }
        var element = (<FormArray>_this.purchaseForm.controls['tcdDetails']).at(index);
        if (value.tcdType == globalConstants.DISCOUNT) {
          poCost = poCost - parseFloat(rate.toString());
          _this.tcdDiscount = parseFloat(_this.tcdDiscount.toString()) + parseFloat(rate.toString());
          _this.tcdDiscount = parseFloat((_this.tcdDiscount).toFixed(3));
        } else {
          poCost = poCost + parseFloat(rate.toString());
          _this.tcdTotal = parseFloat(_this.tcdTotal.toString()) + parseFloat(rate.toString());
          _this.tcdTotal = parseFloat((_this.tcdTotal).toFixed(3));
        }
        _this.purchaseForm.get('poCost').setValue(poCost.toFixed(3));
        (<FormArray>element).controls['tcdAmount'].setValue(rate.toFixed(3));
      });
     // console.log("poCost", poCost);
    }
    _this.toUsd();
  }

  toUsd() {
    let _this = this;
    if (_this.newPo && _this.purchaseForm.value.currencyXid && _this.exchangeRate && _this.exchangeRate.length) {
      var rate = _.find(_this.exchangeRate, { currencyXId: _this.purchaseForm.value.currencyXid });
      var curencyObj = _.find(_this.currencies, { id: _this.purchaseForm.value.currencyXid});
      console.log("currencyObj",_this.purchaseForm.value.currencyXid)
       _this.currencySymbol = curencyObj ? curencyObj.symbol : ''
        if (rate) {
        var InUsd = _this.purchaseForm.value.poCost / rate.Rate;
        this.purchaseForm.get('amountUsd').setValue(InUsd.toFixed(3));
        if(_this.newPo) {
          console.log("newPo")
        this.purchaseForm.get('exRate').setValue(rate.Rate);
    }
      } else {
          //if no exchange rate       
        this.purchaseForm.get('amountUsd').setValue(0);
        //this.purchaseForm.get('exRate').setValue(0);
      }

      this.purchaseForm.get('amountLocal').setValue(_this.purchaseForm.value.poCost);
      let tcdVal = (_this.tcdTotal - _this.tcdDiscount);
      this.purchaseForm.get('tcd').setValue(tcdVal.toFixed(3));
    }
    if (_this.purchaseForm.value.currencyXid && _this.purchaseForm.value.exRate ) {
      //var rate = _.find(_this.exchangeRate, { currencyXId: _this.purchaseForm.value.currencyXid });
      var rate = _this.purchaseForm.value.exRate
      var curencyObj = _.find(_this.currencies, { id: _this.purchaseForm.value.currencyXid });
      _this.currencySymbol = curencyObj ? curencyObj.symbol : ''
      
     console.log("slected rate", rate)
   
      if (rate) {
        var InUsd = _this.purchaseForm.value.poCost / rate;
        this.purchaseForm.get('amountUsd').setValue(InUsd.toFixed(3));

      } else {
        //if no exchange rate       
        this.purchaseForm.get('amountUsd').setValue(0);
        //this.purchaseForm.get('exRate').setValue(0);
      }

      this.purchaseForm.get('amountLocal').setValue(_this.purchaseForm.value.poCost);
      let tcdVal = (_this.tcdTotal - _this.tcdDiscount);
      this.purchaseForm.get('tcd').setValue(tcdVal.toFixed(3));
   } 
  }

  onSubmit() {
    let _this = this;
    _this.tcdError = null;
    _this.tcdSubmitted = true
    _this.customError = { advanceAmountUsd: null };
    let formData = new FormData();
    var imageSize =[]
   var size;
    _.forEach(this.imageSrc, function (field) {
      imageSize.push(field.length)
      console.log(imageSize)
      size = imageSize.reduce((a, b) => a + b)/1000000
      console.log("size",size)
    })
    if(size > 10) {
      this.toastr.error('Total Allowed files is 10 Mb.', 'Invalid!');
      return
    }
    if (this.fileToUpload.length) {
      for (var i = 0; i < this.fileToUpload.length; i++) {
        formData.append("files", this.fileToUpload[i], uuidv4() + this.fileToUpload[i].name);
      }
    }
    //if (_this.purchaseForm.value.tcdDetails.length > 1 && this.purchaseForm.controls.tcdDetails.status == 'VALID') {
      //var checkDiscount = _.countBy(_this.purchaseForm.value.tcdDetails, function (tcd) {
       // return tcd.tcdType == globalConstants.DISCOUNT;
      //});
      //if (checkDiscount.true > 1) {
        //_this.tcdError = 'Only one discount row allowed!';
        //return false;
      //} else {
        //Duplicate tax not allowed
       /* var grouped = _.groupBy(_this.purchaseForm.value.tcdDetails, function (tcd) {
          return tcd.tcdType == globalConstants.TAX ? 'tax' : 'tax';
        });
        if (grouped && grouped.tax && grouped.tax.length > 1) {
          var checkUnique = _.filter(
            _.uniq(
              _.map(grouped.tax, function (item) {
                if (_.filter(grouped.tax, { amount: item.amount }).length > 1) {
                  return item.amount;
                }
                return false;
              })),
            function (value) {
              return value;
            });
          if (checkUnique && checkUnique.length > 0) {
            _this.tcdError = 'Duplicate tax value not allowed!';
            return false;
          }
        }*/
      //}
    //} else if (_this.purchaseForm.value.tcdDetails.length == 1) {
      //console.log("length1");
      //if (!_this.purchaseForm.value.tcdDetails[0].tcdType && !_this.purchaseForm.value.tcdDetails[0].flatOrPercentage && !_this.purchaseForm.value.tcdDetails[0].amount && !_this.purchaseForm.value.tcdDetails[0].tcdAmount) {
       // this.itemsArray = this.purchaseForm.get('tcdDetails') as FormArray;
        //this.itemsArray.removeAt(0);
      /*} else {
        _this.tcdSubmitted = true;
      }
    } else {
      _this.tcdSubmitted = true;
      return false;
    }
    if (_this.purchaseForm.value.poCost < 0) {
      _this.tcdError = 'Discount value should be less than total cost!';
      return false;
    }
    if (_this.purchaseForm.value.poCost && parseFloat(_this.purchaseForm.value.advanceAmountUsd) > parseFloat(_this.purchaseForm.value.poCost)) {
      _this.customError['advanceAmountUsd'] = 'Advance amount should be less than total cost!';
      return false;
    }*/
    // _this.purchaseForm.value['poStatus'] = _this.purchaseForm.value.poDate ? 'PO Sent' : 'PO Received';
    _this.purchaseForm.value['poStatus'] = 1;
    if(_this.arrQuot == null && !_this.newPo) {
      this.toastr.error('Please choose a vendor to add purchase order', 'Invalid!');
      _this.loading = false;
      return
    }
    if (this.purchaseForm.invalid) {
      console.log("this.purchaseForm",this.purchaseForm);
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      _this.loading = false;
    }

    if(_this.purchaseForm.value['poCategory'] == 2 && (_this.purchaseForm.value['customerXid'] != null && _this.purchaseForm.value['vendorXid'] != null) && (_this.purchaseForm.value['customerXid'] == _this.purchaseForm.value['vendorXid'])){
      this.toastr.error('Please select valid customer.', 'Failed!');
      _this.loading = false;
    }else{
      
      if (this.purchaseForm.status == 'VALID') {
        _this.loading = true;
        //this.submitted = true;
         Swal.fire({ 
      title: 'Confirm Submitting Purchase Order',
      text: 'Have you confirmed the details before submit?',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    allowOutsideClick: false,
    position: 'center',
    }).then((result) => {

      if (result.value) {
        this.spinner.show();
      setTimeout(() => this.spinner.hide(), 2000);
        _this.purchaseForm.value['poDate'] = this.toDateObject(_this.purchaseForm.get('poDate').value)
        _this.purchaseForm.value['details'] =  _this.purchaseForm.getRawValue().details
         _this.purchaseForm.value['tcdDetails'] =  _this.purchaseForm.getRawValue().tcdDetails
         
        _this.CS.savePoOrder(_this.purchaseForm.value).subscribe(response => {
          if (response && response.status == "success") {
           // _this.CS.poMailer(response.result).subscribe();
            if (this.fileToUpload.length) {
              formData.append("data", response.result.id);
              _this.CS.saveDocuments(formData).subscribe(res => {
                _this.loading = false;
                if (res && res.data && res.data.status == "success") {
                  this.toastr.success('Purchase Order added...', 'Success!');
                  _this.router.navigate(['/purchase-order/edit', response.result.id]);
                } else {
                  this.toastr.error('Something Went Wrong!', 'Failed!'); 
                }
              });
              
              
            } else {
              _this.loading = false;
              this.toastr.success('Purchase Order added.', 'Success!');
              _this.router.navigate(['/purchase-order/list']);
            }
          } else {
            _this.loading = false;
            this.toastr.error('Something Went Wrong!', 'Failed!');
          }
        });
    }     else if (result.dismiss === Swal.DismissReason.cancel) {
              _this.loading = false;
              _this.submitted = false;
              _this.tcdSubmitted = false;
      /*Swal.fire(
          'Cancelled',
          'No change in vendor',
          'error'
        )*/
      }})

  
      } else {
        _this.submitted = true;
        if (_this.purchaseForm.get('tcdDetails')['controls'].length < 1) {
          _this.createTcdItems();
        }
      }
    }
    

    

  }

  toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date) {
    return this.CF.dateObjectToJsonDate(date)
  }
  resetCustomerValue(){
    this.purchaseForm.get('customerName').setValue(null);
    this.purchaseForm.get('customerXid').setValue(null);
  }


  setForm() {
    console.log("_this.orderDetailsggg", this.orderDetails);
    let advAmtUSD = this.orderDetails.advanceAmountUsd;
    if (this.orderDetails.advanceAmountUsd) {
      advAmtUSD = this.orderDetails.advanceAmountUsd.toFixed(3);
    }

    let _this = this
    this.purchaseForm = this.formBuilder.group({
      poDate: [this.orderDetails.poDate ? this.toJsonDate(this.orderDetails.poDate) : null],
      //poType: [this.orderDetails.poType, Validators.required],
      poCategory: [this.orderDetails.quotationCategory, Validators.required],
      currencyXid: [null],
      currency: [null, Validators.required],
      advanceAmountUsd: [advAmtUSD, [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]], //Validators.required, 
      remarks: [this.orderDetails.remarks, Validators.required],
      vesselXid: [null, Validators.required],
      vesselName: [this.orderDetails.vesselName, Validators.required],
      vendorXid: [this.orderDetails.vendorXid, Validators.required],
      vendorName: [this.orderDetails.vendorName, Validators.required],
      customerXid: [this.orderDetails.customerXid],
      customerName: [this.orderDetails.customerName, Validators.required],
      categoryXid: [this.orderDetails.categoryXid],
      //categoryName: [this.orderDetails.categoryName, Validators.required],
      portXid: [this.orderDetails.portXid],
      port: [this.orderDetails.port, Validators.required],
      companyXid: [this.company],
      poCost: 0,
      tcdDetails: this.formBuilder.array([]),
      details: this.formBuilder.array([]),
      status: 1,
      customerNameAuto: [this.orderDetails.customerName],
      portNameAuto: [this.orderDetails.port],
      currencyNameAuto: null,
      amountUsd: 0,
      amountLocal:0,
      tcd: [this.orderDetails.tcd],
      enqNumber:[this.orderDetails.enqNumber],
      quotationNumber:[null],
      exRate:0,
      quotationXid:[''],
      vendors: this.formBuilder.array([]),
      documents: this.formBuilder.array([]),
      enqXid:[this.orderDetails.id],
      isDirectPO:[_this.newPo =='new' ? 0 : 1],
      poStatus:1,
      vesselNameAuto : ['']

    });
    console.log("this.purchaseForm--->", this.purchaseForm);
    //this.setTcdItems();
    this.setVendors();
    

  }

  setItems(qid) {
    let _this = this;
    
    _.forEach(_this.orderDetails.quotationVendors, function (field, index) {
      if(field.id == qid) {
          _this.arrQuot = field
          _this.poVendorDate =  _this.toJsonDate(field.quotationDate)
        var arr = field.QD
        console.log("arr",arr)
       _this.purchaseForm.get('currencyXid').setValue((_this.arrQuot.quotationCategory ==1 ? _this.arrQuot.vendCurrencyXid : _this.arrQuot.custCurrencyXid))
       _this.purchaseForm.get('currency').setValue((_this.arrQuot.quotationCategory ==1 ? _this.arrQuot.vendCurrency: _this.arrQuot.custCurrency))
       _this.purchaseForm.get('poCategory').setValue(_this.arrQuot.quotationCategory)
        _this.purchaseForm.get('exRate').setValue((_this.arrQuot.quotationCategory ==1 ? _this.arrQuot.vendExRate : _this.arrQuot.custExRate))
        _this.purchaseForm.get('quotationNumber').setValue(_this.arrQuot.quotationNumber)
        _this.purchaseForm.get('quotationXid').setValue(_this.arrQuot.id)
         _this.purchaseForm.get('vendorXid').setValue(_this.arrQuot.vendorXid)
          _this.purchaseForm.get('vendorName').setValue(_this.arrQuot.vendorName)
           
          console.log("this.purchaseForm", _this.purchaseForm);
    if (arr && arr.length) {
      _.forEach(arr, function (field, index) {
        console.log("field",_this.orderDetails.EDE)
        let item = _this.formBuilder.group({
          //id: field.id,
          groupXid: [{value:field.groupXid,disabled:true}],
          group: [field.group, Validators.required],
          description: [{value:field.description,disabled:true}, Validators.required],
          typeXid: [{value:field.typeXid,disabled:true}],
          type: [field.type],
          uomXid: [field.uomXid],
          uom: [field.uom, Validators.required],
          qtQty: [{value:field.qtQty ? field.qtQty.toFixed(2) : field.qtQty,disabled:true}, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
          poDiscount: [{value:field.poDiscount ? field.poDiscount.toFixed(2) : 0.00,disabled:true}, [Validators.pattern(/^(([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/)]],
          qtPrice: [{value:field.custQtUnitPrice ? field.custQtUnitPrice.toFixed(3) : 0.000,disabled:true}, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          qtCost: [{value:field.custQtCost ? field.custQtUnitPrice.toFixed(3) : 0.000,disabled:true}, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          status: field.status,
          groupNameAuto: [{value:field.group,disabled:true}],
          uomNameAuto: [{value:field.uom,disabled:true}],
          poPrice:[{value:(_this.arrQuot.quotationCategory ==1 ? field.venQtUnitPrice.toFixed(3) : field.custQtUnitPrice.toFixed(3)),disabled:true}],
          poCost:[null],
          poQty:['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
          supPrice:[field.venQtUnitPrice],
          supQty:[field.qtQty],
          supCost:[field.vendQtCost]

        });
        _this.itemsArray = _this.purchaseForm.get('details') as FormArray;
        _this.itemsArray.push(item);
        _this.itemTotals[index] = field.poCost;
        _this.venItemTotals[index] = field.vendQtCost
        _this.calculateCost(index);
        console.log("item", _this.purchaseForm);

      });
    } else {
      this.createItems();
    }
  }
  })
   
  }
  setTcdItems(qid) {
    let _this = this;
    _.forEach(_this.orderDetails.quotationVendors, function (field, index) {
      if(field.id == qid) {
        var arr = field.QT
        console.log("arr",_this.arrQuot)
        
 
    if (arr && arr) {
      _.forEach(arr, function (field, index) {
        console.log("field", field);
        var companyXid = JSON.parse(localStorage.getItem('companyXid'));
        let item = _this.formBuilder.group({
          //id: field.id,
          tcdName: null,
          tcdDesc: {value:field.tcdDesc,disabled:true},
          tcdType: [{value:field.tcdType,disabled:true}, Validators.required],
          flatOrPercentage: [{value:field.flatOrPercentage,disabled:true}, Validators.required],
          amount: [{value:field.amount ? field.amount.toFixed(3) : field.amount,disabled:true}, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          tcdAmount: [{value:field.tcdAmount ? field.tcdAmount.toFixed(3) : field.tcdAmount,disabled:true}, [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          companyXid: companyXid,
          status: 1,
          //poXid: _this.orderDetails.id
        });
        console.log("item", item);
        _this.itemsArray = _this.purchaseForm.get('tcdDetails') as FormArray;
        console.log("_this.itemsArray", _this.itemsArray);
        _this.itemsArray.push(item);
        console.log("item", item);
      });
    } else {
      _this.createTcdItems();
    }
  }
})
  }


getQuotationDetails() {
    let _this = this;
    //this.resetVendorDetailsAndCurrency(_this);
    let params = { 'enqid': this.selectedEnqId };
    _this.CS.getEnquiryDetailsByIdPO(params).subscribe(response => {
      console.log("selectedQtId",this.selectedQtId)
      console.log("responseID", response);
      if (response && response.status == "success" && response.result) {
        _this.orderDetails = response.result;
        _this.orderDetails.customerCurrency = response.customerCurrency;
        _this.orderDetails.vendorsCurrency = response.vendorsCurrency;
        _this.orderDetails.quotationVendors = response.quotationVendors

        //_this.min_enqDate = this.CF.dateObjectToJsonDate(new Date(_this.enqDetails.enqDate))

        this.setForm();
        //this.selectCurrencyRate();

        console.log("_this.orderDetails", _this.orderDetails);
      }else if(response && response.result == null){
        this.toastr.error('No such enquiry found', 'Failed!');
        _this.router.navigate(['/po/list']);
        
      } else {
        this.toastr.error('Something Went Wrong', 'Failed!');
      }
    });
  }


/*includeCustomerQuotation(event) {
  console.log("event", event)
  if(event == true){
    console.log(true)
    let year_str = new Date().getFullYear();
    this.search_qt = 'SME/QUO/' + year_str.toString().slice(2) + '/';
    this.searchQuotation(this.search_qt)
    setTimeout(()=>{
        this.auto.focus();
        })
    //this.QuotationSearching()
    //this.ngOnInit()
  }
  else {
     let year_str = new Date().getFullYear();
    this.search_qt = 'SUP/QUO/' + year_str.toString().slice(2) + '/';
    this.searchQuotation(this.search_qt)
    setTimeout(()=>{
        this.auto.focus();
        })
  }

}*/


  createVendors() {
    let item = this.formBuilder.group({
      id: null,
      date: [''],
      details: [''],
      vendorXid: [null],
      vendorName: [null],
      quotationNumber: [null],
      quotationXid: [null],
      status: 1,
      selectVendor:[null],
      quotationStatus:[null]
          });
    this.itemsArray = this.purchaseForm.get('vendors') as FormArray;
    this.itemsArray.push(item);
  }

  setVendors() {
    let _this = this;
    if (_this.orderDetails.quotationVendors && _this.orderDetails.quotationVendors.length) {
      _.forEach(_this.orderDetails.quotationVendors, function (column, index) {
        var arr = column.QV
        console.log("column",column.QV)
      _.forEach(arr, function (field, index) {
        let item = _this.formBuilder.group({
          id: [column.id],
          date: [field.date],
          details: [field.details],
          vendorXid: [field.vendorXid],
          vendorName: [field.vendorName],
          quotationNumber: [column.quotationNumber],
          quotationXid: [field.quotationXid],
          status: 1,
          selectVendor:[ column.selectVendor],
          quotationStatus:[column.quotationStatus]
        });

        _this.itemsArray = _this.purchaseForm.get('vendors') as FormArray;
        _this.itemsArray.push(item);

      });
      })
    }
    else {
      this.createVendors();
    }
  }

  resetCalculation() {
    let _this = this
    _this.purchaseForm.get('poCost').setValue(0.000);
    _this.purchaseForm.get('amountUsd').setValue(0.000);
    _this.tcdTotal = 0
    _this.tcdDiscount = 0
    _this.purchaseForm.get('vesselXid').setValue(null)
    _this.purchaseForm.get('vesselName').setValue(null)
     _this.purchaseForm.get('portXid').setValue(null)
      _this.purchaseForm.get('portNameAuto').setValue(null)
       _this.purchaseForm.get('port').setValue(null)
        _this.purchaseForm.get('remarks').setValue(null)


  }
  alertVendor(vendor,valueRadio) {
    console.log(vendor)
    let _this = this

    const arr = <FormArray>this.purchaseForm.controls.details;
        arr.controls = [];
        //this.purchaseForm.get('quotationDate').setValue(null)
        let qid = vendor.value.id
        this.setItems(qid)
        const arrTcd = <FormArray>this.purchaseForm.controls.tcdDetails;
        arrTcd.controls = []
        this.setTcdItems(qid)
        const arrDoc = <FormArray>this.purchaseForm.controls.documents;
        arrDoc.controls = []
        this.setDocuments(qid)



       _this.purchaseForm.get('poDate').setValue(null);
       this.resetCalculation()
        console.log("vendor",_this.purchaseForm)
        console.log("valueRadio", valueRadio);
       // _this.resetAndReCalculate();
  }
selectVendor(valueRadio,vendor,index,vendorId,length,date) {
    
    let _this = this;
    console.log("length",vendor)
    this.vendorDate =  this.CF.dateObjectToJsonDate(date)
    if(length == 1){
      _this.alertVendor(vendor,valueRadio);
      _this.vendorChoose = false
    }
    else {
      let _this = this;

    Swal.fire({ 
      title: 'Are you sure want to change?',
    text: 'This action will affect the details entered. Are you sure you want to change the vendor.',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    allowOutsideClick: false
    }).then((result) => {

      if (result.value) {
        //click yes reset values
        // change checkbox selection and reset

        this.vendorIdSelected = vendorId
        _this.alertVendor(vendor,valueRadio);
        _this.vendorChoose = false

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        this.vendorSel = this.vendorIdSelected
        console.log("vendorSel",this.vendorSel)
        //this.purchaseForm.controls.vendors.value[4].selectVendor = true
        //this.purchaseForm.controls.vendors.value[2].selectVendor = 'false'
        console.log("vendor",_this.purchaseForm)
        /*Swal.fire(
          'Cancelled',
          'No change in vendor',
          'error'
        )*/
      }
    })
  }
  }


    convertDate(dateString) {
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMONTHYY(objectDate);
  }


    deleteVendorItems(index,vendorName) {
    console.log("vendorName",vendorName)
    let _this = this
    this.itemsArray = this.purchaseForm.get('vendors') as FormArray;
    this.itemsArray.removeAt(index);
    if(this.purchaseForm.get('vendorName').value == vendorName){
      _this.arrQuot = null
    const arr = <FormArray>this.purchaseForm.controls.details;
        arr.controls = [];
        this.purchaseForm.get('poDate').setValue(null)
        const arrTcd = <FormArray>this.purchaseForm.controls.tcdDetails;
        arrTcd.controls = []
        this.resetCalculation()

    }

  }

   setDocuments(qid) {

     console.log("document------>",this.orderDetails.quotationVendors.QDO)
    let _this = this;
    _.forEach(_this.orderDetails.quotationVendors, function (field, index) {
      if(field.id == qid) {
        var arr = field.QDO
        console.log("arrDoc",field.QDO)
        
 
    if (arr) {
      _.forEach(arr, function (field, index) {
        console.log("documentttttttttttt", field)
        let item = _this.formBuilder.group({
          enqXid: [field.enqXid],
          fileName: [field.fileName],
          fileDisplayName: [field.fileDisplayName],
          filePath: [field.filePath],
          companyXid: [_this.company],
          status: 1
        });
        _this.itemsDocsArray = _this.purchaseForm.get('documents') as FormArray;
        _this.itemsDocsArray.push(item);
      });
    }
  }
})
}

    createDocuments() {
    let _this = this;
    let item = this.formBuilder.group({
      enqXid: [''],
      fileName: [''],
      fileDisplayName: [''],
      filePath: [''],
      companyXid: [null],
      status: 1
    });
       _this.itemsDocsArray = _this.purchaseForm.get('documents') as FormArray;
        _this.itemsDocsArray.push(item);
  }

supPrice(index,price) {
        this.vendorChoose = false
    var element = (<FormArray>this.purchaseForm.controls['details']).at(index);
    console.log("poForm",this.purchaseForm);
    if((<FormArray>element).controls['poPrice'].status =='INVALID') {
    (<FormArray>element).controls['poPrice'].setValue(price)
    console.log("true")
  }
}

fixSelectedData(name,index){
  console.log("name",name)
  console.log("index",index)

     var element = (<FormArray>this.purchaseForm.controls['details']).at(index);
     

     if(name =='group') {
      (<FormArray>element).controls['groupNameAuto'].setValue( (<FormArray>element).controls['group'].value);
    }

    if (name =='customer'){
      this.purchaseForm.get('customerNameAuto').setValue(this.purchaseForm.get('customerName').value)
    }

    if (name =='vendor'){
      this.purchaseForm.get('vendorNameAuto').setValue(this.purchaseForm.get('vendorName').value)
    }

    if (name =='vessel'){
      this.purchaseForm.get('vesselNameAuto').setValue(this.purchaseForm.get('vesselName').value)
    }

    if (name =='port'){
      this.purchaseForm.get('portNameAuto').setValue(this.purchaseForm.get('port').value)
    }


     if(name =='uom') {
      (<FormArray>element).controls['uomNameAuto'].setValue( (<FormArray>element).controls['uom'].value);
    }

     if (name =='currency'){
      this.purchaseForm.get('currencyNameAuto').setValue(this.purchaseForm.get('currencyName').value)
    }

     
}


}
