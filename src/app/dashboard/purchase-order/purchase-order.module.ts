import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';


import {PurchaseOrderRoutingModule} from './purchase-order-routing.module';
import {PoAddComponent} from './po-add/po-add.component';
import {PoEditComponent} from './po-edit/po-edit.component';
import {PoListComponent} from './po-list/po-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgSelectModule } from '@ng-select/ng-select';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
  declarations: [PoAddComponent, PoEditComponent, PoListComponent],
  imports: [
    CommonModule,
    PurchaseOrderRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    NgbModule,
    NgxSpinnerModule,
    NgSelectModule,
    ClickOutsideModule
  ]
})
export class PurchaseOrderModule {
}
