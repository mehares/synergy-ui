import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { group } from '@angular/animations';
import * as _ from 'lodash';
import { globalConstants } from '../../../constants/global-constants';
import { v4 as uuidv4 } from 'uuid';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2' 

@Component({
  selector: 'app-po-edit',
  templateUrl: './po-edit.component.html',
  styleUrls: ['./po-edit.component.scss']
})
export class PoEditComponent implements OnInit {

  constructor(private router: Router, private el: ElementRef,
    private formBuilder: FormBuilder, private activatedroute: ActivatedRoute,
    private CS: CommonservicesService,
    private toastr: ToastrService,
    private CF: CommonFunctionsService,private spinner: NgxSpinnerService,) {
  }

  loading = false;
  loaderItem = {};
  purchaseForm: FormGroup;
  submitted = false;
  itemSubmitted = false;
  itemsArray: FormArray;
  orderTypes = [];
  vessels = [];
  vendors = [];
  customers = [];
  port = [];
  companies = [];
  tcdTypes = [];
  itemGroups = [];
  itemTypes = [];
  uoms = [];

  savedItems = [];
  currencies = [];
  edit = null;
  id = null;
  poType = [];
  orderDetails = null;
  poCategories = [{ id: 1, name: "From Customer" }, { id: 2, name: "To Vendor" }];
  selectedPoCategoryId=0;
  selectedCustomerId=null;
  fileToUpload: File[] = [];
  DOC_PATH = globalConstants.PO_DOC_PATH;
  PO_STATUS = globalConstants.PO_STATUS;
  UPLOAD_PATH = globalConstants.UPLOADS_DIR;

  loader = {};
  imageSrc = [];
  tcdSubmitted = false;
  toggle = true;
  FPtypes = globalConstants.FP;
  tcdError = null;
  itemTotals = {};
  itemTotal = 0;
  itemsError = null;
  exchangeRate = [];
  currencySymbol = '';
  customError = {}

  keywordVessel = 'vessel';
  keywordVendor = 'vendorName';
  keywordCurrency = 'name';
  keywordPort = 'name';
  keywordUom = 'uomName';
  keywordGroup = 'groupName';
  previewFn = 0;

  tcdTotal = 0;
  tcdDiscount = 0;

  company = JSON.parse(localStorage.getItem('companyXid'));
  tcdoritems = true

  permissions = this.CF.findPermissions();

  enquiryDetails = []
  itemTotalsSup = {}
  cat = null
  poSentDate = null
  itemTotalSup
  todayDate = null
  poWithoutEnquiry = false
  deliveryDate = null


  itemsDocsArray : FormArray

  ngOnInit() {
    this.id = this.activatedroute.snapshot.params.id;
    this.todayDate = this.CF.dateObjectToJsonDate(new Date())
    this.createForm();
    this.getTypes();
    this.getCompanies();
    this.getVessels();
    this.getCurrencies();
    this.getCustomers();
    this.getPort();
    this.getItemGroups();
    this.getTcdTypes();
    this.getUoms();
    this.getVendors();
    this.getExchangeRate();
    this.tcdoritems = true
    this.getOrderDetails(this.id)


  }

  getOrderDetails(id) {
    let _this = this;
    let params = { where: { id: id} };
    _this.CS.getOrderDetails(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.orderDetails = response.result;
        _this.selectedPoCategoryId = this.orderDetails.poCategory;
        _this.selectedCustomerId = this.orderDetails.customerXid;

        this.setForm();
        if(!_this.orderDetails.enqXid) {
          _this.poWithoutEnquiry = true
        }
        
      } else {
        _this.orderDetails = {};
      }
    });
  }

  getCustomers() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'status': 1, typeUser: 2, approvedVendor: 1 } };
    _this.CS.getCustomerList(params).subscribe(response => {
      if (response && response.status == "success" && response.result) {
        _this.customers = response.result;
      } else {
        _this.customers = [];
      }
    });
  }

  getPort() {
    let _this = this;
    let params = { model_name: 'port', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("port", response);
      if (response && response.status == "success" && response.result) {
        _this.port = response.result;
      } else {
        _this.port = [];
      }
    });
  }

  getItemGroups() {
    let _this = this;
    let params = { model_name: 'itemGroups', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("getItemGroups", response);
      if (response && response.status == "success" && response.result) {
        _this.itemGroups = response.result;
      } else {
        _this.itemGroups = [];
      }
    });
  }

  getTypes() {
    let _this = this;

    let params = { model_name: 'poTypes', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("itemTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.itemTypes = response.result;
      } else {
        _this.itemTypes = [];
      }
    });
  }

  getTcdTypes() {
    let _this = this;
    let params = { model_name: 'tcdTypes', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("tcdTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.tcdTypes = response.result;
        console.log(" _this.tcdTypes", _this.tcdTypes);
      } else {
        _this.tcdTypes = [];
      }
    });
  }

  getUoms() {
    let _this = this;
    let params = { model_name: 'uom', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("getUoms", response);
      if (response && response.status == "success" && response.result) {
        _this.uoms = response.result;
        console.log(" _this.uoms", _this.uoms);
      } else {
        _this.uoms = [];
      }
    });
  }

  setForm() {
    console.log("_this.orderDetailsggg", this.orderDetails);
    let advAmtUSD = this.orderDetails.advanceAmountUsd;
    if (this.orderDetails.advanceAmountUsd) {
      advAmtUSD = this.orderDetails.advanceAmountUsd.toFixed(3);
    }


    this.purchaseForm = this.formBuilder.group({
      id: [this.orderDetails.id],
      poDate: [this.orderDetails.poDate ? this.toJsonDate(this.orderDetails.poDate) : null],
      //poType: [this.orderDetails.poType, Validators.required],
      poCategory: [this.orderDetails.poCategory, Validators.required],
      currencyXid: [this.orderDetails.currencyXid],
      currency: [this.orderDetails.currency, Validators.required],
      advanceAmountUsd: [advAmtUSD, [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]], //Validators.required, 
      remarks: [this.orderDetails.remarks, Validators.required],
      vesselXid: [this.orderDetails.vesselXid, Validators.required],
      vesselName: [this.orderDetails.vesselName, Validators.required],
      vendorXid: [this.orderDetails.vendorXid, Validators.required],
      vendorName: [this.orderDetails.vendorName, Validators.required],
      customerXid: [this.orderDetails.customerXid],
      customerName: [this.orderDetails.customerName, Validators.required],
      categoryXid: [this.orderDetails.categoryXid],
      //categoryName: [this.orderDetails.categoryName, Validators.required],
      portXid: [this.orderDetails.portXid],
      port: [this.orderDetails.port, Validators.required],
      companyXid: [this.orderDetails.companyXid, Validators.required],
      poCost: this.orderDetails.poCost,
      tcdDetails: this.formBuilder.array([]),
      details: this.formBuilder.array([]),
      status: 1,
      customerNameAuto: [this.orderDetails.customerName],
      portNameAuto: [this.orderDetails.port],
      currencyNameAuto: [this.orderDetails.currency],
      amountUsd: null,
      amountLocal:[this.orderDetails.amountLocal],
      tcd: [this.orderDetails.tcd],
      enqNumber:[this.orderDetails.enqNumber],
      quotationNumber:[this.orderDetails.quotationNumber],
      enqXid:[this.orderDetails.enqXid],
      quotationXid:[this.orderDetails.quotationXid],
      poStatus:[this.orderDetails.poStatus],
      poRefId:[this.orderDetails.poCategory ==2 ? this.orderDetails.id : this.orderDetails.poRefId],
      poId:[this.orderDetails.id],
      exRate:[this.orderDetails.exRate],
      poSentDate:[this.orderDetails.poSentDate],
      supcurrencyXid:[''],
      supcurrency:[''],
      supexRate:[''],
      amountUsdSup:0,  
      amountLocalSup:0,
      tcdSup:0,
      supCost:0,
      isDirectPO : [this.orderDetails.isDirectPO],
      directMailSentDate:[this.orderDetails.directMailSentDate ? this.toJsonDate(this.orderDetails.directMailSentDate) : null],
      documents: this.formBuilder.array([]),
    });
    console.log("this.purchaseForm--->", this.purchaseForm);
    this.getEnquiryDetails()
    this.setTcdItems();
    this.setDocuments();

  }

  setItems() {
    let _this = this;
    if (_this.orderDetails.PD && _this.orderDetails.PD.length) {
      _.forEach(_this.orderDetails.PD, function (field, index) {
        let item = _this.formBuilder.group({
          id: _this.cat ==2 ? '':field.id ,
          groupXid: [field.groupXid],
          group: [{value:field.group,disabled:true}, Validators.required],
          description: [{value:field.description,disabled:true}, Validators.required],
          typeXid: [{value:field.typeXid,disabled:true}],
          type: [{value:field.type,disabled:true}],
          uomXid: [field.uomXid],
          uom: [{value:field.uom,disabled:true}, Validators.required],
          poQty: [field.poQty ? field.poQty.toFixed(2) : field.poQty, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
          poDiscount: [{value:field.poDiscount ? field.poDiscount.toFixed(2) : 0.00,disabled:true}, [Validators.pattern(/^(([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/)]],
          poPrice: [{value:_this.orderDetails.poCategory == 1 && _this.orderDetails.isDirectPO ==1 ? field.supPrice.toFixed(3) : field.poPrice.toFixed(3) ,disabled:field.supPrice==0 ? false:true}, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          poCost: [{value:_this.orderDetails.poCategory == 2 ? (field.poCost ? field.poCost.toFixed(3) : field.poCost):field.supCost.toFixed(3),disabled:true}, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          status: field.status,
          groupNameAuto: [{value:field.group,disabled:true}],
          uomNameAuto: [{value:field.uom,disabled:true}],
          supQty:[{value:field.supQty,disabled:field.supQty==0 ? false:true}],
          supPrice:[{value:field.supPrice.toFixed(3),disabled:field.supPrice==0 ? false:true}],
          supCost:[{value:field.supCost,disabled:true}],
          qtQty:[{value:field.qtQty,disabled:true}],
          qtPrice:[{value:field.qtPrice.toFixed(3),disabled:true}],
          qtCost:[{value:field.qtCost,disabled:true}],          

        });
        _this.itemsArray = _this.purchaseForm.get('details') as FormArray;
        _this.itemsArray.push(item);
        _this.itemTotals[index] = field.poCost;
        _this.itemTotalsSup[index] = field.supCost

        _this.calculateCost(index);
        console.log("item", item);
      });
    } else {
      this.createItems();
    }
  }

  createForm() {
    this.purchaseForm = this.formBuilder.group({
      poDate: [null],
      poCategory: ['', Validators.required],
      currencyXid: [null],
      currency: ['', Validators.required],
      advanceAmountUsd: ['', [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]], //Validators.required
      remarks: ['', Validators.required],
      vesselXid: ['', Validators.required],
      vesselName: ['', Validators.required],
      vendorXid: ['', Validators.required],
      vendorName: ['', Validators.required],
      customerXid: [''],
      customerName: ['', Validators.required],
      portXid: [''],
      port: ['', Validators.required],
      companyXid: [this.company],
      tcdDetails: this.formBuilder.array([]),
      details: this.formBuilder.array([]),
      poCost: null,
      status: 1,
      customerNameAuto: [''],
      portNameAuto: [''],
      currencyNameAuto: [''],
      amountLocal: 0,
      tcd: 0,
      poSentDate:[''],
      supcurrencyXid:[''],
      supcurrency:[''],
      supexRate:[''],
      amountUsdSup:0,  
      amountLocalSup:0,
      tcdSup:0,
      supCost:0,
      isDirectPO:[''],
      directMailSentDate:[''],
      documents: this.formBuilder.array([]),
    });
    this.createItems();
    //this.createTcdItems();
    this.createDocuments();
    console.log(" this.purchaseForm", this.purchaseForm);
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.purchaseForm.controls;
  }

  createItems(flag?) {
    if (flag) {
     this.checkValidation()
      if (this.purchaseForm.controls.details.status == 'INVALID') {
        this.submitted = true;
        return false;
      }
    }
    let item = this.formBuilder.group({
      id: null,
      groupXid: [null],
      group: ['', Validators.required],
      description: ['', Validators.required],
      typeXid: [null],
      type: ['', Validators.required],
      uomXid: [null],
      uom: ['', Validators.required],
      poQty: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
      poDiscount: ['', [Validators.pattern(/^\d+(\.\d{1,2})?$/)]],
      poPrice: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      poCost: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      status: 1,
      poXid: this.id,
      groupNameAuto: [''],
      uomNameAuto: [''],
      supQty:['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
      supPrice:['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      supCost:['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      qtQty: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{2}?)?$/)]],
      qtPrice: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      qtCost: ['', [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],

    });
    this.itemsArray = this.purchaseForm.get('details') as FormArray;
    this.itemsArray.push(item);
  }

  createTcdItems(flag?) {
    console.log("this.purchaseForm.controls.tcdDetails");
    if (flag) {
      if (this.purchaseForm.controls.tcdDetails.status == 'INVALID') {
        this.tcdSubmitted = true;
        return false;
      }
    }
    let item = this.formBuilder.group({
      id: '',
      tcdName: null,
      tcdDesc: null,
      tcdType: [''],
      flatOrPercentage: [''],
      amount: [null],// [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      tcdAmount: [''],// [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
      companyXid: [this.company],
      status: 1,
      poXid: this.id,

    });
    this.itemsArray = this.purchaseForm.get('tcdDetails') as FormArray;
    console.log("this.itemsArray ", this.itemsArray);
    this.itemsArray.push(item);
  }

  setTcdItems() {
    let _this = this;
    console.log("cat",_this.cat)
    if (_this.orderDetails.PT && _this.orderDetails.PT.length) {
      _.forEach(_this.orderDetails.PT, function (field) {
        console.log("field", field);
        var companyXid = JSON.parse(localStorage.getItem('companyXid'));
        let item = _this.formBuilder.group({
          id: _this.cat ==2 ? '':field.id,
          tcdName: null,
          tcdDesc: {value:field.tcdDesc,disabled:true},
          tcdType: [{value:field.tcdType ==0 ?null:field.tcdType,disabled:true}, Validators.required],
          flatOrPercentage: [{value:field.flatOrPercentage,disabled:true}, Validators.required],
          amount: [{value:field.amount ? field.amount.toFixed(3) : field.amount,disabled:true}, [Validators.required, Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          tcdAmount: [{value:field.tcdAmount ? field.tcdAmount.toFixed(3) : field.tcdAmount,disabled:true}, [Validators.pattern(/^([0-9]+\.[0-9]{3}?)?$/)]],
          companyXid: companyXid,
          status: 1,
          poXid:[field.poXid]
          //poXid: _this.id
        });
        console.log("item", item);
        _this.itemsArray = _this.purchaseForm.get('tcdDetails') as FormArray;
        console.log("_this.itemsArray", _this.itemsArray);
        _this.itemsArray.push(item);
        console.log("item", item);
      });
    } else {
     // _this.createTcdItems();
    }
    _this.setItems();
  }

  show(field) {
    this.el.nativeElement.querySelector('.' + field).classList.remove("hidden");
    this.el.nativeElement.querySelector('.' + field + '_id').classList.add("hidden");
  }

  close(field) {
    this.el.nativeElement.querySelector('.' + field).classList.add("hidden");
    this.el.nativeElement.querySelector('.' + field + '_id').classList.remove("hidden");
  }
  deleteItems(index) {

    var element = (<FormArray>this.purchaseForm.controls['details']).at(index);
    this.itemsError = null;
    this.submitted = false;
    this.tcdoritems = false
    
    if ((<FormArray>element).controls['id'].value) {
      if (this.purchaseForm.controls.details.status == 'VALID') {
        var data = { status: 4 };
        element.patchValue(data);
        this.tcdoritems = false
      }
       else {
        this.submitted = true;
        this.itemsError = 'Trying to delete stored item.Please correct the validation.';
      }
    } else {
      this.itemsArray = this.purchaseForm.get('details') as FormArray;
      this.itemsArray.removeAt(index);
    }
    this.itemTotals[index] = 0;

    this.calculateTcdAmount();

  }

  deleteTcdItems(index) {
    this.tcdError = null;
    this.tcdSubmitted = false;
    var element = (<FormArray>this.purchaseForm.controls['tcdDetails']).at(index);
    if ((<FormArray>element).controls['id'].value) {
      if (this.purchaseForm.controls.tcdDetails.status == 'VALID') {
        var data = { status: 4 };
        element.patchValue(data);
        this.tcdoritems = false

      } else {
        this.tcdSubmitted = true;
        this.tcdError = 'Trying to delete stored item.Please correct the validation.';
      }
    } else {
      this.itemsArray = this.purchaseForm.get('tcdDetails') as FormArray;
      this.itemsArray.removeAt(index);
    }
    this.itemTotals[index] = 0;
    this.calculateTcdAmount();
  }


  getCompanies() {
    let _this = this;
    let params = { model_name: 'companies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.companies = response.result;
      } else {
        _this.companies = [];
      }
    });
  }

  getVessels() {
    let _this = this;
    let params = { model_name: 'vessels', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.vessels = response.result;
      } else {
        _this.vessels = [];
      }
    });
  }

  getVendors() {
    let _this = this;
    this.toggle = false;
    let params = { model_name: 'vendors', where: { 'status': 1, typeUser: 1, approvedVendor: 1 } };
    _this.CS.getVendorList(params).subscribe(response => {
      _this.toggle = true;
      console.log("response getVendorList", response);
      if (response && response.status == "success" && response.result) {
        _this.vendors = response.result;
        console.log("_this.vendors", _this.vendors);
        _this.getOrderDetails(this.id);
      } else {
        _this.vendors = [];
      }
    });
  }

  getCurrencies() {
    let _this = this;
    let params = { model_name: 'currencies', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencies = response.result;
      } else {
        _this.currencies = [];
      }
    });
  }

  clear(fields, index?, checkField?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = checkField && field == checkField ? '' : null;
        (<FormArray>_this.purchaseForm.controls['details']).at(index).patchValue(data);
      } else {
        _this.purchaseForm.get(field).setValue(checkField && field == checkField ? '' : null);
      }
    });

  }

  saveItems() {
    console.log("this.purchaseForm.controls['details']", this.purchaseForm.controls['details']);
    if (this.edit || this.edit == 0) {
      console.log("this.savedItems", this.savedItems[this.edit]);
      this.savedItems[this.edit] = this.purchaseForm.controls['details'].value[0];
      this.edit = null;
    } else {
      if (this.purchaseForm.controls['details'].status == 'VALID') {
        this.savedItems.push(this.purchaseForm.controls['details'].value[0]);
      } else {
        this.itemSubmitted = true;
      }
    }

  }

  makeDecimalPoint(field?, index?, formArray?, point?) {
    let _this = this;
    if (index || index == 0) {
      var element = (<FormArray>this.purchaseForm.controls[formArray]).at(index);
      var value = (<FormArray>element).controls[field].value;
      var data = {};
      data[field] = (parseFloat(value.toString()).toFixed(point ? 2 : 3));
      value && !isNaN(value) ? element.patchValue(data) : '';
    } else {
      var value = _this.purchaseForm.get(field).value;
      if (value != "") {
        _this.purchaseForm.get(field).setValue((parseFloat(value.toString()).toFixed(point ? 2 : 3)));
        // ( value && !isNaN(value)) ? _this.purchaseForm.get(field).setValue((parseFloat(value.toString()).toFixed(point ? 2 : 3))) : '';
      } else {
        _this.purchaseForm.get(field).setValue("");
      }
    }
  }

  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("from", from);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      console.log("getId", getId);
      if (isNaN(getId)) {
        if (index || index == 0) {
          var element = (<FormArray>this.purchaseForm.controls['details']).at(index);
          if ((<FormArray>element).controls[to].value == getId) {
            return false;
          }
        } else {
          if (this.purchaseForm.controls[to].value == getId) {
            return false;
          }
        }
        var check = {};
        check[dataField] = getId;
        var item = _.find(data, check);
      } else {
        console.log("is num");
        var item = _.find(data, { id: parseInt(getId, 10) });
      }
      console.log("item", item);
      if (item) {
        if (index || index == 0) {
          var input = {};
          input[to] = item[dataField];
          (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = getId;
            (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.purchaseForm.get(to).setValue(item[dataField]);
          if (typeof event == 'object') {
            this.purchaseForm.get(from).setValue(getId);
          }
          console.log("this.purchaseForm", this.purchaseForm);
        }
      } else {
        if (index || index == 0) {
          var input = {};
          input[to] = null;
          (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
          if (typeof event == 'object') {
            var input = {};
            input[from] = null;
            (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
          }
        } else {
          this.purchaseForm.get(to).setValue(null);
          if (typeof event == 'object') {
            this.purchaseForm.get(from).setValue(null);
          }
        }
      }
    } else {
      if (index || index == 0) {
        var input = {};
        input[to] = null;
        (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
        if (typeof event == 'object') {
          var input = {};
          input[from] = null;
          (<FormArray>this.purchaseForm.controls['details']).at(index).patchValue(input);
        }
      } else {
        this.purchaseForm.get(to).setValue(null);
        if (typeof event == 'object') {
          this.purchaseForm.get(from).setValue(null);
        }
      }
    }
  }

  triggerFileUpload() {
    this.el.nativeElement.querySelector("#fileUpload").click();
  }

  deleteDoc(index) {
    this.fileToUpload.splice(index, 1);
    this.imageSrc.splice(index, 1)
  }

   upload(files: File[]) {

    let _this = this;
    _.forEach(files, function (value) {
       console.log("reader", value)
       var fileTypes = ['jpg', 'jpeg', 'png', 'xlsx', 'txt', 'gif', 'pdf', 'xls'];  //acceptable file types
        var extension = value.name.split('.').pop().toLowerCase() 
        console.log(extension)
        var Success = fileTypes.indexOf(extension) > -1
       if(Success) {
         if (value.size < 4096000) {
      var reader = new FileReader();
      reader.readAsDataURL(value);
      reader.onload = () => {
        var src = reader.result as string;
        _this.imageSrc.push(src);
        _this.fileToUpload.push(value);
      }
      }
        else {
        alert('Maximum allowed file size is 4 MB!');
        return
      }
      
    } else {
      alert('File type not supported')
    }
    });

    document.getElementById('fileUpload')['value'] = "";
  }

  resetFileUpload(){
    let _this = this;
    _this.fileToUpload = [];
    _this.imageSrc =[];
  }
/*
   onAfterAddingFile() {
    let _this = this;
       let latestFile = _this.imageSrc.queue[_this.fileUpload.queue.length-1]
       _this.fileToUpload.queue = []; 
       _this.fileToUpload.queue.push(latestFile);
    }*/

  deleteDocFromServer(doc, index) {
    console.log("doc", doc);
    var params = { id: doc.id, model_name: 'poDocument' };
    let _this = this;
    _this.loader[doc.id] = true;
    _this.CS.removeData(params).subscribe(response => {
      _this.loader[doc.id] = false;
      if (response && response.status == "success" && response.result) {
        _this.orderDetails.PDO.splice(index, 1);
      } else {
        this.toastr.error('Something Went Wrong!', 'Failed!');
      }
    });
  }

  calculateCost(index) {
    var element = (<FormArray>this.purchaseForm.controls['details']).at(index);
    if ((<FormArray>element).controls['poQty'].status == 'VALID' && (<FormArray>element).controls['poPrice'].status == 'DISABLED' || (<FormArray>element).controls['poQty'].status == 'VALID' && (<FormArray>element).controls['poPrice'].status == 'VALID' ) {
      var qty = (<FormArray>element).controls['poQty'].value;
      var price = (<FormArray>element).controls['poPrice'].value;
      //var dis = (<FormArray>element).controls['poDiscount'].value;
      var cost = null;
      cost = qty * price;
      console.log("cost",cost)
      //dis = dis / 100;
      var totalValue = cost //- (cost * dis);
      this.itemTotals[index] = totalValue;
      (<FormArray>element).controls['poCost'].setValue(totalValue.toFixed(3));
      this.calculateTcdAmount();
    }
    let _this = this
    var element = (<FormArray>this.purchaseForm.controls['details']).at(index);
    var qtyForSupplierGen = _this.orderDetails.poCategory == 1 ? (<FormArray>element).controls['supQty'] :(<FormArray>element).controls['poQty']
    console.log("qtyForSupplierGen",qtyForSupplierGen)
    if ((qtyForSupplierGen.status == 'DISABLED' || qtyForSupplierGen.status == 'VALID') && (<FormArray>element).controls['supPrice'].status == 'DISABLED' || qtyForSupplierGen.status == 'VALID' && (<FormArray>element).controls['supPrice'].status == 'VALID' ) {
      var qty = qtyForSupplierGen.value;
      var price = (<FormArray>element).controls['supPrice'].value;
      //var dis = (<FormArray>element).controls['poDiscount'].value;
      var cost = null;
      cost = qty * price;
      console.log("costForSup---->",cost)
      //dis = dis / 100;
      var totalValue = cost //- (cost * dis);
      this.itemTotalsSup[index] = totalValue;
      (<FormArray>element).controls['supCost'].setValue(totalValue.toFixed(3));
      console.log("itemSup",this.itemTotalsSup[index])
      this.calculateTcdAmount();
    }
    if ((<FormArray>element).controls['qtQty'].status == 'DISABLED' && (<FormArray>element).controls['qtPrice'].status == 'DISABLED' || (<FormArray>element).controls['qtQty'].status == 'VALID' && (<FormArray>element).controls['qtPrice'].status == 'VALID' ) {
      var qty = (<FormArray>element).controls['qtQty'].value;
      var price = (<FormArray>element).controls['qtPrice'].value;
      cost = qty * price;
      (<FormArray>element).controls['qtCost'].setValue(cost.toFixed(3));
            console.log("costCust",cost)
      }
  }

  saveList(from?, to?, model?, index?, form?) {
    let _this = this;
    this.toggle = false;
    if (index || index == 0) {
      var element = (<FormArray>this.purchaseForm.controls[form]).at(index);
      if ((<FormArray>element).controls[from].value) {
        var value = (<FormArray>element).controls[from].value;
        var params = { model_name: model, data: {} };
        params.data[from] = value;
        params.data['status'] = 1;
        _this.CS.setData(params).subscribe(response => {
          console.log("response", response);
          if (response && response.status == 'success') {
            (<FormArray>_this.purchaseForm.controls['tcdDetails']).at(index).patchValue({
              tcdType: response.result.id
            });
            _this.tcdTypes.push(response.result);
            _this.toggle = true;
            _this.close('tcdType' + index);
            _this.calculateTcdAmount();
          } else {
            this.toastr.error('Something Went Wrong!', 'Failed!');
          }
        });
      } else {
        this.tcdSubmitted = true;
      }
    }
  }

  clearTcd(fields, index?) {
    var _this = this;
    _.forEach(fields, function (field) {
      console.log("field", field);
      if (index || index == 0) {
        var data = {};
        console.log("field");
        data[field] = null;
        (<FormArray>_this.purchaseForm.controls['tcdDetails']).at(index).patchValue(data);
      } else {
        _this.purchaseForm.get(field).setValue(null);
      }
    });
  }

  getExchangeRate() {
    let _this = this;
    let params = {};
    _this.CS.getLatestCurrencyExchangeRates(params).subscribe(response => {
      console.log("exchangeRate", response);
      if (response && response.status == "success" && response.result) {
        let exchangeRates = [];
        _.map(response.result, function (item) {
          if (item.ER && item.ER.length > 0) {
            exchangeRates.push(item.ER[0]);
          }
        });
        _this.exchangeRate = exchangeRates;
        console.log(" _this.exchangeRate", _this.exchangeRate);
      } else {
        _this.exchangeRate = [];
      }
    });
  }

  toUsd() {
    let _this = this;
    if (_this.purchaseForm.value.currencyXid && _this.purchaseForm.value.exRate ) {
      console.log("uSd")
      //var rate = _.find(_this.exchangeRate, { currencyXId: _this.purchaseForm.value.currencyXid });
      var rate = _this.purchaseForm.value.exRate
      var curencyObj = _.find(_this.currencies, { id: _this.purchaseForm.value.currencyXid });
      _this.currencySymbol = curencyObj ? curencyObj.symbol : ''
      console.log("currencySymbol",_this.currencySymbol)
      
     console.log("slected rate", rate)
   
      if (rate) {
        var InUsd = _this.purchaseForm.value.poCost / rate;
        this.purchaseForm.get('amountUsd').setValue(InUsd.toFixed(3));
        //this.purchaseForm.get('exRate').setValue(rate.Rate);
      } else {
        //if no exchange rate       
        this.purchaseForm.get('amountUsd').setValue(0);
        //this.purchaseForm.get('exRate').setValue(0);
      }

      this.purchaseForm.get('amountLocal').setValue(_this.purchaseForm.value.poCost);
      let tcdVal = (_this.tcdTotal - _this.tcdDiscount);
      this.purchaseForm.get('tcd').setValue(tcdVal.toFixed(3));
   } 

       if (_this.purchaseForm.value.supcurrencyXid && _this.purchaseForm.value.supexRate ) {
      console.log("uSd")
      //var rate = _.find(_this.exchangeRate, { currencyXId: _this.purchaseForm.value.currencyXid });
      var rate = _this.purchaseForm.value.supexRate
      var curencyObj = _.find(_this.currencies, { id: _this.purchaseForm.value.supcurrencyXid });
      _this.currencySymbol = curencyObj ? curencyObj.symbol : ''
      
     console.log("slected rate", rate)
   
      if (rate) {
        var InUsdSup = _this.purchaseForm.value.supCost / rate;
        this.purchaseForm.get('amountUsdSup').setValue(InUsdSup.toFixed(3));
        //this.purchaseForm.get('exRate').setValue(rate.Rate);
      } else {
        //if no exchange rate       
        this.purchaseForm.get('amountUsdSup').setValue(0);
        //this.purchaseForm.get('exRate').setValue(0);
      }

      let tcdVal = (_this.tcdTotal - _this.tcdDiscount);
      this.purchaseForm.get('amountLocalSup').setValue(_this.purchaseForm.value.supCost);
      this.purchaseForm.get('tcdSup').setValue(tcdVal.toFixed(3));
   }
  }

  calculateTcdAmount() {
    console.log("calculateTcdAmount");
    let _this = this;
    _this.itemTotal = 0;
    _this.tcdTotal = 0;
    _this.tcdDiscount = 0;
    _this.itemTotalSup = 0
    _.map(_this.itemTotals, function (item) {
      console.log("_this.itemTotal", item);
      if(item == null) {
      _this.itemTotal = _this.itemTotal + 0;
    }
    else {
       _this.itemTotal = _this.itemTotal + item;
    }
    });
     _.map(_this.itemTotalsSup, function (item) {
      console.log("_this.itemTotal", item);
      if(item == null) {
      _this.itemTotalSup = _this.itemTotalSup + 0;
    }
    else {
       _this.itemTotalSup = _this.itemTotalSup + item;
    }
    });
    var poCost = _this.itemTotal;
    var supCost = _this.itemTotalSup
    _this.purchaseForm.get('poCost').setValue(poCost.toFixed(3));
    console.log("supCost", supCost);
    _this.purchaseForm.get('supCost').setValue(supCost.toFixed(3));
    console.log("poCost", poCost);
    if (this.purchaseForm.controls.tcdDetails.status == 'VALID' && _this.itemTotal) {
      _.forEach(_this.purchaseForm.getRawValue().tcdDetails, function (value, index) {
        //value.amount == null ? 0 : value.amount
        var rate = 0;
        rate = value.amount == null ? 0 : parseFloat(value.amount);
        console.log("rate",value)
        if (value.flatOrPercentage == globalConstants.PER) {
          rate = rate / 100;
          rate = (_this.itemTotal * rate);
        }
        var element = (<FormArray>_this.purchaseForm.controls['tcdDetails']).at(index);
        if (value.tcdType == globalConstants.DISCOUNT) {
          poCost = poCost - parseFloat(rate.toString());
          supCost = supCost - parseFloat(rate.toString());
          _this.tcdDiscount = parseFloat(_this.tcdDiscount.toString()) + parseFloat(rate.toString());
          _this.tcdDiscount = parseFloat((_this.tcdDiscount).toFixed(3));
        } else {
          poCost = poCost + parseFloat(rate.toString());
          supCost = supCost + parseFloat(rate.toString());
          _this.tcdTotal = parseFloat(_this.tcdTotal.toString()) + parseFloat(rate.toString());
          _this.tcdTotal = parseFloat((_this.tcdTotal).toFixed(3));
        }
        _this.purchaseForm.get('poCost').setValue(poCost.toFixed(3));
        _this.purchaseForm.get('supCost').setValue(supCost.toFixed(3));
        (<FormArray>element).controls['tcdAmount'].setValue(rate.toFixed(3));
      });
     // console.log("poCost", poCost);
    }
    _this.toUsd();
  }

  onSubmit(cate) {
    let _this = this;
    _this.tcdError = null;
    _this.customError = {};
    _this.tcdoritems = true
    console.log("this.purchaseForm", _this.purchaseForm);
    let formData = new FormData();
    var imageSize =[]
   var size;
    _.forEach(this.imageSrc, function (field) {
      imageSize.push(field.length)
      console.log(imageSize)
      size = imageSize.reduce((a, b) => a + b)/1000000
      console.log("size",size)
    })
    if(size > 10) {
      this.toastr.error('Total Allowed files is 10 Mb.', 'Invalid!');
      return
    }
    if (this.fileToUpload.length) {
      for (var i = 0; i < this.fileToUpload.length; i++) {
        formData.append("files", this.fileToUpload[i], uuidv4() + this.fileToUpload[i].name);
      }
    }
    /*if (_this.purchaseForm.value.tcdDetails.length > 1 && this.purchaseForm.controls.tcdDetails.status == 'VALID') {
      var checkDiscount = _.countBy(_this.purchaseForm.value.tcdDetails, function (tcd) {
        return tcd.tcdType == globalConstants.DISCOUNT;
      });
      if (checkDiscount.true > 1) {
        _this.tcdError = 'Only one discount row allowed!';
        return false;
      } else {
        var grouped = _.groupBy(_this.purchaseForm.value.tcdDetails, function (tcd) {
          return tcd.tcdType == globalConstants.TAX ? 'tax' : 'others';
        });
        if (grouped && grouped.tax && grouped.tax.length > 1) {
          var checkUnique = _.filter(
            _.uniq(
              _.map(grouped.tax, function (item) {
                if (_.filter(grouped.tax, { amount: item.amount }).length > 1) {
                  return item.amount;
                }
                return false;
              })),
            function (value) {
              return value;
            });
          if (checkUnique && checkUnique.length > 0) {
            _this.tcdError = 'Duplicate tax value not allowed!';
            return false;
          }
        }
      }
    } else if (_this.purchaseForm.value.tcdDetails.length == 1) {
      if (!_this.purchaseForm.value.tcdDetails[0].tcdType && !_this.purchaseForm.value.tcdDetails[0].flatOrPercentage && !_this.purchaseForm.value.tcdDetails[0].amount && !_this.purchaseForm.value.tcdDetails[0].tcdAmount) {
        this.itemsArray = this.purchaseForm.get('tcdDetails') as FormArray;
        this.itemsArray.removeAt(0);
      } else {
        _this.tcdSubmitted = true;
      }
    } else {
      _this.tcdSubmitted = false;
      /// not mandatory return false;
    }
    if (_this.purchaseForm.value.poCost < 0) {
      _this.tcdError = 'Discount value should be less than total cost!';
      return false;
    }
    if (_this.purchaseForm.value.poCost && parseFloat(_this.purchaseForm.value.advanceAmountUsd) > parseFloat(_this.purchaseForm.value.poCost)) {
      _this.customError['advanceAmountUsd'] = 'Advance amount should be less than total cost!';
      return false;
    }*/
    //_this.purchaseForm.value['poStatus'] = _this.purchaseForm.value.poDate ;
    _this.checkValidation()
     
     if (this.purchaseForm.invalid) {
      console.log("this.purchaseForm",this.purchaseForm);
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      _this.loading = false;
    }
    if (this.purchaseForm.status == 'VALID') {
      _this.loading = true;
      if(cate ==2) {
        var title = 'Generate Purchase Order'
        var text = 'Do you want to generate PO to Supplier ?'
      }
      else {
        title = 'Confirm Submitting Purchase Order'
        text = 'Have you confirmed the details before submit?'
      }
    Swal.fire({ 
    title: title,
    text: text,
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    allowOutsideClick: false,
    position: 'center',
    }).then((result) => {

      if (result.value) {
      _this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);

   
      
     if(cate ==2) {
               console.log("new vendor po order")
       _this.purchaseForm.get('id').setValue(null)
       //_this.purchaseForm.get('poStatus').setValue('2')
                       _this.purchaseForm.get('exRate').setValue(_this.purchaseForm.value['supexRate']) 
       _this.purchaseForm.get('poCost').setValue(_this.purchaseForm.value['supCost'])
        _this.purchaseForm.get('amountUsd').setValue(_this.purchaseForm.value['amountUsdSup'])
       _this.purchaseForm.get('amountLocal').setValue(_this.purchaseForm.value['amountLocalSup'])
       _this.purchaseForm.get('tcd').setValue(_this.purchaseForm.value['tcdSup'])
       var poGen = 2
       var params = this.purchaseForm.value
       params['poGen'] = poGen
       // _this.purchaseForm.value['poDate'] =_this.toDateObject(_this.poSentDate)
        _this.purchaseForm.value['poDate'] = null
                   _this.purchaseForm.value['details'] =  _this.purchaseForm.getRawValue().details
          _this.purchaseForm.value['tcdDetails'] =  _this.purchaseForm.getRawValue().tcdDetails
        _this.purchaseForm.value['poCategory'] = 1

        console.log("poForm",_this.purchaseForm)

        var saveUpdateOrder = _this.CS.savePoOrder(params)
         for (var i in  _this.purchaseForm.value['details']) {
         _this.purchaseForm.value['details'][i].poPrice =  _this.purchaseForm.value['details'][i].supPrice;
         _this.purchaseForm.value['details'][i].poCost =  _this.purchaseForm.value['details'][i].supCost;
        console.log("ff", _this.purchaseForm.value['details'])
        console.log("poForm",_this.purchaseForm)
        
         
        

     }
       
      }
      else {
          saveUpdateOrder =  _this.CS.updatePoOrder(_this.purchaseForm.value)
          poGen = 0
          _this.purchaseForm.value['poDate'] =  _this.toDateObject(_this.purchaseForm.get('poDate').value)
          _this.purchaseForm.value['directMailSentDate'] =  _this.toDateObject(_this.purchaseForm.get('directMailSentDate').value)
          _this.purchaseForm.value['details'] =  _this.purchaseForm.getRawValue().details
          _this.purchaseForm.value['tcdDetails'] =  _this.purchaseForm.getRawValue().tcdDetails
          console.log("tcdDetailsc", _this.purchaseForm.getRawValue().tcdDetails)
      }

     

         saveUpdateOrder.subscribe(response => {
        if (response && response.status == "success") {
          if (this.fileToUpload.length) {
            formData.append("data", _this.id);
            _this.CS.saveDocuments(formData).subscribe(response => {
              _this.loading = false;
              if (response && response.data && response.data.status == "success") {
                console.log(response);
                if(cate ==2) {
                   _this.router.navigate(['/purchase-order/edit/' + response.result.id]);
                   this.getOrderDetails(response.result.id); 
                   this.toastr.success('Po Generated.', 'Success!');
                   this.router.routeReuseStrategy.shouldReuseRoute = function () {
                    return false;
                    };
                   
                 }
                 else {
                this.toastr.success('Order Details Updated.', 'Success!');
                this.resetFileUpload();
                this.getOrderDetails(this.id); 
              }
              } else {
                this.toastr.error('Something Went Wrong!', 'Failed!');
              }
            });
            // stay in same page, _this.router.navigate(['/purchase-order/list']);
          } else {
             if(cate ==2) {
                   _this.router.navigate(['/purchase-order/edit/' + response.result.id]);
                   this.getOrderDetails(response.result.id); 
                  this.router.routeReuseStrategy.shouldReuseRoute = function () {
                  return false;
                  };
                   this.toastr.success('Po Generated.', 'Success!');
                    
                 }
             else{  
            this.getOrderDetails(this.id);
            _this.loading = false;
            this.toastr.success('Order Details Updated.', 'Success!'); 
          }
            // stay in same page, _this.router.navigate(['/purchase-order/list']);

          }
          this.getOrderDetails(response.result.id);
        } else {
          _this.loading = false;
          this.toastr.error('Something Went Wrong!', 'Failed!');
        }
      });
       }     else if (result.dismiss === Swal.DismissReason.cancel) {
              _this.loading = false;
              _this.submitted = false;
              _this.tcdSubmitted = false;
               if(cate ==2) {
                 var cancelMsg = 'Purchase Order not generated'
                 } else {
                   cancelMsg = 'Purchase order not submitted'
                 }
      /*Swal.fire(
          'Cancelled',
          'No change in vendor',
          'error'
        )*/
      }})

    } else {
      _this.submitted = true;
      console.log("final.purchaseForm", _this.purchaseForm);
      if (_this.purchaseForm.get('tcdDetails')['controls'].length < 1) {
        _this.createTcdItems();
      }
    }

  }

  viewPdf() {
    this.previewFn = 1;

    /*if(this.purchaseForm.get('poDate').value ==null){
      this.toastr.error('Please Enter PO Date')
      this.loading = false
      return
    }*/
    if(this.previewFn && this.tcdoritems == false){
      if (!confirm("Make sure all changes saved before preview,Do you want to continue?")) {
        return false;
      }
    }

    this.toastr.info('Loading Preview!', 'Loading..');
    this.generatePurchase();
  }
  generatePurchase() {

    let _this = this
    _this.loading = true;



    if(!_this.previewFn) {
      if(_this.orderDetails.isDirectPO ==0 && _this.orderDetails.poCategory ==2 && _this.orderDetails.directMailSentDate==null){
    this.toastr.error('Please choose and submit PO Sent Date')
    _this.loading = false
    return
        
      }
      else if(_this.purchaseForm.get('poDate').value ==null){
    this.toastr.error('Please enter PO Sent Date')
    _this.loading = false
    return
  }
    var checkCost = []
    _.forEach(_this.purchaseForm.getRawValue().details, function(row,index){
      checkCost.push(row.poCost)
      console.log("details",row)

    })
    console.log("details",checkCost)
    if (checkCost.includes("0.000")){
      console.log("checkCost",checkCost)
      _this.loading = false
        this.toastr.error("Qty & Unit price can't be zero. Please correct and save before sending PO to Sup")
        return
      }
        Swal.fire({
        title: 'Sent Purchase Order', 
    text: 'Have you confirmed the details before sending purchase order?',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    allowOutsideClick: false,
    position: 'center',
    }).then((result) => {

      if (result.value) {
      _this.spinner.show();
            setTimeout(() => this.spinner.hide(), 2000);
    
    let email = (localStorage.getItem('mdata'));

    _this.CS.generatePurchase({ 'order': _this.orderDetails, preview: this.previewFn, setterMail: email, 'deliveryDate': _this.deliveryDate}).subscribe(response => {
     console.log("_this.orderDetails",_this.orderDetails);
      _this.loading = false;
      //_this.poSentS = true
      if (response && response.status == "success") {
        if (this.previewFn) {
          // open file
          window.open(this.UPLOAD_PATH + 'poPdf/' + response.fileData, '_blank');          
          this.toastr.info('Verify The Details!', 'Info');

        } else {
          this.toastr.success('Successfuly Sent PO to Vendor!', 'Success!');
          this.updatePoStatus(this.id,2, this.orderDetails.poRefId);
          this.getOrderDetails(_this.id)
          if (typeof (response.customMessage) != 'undefined') {
            this.toastr.error(response.customMessage, 'Failed!');
          }
        }

      } else {
        this.toastr.error(response.customMessage, 'Failed!');
      }
      this.previewFn = 0;

    });
    }     else if (result.dismiss === Swal.DismissReason.cancel) {
              _this.loading = false;
              _this.submitted = false;
              _this.tcdSubmitted = false;
      /*Swal.fire(
          'Cancelled',
          'No change in vendor',
          'error'
        )*/
      }})
  }
  else {
     let email = (localStorage.getItem('mdata'));

    _this.CS.generatePurchase({ 'order': _this.orderDetails, preview: this.previewFn, setterMail: email, 'deliveryDate': _this.deliveryDate }).subscribe(response => {
     console.log("_this.orderDetails",_this.orderDetails);
      _this.loading = false;
      if (response && response.status == "success") {
        if (this.previewFn) {
          // open file
          window.open(this.UPLOAD_PATH + 'poPdf/' + response.fileData, '_blank');          
          this.toastr.info('Verify The Details!', 'Info');

        } else {
          this.toastr.success('Successfuly Sent PO to Vendor!', 'Success!');
          this.updatePoStatus(this.id, 2,this.orderDetails.poRefId);
          if (typeof (response.customMessage) != 'undefined') {
            this.toastr.error(response.customMessage, 'Failed!');
          }
        }

      } else {
        this.toastr.error(response.customMessage, 'Failed!');
      }
      this.previewFn = 0;

    });
  }

  }
  updatePoStatus(poiD, status_value,poRefId) {
    this.CS.updatePoStatus({ "poId": poiD, "poStatus": status_value ,"poRefId":poRefId}).subscribe(response => {

      if (response && response.status == "success") {
        console.log("Status changed", status_value);
        this.orderDetails.poStatus = status_value;
      } else {
        console.log("Status change failed", response);
      }
    });
  }

  generateInvoice(item) {
     Swal.fire({ 
     title : 'Generate Invoice',
    text: 'Have you confirmed the details before generating invoice?',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    allowOutsideClick: false,
    position: 'center',
    }).then((result) => {

      if (result.value) {
      var params = { id: item.id };
      let _this = this;
      _this.loaderItem[item.id] = true;
      //_this.CS.generateInvoice(params).subscribe(response => {        
      let poIds = [];
      poIds.push(item.id)
      let poDetails = {
        poIds: poIds
      }
      _this.CS.generateNewInvoice(poDetails).subscribe(response => {
        _this.loaderItem[item.id] = false;
        if (response && response.status == "success" && response.result) {
          //
          this.toastr.success('Invoice Generations Process Started!', 'Success!');
          _this.router.navigate(['/invoice/invoice-generate/' + response.result.id]);
        } else {
          this.toastr.error(response.message, 'Failed!');
        }
      });
    }

    else if (result.dismiss === Swal.DismissReason.cancel) {
              this.loading = false;
              this.submitted = false;
              this.tcdSubmitted = false;
      /*Swal.fire(
          'Cancelled',
          'No change in vendor',
          'error'
        )*/
      }})
    
  }

  toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date) {
    return this.CF.dateObjectToJsonDate(date)
  }
  resetCustomerValue(){
    console.log("*****")
    this.purchaseForm.get('customerName').setValue(null);
    this.purchaseForm.get('customerXid').setValue(null);
  }

   convertDate(dateString) {
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMONTHYY(objectDate);
  }
    getEnquiryDetails() {
          let _this = this;
    let params = { 'enqid': _this.orderDetails.enqXid };
    _this.CS.getEnquiryDetailsByIdPO(params).subscribe(response => {
      console.log("responseID", response);
      if (response && response.status == "success" && response.result) {
        _this.enquiryDetails = response.result;
        _this.deliveryDate = response.quotationVendors[0].deliveryDate
         _.forEach(response.quotationVendors, function (value, index) {
           if(value.id == _this.orderDetails.quotationXid && _this.orderDetails.poCategory==1) {
             _this.purchaseForm.get('currencyXid').setValue(value.vendCurrencyXid)
              _this.purchaseForm.get('currency').setValue(value.vendCurrency)
           } 
           else if(value.id == _this.orderDetails.quotationXid ) {
               _this.purchaseForm.get('currencyXid').setValue(value.custCurrencyXid)
              _this.purchaseForm.get('currency').setValue(value.custCurrency)
              _this.purchaseForm.get('supcurrencyXid').setValue(value.vendCurrencyXid)
              _this.purchaseForm.get('supcurrency').setValue(value.vendCurrency)
              _this.purchaseForm.get('supexRate').setValue(value.vendExRate) 
           }
         })
      }
       });
    }


    supPrice(index,price) {
        var element = (<FormArray>this.purchaseForm.controls['details']).at(index);
    console.log("poForm",this.purchaseForm);
    if((<FormArray>element).controls['poPrice'].status =='INVALID') {
    (<FormArray>element).controls['poPrice'].setValue(price)
    console.log("true")
  }
}

generatePOVendor(cate,index){
  let _this = this
  _this.cat =cate
  /*if(_this.poSentDate ==null){
    this.toastr.error('Please enter PO Sent Date')
    _this.loading = false
    return
  }*/
const arr = <FormArray>this.purchaseForm.controls.details;
        arr.controls = [];
const arrTcd = <FormArray>this.purchaseForm.controls.tcdDetails;
 arrTcd.controls = []
         this.setTcdItems()
 
  _this.onSubmit(cate)
  _this.loading = false
}


checkValidation() {
  let _this = this
  _.forEach(_this.purchaseForm.get('details')['controls'], function (itemData, index) {
      console.log("itemData", itemData)
      if(_this.orderDetails.poCategory ==1 && _this.orderDetails.isDirectPO ==1) {
      itemData.get("qtCost").setErrors(null);
      itemData.get("qtPrice").setErrors(null);
      itemData.get("qtQty").setErrors(null);
      itemData.get("qtCost").setValidators(null);
      itemData.get("qtPrice").setValidators(null);
      itemData.get("qtQty").setValidators(null);
    }
    else if(_this.orderDetails.isDirectPO ==0) {
      itemData.get("supCost").setErrors(null);
      itemData.get("supPrice").setErrors(null);
      itemData.get("supQty").setErrors(null);
      itemData.get("supCost").setValidators(null);
      itemData.get("supPrice").setValidators(null);
      itemData.get("supQty").setValidators(null);
      itemData.get("qtCost").setErrors(null);
      itemData.get("qtPrice").setErrors(null);
      itemData.get("qtQty").setErrors(null);
      itemData.get("qtCost").setValidators(null);
      itemData.get("qtPrice").setValidators(null);
      itemData.get("qtQty").setValidators(null);
      //(<FormArray>element).controls['custQtUnitPrice'].status == 'VALID'
    }
    else {
      itemData.get("supCost").setErrors(null);
      itemData.get("supPrice").setErrors(null);
      itemData.get("supQty").setErrors(null);
      itemData.get("supCost").setValidators(null);
      itemData.get("supPrice").setValidators(null);
      itemData.get("supQty").setValidators(null);
    }
    })
}

    createDocuments() {
    let _this = this;
    let item = this.formBuilder.group({
      enqXid: [''],
      fileName: [''],
      fileDisplayName: [''],
      filePath: [''],
      companyXid: [null],
      status: 1
    });
       _this.itemsDocsArray = _this.purchaseForm.get('documents') as FormArray;
        _this.itemsDocsArray.push(item);
  }


    setDocuments() {
    let _this = this;
    if (_this.orderDetails && _this.orderDetails.PDO && _this.orderDetails.PDO.length) {
      _.forEach(_this.orderDetails.PDO, function (field, index) {
        console.log("documentttttttttttt", field)
        let item = _this.formBuilder.group({
          enqXid: [field.enqXid],
          fileName: [field.fileName],
          fileDisplayName: [field.fileDisplayName],
          filePath: [field.filePath],
          companyXid: [_this.company],
          status: 1
        });
        _this.itemsDocsArray = _this.purchaseForm.get('documents') as FormArray;
        _this.itemsDocsArray.push(item);
      });
    }
  }

  fixSelectedData(name,index){
  console.log("name",name)
  console.log("index",index)

     var element = (<FormArray>this.purchaseForm.controls['details']).at(index);

     if(name =='group') {
      (<FormArray>element).controls['groupNameAuto'].setValue( (<FormArray>element).controls['group'].value);
    }

     if(name =='uom') {
      (<FormArray>element).controls['uomNameAuto'].setValue( (<FormArray>element).controls['uom'].value);
    }

}
}
