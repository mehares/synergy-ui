import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { globalConstants } from '../../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-po-list',
  templateUrl: './po-list.component.html',
  styleUrls: ['./po-list.component.scss']
})
export class PoListComponent implements OnInit {

  polist = [];
  poSeacrhForm: FormGroup;
  isValidFormSubmitted = null;
  submitted = false;
  sourceType = [{ id: 2, name: "From Customer" }, { id: 1, name: "To Vendor" }];
  sourceTypeVal = this.sourceType[0]['id'];
  currentPOType = this.sourceType[0]['id'];
  customers = [];
  vendors = [];
  itemsPerPage = 18;
  currentPage = 1;
  totalItems = 0;
  offset = 0;
  searchToggle = false;
  searchLoader = false;
  search = '';
  clearFilter = false;
  searchFilterObject = null;
  pageOffset = 0;
  isAdvanceSearch = false;
  customerName = '';
  vendorName = '';
  selCustomerId = null;
  selVendorId = null;

  permissions = this.CF.findPermissions();
  loader = false;
  loaderItem = {};
  keywordVendor = 'vendorName'
  keywordCustomer = 'vendorName'
  userQuestionUpdate = new Subject<string>();
  public innerHeight: any;
  UPLOAD_PATH = globalConstants.UPLOADS_DIR  
  PO_STATUS = globalConstants.PO_STATUS
  PO_SORCE = ['','To Vendor','From Customer']

  firstLoad = false

  constructor(private http: HttpClient,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private CS: CommonservicesService, private CF: CommonFunctionsService,private spinner: NgxSpinnerService,) {
    this.userQuestionUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.resetPagination();
        this.onSubmit();
      });
  }

  ngOnInit() {
    this.innerHeight = window.innerHeight;
    console.log("innerHeight", innerHeight);
    if (this.innerHeight < 700) {
      this.itemsPerPage = 10;
    }
    if (this.innerHeight > 900) {
      this.itemsPerPage = 20;
    }
    if (this.innerHeight > 1000) {
      this.itemsPerPage = 25;
    }


    this.getPOList();
    this.getVendors();
    this.getCustomers();
    this.createSearchForm();
    this.firstLoad = true
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);

  }
  createSearchForm() {
    this.poSeacrhForm = this.formBuilder.group({
      fromDate: [null, []],
      toDate: [null, []],
      source: [null, []],
      search: ['', []],
      customer: ['', []],
      vendor: ['', []],
      pono: ['', []],
      enqno: ['', []],
      customerName: ['', []],
      vendorName: ['', []],
    });
  }

  get f() {
    return this.poSeacrhForm.controls;
  }

  convertDate(dateString) {
    console.log("dateString-", dateString);
    if (dateString == "" || dateString == null) {
      return "";
    }
    let _this = this;
    let objectDate = new Date(dateString);
    return _this.CF.convertDateObjectToDDMONTHYY(objectDate);
  }
  amountDecimal(amt) {
    if (amt != "" && amt!=null) {
      amt = amt.toFixed(3);
    }
    return amt;
  }

  getCustomers() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'approvedVendor': 1, 'status': 1, typeUser: 2 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.customers = response.result;
      } else {
        _this.customers = [];
      }
    });
  }

  getVendors() {
    let _this = this;
    let params = { model_name: 'vendors', where: { 'approvedVendor': 1, 'status': 1, typeUser: 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.vendors = response.result;
      } else {
        _this.vendors = [];
      }
    });
  }


  getPOList() {
    var params = {limit: this.itemsPerPage, offset: this.offset, filterOption: this.searchFilterObject };
    this.pageOffset = this.offset;
    let _this = this;
    _this.loader = true;
    this.CS.getPurchaseOrderList(params).subscribe(response => {
      if (response && response.status == 'success') {
        _this.polist = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
        console.log("orderlist", response);
      } else {
        this.toastr.error('Error Occured,Try again!', 'Failed!');
      }
      _this.loader = false;
    });
  }

  /*
   deleteOrder(item) {
   var params = { id: item.id, model_name: 'poHeader' }
   let _this = this
   _this.loaderItem[item.id] = true
   _this.CS.removeData(params).subscribe(response => {
   _this.loaderItem[item.id] = false
   if (response && response.status == "success" && response.result) {
   _this.getPOList()
   this.toastr.error('Order details deleted successfully!', 'Success!');
   } else {
     this.toastr.error('Something went wrong!', 'Failed!');
   }
   });
   }
   */
  getPOByType() {
    console.log(this.sourceTypeVal);
    this.currentPOType = this.sourceTypeVal;
    this.searchFilterObject=null;
    this.search='';
    this.isAdvanceSearch=false;
    this.customerName = null;
    this.vendorName = null;
    this.resetPagination();
    this.getPOList();
  }

  preventNavClose(event) {
    console.log(event);
    // $(this).parents('.dropdown').find('button.dropdown-toggle').dropdown('toggle')
    return false;
  }

  onSubmit(from = '') {

    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;
    if (this.poSeacrhForm.invalid) {
      this.toastr.error('Please Enter The Fields.', 'Invalid!');
      return;
    }
        if(this.poSeacrhForm.value.fromDate || this.poSeacrhForm.value.toDate){
      console.log(true)
      if(this.poSeacrhForm.value.toDate ==null || this.poSeacrhForm.value.fromDate ==null) {
      this.toastr.error('Please Validate Date.', 'Invalid!');
      _this.searchLoader = false;
      console.log("FORM",this.poSeacrhForm)
      return;
    }
}
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;
    this.sourceTypeVal = this.f.source.value;

    const poSeacrhObj = {
      fromDate: this.toDateObject(this.f.fromDate.value),
      toDate: this.toDateObject(this.f.toDate.value),
      source: this.f.source.value,
      // search: this.f.search.value,
      vendor: this.f.vendor.value,
      customer: this.f.customer.value,
      pono: this.f.pono.value,
      enqno: this.f.enqno.value,
      search: this.search
    };
    _this.searchLoader = false;
    _this.clearFilter = true;    
    _this.searchToggle = false;
    this.searchFilterObject = poSeacrhObj;
    this.getPOList();

    // this.CS.searchPurchaseOrders(poSeacrhObj).subscribe(response => {
    //   console.log("response", response);
    //   _this.searchLoader = false;
    //   _this.searchToggle = false;
    //   _this.clearFilter = true
    //   if (response && response.status == 'success') {
    //     _this.polist = response.result;
    //   } else {
    //     this.toastr.error('Error occured while search,Try again!, 'Failed!');
    //   }
    // });
  }

  preventChange(event) {
    //check if needed function
  }

  pageChanged(event) {
    console.log("event", event)
    this.currentPage = event
    var setoffset = event - 1
    setoffset = setoffset * this.itemsPerPage
    this.offset = setoffset
    this.getPOList();
  }

  changeSelection(event, to) {
    console.log("event", event)
    console.log("to", to);
    // this.poSeacrhForm.get(to).setValue(event.id);

    if (to == 'customer') {
      this.customerName = event.vendorName ? event.vendorName : this.customerName;
      this.selCustomerId = event.id ? event.id : this.selCustomerId;
      this.poSeacrhForm.get(to).setValue(this.selCustomerId);
    }
    if (to == 'vendor') {
      this.vendorName = event.vendorName ? event.vendorName : this.vendorName;
      this.selVendorId = event.id ? event.id : this.selVendorId;
      this.poSeacrhForm.get(to).setValue(this.selVendorId);
    }
  }

  clear(to) {
    this.poSeacrhForm.get(to).setValue(null);
  }

  exportPurchase() {
    let _this = this;
    _this.CS.purchaseSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'purchase/' + response.filename, '_blank');
        this.toastr.success('Successfuly Exported Excel!', 'Success!');
      }
      else {
        this.toastr.error(response.msg, 'Failed!');
      }
    });
  }

  clearSearch(type) {

    if (type =="reset") {
      this.isAdvanceSearch = false;
      this.customerName = null;
      this.vendorName = null;
      this.searchToggle = true;
    }
    else  {
      this.isAdvanceSearch = false;
      this.customerName = null;
      this.vendorName = null;
      this.searchToggle = false
     // this.firstLoad = false
    }
    this.searchFilterObject = null;
    this.sourceTypeVal = this.sourceType[0]['id'];
    this.currentPOType = this.sourceType[0]['id'];
    this.createSearchForm();
    this.getPOList();
  }

  clearMainSearch(type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.clearSearch(type);
      this.searchToggle = true
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }
  toDateObject(date) {
    return this.CF.jsonDatetoDateObject(date)
  }
  resetPagination() {
    this.offset = 0;
    this.currentPage = 1;
  }
  resetCustomerValue(){
    this.poSeacrhForm.get('customer').setValue(null);
  }


      onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container') || e.path[5].className.includes('ng-select ng-select') || e.path[6].className.includes('dropdown-menu show')){
        console.log('wow--->closeee----')
      }
      else {
    this.firstLoad = false
    this.searchToggle = false
  }
  }
}