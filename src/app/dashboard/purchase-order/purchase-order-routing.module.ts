import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PoAddComponent } from './po-add/po-add.component';
import { PoEditComponent } from './po-edit/po-edit.component';
import { PoListComponent } from './po-list/po-list.component';
import { PageGuard } from '../../guards/page-guard.service';

const routes: Routes = [
  {
    path: 'list',
    canActivate: [PageGuard],
    component: PoListComponent,
    data:{permissions: 'po_view' }
  },
  {
    path: 'add',
    canActivate: [PageGuard],
    component: PoAddComponent,
    data:{permissions: 'po_add' }
  },
  {
    path: 'add/qt/:qtid',
    component: PoAddComponent,
    data:{permissions: 'po_add' }
  },
   {
    path: 'add/:nid',
    component: PoAddComponent,
    data:{permissions: 'po_add' }
  },
  {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: PoEditComponent,
    data:{permissions: 'po_edit' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseOrderRoutingModule { }
