import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { globalConstants } from '../../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-invoice-instruction-list',
  templateUrl: './invoice-instruction-list.component.html',
  styleUrls: ['./invoice-instruction-list.component.scss']
})
export class InvoiceInstructionListComponent implements OnInit {

instructionList=[];
  currentPage = 1
  totalItems = 0
  offset = 0
  UPLOAD_PATH = globalConstants.UPLOADS_DIR
  

  constructor( private http: HttpClient,private spinner: NgxSpinnerService,private CF: CommonFunctionsService ,private CS: CommonservicesService,private toastr: ToastrService) {  }
  permissions = this.CF.findPermissions()
  itemsPerPage = this.CS.getItemPerPage();
  ngOnInit() {
    this.getInstruction();
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);


    
  }

getInstruction() {
  let _this = this;
  let params = {limit: this.itemsPerPage, offset: this.offset };
    _this.CS.countInstructionData(params)
    .subscribe((response) => { 
      console.log("instructions",response.result)
      if(response.status && response.status == 'success'){
        _this.instructionList = response.result && response.result.rows ? response.result.rows : [];
         this.totalItems = response.result && response.result.count ? response.result.count : 0;
      }else{
        this.toastr.error('Error Occured While Saving', 'Failed!');
      }
    });
  }
pageChanged(event) {
    console.log("event", event)
    this.currentPage = event
    var setoffset = event - 1
    setoffset = setoffset * this.itemsPerPage
    this.offset = setoffset
    this.getInstruction()
}

changeInstructionStatus(id, status) {

    let _this = this;
    let params = { id, status };
    //if (confirm("Are you sure to change status ? ")) {

      this.CS.setInstructionStatus(params).subscribe(response => {
        console.log("response", response);
        if (response && response.status == 'success') {
          this.getInstruction();
          this.toastr.success('Successfuly updated', 'Success!');
        } else {
          this.toastr.error('Error Occured While Saving', 'Failed!');
        }
      });
    //}
  }

  exportInvoiceInstruction() {
  let _this = this;
  _this.CS.invoiceInstructionSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'instruction/' + response.filename, '_blank');
        this.toastr.success('Successfuly Exported Excel', 'Success!');
      }
      else {
        this.toastr.error(response.msg, 'Failed!');

      }
    });
}
}
