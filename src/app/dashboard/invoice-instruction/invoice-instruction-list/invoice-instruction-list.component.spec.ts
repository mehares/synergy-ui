import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceInstructionListComponent } from './invoice-instruction-list.component';

describe('InvoiceInstructionListComponent', () => {
  let component: InvoiceInstructionListComponent;
  let fixture: ComponentFixture<InvoiceInstructionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceInstructionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceInstructionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
