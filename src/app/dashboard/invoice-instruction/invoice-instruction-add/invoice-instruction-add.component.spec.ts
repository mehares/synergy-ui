import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceInstructionAddComponent } from './invoice-instruction-add.component';

describe('InvoiceInstructionAddComponent', () => {
  let component: InvoiceInstructionAddComponent;
  let fixture: ComponentFixture<InvoiceInstructionAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceInstructionAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceInstructionAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
