import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { group } from '@angular/animations';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-invoice-instruction-add',
  templateUrl: './invoice-instruction-add.component.html',
  styleUrls: ['./invoice-instruction-add.component.scss']
})
export class InvoiceInstructionAddComponent implements OnInit {

  instructionForm: FormGroup;
  submitted = false;
  loading = false;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private CS: CommonservicesService,private toastr: ToastrService,private spinner: NgxSpinnerService,) {
  }

  ngOnInit() {
    this.createInstructionForm();
  }

  createInstructionForm() {
    this.instructionForm = this.formBuilder.group({
      content: ['', Validators.required],
      status:1
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.instructionForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    console.log("instructionForm", this.instructionForm);
    if (this.instructionForm.invalid) {
       this.toastr.error('Please Fill The Details', 'Error!');
      return;
    } else {
      let _this = this;
      _this.loading = true;
      let params = this.instructionForm.value;
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
      _this.CS.setInstructionData(params).subscribe(response => {
        _this.loading = false;
        if (response && response.status == "success") {
          this.toastr.success('Instructions Added Successfully', 'Success!');
          _this.router.navigate(['/invoiceInstruction/edit', response.result.id]);
        } else {
          let errorMsg = 'Failed to Add Instruction.!';
          if (response.messageTxt) {
            errorMsg = response.messageTxt;
          }
          this.toastr.error(errorMsg, 'Failed!');
        }
      });
    }
  }


omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}

}