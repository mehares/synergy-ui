import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceInstructionListComponent } from './invoice-instruction-list/invoice-instruction-list.component';
import { InvoiceInstructionAddComponent } from './invoice-instruction-add/invoice-instruction-add.component';
import { InvoiceInstructionEditComponent } from './invoice-instruction-edit/invoice-instruction-edit.component';
import { PageGuard } from '../../guards/page-guard.service';
const routes: Routes = [
  {
    path: 'list',
    canActivate: [PageGuard],
    component: InvoiceInstructionListComponent,
    data:{permissions: 'invoiceInstruction_view' }
  },
  {
    path: 'add',
    canActivate: [PageGuard],
    component: InvoiceInstructionAddComponent,
    data:{permissions: 'invoiceInstruction_add' }
  },
  {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: InvoiceInstructionEditComponent,
    data:{permissions: 'invoiceInstruction_edit' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceInstructionRoutingModule { }
