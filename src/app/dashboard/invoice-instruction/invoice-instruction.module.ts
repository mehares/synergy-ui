import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceInstructionRoutingModule } from './invoice-instruction-routing.module';
import { InvoiceInstructionListComponent } from './invoice-instruction-list/invoice-instruction-list.component';
import { InvoiceInstructionAddComponent } from './invoice-instruction-add/invoice-instruction-add.component';
import { InvoiceInstructionEditComponent } from './invoice-instruction-edit/invoice-instruction-edit.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from "ngx-spinner";
@NgModule({
  declarations: [InvoiceInstructionListComponent, InvoiceInstructionAddComponent, InvoiceInstructionEditComponent],
  imports: [
    CommonModule,
    InvoiceInstructionRoutingModule,
    AngularFontAwesomeModule,
    FormsModule, 
    ReactiveFormsModule,
    NgbModule,
    NgxSpinnerModule

  ],
  
})
export class InvoiceInstructionModule { }
