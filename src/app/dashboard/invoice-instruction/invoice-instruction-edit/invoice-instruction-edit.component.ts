import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import {globalConstants} from '../../../constants/global-constants';
import {v1 as uuidv1} from 'uuid';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-invoice-instruction-edit',
  templateUrl: './invoice-instruction-edit.component.html',
  styleUrls: ['./invoice-instruction-edit.component.scss']
})
export class InvoiceInstructionEditComponent implements OnInit {

 instructionForm: FormGroup;
  submitted = false;
  id = null;
  instructionList = null;
  loading = false;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private activatedroute: ActivatedRoute,
    private CS: CommonservicesService,
    private el: ElementRef,private toastr: ToastrService,private spinner: NgxSpinnerService,) {

  }

  ngOnInit() {
    this.id = this.activatedroute.snapshot.params.id;
    console.log('this.activatedroute.snapshot.params',this.activatedroute.snapshot.params);
    this.createInstructionForm();
    this.getInstructionData();
  }

  


   getInstructionData() {
    let _this = this;
    let params = { model_name: 'invoiceInstruction', where: { 'id': this.id }, id: this.id };
    _this.CS.getInstructionData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.instructionList = response.result;
        
        _this.setInstructionForm();
        console.log("_this.instructionList", _this.instructionList);
      } else {
        this.toastr.error('Something Went Wrong', 'Failed!');
      }
    });
  }
  createInstructionForm() {
    this.instructionForm = this.formBuilder.group({
      content: ['', Validators.required],
      status:1
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.instructionForm.controls;
  }


    onSubmit() {
    let _this = this;
    this.submitted = true;
  
     if (this.instructionForm.invalid) {
      this.toastr.error('Please Fill The Details', 'Error!');
      return;
    }
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    const instructionObj = {
      content: this.form.content.value,
      status:this.form.status.value,
      id : this.id,
    };

     _this.CS.updateInstructionData(instructionObj)
      .subscribe((response:any) => {
        console.log("response",response);
        if(response && response.status == "success"){
          this.toastr.success('Instructions Updated Successfully', 'Success!');
        }else{
          let errorMsg = 'Failed to Add Instruction.!';
          if (response.messageTxt) {
            errorMsg = response.messageTxt;
          }
          this.toastr.error(errorMsg, 'Failed!');

        }
       
      });
  }
  

  setInstructionForm() {
    this.instructionForm = this.formBuilder.group({
      content: [this.instructionList.content, Validators.required],
      status:[this.instructionList.status],
    });
  }


  omit_special_char(event)
{   
   var k;  
   k = event.charCode; 
   return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
}
}
