import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceInstructionEditComponent } from './invoice-instruction-edit.component';

describe('InvoiceInstructionEditComponent', () => {
  let component: InvoiceInstructionEditComponent;
  let fixture: ComponentFixture<InvoiceInstructionEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceInstructionEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceInstructionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
