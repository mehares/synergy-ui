import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { group } from '@angular/animations';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-uom-add',
  templateUrl: './uom-add.component.html',
  styleUrls: ['./uom-add.component.scss']
})
export class UomAddComponent implements OnInit {

  uomForm: FormGroup;
  submitted = false;
  loading = false;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private CS: CommonservicesService,
   private http: HttpClient,private toastr: ToastrService,private spinner: NgxSpinnerService,) {
  }

  ngOnInit() {
    this.createUomForm();
  }

  createUomForm() {
    this.uomForm = this.formBuilder.group({
      uomName: ['', Validators.required],
      uomDescripton: ['', [Validators.required]],
      status:1
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.uomForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    console.log("uomForm", this.uomForm);
    if (this.uomForm.invalid) {
    	this.toastr.error('Please Fill The Details', 'Error!');
      return;
    } else {
      let _this = this;
      _this.loading = true;
       _this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
      let params = this.uomForm.value;
      _this.CS.setUomData(params).subscribe(response => {
        _this.loading = false;
        if (response && response.status == "success") {
        this.toastr.success('Uom Details Saved', 'Success!');
          _this.router.navigate(['/uom/edit', response.result.id]);
        } else {
          let errorMsg = 'Failed to Add Uom.!';
           if (response.messageTxt) {
            errorMsg = response.messageTxt;
          }
          this.toastr.error(errorMsg, 'Failed!');
        }
      });
    }
  }




}

