import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonFunctionsService } from '../../../helper/commonFunctions/common-functions.service';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { globalConstants } from '../../../constants/global-constants';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-uom-list',
  templateUrl: './uom-list.component.html',
  styleUrls: ['./uom-list.component.scss']
})
export class UomListComponent implements OnInit {

  uomList=[];
  currentPage = 1
  totalItems = 0
  offset = 0
  UPLOAD_PATH = globalConstants.UPLOADS_DIR
  userQuestionUpdate = new Subject<string>();
  search = '';
  searchToggle = false;
  searchLoader = false;
  isValidFormSubmitted = null;
  submitted = false;
  searchBox = false;
  uomSearchForm:FormGroup;
  pageOffset = 0;
  serachType = 'all';
  firstLoad = false;
  isSearchRequest=false;

  
  constructor( private http: HttpClient,private CF: CommonFunctionsService , private CS: CommonservicesService , private formBuilder: FormBuilder,private toastr: ToastrService,private spinner: NgxSpinnerService,) {  
    this.userQuestionUpdate.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.resetPagination();
        this.onSubmit();
        if (this.search !='') {
          this.searchBox = true
            }
        else {
          this.searchBox = false
          }
      });
  }
  permissions = this.CF.findPermissions();
   itemsPerPage = this.CS.getItemPerPage();
  ngOnInit() {
    this.getUom();
    this.firstLoad = true;
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);

     this.uomSearchForm = this.formBuilder.group({
      search: ['', []],
      uomName: ['', []],
      uomDescripton: ['', []],
    });

    
  }

getUom() {
  this.pageOffset = this.offset;
  this.isSearchRequest=false;
    this.CS.countUomData({limit: this.itemsPerPage, offset: this.offset })
    .subscribe(response => { 
      console.log("uoms",response.result)
      if(response.status && response.status == 'success'){
        this.uomList = response.result && response.result.rows ? response.result.rows : [];
         this.totalItems = response.result && response.result.count ? response.result.count : 0;
      }else{
      	this.toastr.error('Error Occured While Saving', 'Failed!');
      }
    });
  }
pageChanged(event) {
    console.log("event", event)
     if(this.submitted == true) {
    this.itemsPerPage = this.CS.getItemPerPage();
     this.currentPage = event
    var setoffset = event - 1
    setoffset = setoffset * this.itemsPerPage
    this.offset = setoffset
    this.uomList
  }
  else {
    this.itemsPerPage = this.CS.getItemPerPage();
     this.currentPage = event
    var setoffset = event - 1
    setoffset = setoffset * this.itemsPerPage
    this.offset = setoffset
    this.getUom()
  }
}


exportUom() {
  let _this = this;
  _this.CS.uomSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'uom/' + response.filename, '_blank');
        this.toastr.success('Successfuly Exported excel', 'Success!');
      }
      else {
      	this.toastr.error(response.msg, 'Failed!');

      }
    });
}

 get f() {
    return this.uomSearchForm.controls;
  }

  onSubmit(from='') {

    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;
    if (this.uomSearchForm.invalid) {
    	this.toastr.error('Please Fill The Details', 'Error!');
      return;
    }
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;
    const searchObj = {
      search: this.search,
      uomName: this.f.uomName.value,
      uomDescripton: this.f.uomDescripton.value,
    };
    this.isSearchRequest=true;
    this.resetPagination();		
    this.CS.searchUom(searchObj).subscribe(response => {
      console.log("response", response);
      if(from != "dropdownclear" ){
        _this.searchToggle = false;
      }

      _this.searchLoader = false;
      
      if (response && response.status == 'success') {
        _this.uomList = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
        this.pageOffset = 0;
        //this.pageChanged(event)
      } else {
      	this.toastr.error('Error Occured While Search', 'Failed!');
      }
    });
  }
 clearSearch(from='',type) { 
    if(from != "dropdownclear" && type=="reset"){
      this.searchToggle =true;  
    }
    else if (from != "dropdownclear" && type == '') {
      this.searchToggle =false;  
    }  
    this.searchBox = false;
     this.resetPagination();
    this.uomSearchForm = this.formBuilder.group({
      search: ['', []],
      uomName: ['', []],
      uomDescripton: ['', []],
    });
    this.serachType = 'all';
    this.getUom();
  }

  clearMainSearch(from='',type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.clearSearch(from='',type);
      this.searchToggle = true
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }
   resetPagination(){    
    this.offset=0;
    this.currentPage=1;
  }

    onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container')){
        console.log('wow--->closeee----')
      }
      else {
    this.searchToggle = false
  }
  }

}