import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import {globalConstants} from '../../../constants/global-constants';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as _ from 'lodash';
import {v1 as uuidv1} from 'uuid';
import { CommonservicesService } from '../../../helper/commonservices/commonservices.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-uom-edit',
  templateUrl: './uom-edit.component.html',
  styleUrls: ['./uom-edit.component.scss']
})
export class UomEditComponent implements OnInit {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })  
  };
  uomForm: FormGroup;
  submitted = false;
  id = null;
  uomList = null;
  userData = null;
  loading = false;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private activatedroute: ActivatedRoute,
    private http: HttpClient,
    private CS: CommonservicesService,
    private el: ElementRef,private toastr: ToastrService,private spinner: NgxSpinnerService,) {

  }

  ngOnInit() {
    this.id = this.activatedroute.snapshot.params.id;
    console.log('this.activatedroute.snapshot.params',this.activatedroute.snapshot.params);
    this.createUomForm();
    this.getUomData();
  }

  


   getUomData() {
    let _this = this;
    let params = { model_name: 'uom', where: { 'id': this.id }, id: this.id };
    _this.CS.getUomData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.uomList = response.result;
        
        _this.setForm();
        console.log("_this.uomList", _this.uomList);
      } else {
      	this.toastr.error('Error Occured While Saving', 'Failed!');
      }
    });
  }
  createUomForm() {
    this.uomForm = this.formBuilder.group({
      uomName: ['', Validators.required],
      uomDescripton: ['', [Validators.required]],
      status:1
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.uomForm.controls;
  }


    onSubmit() {
    let _this = this;
    this.submitted = true;
  
     if (this.uomForm.invalid) {
     	this.toastr.error('Please Fill The Details', 'Error!');
      return;
    }
    
    const uomObj = {
      uomName: this.form.uomName.value,
      uomDescripton: this.form.uomDescripton.value,
      status:1,
      id : this.id,
    };
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
     this.CS.uomUpdate(uomObj).subscribe(response => {
        console.log("response",response);
        if(response && response.status == "success"){
        	this.toastr.success('Uom Details Updated ', 'Success!');
        }else{
          let errorMsg = 'Failed to Add Uom.!';
           if (response.messageTxt) {
            errorMsg = response.messageTxt;
          }
          this.toastr.error(errorMsg, 'Failed!');

        }
       
      });
  }





  

  setForm() {
    this.uomForm = this.formBuilder.group({
      uomName: [this.uomList.uomName, Validators.required],
       uomDescripton: [this.uomList.uomDescripton, Validators.required],
      status:1,
    });
  }





  
}
