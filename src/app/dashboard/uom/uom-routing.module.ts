import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UomListComponent } from './uom-list/uom-list.component';
import { UomAddComponent } from './uom-add/uom-add.component';
import { UomEditComponent } from './uom-edit/uom-edit.component';
import { PageGuard } from '../../guards/page-guard.service';

const routes: Routes = [
 {
    path: 'list',
    canActivate: [PageGuard],
    component: UomListComponent,
    data:{permissions: 'uom_view' }
  },
  {
    path: 'add',
    canActivate: [PageGuard],
    component: UomAddComponent,
    data:{permissions: 'uom_add' }
  },
  {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: UomEditComponent,
    data:{permissions: 'uom_edit' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UomRoutingModule { }
