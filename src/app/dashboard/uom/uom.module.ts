import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UomRoutingModule } from './uom-routing.module';
import { UomListComponent } from './uom-list/uom-list.component';
import { UomAddComponent } from './uom-add/uom-add.component';
import { UomEditComponent } from './uom-edit/uom-edit.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { ClickOutsideModule } from 'ng-click-outside';
@NgModule({
  declarations: [UomListComponent, UomAddComponent, UomEditComponent],
  imports: [
    CommonModule,
    UomRoutingModule,
    AngularFontAwesomeModule,
    FormsModule, 
    ReactiveFormsModule,
    NgbModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    ClickOutsideModule
  ],
  
})
export class UomModule { }
