import { Component, OnInit, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {NgModule} from '@angular/core';
import {FormGroup, FormArray, FormBuilder, FormControl, Validators} from '@angular/forms';
import {CommonservicesService} from '../../helper/commonservices/commonservices.service';
import {CommonFunctionsService} from '../../helper/commonFunctions/common-functions.service';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import * as CryptoJS from 'crypto-js';
import {globalConstants} from '../../constants/global-constants';

@Component({
  selector: 'app-user-rights',
  templateUrl: './user-rights.component.html',
  styleUrls: ['./user-rights.component.scss']
})
export class UserRightsComponent implements OnInit {

 message_content = false;
  permissionForm: FormGroup;
  userRightsId = null;

  constructor(private router: Router,
              private formBuilder: FormBuilder,private CF: CommonFunctionsService,
              private CS: CommonservicesService, private activatedroute: ActivatedRoute,private toastr: ToastrService,private spinner: NgxSpinnerService,) {
  }

  id = null;
  userData = null;
  loading = false;
  permissions = this.CF.findPermissions()

  ngOnInit() {
    this.id = this.activatedroute.snapshot.params.id;
    this.getUserData();
    this.createForm();
  }

  createForm() {
    this.permissionForm = this.formBuilder.group({
      vessels_add: '',
      vessels_edit: '',
      vessels_view: '',
      vessels_delete: '',

      vendors_add: '',
      vendors_edit: '',
      vendors_view: '',
      vendors_delete: '',

      customers_add: '',
      customers_edit: '',
      customers_view: '',
      customers_delete: '',

      companies_add: '',
      companies_edit: '',
      companies_view: '',
      companies_delete: '',


      user_rights_add: '',
      user_rights_edit: '',
      user_rights_view: '',
      user_rights_delete: '',


      users_add: '',
      users_edit: '',
      users_view: '',
      users_delete: '',

      invoice_view: '',
      invoice_add: '',
      invoice_edit:'',
      invoice_delete:'',      

      currency_add: '',
      currency_edit: '',
      currency_view: '',
      currency_delete: '',

      itemGroups_add: '',
      itemGroups_edit: '',
      itemGroups_view: '',
      itemGroups_delete: '',

      ports_add: '',
      ports_edit: '',
      ports_view: '',
      ports_delete: '',

      uom_add: '',
      uom_edit: '',
      uom_view: '',
      uom_delete: '',

      po_add: '',
      po_edit: '',
      po_view: '',
      po_delete: '',

      invoiceInstruction_add: '',
      invoiceInstruction_edit:'',
      invoiceInstruction_view:'',
      invoiceInstruction_delete:'',

      enquiry_add: '',
      enquiry_edit: '',
      enquiry_view: '',
      enquiry_cancel: '',
      enquriy_delete: '',

      quotation_add: '',
      quotation_edit: '',
      quotation_view: '',
      quotation_delete: '',


    });
  }

  selectionClick(event, type) {
    const input = (event.target as HTMLInputElement);
    var setVlaue = input.checked ? true : false;
    this.permissionForm.get(type + "_add").setValue(setVlaue);
    this.permissionForm.get(type + "_edit").setValue(setVlaue);
    this.permissionForm.get(type + "_view").setValue(setVlaue);
    if(type =='enquiry') {
    this.permissionForm.get(type + "_cancel").setValue(setVlaue);
    }
    else {
      this.permissionForm.get(type + "_delete").setValue(setVlaue);
    }
  

  }


  getUserData() {
    let _this = this;
    let params = {model_name: 'users', status:1, where: {'id': this.id}, id: this.id};
    _this.CS.getUserData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.userData = response.result;
        _this.userRightsId = response.result && response.result.UR && response.result.UR.length && response.result.UR[0] ? response.result.UR[0].id : null;
        var permissions = response.result.UR && response.result.UR.length ? JSON.parse(response.result.UR[0].permissions) : null;
        permissions ? _this.setPermissions(permissions) : false;
        console.log("_this.userData", _this.userData);
      } else {
      	this.toastr.error('Error occured ', 'Failed!');
      }
    });
  }


  setPermissions(permissions) {
    let _this = this;
    _.map(permissions, function (value, key) {
      _this.permissionForm.get(key).setValue(value);
    });
  }

  onSubmit() {
    this.loading = true;
    console.log("permissionForm", this.permissionForm);
    // stop here if form is invalid

    let _this = this;
    var data = {
      userId: this.userData.id,
            status: 1,
      permissions: JSON.stringify(this.permissionForm.value),
      id: this.id
    };
    var params = {data: data, model_name: 'user_rights', id: this.id};
     this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    _this.CS.setData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success") {
        console.log("success");
        console.log(this.id);
        console.log(this.userRightsId)
        var proId = localStorage.getItem('sender_pro_id')
        if (proId == this.id){
          _this.CS.getUserData({model_name: 'users', status:1, where: {'id': this.id}, id: this.id}).subscribe(response => {
            console.log("resposneUsercheck", response)
          let permissions = null;
          permissions = response.result.UR && response.result.UR.length ? response.result.UR[0].permissions : '';
          var permissions_enc = CryptoJS.AES.encrypt(JSON.stringify(permissions), globalConstants.ENC_KEY).toString();
          localStorage.setItem('permissions', permissions_enc);
           window.location.reload();
        })
        }
        this.toastr.success('User rights updated successfully', 'Success!');
      } else {
      	this.toastr.error('Error occured while saving', 'Failed!');
      }
      _this.loading = false;
    });

  }
}
