import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserRightsComponent } from './user-rights.component';
import { PageGuard } from '../../guards/page-guard.service';


const routes: Routes = [
{
    path: ':id',
    canActivate: [PageGuard],
    component: UserRightsComponent,
    data:{permissions: 'user_rights_edit' }
  },];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRightsRoutingModule { }
