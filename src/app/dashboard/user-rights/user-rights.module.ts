import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRightsRoutingModule } from './user-rights-routing.module';
import { UserRightsComponent } from './user-rights.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [UserRightsComponent],
  imports: [
    CommonModule,
    UserRightsRoutingModule,
    FormsModule, ReactiveFormsModule,
    NgxSpinnerModule
  ],
  
})
export class UserRightsModule { }
