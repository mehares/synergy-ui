import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {CurrencyListComponent } from './currency-list/currency-list.component';
import { CurrencyAddComponent } from './currency-add/currency-add.component';
import { CurrencyEditComponent } from './currency-edit/currency-edit.component';
import { PageGuard } from '../../guards/page-guard.service';
const routes: Routes = [
  {
    path: 'list',
    canActivate: [PageGuard],
    component: CurrencyListComponent,
    data:{permissions: 'currency_view' }
  },
    {
    path: 'add',
    canActivate: [PageGuard],
    component: CurrencyAddComponent,
    data:{permissions: 'currency_add' }
  },
    {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: CurrencyEditComponent,
    data:{permissions: 'currency_edit' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CurrencyRoutingModule { }
