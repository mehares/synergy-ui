import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import {CommonservicesService} from '../../../helper/commonservices/commonservices.service';
import {CommonFunctionsService} from '../../../helper/commonFunctions/common-functions.service';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
 
import { Router ,ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import {globalConstants} from '../../../constants/global-constants';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-currency-edit',
  templateUrl: './currency-edit.component.html',
  styleUrls: ['./currency-edit.component.scss']
})
export class CurrencyEditComponent implements OnInit {

 currencyForm: FormGroup;
  submitted = false;
  id = null;
  currencyList = null;
  // position: NbGlobalPosition = NbGlobalPhysicalPosition.BOTTOM_RIGHT;
  loading = false;

  
  constructor(private http: HttpClient,
    private router: Router,
    private formBuilder: FormBuilder,private spinner: NgxSpinnerService,
    private CS: CommonservicesService, private CF: CommonFunctionsService, private calendar: NgbCalendar,private activatedroute: ActivatedRoute,private toastr: ToastrService ) { }
  
  ngOnInit() {
    this.id = this.activatedroute.snapshot.params.id;
    console.log('this.activatedroute.snapshot.params',this.activatedroute.snapshot.params);
    this.createCurrencyForm();
    this.getCurrencyData();
  }

  


   getCurrencyData() {
    let _this = this;
    let params = { model_name: 'currency', where: { 'id': this.id }, id: this.id };
    _this.CS.getCurrencyData(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == "success" && response.result) {
        _this.currencyList = response.result;
        
        _this.setCurrencyForm();
      } else {
         this.toastr.error('Failed to Fetch Currency Details',"Failed");
      }
    });
  }
  createCurrencyForm() {
    this.currencyForm = this.formBuilder.group({
      name: ['', Validators.required],
      shortKey:['',Validators.required],
      symbol:['',Validators.required],
      Rate:['',Validators.required],
      status:1


    });

  }



  // convenience getter for easy access to form fields
  get form() {
    return this.currencyForm.controls;
  }


    onSubmit() {
    let _this = this;
    this.submitted = true;
  
      if (this.currencyForm.invalid) {
       this.toastr.error('Please validate fields',"Validation Errors");
       return;
    }
     this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    const groupObj = {
      name: this.form.name.value,
      shortKey:this.form.shortKey.value,
      symbol:this.form.symbol.value,
      Rate:this.form.Rate.value,
      status:1,
      id : this.id,
    };

     _this.CS.updateCurrencyData(groupObj)
      .subscribe((response:any) => {
        console.log("response",response);
        if (response && response.status == true) {
          this.toastr.success('Currency Updated',"Success");
        }else{
          let errorMsg = 'Failed to Add Currency.!';
          if (response.messageTxt) {
            errorMsg = response.messageTxt;
          }
          this.toastr.error( errorMsg ,"Error");
        }
       
      });
  }

setCurrencyForm() {
    this.currencyForm = this.formBuilder.group({
      name: [this.currencyList.name, Validators.required],
      shortKey: [this.currencyList.shortKey, Validators.required],
      symbol: [this.currencyList.symbol, Validators.required],
      Rate: [this.currencyList.ER[0].Rate, Validators.required],
      status:1,
    });
}





  
}