import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ToastrModule } from 'ngx-toastr';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {CurrencyRoutingModule} from './currency-routing.module';

import { CurrencyListComponent } from './currency-list/currency-list.component';
import { CurrencyAddComponent } from './currency-add/currency-add.component';
import { CurrencyEditComponent } from './currency-edit/currency-edit.component';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [CurrencyListComponent, CurrencyAddComponent, CurrencyEditComponent],
  imports: [
    CommonModule,
    CurrencyRoutingModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    AngularFontAwesomeModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
  ],
  
})
export class CurrencyModule { }
