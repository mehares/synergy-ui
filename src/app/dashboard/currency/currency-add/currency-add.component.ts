import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import {CommonservicesService} from '../../../helper/commonservices/commonservices.service';
import {CommonFunctionsService} from '../../../helper/commonFunctions/common-functions.service';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
 
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import {globalConstants} from '../../../constants/global-constants';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-currency-add',
  templateUrl: './currency-add.component.html',
  styleUrls: ['./currency-add.component.scss']
})
export class CurrencyAddComponent implements OnInit {

  currencyForm: FormGroup;
  submitted = false;
  // position: NbGlobalPosition = NbGlobalPhysicalPosition.BOTTOM_RIGHT;
  loading = false;
  selectedExchangeRate = [];

     constructor(private http: HttpClient,
    private router: Router,
    private formBuilder: FormBuilder,
    private CS: CommonservicesService, private CF: CommonFunctionsService ,private spinner: NgxSpinnerService, private calendar: NgbCalendar ,private toastr: ToastrService) { }
  
  
  ngOnInit() {
    this.createCurrencyForm();
  }

  createCurrencyForm() {
    this.currencyForm = this.formBuilder.group({
      name: ['', Validators.required],
      shortKey: ['', Validators.required],
       symbol: ['', Validators.required],
       Rate: ['', [Validators.required,Validators.maxLength(13)]],
      status:1
    });
  }

  // convenience getter for easy access to form fields
  get form() {
    return this.currencyForm.controls;
  }

  fieldUpdate(value, type) {
    let updatedRes = [];
    if (value) {
      value.forEach(element => {
        let groupElement = {
          [type]: element,
        }
        updatedRes.push(groupElement);
      });
      return updatedRes;
    } else {
      return updatedRes;
    }
  }

  onSubmit() {
    this.submitted = true;

    console.log("currencyForm", this.currencyForm);
    if (this.currencyForm.invalid) {
      return;
    } else {
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
      let _this = this;
      _this.loading = true;
      let params = this.currencyForm.value;
      _this.CS.setCurrencyData(params).subscribe(response => {
        _this.loading = false;
        console.log("********",response);
        if (response && response.status == "success") {
          this.toastr.success('Currency Added',"Success");
          _this.router.navigate(['/currency/edit', response.result.id]);
        }  else {
          let errorMsg = 'Failed to Add Currency.!';
          if (response.errObject) {
            errorMsg = response.errObject;
          }
          this.toastr.error(errorMsg,"Success");
        }
      });
    }
  }

  



}
