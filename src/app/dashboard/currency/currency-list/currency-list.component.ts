import {
	Component,
	Input,
	Output,
	EventEmitter,
	OnChanges,
	SimpleChanges,
	ChangeDetectorRef,
	ChangeDetectionStrategy,
	OnInit
} from '@angular/core';
import {
	Observable,
	Subject
} from 'rxjs';
import {
	debounceTime,
	distinctUntilChanged,
	map
} from 'rxjs/operators';
import {
	HttpClient
} from '@angular/common/http';
import {
	CommonservicesService
} from '../../../helper/commonservices/commonservices.service';
import {
	CommonFunctionsService
} from '../../../helper/commonFunctions/common-functions.service';
import {
	ToastrService
} from 'ngx-toastr';

import {
	FormBuilder,
	FormGroup,
	Validators
} from '@angular/forms';
import {
	Router
} from '@angular/router';
import {
	globalConstants
} from '../../../constants/global-constants';
import * as _ from 'lodash';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
	selector: 'app-currency-list',
	templateUrl: './currency-list.component.html',
	styleUrls: ['./currency-list.component.scss']
})
export class CurrencyListComponent implements OnInit {

	currencyList = [];

	currentPage = 1
	totalItems = 0
	offset = 0
	public display: any = [];
	public innerHeight: any;
	UPLOAD_PATH = globalConstants.UPLOADS_DIR


	constructor(private http: HttpClient,
		private router: Router,
		private formBuilder: FormBuilder,
		private CS: CommonservicesService,private spinner: NgxSpinnerService, private CF: CommonFunctionsService, private toastr: ToastrService) {}

	permissions = this.CF.findPermissions()
	itemsPerPage = this.CS.getItemPerPage();
	ngOnInit() {

		this.getCurrency();
		this.spinner.show();
        setTimeout(() => this.spinner.hide(), 2000);
		this.display = this.currencyList.sort((a, b) => b - a);

	}

	getCurrency() {
		let _this = this;
		let params = {
			limit: this.itemsPerPage,
			offset: this.offset
		};
		_this.CS.countCurrencyData(params)
			.subscribe((response) => {
				console.log("currency", response.result)
				if (response.status && response.status == 'success') {
					this.currencyList = response.result && response.result.rows ? response.result.rows : [];
					this.totalItems = response.result && response.result.count ? response.result.count : 0;
				} else {
					this.toastr.error('Port List Not available', 'Failed!');
				}
			});
	}
	pageChanged(event) {
		console.log("event", event)
		this.currentPage = event
		var setoffset = event - 1
		setoffset = setoffset * this.itemsPerPage
		this.offset = setoffset
		this.getCurrency()
	}

	exportCurrency() {
		let _this = this;
		_this.CS.currencySetExcel({}).subscribe(response => {
			if (response && response.status == "success") {
				console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'currency/' + response.filename, '_blank');
        this.toastr.success('Successfuly exported excel!', 'Success!');
      } else {
        this.toastr.error(response.msg, 'Failed!');
			}
		});
	}
}