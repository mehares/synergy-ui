import { Component, OnInit, ElementRef, ViewChild  } from '@angular/core';
import { CommonFunctionsService } from '../../helper/commonFunctions/common-functions.service';
import { CommonservicesService } from '../../helper/commonservices/commonservices.service';
import { globalConstants } from '../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import * as _ from 'lodash';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

	reportList = [{id : 1, name : 'yearly'}, {id :2 , name : 'quarterly'}]

  reportForm : FormGroup
  public barChartOptions: ChartOptions = {responsive: true,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
              max : 100,
          },
          scaleLabel: {
            display: true,
            labelString: 'Profit %',

         }
        }
      ],
      xAxes: [
         {
         scaleLabel: {
            display: true,
            labelString: 'Year'
         }
        }
      ],

    }
  }
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [];

  customer = {}
  vendor = {}
  total = []

  hideTo = false
  arrYear = []


  @ViewChild('screen') screen: ElementRef;


  constructor(private CS: CommonservicesService, private toastr: ToastrService, private spinner: NgxSpinnerService, private formBuilder: FormBuilder) { }

  ngOnInit() {

  	this.createReporForm()

    var yearStart = 2000;
    var yearEnd = new Date().getFullYear() + 10;


    while(yearStart < yearEnd+1){
    this.arrYear.push({name: yearStart++});
    }
  }

  	createReporForm(){
  		this.reportForm = this.formBuilder.group({
  			fromDate : [null],
  			toDate : [null],
  			reportType : [null]
  		})
  	}


  	onSubmit() {
  			let _this = this
  			console.log("--->Form", _this.reportForm.value)
  			_this.CS.generateReport(_this.reportForm.value).subscribe(response => {

  				console.log("---->response", response)

  				if(response && response.status == "success"){
  					this.total = response.result
            console.log(this.total)
            var totaldata = []
            _.forEach(_this.total, function(val){

              totaldata.push(val.total)
              _this.barChartLabels.push(val.year + '')


            })

            _this.barChartData.push({data:totaldata,label:'Profit/Loss'})
  					

  				}

  				else {

  				}
  			})
  		}


   generarPDF() {

    const div = document.getElementById('screen');
    const options = {
      background: 'white',
      scale: 3
    };

    html2canvas(div, options).then((canvas) => {

      var img = canvas.toDataURL("image/PNG");
      var doc = new jsPDF('l', 'mm', 'a4');

      // Add image Canvas to PDF
      const bufferX = 5;
      const bufferY = 5;
      const imgProps = (<any>doc).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, bufferX, bufferY, pdfWidth, pdfHeight, undefined,);

      return doc;
    }).then((doc) => {
      doc.save('postres.pdf');  
    });
  }


  selectedType(event){
    if (event.id ==2){
      this.hideTo = true
    }
    if(event.id ==1){
      this.hideTo = false
      this.reportForm.get('toDate').setValue(null)
    }
  }

}
