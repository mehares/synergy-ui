
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonservicesService } from '../../helper/commonservices/commonservices.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import AOS from 'aos';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	dashboard = null

  constructor(private router: Router,
    private CS: CommonservicesService,private toastr: ToastrService,private spinner: NgxSpinnerService,) {
     
  }

  ngOnInit() {
  	this.getDashboard()
    AOS.init();  

  }

  getDashboard (){
  	let _this = this
  	let params
  	_this.CS.getDashboard(params).subscribe(response => {
  		console.log("response",response)
  		if(response && response.status =='success') {
  			console.log("response",response)
  		_this.dashboard = response.result[0]
  	}

  	})
  }

}
