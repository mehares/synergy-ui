import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {VesselListComponent } from './vessel-list/vessel-list.component';
import { VesselAddComponent } from './vessel-add/vessel-add.component';
import { VesselEditComponent } from './vessel-edit/vessel-edit.component';
import { PageGuard } from '../../guards/page-guard.service';

const routes: Routes = [
  {
    path: 'list',
    canActivate: [PageGuard],
    component: VesselListComponent,
    data:{permissions: 'vessels_view' }
  },
    {
    canActivate: [PageGuard],
    path: 'add',
    component: VesselAddComponent,
    data:{permissions: 'vessels_add' }
  },
    {
    path: 'edit/:id',
    canActivate: [PageGuard],
    component: VesselEditComponent,
    data:{permissions: 'vessels_edit' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VesselRoutingModule { }
