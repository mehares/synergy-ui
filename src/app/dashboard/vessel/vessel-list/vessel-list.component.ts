import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {CommonservicesService} from '../../../helper/commonservices/commonservices.service';
import {CommonFunctionsService} from '../../../helper/commonFunctions/common-functions.service';
import { ToastrService } from 'ngx-toastr';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {globalConstants} from '../../../constants/global-constants';
import * as _ from 'lodash';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-vessel-list',
  templateUrl: './vessel-list.component.html',
  styleUrls: ['./vessel-list.component.scss']
}) 


export class VesselListComponent implements OnInit {

  page = 1;
  vesselSeacrhForm: FormGroup;
  vessel_list = [];
  message_content = "";
  vesselList = [];
  vesselTypeList = [];
  vesselSubTypeList = [];
  ownerList = [];
  portOfRegistryList = [];
  managementList = [];
  currentPage = 1;
  totalItems = 0;
  offset = 0;
  searchToggle = false;
  searchLoader = false;
  search = '';
  clearFilter = false
  isValidFormSubmitted = null;
  submitted = false;
  searchVesselCheck = new Subject<string>();
  kyPort = 'name';
  kyOwner = 'ownername';
  kyVesselType = 'Vtype';
  kyVesselSubType = 'VSubtype';
  firstLoad = false;

  serachType = 'all';
  serachObject = {};

  vesselTypeAuto = ''
  vesselSubTypeAuto = ''
  ownerName = ''
  portName = ''
  portId = ''
  vesselTypeId = ''
  vesselSubTypeId = ''
  ownerId = ''

  // position: NbGlobalPosition = NbGlobalPhysicalPosition.BOTTOM_RIGHT;

  permissions = this.CF.findPermissions()
  itemsPerPage = this.CS.getItemPerPage();
  UPLOAD_PATH = globalConstants.UPLOADS_DIR
  

constructor(private http: HttpClient,
              private router: Router,
              private formBuilder: FormBuilder,
              private CS: CommonservicesService, private CF: CommonFunctionsService,private toastr: ToastrService,private spinner: NgxSpinnerService,) {

    this.searchVesselCheck.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        console.log("valueee", value);
        this.onSubmit();
      });
      
   }

  getVesselData() {
    /*
    this.http.post<any>(`/api/vessel/count`,{limit: this.itemsPerPage, offset: this.offset })
    .subscribe((response) => { */
    let _this = this;
    let params = { limit: this.itemsPerPage, offset: this.offset };

    if (_this.serachType == "search" && _this.serachObject) {
      params['search'] = _this.serachObject
    }
    _this.CS.countVesselsData(params).subscribe(response => {
      _this.searchLoader = false;
      console.log("ALl list", response.result)
      if (response.status && response.status == 'success') {
        _this.vesselList = response.result && response.result.rows ? response.result.rows : [];
        _this.totalItems = response.result && response.result.count ? response.result.count : 0;
      } else {
         this.toastr.error('Failed to fetch Vessel List', 'Failed!');
      }
    });
  }


  ngOnInit() {
    this.getVesselData();
    this.serachForm();
    this.getVesselTyes();
    this.getownerList();
    this.getManagementList();
    this.getPortOfRegistry();
    this.firstLoad = true;
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);

  }

  serachForm() {
    this.vesselSeacrhForm = this.formBuilder.group({
      vessel: ['', []],
      vesselCode: ['', []],
      vesselType: ['', []],
      vesselSubType: ['', []],
      owner: ['', []],
      portOfRegistryXid: ['', []],
      search: ['', []],
      Vtype:['', []],
      VSubtype:['', []],
      ownername:['', []],
      name:['', []],
      portName:[''],
      ownerName:[''],
      vesselTypeAuto: [''],
      vesselSubTypeAuto:['']
    });
    this.vesselSeacrhForm.get("vesselSubType").setValue(null);
    
  }
  get f() {
    return this.vesselSeacrhForm.controls;
  }
  clearMainSearch(type) {
    if (this.search != '') {
      this.firstLoad = false;
      console.log("Clearing Data");
      this.search = '';
      this.resetSearchForm('type');
    }
    if (!this.firstLoad) {
      this.firstLoad = true;
    }
  }
  onSubmit() {
     if (this.search == "" && this.searchToggle == false) {
      return;
    }
    let _this = this;
    this.submitted = true;
    this.searchLoader = true;
    this.isValidFormSubmitted = false;
   
    
    if (this.vesselSeacrhForm.invalid) {
       this.toastr.error('Please validate fields', 'Failed!');
      return;
    }
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);

    console.log('FORM', this.f);
    this.isValidFormSubmitted = true;

    const vesselSeacrhObj = {
      vessel: this.f.vessel.value,
      vesselCode: this.f.vesselCode.value,
      vesselType: this.f.vesselType.value,
      vesselSubType: this.f.vesselSubType.value,
      owner: this.f.owner.value,
      portOfRegistryXid: this.f.portOfRegistryXid.value,
      search: this.search
    };
   
    _this.searchToggle = !_this.searchToggle;
    _this.offset = 0;
    _this.itemsPerPage = this.CS.getItemPerPage();
    _this.serachType = "search";
    _this.serachObject = {
      search: (this.search ? this.search : null),
      advance: (this.search ? null : this.vesselSeacrhForm.value)
    };
    this.getVesselData();
    
    /*this.CS.searchVessels(vesselSeacrhObj).subscribe(response => {
      console.log("response", response);
      _this.searchLoader = false;
      _this.searchToggle = false;
      _this.clearFilter = true
      if (response && response.status == 'success') {
        _this.vesselList = response.result;
      } else {
          this.toastr.error('Searching Failed', 'Failed!');
      }
    });*/

     
  }
  removeAdvSearch(type) {
    this.firstLoad = false;
    if (this.search=="") {
      this.resetSearchForm(type);
       this.serachType = 'all';
    }
  }
  

  resetSearchForm(type) {
    if (type =='reset') {
          this.searchToggle = true
    }
    else {
        this.firstLoad = false
          this.searchToggle = false
    }
    this.serachForm();
    this.serachObject = {
      search: null,
      advance: null
    };
    let _this = this;
    this.serachType = 'all';
    this.getVesselData(); //serachType
    setTimeout(function () {
      //_this.searchToggle = false;
     //  _this.firstLoad = true;
    },0);
    
  }

  getManagementList() {
    let _this = this;
    let params = { model_name: 'management', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response management", response);
      if (response && response.status == "success" && response.result) {
        _this.managementList = response.result;
      } else {
        _this.managementList = [];
      }
    });
  }

  getVesselTyes() {
    let _this = this;
    let params = { model_name: 'vesselTypes', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response vesselTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.vesselTypeList = response.result;
      } else {
        _this.vesselTypeList = [];
      }
    });
  }

  getVesselSubTypes(event, VtypeId) {
    let _this = this;
    if (VtypeId && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var VtypeId = _.find(VtypeId, { id: parseInt(getId, 10) })
      console.log("VtypeId", VtypeId);
      _this.CS.getVesselSubTypes({ id: VtypeId.id }).subscribe(response => {
        console.log("vesselSubTypes", response);
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          _this.vesselSubTypeList = response.result[0].VSST;
          console.log("_this.vesselSubTypeList", _this.vesselSubTypeList);
        } else {
          _this.vesselSubTypeList = [];
        }
      });
    }
  }

  getownerList() {
    let _this = this;
    let params = { model_name: 'owners', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response ownler", response);
      if (response && response.status == "success" && response.result) {
        _this.ownerList = response.result;
      } else {
        _this.ownerList = [];
      }
    });
  }
  getPortOfRegistry() {
    let _this = this;
    let params = { model_name: 'port', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response PortOfRegistry", response);
      if (response && response.status == "success" && response.result) {
        _this.portOfRegistryList = response.result;
      } else {
        _this.portOfRegistryList = [];
      }
    });
  }
 
  changevesselStatus(id, status) {
    //if (confirm("Are you sure to change status ? ")) {
    // var params = {data: {status : status ,id: id }, model_name: 'vessels', id: id};
    console.log("id", id);
    let _this = this;
    let params = { id, status };

    _this.CS.changeVesselStatus(params).subscribe(response => {
      console.log("response", response);
      if (response && response.status == 'success') {
        this.getVesselData();
         this.toastr.success('Status Updated', 'Success!');
      
      } else { 
       this.toastr.error('Failed to Update', 'Failed!');
      }
    });
  }
  
  pageChanged(event) {
    console.log("event", event)
    this.currentPage = event
    var setoffset = event - 1
    setoffset = setoffset * this.itemsPerPage
    this.offset = setoffset
    this.getVesselData()

  }

  exportVessel() {
    let _this = this;
    _this.CS.vesselSetExcel({}).subscribe(response => {
      if (response && response.status == "success") {
        console.log("openining", response);
        window.open(this.UPLOAD_PATH + 'vessel/' + response.filename, '_blank');
         this.toastr.info('Successfuly exported excel', 'Success!');
      }
      else {
      this.toastr.error(response.msg, 'Failed!');
      } 
    });
  }
 
  changeSelection(event, to) {
    console.log("event", event)
    console.log("to", to);

    if (to == 'port') {
      this.portName = event.name ? event.name : this.portName;
      this.portId = event.id ? event.id : this.portId;
      this.vesselSeacrhForm.get('portOfRegistryXid').setValue(this.portId);
    }
    if (to == 'vesselType') {
      this.vesselTypeAuto = event.Vtype ? event.Vtype : this.vesselTypeAuto;
      this.vesselTypeId = event.id ? event.id : this.vesselTypeId;
      this.vesselSeacrhForm.get(to).setValue(this.vesselTypeId);
    }

       if (to == 'vesselSubType') {
      this.vesselSubTypeAuto = event.VSubtype ? event.VSubtype : this.vesselSubTypeAuto;
      this.vesselSubTypeId = event.id ? event.id : this.vesselSubTypeId;
      this.vesselSeacrhForm.get(to).setValue(this.vesselSubTypeId);
    }

        if (to == 'owner') {
      this.ownerName = event.ownername ? event.ownername : this.ownerName;
      this.ownerId = event.id ? event.id : this.ownerId;
      this.vesselSeacrhForm.get(to).setValue(this.ownerId);
    }
  }
  
  clearSelection(to) {
    this.vesselSeacrhForm.get(to).setValue(null);
  }

    onFocused(e) {
    // do something
  }

    onClickedOutside(e) {
    console.log('Clicked outside:', (e.target as Element).className);
    console.log(e)
      if((event.target as Element).className.includes('ng') || e.path[5].className.includes('autocomplete-container')){
        console.log('wow--->closeee----')
      }
      else {
    this.firstLoad = false
  }
  }

}
