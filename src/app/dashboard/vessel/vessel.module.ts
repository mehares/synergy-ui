import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ToastrModule } from 'ngx-toastr';


import {VesselRoutingModule} from './vessel-routing.module';

import { VesselListComponent } from './vessel-list/vessel-list.component';
import { VesselAddComponent } from './vessel-add/vessel-add.component';
import { VesselEditComponent } from './vessel-edit/vessel-edit.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgSelectModule } from '@ng-select/ng-select';
import { ClickOutsideModule } from 'ng-click-outside';


@NgModule({
  declarations: [VesselListComponent, VesselAddComponent, VesselEditComponent],
  imports: [
    CommonModule,
    VesselRoutingModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    AngularFontAwesomeModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    NgSelectModule,
    ClickOutsideModule
  ],
  
})
export class VesselModule {
}
