 import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import {CommonservicesService} from '../../../helper/commonservices/commonservices.service';
import {CommonFunctionsService} from '../../../helper/commonFunctions/common-functions.service';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
 
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router ,ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import {globalConstants} from '../../../constants/global-constants';
import * as moment from 'moment';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-vessel-edit',
  templateUrl: './vessel-edit.component.html',
  styleUrls: ['./vessel-edit.component.scss']
})
export class VesselEditComponent implements OnInit {
  takeOverDate = {};
  handOverDate ={};
  buildDate =  {};
   
  selectedId=0;
  vesselForm: FormGroup;
  submitted: boolean = false;
  loading = false;
  vesselTypeList = [];
  vesselSubType = [];
  vesselSubTypeList: [];
  ownerList = [];
  engineList = [];
  auxEngineList = [];
  craneList = [];
  managementList = [];
  portOfRegistryList = [];
  keywordPortOfRegistry = 'name';
  vesselSubTypeLoader = {};
  vendorData = null;
  i:any;
  // position: NbGlobalPosition = NbGlobalPhysicalPosition.BOTTOM_RIGHT;
  v =  {
    id:0 ,
    vessel:'',
    vesselCode : '' ,
    imoNo :'',
    vesselType : 0,
    vesselSubType: 0,
    owner :'',
    shipBuilder :'',
    portOfRegistry :'',
    portOfRegistryXid :0,
    portOfRegistryName: '',
    takeOverDate :'',
    handOverDate : '',
    buildDate :'',
    management :0,
    engineType :0,
    auxiliaryEngineType :0,
    craneType :'',
    vesselTypeList:[],
    
};
 
  constructor(private formBuilder: FormBuilder,private toastr: ToastrService , private router: Router,private http: HttpClient ,private route: ActivatedRoute, private CF: CommonFunctionsService ,private CS: CommonservicesService,private spinner: NgxSpinnerService, ) { }
  
  ngOnInit() {
    this.getPortOfRegistry();
    this.getVesselTyes();
    //this.getVesselSubTyes();
    this.getengineList();
    this.getownerList();
    this.getAuxList();
    this.getCraneList();
    this.getManagementList();

    this.vesselForm = this.formBuilder.group({
      vessel: ['', [Validators.required,Validators.maxLength(100)]],
      vesselCode: ['', [Validators.required,Validators.maxLength(24)]],
      imoNo: ['', [Validators.required]],
      vesselType: [null, [Validators.required]],
      vesselSubType: [null, [Validators.required]],
      owner: [null, [Validators.required,Validators.maxLength(100)]],
      shipBuilder: ['', [Validators.required]],
      portOfRegistry: ['', [Validators.required]],
      portOfRegistryXid: ['', [Validators.required]],
      portOfRegistryName:[''],
      takeOverDate: ['', [Validators.required]],
      handOverDate: [''],
      buildDate: ['', [Validators.required]],
      management: [null, [Validators.required]],
      engineType: [null, [Validators.required]],
      auxiliaryEngineType: [null, [Validators.required]],
      craneType: [null, [Validators.required]],
    });
    
    var id = this.route.snapshot.paramMap.get('id');
    this.selectedId =parseInt(id);

    this.fetchData(id,this.i);
  }
  getManagementList() {
    let _this = this;
    let params = {model_name: 'management', where: {'status': 1}};
    _this.CS.getData(params).subscribe(response => {
      console.log("response management", response);
      if (response && response.status == "success" && response.result) {
        _this.managementList = response.result;
      } else {
        _this.managementList = [];
      }
    });
  }
  getVesselTyes() {
    let _this = this;
    let params = {model_name: 'vesselTypes', where: {'status': 1}};
    _this.CS.getData(params).subscribe(response => {
      console.log("response vesselTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.vesselTypeList = response.result;
      } else {
        _this.vesselTypeList = [];
      }
    });
  }

 /* getVesselSubTyes() {
    let _this = this;
    let params = {model_name: 'vesselSubTypes', where: {'status': 1}};
    _this.CS.getData(params).subscribe(response => {
      console.log("response vesselSubTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.vesselSubType= response.result;
      } else {
        _this.vesselSubType = [];
      }
    });
  }*/

  // _getVesselSubTypes1(VtypeId,i) {

    
    // console.log("VtypeId",VtypeId);
    // console.log("-------i",i);
    // let _this = this;
    // if(VtypeId > 0) {
    //   _this.vesselSubTypeLoader[i] = true;
    //   _this.vesselSubType[i] = [];
    //   _this.CS.getVesselSubTypes({id: VtypeId}).subscribe(response => {
    //     console.log("response.result[0].VSST",response.result[0].VSST);
    //     if (response && response.status == "success" && response.result && response.result.length > 0) {
    //      _this.vesselSubTypeLoader[i] = false
    //       _this.vesselSubType[i] = response.result[0].VSST;
    //       _this.vesselSubTypeList = response.result[0].VSST;
    //       console.log("_this.vesselSubType[i]"+i,_this.vesselSubType[i]);
    //     } else {
    //       _this.vesselSubType[i] = [];
    //     }
    //   });
    // }
    // }
  getVesselSubTypes(event, VtypeList) {
    let _this = this;
    // console.log(event);
    // console.log("eventevent", event.target.value);
    //let event_id = event.target.value;
    if (VtypeList && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var VtypeList = _.find(VtypeList, { id: parseInt(getId, 10) })
      console.log("VtypeId", VtypeList);
      _this.CS.getVesselSubTypes({ id: VtypeList.id }).subscribe(response => {
        console.log("vesselSubTypes", response);
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          _this.vesselSubType = response.result[0].VSST;
          console.log("_this.vesselSubType", _this.vesselSubType);
        } else {
          _this.vesselSubType = [];
        }
      });
    } 
  }
  getownerList() {
    let _this = this;
    let params = {model_name: 'owners', where: {'status': 1}};
    _this.CS.getData(params).subscribe(response => {
    console.log("response ownler", response);
      if (response && response.status == "success" && response.result) {
        _this.ownerList = response.result;
      } else {
        _this.ownerList = [];
      }
    });
  }
  
  getAuxList() {
    let _this = this;
    let params = {model_name: 'AuxEngineTypes', where: {'status': 1}};
    _this.CS.getData(params).subscribe(response => {
      console.log("response AuxEngineTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.auxEngineList = response.result;
      } else {
        _this.auxEngineList = [];
      }
    });
  }   
  getCraneList() {
    let _this = this;
    let params = {model_name: 'Cranes', where: {'status': 1}};
    _this.CS.getData(params).subscribe(response => {
      console.log("response getCraneList", response);
      if (response && response.status == "success" && response.result) {
        _this.craneList = response.result;
      } else {
        _this.craneList = [];
      }
    });
  }   
  

  getengineList() {
    let _this = this;
    let params = {model_name: 'engines', where: {'status': 1}};
    _this.CS.getData(params).subscribe(response => {
      console.log("response getCrangetengineListeList", response);
      if (response && response.status == "success" && response.result) {
        _this.engineList = response.result;
      } else {
        _this.engineList = [];
      }
    });
  }   
   
  toDateObject(date){
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date){
    return this.CF.dateObjectToJsonDate(date)
  }
  fetchData = (id_selected,i)=> { 
    
    var idObj={id_selected: id_selected} ;
    /*this.http.post(`/api/vessel/edit/`,idObj ,this.httpOptions)
      .subscribe((response:any) => {*/
        
    this.CS.getVesselData(idObj).subscribe(response => {
        let _this =this;
        console.log("response from node",response);
        if(response.status == "success"){
          var id=response.message.id ;
          this.v = response.message ;
         // this.getVesselSubTypes(this.v.vesselType,i);
          
         
         this.takeOverDate = this.toJsonDate(response.message.takeOverDate );
         this.handOverDate =  this.toJsonDate( response.message.handOverDate );
          this.buildDate =   this.toJsonDate(response.message.buildDate );
         _this.vendorData = response.message;
         console.log("_this.vendorData **",_this.vendorData)
          if(this.v.vesselType > 0){
            _this.CS.getVesselSubTypes({id: this.v.vesselType}).subscribe(response => {

              console.log("Getting Vessel Type",response.result);
              if (response && response.status == "success" && response.result && response.result.length > 0) {
                _this.vesselSubType = response.result[0].VSST;
                _this.vesselSubTypeList = response.result[0].VSST;
                
              } else {
                _this.vesselSubType[i] = [];
                _this.vesselSubTypeList=[];
              }
             // console.log("_this.vesselSubType[i]"+i,_this.vesselSubType)
             // console.log("_this.vesselSubTypeList",_this.vesselSubTypeList)

              // setTimeout(()=>{    
                _this.setForm();
          //  },1);
              

            });
          }else{
            _this.setForm();
          }

        
         
          console.log('response.message.auxiliaryEngineType ',response.message.auxiliaryEngineType);
        }else{
         this.toastr.error('Trouble fetching Data',"Failed");
        }
         
      });
  }

  setForm(){    
    let portRN = _.find(this.portOfRegistryList, {id:this.vendorData.portOfRegistryXid});
    let pRegistryName = ((portRN && portRN.name) ? portRN.name : '');

    this.vesselForm = this.formBuilder.group({
      vessel: [this.vendorData.vessel, [Validators.required,Validators.maxLength(100)]],
      vesselCode: [this.vendorData.vesselCode, [Validators.required,Validators.maxLength(24)]],
      imoNo: [this.vendorData.imoNo, [Validators.required]],
      vesselType: [this.vendorData.vesselType, [Validators.required]],
      vesselSubType: [this.vendorData.vesselSubType,  [Validators.required]],
      owner: [this.vendorData.owner, [Validators.required]],
      shipBuilder: [this.vendorData.shipBuilder , [Validators.required,Validators.maxLength(100)]],
      portOfRegistry: [this.vendorData.portOfRegistry],
      portOfRegistryXid: [this.vendorData.portOfRegistryXid, [Validators.required]],
      portOfRegistryName:[ pRegistryName ],
      takeOverDate: [this.toJsonDate(this.vendorData.takeOverDate), [Validators.required]],
      handOverDate: [this.toJsonDate(this.vendorData.handOverDate) ],
      buildDate: [this.toJsonDate(this.vendorData.buildDate), [Validators.required]],
      management: [this.vendorData.management, [Validators.required]],
      engineType: [this.vendorData.engineType,  [Validators.required]],
      auxiliaryEngineType: [this.vendorData.auxiliaryEngineType, [Validators.required]],
      craneType: [this.vendorData.craneType, [Validators.required]],
    });

    
  }
  get f() { return this.vesselForm.controls; }
  dataDD = (objectDate )=>{
    var ddmmyy="";//2020-03-12 06:20:50.194 +00:00
    var month=parseInt (objectDate.getMonth()) ;
    var dt = moment(objectDate).format("YYYY-MM-DD") 
    return dt;
    
  }
  onSubmit() {
    let _this = this;
    this.submitted = true;
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);

    // stop here if form is invalid
     console.log('Form', this.vesselForm)
     console.log('Form Status', this.vesselForm.invalid);
    if (this.vesselForm.invalid) {
       this.toastr.success('Please Validate Fields', 'Failed!');
      return;
    }
    
    let handoverDate = this.vesselForm.get('handOverDate').value;
    console.log("handoverDate",handoverDate);
    let takeoverDate = this.vesselForm.get('takeOverDate').value;
if (handoverDate == "Thu Jan 01 1970 05:30:00 GMT+0530 (India Standard Time)") { ///,why this code?? code check
       this.vesselForm.get('handOverDate').setValue(null); 
    }
    else {

      handoverDate=this.toDateObject(handoverDate);
      takeoverDate=this.toDateObject(takeoverDate);
      
  if (takeoverDate && (handoverDate < takeoverDate) && (handoverDate != null)) {
       this.toastr.error( 'Handed over date should be greater than take over date.', 'Failed!');
      this.vesselForm.get('handOverDate').setValue(null);
      _this.loading = false;
      return;
    }
  }
    
    const vessObj = {
      vessel: this.f.vessel.value,
      vesselCode: this.f.vesselCode.value,
      imoNo: this.f.imoNo.value,
      vesselType: this.f.vesselType.value,
      vesselSubType: this.f.vesselSubType.value,
      owner: this.f.owner.value,
      shipBuilder: this.f.shipBuilder.value,
      portOfRegistry: this.f.portOfRegistry.value,
      portOfRegistryXid: this.f.portOfRegistryXid.value,
      takeOverDate: this.toDateObject(this.f.takeOverDate.value), 
      handOverDate:  this.toDateObject( this.f.handOverDate.value),
      buildDate:  this.toDateObject (this.f.buildDate.value),  
      management: this.f.management.value,
      engineType: this.f.engineType.value,
      auxiliaryEngineType: this.f.auxiliaryEngineType.value,
      craneType: this.f.craneType.value,
      id : this.selectedId,
    };
/*
     this.http.post(`/api/vessel/update`, vessObj ,this.httpOptions)
      .subscribe((response:any) => {
*/
    _this.CS.updateVessel(vessObj).subscribe(response => {      
          _this.loading = false;
        console.log("response from node",response);
      if (response && response.status == 'success') {
           this.toastr.success( 'Details Updated', 'Success!');
        }else{
        let errorMsg = 'Error occured while updating,Try again!';
           if (response.messageTxt) {
            errorMsg = response.messageTxt;
           }
        this.toastr.success( errorMsg, 'Success!');
        }       
      });
  }

  getPortOfRegistry() {
    let _this = this;
    let params = { model_name: 'port', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response PortOfRegistry", response);
      if (response && response.status == "success" && response.result) {
        _this.portOfRegistryList = response.result;
      } else {
        _this.portOfRegistryList = [];
      }
    });
  }

  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, { id: parseInt(getId, 10) });
      console.log("item**", item);
      if (item) {
        this.vesselForm.get(to).setValue(item[dataField]);
        if (typeof event == 'object') {
          this.vesselForm.get(from).setValue(getId);
        }
      }
    }
  }

  clearSelection(to) {
    this.vesselForm.get(to).setValue(null);
  }

  checkHandoverDateGreater(event = null) {
    let _this = this;
    let handoverDate = event;
    let takeoverDate = this.vesselForm.get('takeOverDate').value;

    if (handoverDate < takeoverDate) {
      this.toastr.error('Handed over date should be greater than take over date.', 'Failed!');
    }
  }

  checkTakeoverDateLesser(event) {
    let _this = this;
    let handoverDate = this.vesselForm.get('handOverDate').value;
    let takeoverDate = event;
    if (handoverDate && (handoverDate < takeoverDate)) {
      this.toastr.error('Handed over date should be greater than take over date.', 'Failed!');
      this.vesselForm.get('handOverDate').setValue(null);
    }
  }

}
