import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselAddComponent } from './vessel-add.component';

describe('VesselAddComponent', () => {
  let component: VesselAddComponent;
  let fixture: ComponentFixture<VesselAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VesselAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
