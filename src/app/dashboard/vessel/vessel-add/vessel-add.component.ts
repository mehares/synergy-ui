import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {CommonservicesService} from '../../../helper/commonservices/commonservices.service';
import {CommonFunctionsService} from '../../../helper/commonFunctions/common-functions.service';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
 
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import {globalConstants} from '../../../constants/global-constants';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-vessel-add',
  templateUrl: './vessel-add.component.html',
  styleUrls: ['./vessel-add.component.scss']
})

export class VesselAddComponent implements OnInit {
   model: NgbDateStruct;
  vesselForm: FormGroup;
  submitted: boolean = false;
  loading = false;
  vesselTypeList = [];
  vesselSubType = [];
  ownerList = [];
  engineList = [];
  auxEngineList = [];
  craneList = [];
  managementList = [];
  portOfRegistryList = [];
  keywordPortOfRegistry = 'name';
  buildDate = '';
  handOverDate = '';
  // position: NbGlobalPosition = NbGlobalPhysicalPosition.BOTTOM_RIGHT;

   
  
  constructor(private http: HttpClient,
    private router: Router,
    private formBuilder: FormBuilder,
    private CS: CommonservicesService, private CF: CommonFunctionsService , private calendar: NgbCalendar ,private toastr: ToastrService,private spinner: NgxSpinnerService,  ) { }
  
  
  // constructor(private formBuilder: FormBuilder, private router: Router, private http: HttpClient, private CS: CommonservicesService, private toastrService: NbToastrService,
   
  //   private CF: CommonFunctionsService) {
  // }

  ngOnInit() {

    this.getVesselTyes();
    //this.getVesselSubTyes();
    this.getengineList();
    this.getownerList();
    this.getAuxList();
    this.getCraneList();
    this.getManagementList();
    this.getPortOfRegistry();

    this.vesselForm = this.formBuilder.group({
      vessel: ['', [Validators.required,Validators.maxLength(100)]],
      vesselCode: ['', [Validators.required,Validators.maxLength(24)]],
      imoNo: ['', [Validators.required]],
      vesselType: [null, [Validators.required]],
      vesselSubType: [null, [Validators.required]],
      owner: [null, [Validators.required]],
      shipBuilder: ['', [Validators.required,Validators.maxLength(100)]],
      //portOfRegistry: ['', [Validators.required]],
      portOfRegistry: [''],
      portOfRegistryXid: ['', [Validators.required]],
      takeOverDate: ['', [Validators.required]],
      handOverDate: [null],
      buildDate: ['', [Validators.required]],
      management: [null, [Validators.required]],
      engineType: [null, [Validators.required]],
      auxiliaryEngineType: [null, [Validators.required]],
      craneType: [null, [Validators.required]],
    });
  }
  getManagementList() {
    let _this = this;
    let params = { model_name: 'management', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response management", response);
      if (response && response.status == "success" && response.result) {
        _this.managementList = response.result;
      } else {
        _this.managementList = [];
      }
    });
  }

  getVesselTyes() {
    let _this = this;
    let params = { model_name: 'vesselTypes', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response vesselTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.vesselTypeList = response.result;
      } else {
        _this.vesselTypeList = [];
      }
    });
  }

  getVesselSubTypes(event, VtypeList) {
    let _this = this;
    // console.log(event);
    if (VtypeList && event) {
      var getId = typeof event == 'object' ? event.id : event;
      var VtypeList = _.find(VtypeList, { id: parseInt(getId, 10) })
      console.log("VtypeId", VtypeList);
      _this.CS.getVesselSubTypes({ id: VtypeList.id }).subscribe(response => {
        console.log("vesselSubTypes", response);
        if (response && response.status == "success" && response.result && response.result.length > 0) {
          _this.vesselSubType = response.result[0].VSST;
          console.log("_this.vesselSubType", _this.vesselSubType);
        } else {
          _this.vesselSubType = [];
        }
      });
    } 
  }

  getownerList() {
    let _this = this;
    let params = { model_name: 'owners', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response ownler", response);
      if (response && response.status == "success" && response.result) {
        _this.ownerList = response.result;
      } else {
        _this.ownerList = [];
      }
    });
  }

  getAuxList() {
    let _this = this;
    let params = { model_name: 'AuxEngineTypes', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response AuxEngineTypes", response);
      if (response && response.status == "success" && response.result) {
        _this.auxEngineList = response.result;
      } else {
        _this.auxEngineList = [];
      }
    });
  }
  getCraneList() {
    let _this = this;
    let params = { model_name: 'Cranes', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response getCraneList", response);
      if (response && response.status == "success" && response.result) {
        _this.craneList = response.result;
      } else {
        _this.craneList = [];
      }
    });
  }
  getPortOfRegistry() {
    let _this = this;
    let params = { model_name: 'port', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response PortOfRegistry", response);
      if (response && response.status == "success" && response.result) {
        _this.portOfRegistryList = response.result;
      } else {
        _this.portOfRegistryList = [];
      }
    });
  }
  getengineList() {
    let _this = this;
    let params = { model_name: 'engines', where: { 'status': 1 } };
    _this.CS.getData(params).subscribe(response => {
      console.log("response getCrangetengineListeList", response);
      if (response && response.status == "success" && response.result) {
        _this.engineList = response.result;
      } else {
        _this.engineList = [];
      }
    });
  }

  get f() { return this.vesselForm.controls; }
  dataDD = (objectDate) => {
    var ddmmyy = "";//2020-03-12 06:20:50.194 +00:00
    var month = parseInt(objectDate.getMonth());
    month++;
    ddmmyy = objectDate.getFullYear() + '-' + month + '-' + objectDate.getDate();
    // ddmmyy=new Date(objectDate).toISOString();
    console.log(ddmmyy);
    return ddmmyy;

  }
  saveVessel() {

    this.submitted = true;
    let _this = this;
    _this.loading = true;
    // stop here if form is invalid
    console.log('Form Status', this.vesselForm.invalid);
    console.log('Form Status', this.vesselForm);
   
   
    let handoverDate = this.vesselForm.get('handOverDate').value;
    let takeoverDate = this.vesselForm.get('takeOverDate').value;
     if (handoverDate == "") {
       this.vesselForm.get('handOverDate').setValue(null);
    }
    else {
      handoverDate=this.toDateObject(handoverDate);
      takeoverDate=this.toDateObject(takeoverDate);
       if (takeoverDate && (handoverDate < takeoverDate) && (handoverDate != null)) {
        this.toastr.error('Handed over date should be greater than take over date');
 
      _this.loading = false;
      return;
    }
  }

   if (this.vesselForm.invalid) {

      this.toastr.error('Please validate fields!', 'Failed!');
   
        _this.loading = false;
      return;
    }
    const vessObj = {
      vessel: this.f.vessel.value,
      vesselCode: this.f.vesselCode.value,
      imoNo: this.f.imoNo.value,
      vesselType: this.f.vesselType.value,
      vesselSubType: this.f.vesselSubType.value,
      owner: this.f.owner.value,
      shipBuilder: this.f.shipBuilder.value,
      portOfRegistry: this.f.portOfRegistry.value,
      portOfRegistryXid: this.f.portOfRegistryXid.value,
      takeOverDate: this.toDateObject(this.f.takeOverDate.value),
      handOverDate: this.toDateObject(this.f.handOverDate.value),
      buildDate: this.toDateObject(this.f.buildDate.value),
      management: this.f.management.value,
      engineType: this.f.engineType.value,
      auxiliaryEngineType: this.f.auxiliaryEngineType.value,
      craneType: this.f.craneType.value,
      status: 1
    };
    this.spinner.show();
    setTimeout(() => this.spinner.hide(), 2000);
    _this.CS.saveVessel(vessObj).subscribe(response => {      
      _this.loading = false;
      if (response && response.status == "success") {
        this.toastr.success('Veseel Details Saved','Sucess');
 
        this.router.navigate(['/vessel/edit', response.ins_data.id]); 
      } else { 
        let errorMsg = 'Error occured while updating,Try again!';
           if (response.messageTxt) {
            errorMsg = response.messageTxt;
           }
       this.toastr.error(errorMsg,"Failed");
      }
    },
      (error: any) => {
        _this.loading = false;
         this.toastr.error('Failed to add vessel',"Failed");
       
      } // error path
    );
    //  return this.http.post<any>('/api/vessel-create',vessObj);
    // this.router.navigate(['/masters/vessels/add']);
  }

  changeSelection(event, data, dataField, to, index?, from?) {
    console.log("event", event);
    console.log("data", data);
    console.log("to", to);
    console.log("dataField", dataField);
    if (data && event) {
     var getId = typeof event == 'object' ? event.id : event;
      var item = _.find(data, { id: parseInt(getId, 10) });
      console.log("item**", item);
      if (item) {
        this.vesselForm.get(to).setValue(item[dataField]);
        if (typeof event == 'object') {
          this.vesselForm.get(from).setValue(getId);
        }
      } 
    }
  }
  toDateObject(date){
    return this.CF.jsonDatetoDateObject(date)
  }
  toJsonDate(date){
    return this.CF.dateObjectToJsonDate(date)
  }
  clearSelection(to) {
    this.vesselForm.get(to).setValue(null);
  }

  checkHandoverDateGreater(event = null) {
    let _this = this;
    let handoverDate = event;
    let takeoverDate = this.vesselForm.get('takeOverDate').value;
console.log("handoverDate",handoverDate)
console.log("takeoverDate",takeoverDate)
    if (handoverDate < takeoverDate) {
   this.toastr.error('Handed over date should be greater than take over date',"Failed");

    }
  }

  checkTakeoverDateLesser(event) {
    let _this = this;
    let handoverDate = this.vesselForm.get('handOverDate').value;
    let takeoverDate = event;
    
console.log("handoverDate",handoverDate)
console.log("takeoverDate",takeoverDate)
    if (handoverDate && (handoverDate < takeoverDate)) {
         this.toastr.error('Handed over date should be greater than take over date',"Failed");
         this.vesselForm.get('handOverDate').setValue(null);

    }
  }



}
