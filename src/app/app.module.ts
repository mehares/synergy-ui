import { BrowserModule } from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


/**** Auth guard provider  **/
import {CanDeactivateGuard} from './guards/can-deactivate.guard';
import {AuthGuard} from './guards/auth-guard.service';
import {RoleGuard} from './guards/role-guard.service';
import {PageGuard} from './guards/page-guard.service';


/** Http calls Modules **/
import {IntercepterService} from './@core/intercepter/intercepter.service';
import {HttpClientModule, HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';

/** Imports modules **/
import {AuthModule} from './auth/auth.module';
import { LayoutComponent } from './dashboard/layout/layout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: IntercepterService, multi: true},
    AuthGuard, RoleGuard, CanDeactivateGuard, PageGuard
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
